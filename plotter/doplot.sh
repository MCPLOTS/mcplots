#!/bin/bash -e

if (( $# < 1 )) ; then
  echo "Usage: ./doplot.sh file1.dat [file2.dat ...]"
  exit 1
fi

fout="$(mktemp -d)/plot"
fscript=$fout.script

cat >$fscript <<EOF
# BEGIN PLOT
textField1=
textField2=
drawRatioPlot=1
outputFileName=$fout
# END PLOT

EOF

for i in "$@" ; do
cat >>$fscript <<EOF
# BEGIN HISTOGRAM
filename=$i
reference=1
markerStyle=21
markerSize=1
lineStyle=1
lineWidth=1
color=0 0 0
legend=$i
# END HISTOGRAM

EOF
done

./plotter.exe chi2=5 $fscript

echo "Chi2 = $(cat $fout.txt | cut -d';' -f1)"

ls -l $fout.*
