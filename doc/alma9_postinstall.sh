#!/bin/bash -e

# post-install instructions:
# https://clouddocs.web.cern.ch/tutorial_using_a_browser/create_a_virtual_machine.html
# https://linux.web.cern.ch/almalinux/alma9/stepbystep/#manual-post-install-configuration-adjustment
# https://linux.web.cern.ch/almalinux/alma9/locmap/
# `man locmap`

dnf install -y dnf-autoupdate
dnf install -y locmap-release
dnf install -y locmap


# module "afs": configure home directory at /afs/cern.ch/home/xxx
echo "LOCMAP_HOMEDIRECTORY_LOCAL=False" >> /etc/sysconfig/locmap-initialsetup

locmap --enable afs
locmap --enable kerberos
locmap --enable sudo
locmap --enable cvmfs
locmap --verbose --configure all

locmap --list
