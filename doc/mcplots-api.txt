   MCPLOTS API
   -----------

Entry point:
  
  http://mcplots-dev.cern.ch/api.php

Output:
  
  A list of JSON objects.

Functions:
  
  * List of top users:
    
    ?top_users={N_USERS}&key={SORT_KEY}
    
      N_USERS: number of entries in the list, maximum is 100
      SORT_KEY: list sort key (optional, default value is n_events),
        possible values:
        * cpu_time
        * n_events
        * n_jobs
        * n_good_jobs
        * n_hosts
  
  * User stats:
    
    ?user={BOINC_PROJECT}-{BOINC_USERID}
    ?user={BOINC_USERID}                 // compatibility (assume BOINC_PROJECT = 1), to be deprecated

  * Host stats:
    
    ?host={BOINC_PROJECT}-{BOINC_USERID}-{BOINC_HOSTID}

  * Totals:
    
    ?totals
  
  * Achievement:
    
    ?achievement={KEY}&value={VALUE}

Examples:
  
  List of top 10 users (according to n_events as sort key parameter is omitted):
    $ curl -i http://mcplots-dev.cern.ch/api.php?top_users=10
  
  List of top 5 users according to cpu_time:
    $ curl -i http://mcplots-dev.cern.ch/api.php?top_users=5&key=cpu_time
  
  Stats for user with BOINC_PROJECT = 1 (vLHC) and BOINC_USERID = 8:
    $ curl -i http://mcplots-dev.cern.ch/api.php?user=1-8
  
  Stats for host with BOINC_PROJECT = 1, BOINC_USERID = 37, BOINC_HOSTID = 14469:
    $ curl -i http://mcplots-dev.cern.ch/api.php?host=1-37-14469
  
  Total stats over all users:
    $ curl -i http://mcplots-dev.cern.ch/api.php?totals
  
  List of users who have 1 billion of generated events:
    $ curl -i http://mcplots-dev.cern.ch/api.php?achievement=n_events&value=1000000000
  
