#!/bin/bash -ev

# Install packages:
#   the libxcrypt-compat - for cvmfs/rivet3 setup
yum install -y \
  httpd mariadb mariadb-server php php-mysqlnd php-fpm \
  ImageMagick optipng root root-physics zlib-devel \
  git gcc gcc-c++ nano mc \
  which bc libxcrypt-compat

# Lock installed version of ROOT to avoid plotter break in case of automatic update:
rpm -qa | grep ^root- | tee /etc/yum/pluginconf.d/versionlock.list

# PHP - defaults:
cp mcplots.ini /etc/php.d/

# MySQL - defaults:
cp mcplots.cnf /etc/my.cnf.d/

# MySQL - Run and configure:
systemctl restart mariadb
systemctl enable mariadb
systemctl -l --no-pager status mariadb

# mysql_secure_installation automation:
#   https://stackoverflow.com/q/24270733
mysql_secure_installation <<EOF

y
y
root
root
y
y
y
y
EOF

# MySQL - Create user mcplots:
mysql --user=root --password=root <<EOF
SELECT Host, User FROM mysql.user;
CREATE USER IF NOT EXISTS 'mcplots'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'mcplots'@'localhost';
SELECT Host, User FROM mysql.user;
EOF
  
# MySQL - Create database 'mcplots':
mysql -u mcplots <<EOF
CREATE DATABASE IF NOT EXISTS mcplots;
SHOW DATABASES;
EOF


# Web server - defaults:
cp mcplots.conf /etc/httpd/conf.d/

# disable some of unused options and modules:
# (current modules listing: `httpd -M`)
#sed -e '/^LoadModule alias_module/ s,.,#&,' -i /etc/httpd/conf.modules.d/00-base.conf
sed -e '/^ *ScriptAlias/ s,.,#&,' -i /etc/httpd/conf/httpd.conf

# Web server - Run:
systemctl restart httpd
systemctl enable httpd
systemctl -l --no-pager status httpd


# Firewall - allow HTTP incoming traffic:
# reference:
#   http://ask.xmodulo.com/open-port-firewall-centos-rhel.html
firewall-cmd --permanent --add-service=http
firewall-cmd --reload

# SELinux - disable:
# change the current mode
# the ` || true` is to bypass the case of already OFF mode
setenforce 0 || true
# completely disable, reboot is needed
sed -i /etc/selinux/config -e 's,SELINUX=.*,SELINUX=disabled,'
#reboot
sestatus

# reference:
#   http://www.crypt.gen.nz/selinux/disable_selinux.html
#   https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Security-Enhanced_Linux/sect-Security-Enhanced_Linux-Enabling_and_Disabling_SELinux-Disabling_SELinux.html
#   http://computernetworkingnotes.com/manage-system-security/how-to-change-selinux-mode.html


# add HTTPS:
yum install -y httpd mod_ssl
grep ^SSLCert /etc/httpd/conf.d/ssl.conf
yum install -y cern-get-certificate
# the ' || true' is to bypass the case of the already activated autoenrollment
cern-get-certificate --autoenroll || true
cern-get-certificate --status
ln -s -f -T $(hostname).key /etc/pki/tls/private/localhost.key
ln -s -f -T $(hostname).pem /etc/pki/tls/certs/localhost.crt
firewall-cmd --permanent --add-service=https
firewall-cmd --reload
systemctl restart httpd
systemctl -l --no-pager status httpd

# verify:
#nmap localhost
#netstat -l | grep http

# reference:
#   https://twiki.cern.ch/twiki/bin/view/LinuxSupport/ConfigureApacheSSLonSLC
#   https://ca.cern.ch/ca/Help/?kbid=024000
# next:
#   https://letsencrypt.org/docs/


# security: disable unused rpcbind service
#   (in use only for NFS server)
# alternative: `dnf remove rpcbind`
#   (but removes also the nfs client)
systemctl stop rpcbind.service
systemctl disable rpcbind.service
systemctl mask rpcbind.service
systemctl stop rpcbind.socket
systemctl disable rpcbind.socket
systemctl mask rpcbind.socket
