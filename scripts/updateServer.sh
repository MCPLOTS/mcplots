#!/bin/bash -e

# print usage instruction
function print_usage () {
  echo "Script for update of public server"
  echo ""
  echo "Usage:"
  echo "  updateServer.sh revid [datpath]"
  echo ""
  echo "Options:"
  echo "  revid   - git commit id, obligatory"
  echo "  datpath - path to dat/ directory with histograms, optional"
  echo ""
  echo "Examples:"
  echo "  Update code of server scripts to the latest revision:"
  echo "    ./updateServer.sh HEAD"
  echo ""
  echo "  Update code of server scripts to the certain revision 022344c0:"
  echo "    ./updateServer.sh 022344c0"
  echo ""
  echo "  Update code and data:"
  echo "    ./updateServer.sh HEAD /path/to/directory/dat"
  echo ""
}

#  helper function to show progress during tarball creation
function print_progress () {
  local fname="$1"
  
  while read ; do
    local size=$(du -k $fname | cut -f 1)
    echo -ne "\r            $size Kb"
  done
  
  echo ""
}

revision="$1"
datdir="$2"

if [[ "$revision" == "" ]] ; then
  print_usage
  exit 1
fi

prodhost=mcplots.cern.ch
gitrepo="https://gitlab.cern.ch/MCPLOTS/mcplots.git"
stamp="$(date +%F-%H.%M.%S)"
dattgz="dat.$stamp.tgz"
mccode="mcplots.$stamp"
mctgz="$mccode.tgz"
mcpath="/home/$mccode"

echo "Checking passwordless access to the $prodhost ..."
#ssh -o "BatchMode=yes" $prodhost exit
ssh $prodhost exit

# download source code
echo "Exporting $gitrepo to $mccode ..."
git clone -q $gitrepo $mccode
cd $mccode
git checkout $revision
git log -1 | tee revision.txt
rm -rf .git
cd -
tar zcf $mctgz $mccode
scp $mctgz $prodhost:/home/
rm -rf $mccode $mctgz

# copy tarball with histograms to server
if [[ "$datdir" != "" ]] ; then
  tmptgz="$(pwd)/$dattgz"
  
  if test -e $tmptgz ; then
    echo "ERROR: temporary file exists already, tmptgz = $tmptgz"
    exit 1
  fi
  
  echo "Preparing tarball with histograms..."
  echo "  source  = $datdir"
  echo "  tarball = $tmptgz"
  
  tar --checkpoint -b 256 -zcf $tmptgz -C $datdir . 2>&1 | print_progress $tmptgz
  
  echo "Copying tarball to $prodhost..."
  echo "  dest    = $prodhost:/home/$dattgz"
  
  scp $tmptgz $prodhost:/home/$dattgz
  
  rm $tmptgz
fi

# connect to server and setup site
ssh -T $prodhost bash -e <<EOT
# unpack code
cd /home
tar zxf $mctgz

# build plotter
echo "Building plotter ..."
make -C $mcpath/plotter

#
echo "Creating symlinks and directories ..."
cd $mcpath/www
ln -sf ../plotter/plotter.exe
ln -sf ../scripts/updatedb.sh

mkdir -p dat cache/plots
chmod a+w -R cache/

# prepare histograms
if [[ "$datdir" != "" ]] ; then
  # unpack tarball with histograms
  echo "Unpacking histograms /home/$dattgz to $mcpath/www/dat ..."
  tar -zxf /home/$dattgz -C dat/
  rm /home/$dattgz
else
  # copy histograms from latest publication
  echo "Copying histograms from latest publication /home/mcplots/www/dat to $mcpath/www/dat ..."
  cp -al /home/mcplots/www/dat/* dat/
fi

# create database
echo "Creating database ..."
./updatedb.sh

# remove cache of the old site
#echo "Run old site release cache cleanup ..."
#rm -rf /home/mcplots/www/cache &

# re-point 'mcplots' symlink to new site root
echo "Switching site root to $mcpath ..."
ln -sf -T $mcpath /home/mcplots
EOT

echo "Site updated successfully!"
