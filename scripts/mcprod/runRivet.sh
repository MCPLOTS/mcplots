#!/bin/bash

# setup paths and environment to run Rivet
set_environment () {
  echo "Setting environment..."
  
  # debug, print the system information
  echo "INFO: uname:"
  uname -a
  echo "INFO: /etc/redhat-release:"
  cat /etc/redhat-release
  echo ""
  
  local mode=$1
  
  # preserve original environment
  origEnv="PATH=$PATH LD_LIBRARY_PATH=$LD_LIBRARY_PATH PYTHONPATH=$PYTHONPATH COMPILER_PATH=$COMPILER_PATH"
  
  if [[ "$mode" == "lxbatch" || "$mode" == "boinc" ]] ; then
    # in lxbatch or boinc mode the script has access only to the current directory,
    # repoint the temp directory root to $PWD/tmp
    export TMPDIR="$(pwd)/tmp"
    mkdir -p $TMPDIR || exit 1
  fi
  
  source rivetenv-3.1.10.sh
  
  #rivet --help
  #rivet -v --list-analyses -l TRACE
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "g++ = $(which g++)"
  echo "g++ version = $(g++ -dumpversion)"
  echo "RIVET=$RIVET"
  echo "YODA=$YODA"
  echo "Rivet version = $(rivet --version)"
  echo "RIVET_ANALYSIS_PATH=$RIVET_ANALYSIS_PATH"
  echo "RIVET_DATA_PATH=$RIVET_DATA_PATH"
  echo "GSL=$GSL"
  echo "HEPMC=$HEPMC"
  echo "FASTJET=$FASTJET"
  echo "PYTHON=$PYTHON"
  echo ""
  
  # check paths to essential parts of machinery:
  for path in MCGENERATORS RIVET GSL HEPMC FASTJET PYTHON ; do
    if [[ ! -e "${!path}" ]] ; then
      echo "ERROR: fail to set environment ($path)"
      exit 1
    fi
  done
  
  check_files $RIVET/bin/{rivet-config,rivet-build} $HEPMC/lib/libHepMC.so
}

#
check_files () {
  local files=$@
  
  for f in $files ; do
    if ! cp $f /dev/null ; then
      echo "ERROR: fail to access $f"
      exit 1
    fi
  done
}


# check collisions between histograms,
check_overlap () {
  # the combination 'beam process energy histogram_id observable cuts' should be uniq
  cat configuration/rivet-histograms.map | weed | cut -d ' ' -f 1-3,5-7 | sort | uniq -d | grep -v "\- -$"
  
  # the combination 'histogram_id' should be uniq
  # skip MC-only histograms (possible multiple definitions for different process/beam configurations)
  cat configuration/rivet-histograms.map | weed | cut -d ' ' -f 5 | sort | uniq -d | grep -v "^MC_"
}

# get analysis name from histogram name
# example: ATLAS_2013_I1243871_d02-x01-y01 -> ATLAS_2013_I1243871
# example: ALICE_2019_I1672860:cent=GEN_d07-x01-y01-rho_pion_ratio-pp -> ALICE_2019_I1672860:cent=GEN
# + usualy the last `_` char is the separator between analysis name and histogram name
# + the `:key=value` option helps to find the analysis name
# + special handling due to different naming scheme in several analyses
histoname2analysis() {
  sed -e 's,_[^_]*$,,' \
      -e 's,\(:[^=]*=[^_]*\).*,\1,' \
      -e 's,ALICE_2012_I1127497_.*,ALICE_2012_I1127497,' \
      -e 's,ATLAS_2021_I1941095_.*,ATLAS_2021_I1941095,' \
      -e 's,ATLAS_2021_I1887997_.*,ATLAS_2021_I1887997,' \
      -e 's,CMS_2019_I1744604_.*,CMS_2019_I1744604,' \
      -e 's,PHENIX_2019_I1672015_.*,PHENIX_2019_I1672015,' \
      -e 's,CMS_2020_I1776758_.*,CMS_2020_I1776758,' \
      -e 's,CMS_2021_I1963239_.*,CMS_2021_I1963239,' \
      -e 's,ALICE_2015_PBPBCentrality_.*,ALICE_2015_PBPBCentrality,' \
      -e 's,ATLAS_PBPB_CENTRALITY_.*,ATLAS_PBPB_CENTRALITY,'
  
  #TODO: rework the configuration/rivet-histograms.map to change
  #      the delimiting char to non-"_", for example to "/" or " "
}

# list of analyses which we use:
list_analyses() {
  cat configuration/rivet-histograms.map | weed | cut -d ' ' -f 5 | histoname2analysis | sed -e 's,:.*,,' | sort -u
}

list_histograms() {
  cat configuration/rivet-histograms.map | weed | cut -d ' ' -f 5 | sort
}

print_list () {
  while read analysis ; do
    local experiment=${analysis%%_*}
    
    if [[ "$experiment" != "$experiment0" ]] ; then
      if [[ "$experiment0" != "" ]] ; then
        echo ""
      fi
      
      echo -n "$experiment: $analysis "
      experiment0="$experiment"
    else
      echo -n "$analysis "
    fi
  done
  
  echo ""
}

# check consistency of configuration/rivet-histograms.map file
# TODO: exit with error on the check failure
check_analyses () {
  echo "Enumerating analyses list ..."
  
  # first check all analyses which we use are available in Rivet
  # (analyses can be removed or renamed in new version of Rivet)
  
  # list of private analyses (in analyses/*)
  local ourA=$(find analyses/ -name '*.cc' | xargs -I @ basename @ .cc)
  
  # list of Rivet analyses
  # (`xargs -n 1` to strip trailing spaces/tabs)
  local rivetA=$(rivet --list-analyses -q | xargs -n 1)
  
  # list of analyses covered in mcplots (mapped in configuration/rivet-histograms.map)
  local coveredA=$(list_analyses)
  
  # list of uncovered analyses
  local uncoveredA=$( (echo "$rivetA"; echo "$coveredA"; echo "$coveredA") | sort | uniq -u)
  
  echo "List of private analyses (analyses/*):"
  echo "$ourA"
  echo ""
  
  # group by experiment:
  echo "$coveredA" | sort | print_list > list-analyses-covered-byexp.txt
  echo "$uncoveredA" | print_list > list-analyses-uncovered-byexp.txt
  
  # save analyses list
  echo "$rivetA" > list-analyses-rivet-all.txt
  echo "$uncoveredA" > list-analyses-uncovered.txt
  
  echo "Summary:"
  echo "Rivet analyses total: $(echo "$rivetA" | wc -l) (full list - file list-analyses-rivet-all.txt)"
  echo "Covered analyses total: $(echo "$coveredA" | wc -l)  (full list - file list-analyses-covered-byexp.txt)"
  echo "Uncovered analyses total: $(echo "$uncoveredA" | wc -l) (full list - file list-analyses-uncovered.txt / list-analyses-uncovered-byexp.txt)"
  echo ""
  
  echo "Integrity checks:"
  
  echo "List of overlapping histograms:"
  check_overlap
  
  echo "List of analyses mapped in configuration/rivet-histograms.map but not available in Rivet or in analyses/*:"
  {
    echo "$ourA"
    echo "$ourA"
    echo "$rivetA"
    echo "$rivetA"
    echo "$coveredA"
  } | sort | uniq -u
  
  echo "List of overlapping analyses (available both in Rivet and in analyses/*):"
  {
    echo "$ourA"
    echo "$rivetA"
  } | sort | uniq -d
}

# add 'xfocus' column to histogram bins
extend_hist () {
  while read -r line ; do
    if [[ "${line:0:1}" == "#" || "$line" == "" || "${line/=/}" != "$line" ]] ; then
      # comment, empty or parameter line
      echo "$line"
    else
      # histogram bin, insert xfocus field
      echo "$line" | awk '{print $1, ($1 + $2) / 2, $2, $3, $4, $5}'
    fi
  done
}

# delete comments, empty lines, double spaces, heading and trailing spaces from the input stream
weed () {
  sed -e '/^#.*/ d' -e '/^ *$/ d' -e 's,  *, ,g' -e 's,^ ,,g' -e 's, $,,g'
}

do_display () {
  echo "Updating display..."
  
  local datdir="$1"   # directory with input .dat files
  local dispdir="$2"  # output directory
  local vars=( $3 )   # job metadata
  
  # temporary directory for all intermediate files:
  local tmpdir=$(mktemp -d)
  
  # preserve path to plotter utility:
  local plotter="$(pwd)/plotter/plotter.exe"
  
  cp resources.json $(find $datdir/ -name '*.dat') $tmpdir/
  cd $tmpdir/
  
  # TODO: add display of reference histograms as well.
  #       Not possible to do right now as MC and DATA
  #       histograms have different normalisation.
  
  #echo "Preparing steering files..."
  for fname in $(find . -type f -name '*.dat') ; do
    #echo "---> $fname"
    
    # prepare plotter steering file:
    ( echo "# BEGIN PLOT"
      echo "LogY=1"
      echo "LogX=0"
      echo "Title=%observable% (%cuts%)"
      echo "upperLeftLabel=%energy% GeV %beam%"
      echo "upperRightLabel=%process%"
      echo "textField1=%generator% %version%"
      echo "textField2="
      echo "outputFileName=$fname"
      echo "drawRatioPlot=0"
      echo "# END PLOT"
      echo ""
      echo "# BEGIN HISTOGRAM"
      echo "filename=$fname"
      echo "markerStyle=33"
      echo "markerSize=1.4"
      echo "lineStyle=2"
      echo "lineWidth=1.5"
      echo "color=1.0 0.0 0.0"
      echo "legend=%tune%"
      echo "reference=0"
      echo "# END HISTOGRAM"
    ) > $fname.script
  done
  
  # extract number of processed events from one of histograms
  local nevts=$(find -name '*.dat' | xargs cat | grep "^nevts=" | head -n 1 | cut -d = -f 2)
  if [[ "$nevts" == "" ]] ; then
    nevts="0"
  fi
  
  # complete *.scrips and resources.json by metadata:
  # $vars is array: "$beam $process $energy $params $generator $version $tune"
  sed -e "s,%beam%,${vars[0]},"      \
      -e "s,%process%,${vars[1]},"   \
      -e "s,%energy%,${vars[2]},"    \
      -e "s/%cuts%/${vars[3]}/"      \
      -e "s,%generator%,${vars[4]}," \
      -e "s,%version%,${vars[5]},"   \
      -e "s,%tune%,${vars[6]},"      \
      -e "s,%observable%,,"          \
      -e "s,%nevts%,$nevts,"         \
      -i $(find -name '*.script') resources.json
  
  #echo "Preparing plots..."
  # run plotter
  $plotter *.script >& /dev/null
  
  # complete resources.json by the list of generated .png files:
  find . -type f -name '*.png' | sed -e 's,^.,",' -e 's/$/",/' > histograms.txt
  sed -n '1h;1!H;${;g;s/,$//;p;}' -i histograms.txt  # remove last ',' from histograms.txt
  sed -e "/%histograms%/ r histograms.txt" -e "/%histograms%/ d" -i resources.json
  
  # calc number of histograms:
  local nhist=$(find -name '*.png' | wc -l)
  
  # update output directory:
  mv -f * $dispdir/
  
  # remove temp dir
  rm -rf $tmpdir
  
  echo "Display update finished ($nhist histograms, $nevts events)."
}

# function runs online display update machinery
# two conditions are neccessary to run:
# 1. T4T_DISPLAY environment variable is set and points to pictures output directory
# 2. plotter/ should be available in current directory
display_service () {
  local datdir="$1"
  local vars="$2"
  
  echo "INFO: (display) T4T_DISPLAY=$T4T_DISPLAY"
  echo "INFO: (display) datdir=$datdir"
  echo "INFO: (display) vars=$vars"
  
  if [[ ! -d plotter/ || "$T4T_DISPLAY" == "" || ! -d "$T4T_DISPLAY" ]] ; then
    echo "INFO: display service switched off"
    return 0
  fi
  
  # save current directory:
  local wd=$(pwd)
  
  # setup ROOT
  local ROOT=$EXTERNAL/ROOT/6.18.00/$LCG_PLATFORM
  echo "ROOT=$ROOT"
  source $ROOT/ROOT-env.sh
  
  # compile plotter:
  make -B -C plotter
  
  if [[ "$?" != "0" ]] ; then
    # failed to compile plotter - this is only possible if
    # some dependencies is not accessible due to network
    # connection problems
    
    # exit without error to avoid interruption of events generation
    return 0
  fi
  
  while true ; do
    cd $wd
    do_display $datdir $T4T_DISPLAY "$vars"
    sleep 60
  done
}

# kill full process tree
# inspired by: http://stackoverflow.com/questions/392022/best-way-to-kill-all-child-processes
killtree () {
  local pid="$@"
  if [[ "$pid" == "" ]] ; then return 0; fi
  
  # stop parent from producing new childs
  kill -STOP $pid >& /dev/null
  
  # kill child processes
  killtree $(ps --ppid "$pid" -o pid= | xargs)
  
  # kill parent
  kill -KILL $pid >& /dev/null
}


# run all machinery and produce histograms
run () {
  # input parameters
  local mode=$1
  local beam=$2
  local process=$3
  local energy=$4
  local params=$5
  local specific=$6
  local generator=$7
  local version=$8
  local tune=$9
  local nevts=${10}
  local seed=${11}
  
  echo "Input parameters:"
  echo "mode=$mode"
  echo "beam=$beam"
  echo "process=$process"
  echo "energy=$energy"
  echo "params=$params"
  echo "specific=$specific"
  echo "generator=$generator"
  echo "version=$version"
  echo "tune=$tune"
  echo "nevts=$nevts"
  echo "seed=$seed"
  echo ""
  
  # check mode:
  if [[ "$mode" != "local" && "$mode" != "lxbatch" && "$mode" != "boinc" ]] ; then
    echo "ERROR: unknown mode: $mode"
    exit 1
  fi
  
  # paths to temporary directories and files
  echo "Prepare temporary directories and files ..."
  local workd=$(pwd)
  
  local tmpd
  tmpd="$(mktemp -d)"
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to create temp directory"
    exit 1
  fi

  # path to the destination (/dat) directory
  local datdir="$workd/dat"
  local ydatadir="yoda-data"
  mkdir -p $datdir/$ydatadir
  
  # clean temp directory
  rm -rf $tmpd/*
  
  local tmp_params="$tmpd/generator.params"
  local tmp_hepmc="$tmpd/generator.hepmc"
  mkfifo "$tmp_hepmc" || exit 1
  local tmp_yoda="$tmpd/generator.yoda"
  local tmp_jobs="$tmpd/jobs.log"
  local tmpd_flat="$tmpd/flat"
  mkdir $tmpd_flat
  local tmpd_dump="$tmpd/dump"
  mkdir $tmpd_dump
  local tmpd_statusflag="$tmpd/status.flag"
  local tmpd_rivetdb="$tmpd/rivetdb.map"
  
  echo "workd=$workd"
  echo "tmpd=$tmpd"
  echo "tmp_params=$tmp_params"
  echo "tmp_hepmc=$tmp_hepmc"
  echo "tmp_yoda=$tmp_yoda"
  echo "tmp_jobs=$tmp_jobs"
  echo "tmpd_flat=$tmpd_flat"
  echo "tmpd_dump=$tmpd_dump"
  echo "tmpd_rivetdb=$tmpd_rivetdb"
  echo ""

  # Change status to "preparing"
  echo "preparing" > $tmpd_statusflag
  
  #
  echo "Prepare Rivet parameters ..."
  
  # make the local copy of rivet-histograms.map to have file read only once at the run start
  cat configuration/rivet-histograms.map | weed > $tmpd_rivetdb
  
  # beam process energy params -> histogramNames
  # prepare list of histograms
  local histogramsNames=$(cat $tmpd_rivetdb | grep "^$beam $process $energy $params " | grep -v " - -$" | cut -d ' ' -f 5 | sort)
  
  if [[ "$ANALYSIS" != "" ]] ; then
    echo "INFO: limiting analysis list to ANALYSIS=$ANALYSIS"
    histogramsNames=$(echo "$histogramsNames" | grep "^${ANALYSIS}_")
    
    if [[ "$histogramsNames" == "" ]] ; then
      echo "ERROR: no histograms remain after the limit"
      exit 1
    fi
  fi
  
  local nHistoSelected=$(echo "$histogramsNames" | wc -l)
  echo "Total histograms selected: $nHistoSelected"
  #echo "Selected histograms list:"
  #echo "$histogramsNames"
  
  # integrity check
  local dupNames=$(echo "$histogramsNames" | uniq -d)
  if [[ "$dupNames" != "" ]] ; then
    echo "ERROR: detected duplicated definitions of same histogram ID"
    echo "       dupNames=$dupNames"
    echo "Suspicious lines from configuration/rivet-histograms.map:"
    for i in $dupNames ; do
      cat configuration/rivet-histograms.map | weed | grep -n "$i"
    done
    exit 1
  fi
  
  # prepare list of analyses to run
  local analysesNames=$(echo "$histogramsNames" | histoname2analysis | sort -u)
  echo "analysesNames="$analysesNames
  echo "Total analyses selected: " $(echo "$analysesNames" | wc -l)
  
  local analysesBaseNames=$(echo "$analysesNames" | cut -d : -f 1 | sort -u)
  echo "analysesBaseNames="$analysesBaseNames
  echo "Total base analyses selected: " $(echo "$analysesBaseNames" | wc -l)
  
  if [[ "$analysesNames" == "" ]] ; then
    echo "ERROR: can not find any analysis for required parameters:"
    echo "         beam       = $beam"
    echo "         process    = $process"
    echo "         energy     = $energy"
    echo "         params     = $params"
    echo ""
    echo "       in map file:"
    echo "         configuration/rivet-histograms.map"
    exit 1
  fi
  
  echo ""
  
  # check presence of Rivet data files:
  local analysesFiles=$(find ${RIVET_DATA_PATH//:/ } -maxdepth 1 -type f)
  
  for i in $analysesBaseNames ; do
    # list of all files belonging to analysis $i
    local analysisFiles=$(echo "$analysesFiles" | grep $i)
    
    if [[ "$analysisFiles" == "" ]] ; then
      echo "ERROR: no files for $i analysis"
      exit 1
    fi
    
    check_files $analysisFiles
  done
  
  # flatten DATA histograms
  # (split .yoda files to separate histograms - .dat files)
  echo "Unpack data histograms..."
  
  local dataFiles=$(for i in $analysesBaseNames ; do echo "$analysesFiles" | grep "$i.yoda" ; done)
  if [[ "$dataFiles" != "" ]] ; then
    echo "dataFiles ="
    echo "$dataFiles"
    echo "output = $tmpd_flat"
    
    make -B -C rivetvm yoda2flat-split.exe YODA=$YODA
    if [[ "$?" != "0" ]] ; then
      echo "ERROR: fail to compile yoda2flat-split"
      exit 1
    fi
    echo ""
  
    cd $tmpd_flat
    $workd/rivetvm/yoda2flat-split.exe $dataFiles
    
    local nUnpackedData=$(find -type f -name '*.dat' | wc -l)
    echo "Total histograms unpacked=$nUnpackedData / selected=$nHistoSelected"
    
    #find -type f -name '*.dat' | sort | xargs -t -L 1 time $workd/rivetvm/complete.sh
    
    # remove ':KEY=VALUE' option string from histogram name
    local filterList=$(echo "$histogramsNames" | sed -e 's,:[^_]*_,_,')
    
    # process only subset of .dat files corresponding to histograms list of current run configuration
    # TODO: parallel xargs run of complete.sh
    
    find -type f -name '*.dat' | sort | while read f ; do
      # extract histo name from file name
      fh=${f/*REF_}
      fh=${fh/.dat}
      fhneed=$(echo "$filterList" | grep "^$fh")
      #echo "f=$f  fh=$fh  fhneed=$fhneed"
      
      if [[ "$fhneed" != "" ]] ; then
        echo "complete $f"
        $workd/rivetvm/complete.sh $f
        
        if [[ "$?" != "0" ]] ; then
          echo "ERROR: failed to unpack data histogram $f"
          exit 1
        fi
      else
        # this file is not needed for current run, remove to speedup processing
        #echo "skip $f"
        rm -f "$f"
      fi
    done
    
    if [[ "$(find . -maxdepth 1 -name '*.dat' | wc -l)" == "0" ]] ; then
      echo "ERROR: failed to unpack data histograms"
      exit 1
    fi
  fi
  
  echo ""
  
  cd $workd
  
  # Change status to "building_rvm"
  echo "building_rvm" > $tmpd_statusflag

  # build rivetvm
  echo "Building rivetvm ..."
  make -B -C rivetvm HEPMC=$HEPMC RIVET=$RIVET
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to compile rivetvm"
    exit 1
  fi
  echo ""
  
  # build user analyses
  rm -f analyses/*.so
  local allUserAnalyses=$(find analyses/ -name '*.cc' | xargs -I @ basename @ .cc)
  local runUserAnalyses=$(echo -e "${allUserAnalyses}\n${analysesNames}" | sort | uniq -d)
  
  # skip compilation if user analyses are not used in current run
  if [[ "$runUserAnalyses" != "" ]] ; then
    echo "Building user analyses ..."
    
    # patch rivet-build to use default g++ instead of hardcoded
    ( cd analyses &&
      cp $RIVET/bin/rivet-build $tmpd/ &&
      sed -e 's,mycxx=.*,mycxx="g++",' \
          -i $tmpd/rivet-build &&
      $tmpd/rivet-build *.cc )
    
    if [[ "$?" != "0" ]] ; then
      echo "ERROR: fail to compile user analyses"
      exit 1
    fi
    echo ""
  fi
  
  local runconf="$beam $energy $process $params $specific $generator $version $tune"
  runconf=${runconf// /_}
  local calfile=""
  
  if [[ "$process" == "heavyion-mb" && "$beam" == "PbPb" ]] ; then
    # note, pp runs of "heavyion-mb" doesn't require the calibration data
    echo
    echo "HEAVY ION RUN : preparing the calibration file"
    calfile=$workd/heavyion/calibration/calibration-$runconf.yoda
    echo "Calibration: $calfile"
    echo
  fi
  
  local analysesString="$(echo "$analysesNames" | sed 's,^,-a ,' | xargs)"
  local rivetExecString="$workd/rivetvm/rivetvm.exe $analysesString -i $tmp_hepmc -o $tmpd_flat -H $tmp_yoda"
  
  # activate periodic histograms dump only if needed
  if [[ "$T4T_DISPLAY" != "" ]] ; then
    rivetExecString="$rivetExecString -d $tmpd_dump"
  fi
  
  if [[ $calfile != "" ]] ; then
    rivetExecString="$rivetExecString -p $calfile"
  fi
  
  # activate Rivet debug log
  if [[ "$RIVETLOGLEVEL" != "" ]] ; then
    rivetExecString="$rivetExecString -loglevel $RIVETLOGLEVEL"
  fi
  
  # print output histograms list
  if [[ "$PRINTLIST" != "" ]] ; then
    rivetExecString="$rivetExecString -printlist"
  fi
  
  if [[ "$HICAL" == "0" ]] ; then
  # Searching for local calibration data
  local calibrationFileNames=$(cat $tmpd_rivetdb | grep "^$beam $process $energy $params " | cut -d ' ' -f 8 | grep -v "-" | sed 's,\,,\n,g' | sort -u)
  local calibrationBase="$workd/calibration/$beam/$process/$energy/$generator/$version/$tune"
  local calibrationInput=""
  
  for ical in $calibrationFileNames ; do
    local fcal="$calibrationBase/$ical"
    
    if ! test -f $fcal ; then
      echo "ERROR: the calibration file is missing:"
      echo "       $fcal"
      exit 1
    fi
    
    calibrationInput="$calibrationInput -p $fcal"
  done
  
  local rivetExecString="$rivetExecString $calibrationInput"
  fi
  
  local generatorExecString="./rungen.sh $mode $beam $process $energy $params $specific $generator $version $tune $nevts $seed $tmp_hepmc"

  # Change status to "running"
  echo "running" > $tmpd_statusflag

  # run generator and rivet
  # .param -> .hepmc -> .yoda
  echo "Run $generator $version and Rivet ..."
  echo "generatorExecString = $generatorExecString"
  echo "rivetExecString = $rivetExecString"
  
  cd $workd
  ( env $origEnv $generatorExecString ; exit $? ) &
  
  cd $tmpd
  ( $rivetExecString ; exit $? ) &
  
  # NOTE: the above `()&` is to properly propagate exit status to `jobs` list
  #       in the wait loop in case of the program crash
  
  cd $workd
  display_service $tmpd_dump "$beam $process $energy $params $generator $version $tune" &
  
  local timeout="30"
  
  # wait until all jobs will be finished
  while true ; do
    jobs -l > $tmp_jobs
    #echo "=> jobList :"
    #cat $tmp_jobs
    
    # at least one job finished with error exit code:
    if grep -q "Exit" $tmp_jobs ; then
      cat $tmp_jobs
      echo "ERROR: fail to run $generator $version or Rivet (error exit code)"
      # kill all jobs
      local pids="$(cat $tmp_jobs | weed | cut -d ' ' -f 2 | xargs)"
      #echo "pids = $pids"
      killtree $pids
      # and stop script
      exit 1
    fi
    
    # get number of running jobs:
    local njrun=$(grep -c -v display_service $tmp_jobs)
    
    # normaly two jobs (generator and rivet) should be running,
    # stop with error if only one job is running for a long time
    # TODO: there is race here due to timeout period,
    #       implement proper exit code checking of rivet and generator jobs
    if [[ "$njrun" == "1" ]] ; then
      let timeout--
      echo "INFO: waiting for jobs completion timeout=$timeout"
      cat $tmp_jobs
      
      if [[ "$timeout" == "0" ]] ; then
        echo "ERROR: fail to run $generator $version or Rivet (timeout)"
        local pids="$(cat $tmp_jobs | weed | cut -d ' ' -f 2 | xargs)"
        killtree $pids
        exit 1
      fi
    fi
    
    # rivet and generator jobs finished successfully
    if [[ "$njrun" == "0" ]] ; then
      local pids="$(cat $tmp_jobs | weed | cut -d ' ' -f 2 | xargs)"
      killtree $pids
      
      #echo "INFO: generator and Rivet jobs finished successfully"
      # exit from the loop
      break
    fi
    
    sleep 3
  done
  
  echo ""
  
  # Change status to "finalizing"
  echo "finalizing" > $tmpd_statusflag

  if [[ "$PRINTLIST" != "" ]] ; then
    echo "Histograms summary..."
    cat "$tmpd/printlist.txt"
    echo ""
  fi
  
  # at this stage event generation is finished
  # proccess output histograms:
  #   1. re-arrange according to directory structure
  #   2. extend DATA histograms with 'xfocus' column
  #   3. add metadata fields
  
  echo "Processing histograms..."
  echo "input  = $tmpd_flat"
  echo "output = $workd"
  
  cd $tmpd_flat
  
  # counter of skipped histograms
  local nSkip="0"
  
  # list of produced histograms
  local producedHistograms=""
  
  # prepare the file for the index listing
  echo "runconf=$runconf"
  mkdir -p "$datdir/index"
  local findex="$datdir/index/$runconf.txt"
  rm -f "$findex"
  
  # copy .yoda MC file with the gen card
  # add mcplots metadata to the yoda file
  local fmeta=${tmp_yoda}.meta
  if [[ ! -e "$fmeta" ]] ; then
    echo "ERROR: missing metadata file $fmeta"
    exit 1
  fi
  # complete the metadata (note, this is the copy of same code below for .dat file)
  sed -e "s,%beam%,$beam,"         \
      -e "s,%process%,$process,"   \
      -e "s,%energy%,$energy,"     \
      -e "s/%params%/$params/"     \
      -e "s/%specific%/$specific/" \
      -e "s,%generator%,$generator," \
      -e "s,%version%,$version,"     \
      -e "s/%tune%/$tune/"         \
      -e "s,%seed%,$seed,"         \
      -e "/%observable%/ d"   \
      -e "/%cuts%/ d"         \
      -i $fmeta
  # - skip few unneded lines from the .meta file
  # - prepend "# MCPLOTS " label
  cat $fmeta | grep -vE "METADATA$|^$" | sed -e 's,^,# MCPLOTS ,' >> $tmp_yoda
  
  local ymcdir="yoda-mc/$beam-$energy/$process/$generator-$version-$tune"
  mkdir -p $datdir/$ymcdir
  cp $tmp_params $datdir/$ymcdir/$runconf.params
  cp $tmp_yoda $datdir/$ymcdir/$runconf.yoda
  echo "yoda mc: $tmp_yoda -> $datdir/$ymcdir/$runconf.yoda"
  
  # TEMPORARY: generate calibration yoda files, 1 per configuration
  #mkdir -p "$datdir/calibration"
  #cp $tmp_yoda $datdir/calibration/calibration-$runconf.yoda
  
  # copy .yoda data files
  for f in $dataFiles ; do
    fdst=$datdir/$ydatadir/$(basename "$f")
    cp $f $fdst
    echo "yoda data: $f -> $fdst"
    
    local ext="${f##*.}"
    if [[ "$ext" == "gz" ]] ; then
        gunzip -f $fdst
    fi
  done
  
  # arrange histograms according to our convension on file/directory structure:
  for fname in *.dat ; do
    # form histogram name in Rivet notation:
    # strip "REF_" from the front of file name
    local hname="${fname#REF_}"
    # and strip ".dat" from the back of file name
    hname="${hname%.dat}"
    
    # check does it MC or data histogram:
    # (file names of data histograms have "REF_" prefix)
    local type="mc"
    if [[ "$fname" == "REF_$hname.dat" ]] ; then
      type="data"
    fi
    
    local matchMC="$hname"
    if [[ "$type" == "data" ]] ;then
      # match MC histogram name (could include options part)
       matchMC=$(cat $tmpd_rivetdb | cut -d ' ' -f 5 | awk '{print gensub(/:.*_/, "_", 1), $1}' | grep "^$hname " | cut -d ' ' -f 2)
    fi
    
    #echo "fname=$fname, hname=$hname, type=$type, matchMC=$matchMC"
    
    # skip data histogram if there is no corresponding MC histogram:
    # (this is neccessary as part of data histograms can correspond to
    # different beam energy or unsupported observables)
    if [[ "$type" == "data" && ! -e "$matchMC.dat" ]] ; then
      #echo "INFO: skip data as no corresponding MC file"
      continue
    fi
    
    # skip temporary histograms, the sign of temporary histogram is the leading "_" character:
    # so the hname is 'analysis__histoid'
    if [[ "$hname" =~ __ ]] ; then
      echo "INFO: skip temporary MC histogram file $fname"
      continue
    fi
    
    # find histogram description from map file:
    local line=$(cat $tmpd_rivetdb | grep " $matchMC ")
    
    if [[ "$line" == "" ]] ; then
      echo "WARNING: unknown histogram file $fname (missing entry for $matchMC in map file configuration/rivet-histograms.map)"
      let nSkip++
      continue
    fi
    
    # skip histogram if it do not corresponds to run parameters
    if [[ "$(echo "$histogramsNames" | grep "^$matchMC$")" == "" ]] ; then
      #echo "INFO: skip incompatible histogram"
      continue
    fi
    
    # split description string into components
    local descr=( $line )
    local hist_observable=${descr[5]}
    local hist_cuts=${descr[6]}
    
    # skip histogram if it is marked as non-interesting
    if [[ "$hist_observable" == "-" || "$hist_cuts" == "-" ]] ; then
      continue
    fi
    
    # check observable and cuts definitions
    if [[ "$hist_observable" == "" || "$hist_cuts" == "" ]] ; then
      echo "WARNING: incomplete $matchMC histogram definition (missing observable or cuts field in map file configuration/rivet-histograms.map)"
      let nSkip++
      continue
    fi
    
    # histogram pass all checks, add it to the list
    if [[ "$type" == "mc" ]] ; then
      producedHistograms="$producedHistograms $hname"
    fi
    
    # extract IDs: analysis, options, histogram
    local hpath=$(grep -m1 ^Path= $fname | cut -d/ -f2- | sed -e 's,^REF/,,')
    local analysis_options_id=$(echo $hpath | cut -d/ -f1)
    local analysis_id=$(echo $analysis_options_id | cut -d: -f1)
    local options_id=$(echo $analysis_options_id | cut -s -d: -f2-)
    local hist_id=$(echo $hpath | cut -d/ -f2- | tr / _)
    
    # TODO: decide fs layout for MC analyses with options,
    #       the current layout gives overlaps
    #
    # TODO: check more on the overlap for non-MC analyses,
    #       for example heavy-ion with "centr=XXX" option
    if [[ "$analysis_id" != "${analysis_id#MC_}" && "$options_id" != "" ]] ; then
      echo "ERROR: MC_xxx histogram with options, possible fs layout overlap."
      exit 1
    fi
    
    # debug: the 'hist_id' is empty sometimes for some runs of:
    #   `./runRivet.sh local ppbar top-mc 1960 - - sherpa 1.2.3 default 100 1`
    if [[ "$hist_id" == "" ]] ; then
      echo "ERROR: empty hist_id=$hist_id hpath=$hpath fname=$fname"
      exit 1
    fi
    
    # subpath for histogram:
    local fpath="$analysis_id-$beam-$energy/$process-$hist_observable-$hist_cuts-$hist_id"
    
    # MC: append destination path by $specific if it defined
    # this is neccessary to handle different alpgen parton multiplicity bins
    if [[ "$type" == "mc" && "$specific" != "-" ]] ; then
      fpath="$fpath/$specific"
    fi
    
    if [[ "$type" == "data" ]] ; then
      # DATA histogram file name:
      fpath="$fpath/$analysis_id.dat"
    else
      # MC histogram file name:
      fpath="$fpath/$generator-$version-$tune.dat"
    fi
    
    local dstf="$datdir/$fpath"
    local dstd=$(dirname $dstf)
    mkdir -p $dstd
    
    if [[ "$type" == "data" ]] ; then
      # add bin mid point to data histograms
      cat $fname | extend_hist > $dstf
    else
      # mc histograms
      cp $fname $dstf
      # complete mc histograms with PLOT section
      $workd/rivetvm/complete.sh  $dstf

      # and METADATA section
      sed -e "s,%beam%,$beam,"         \
          -e "s,%process%,$process,"   \
          -e "s,%energy%,$energy,"     \
          -e "s/%params%/$params/"     \
          -e "s/%specific%/$specific/" \
          -e "s,%generator%,$generator," \
          -e "s,%version%,$version,"     \
          -e "s/%tune%/$tune/"         \
          -e "s,%seed%,$seed,"         \
          -e "s,%observable%,$hist_observable," \
          -e "s/%cuts%/$hist_cuts/"             \
          -i $dstf
    
      # copy .param file beside of .dat to have a reference of MC generator card:
      local dstfp=${dstf/%.dat/.params}
      cp $tmp_params $dstfp
    fi
    
    echo "$type:  $fname -> $dstf"
    
    local gvt="$generator $version $tune"
    if [[ "$type" == "data" ]] ; then
      gvt="- - -"
    fi
    
    local opt="$options_id"
    if [[ "$opt" == "" ]] ; then
      opt="-"
    fi
    
    # now rewrite the path to the hist for the index file according to the new structure
    local yfpath="$ymcdir/$runconf.yoda"
    if [[ "$type" == "data" ]] ; then
        yfpath="$ydatadir/$analysis_id.yoda"
    fi
    fpath=$yfpath
    
    local index="$fpath $type $analysis_id $opt $hist_id $beam $energy $process $hist_observable $hist_cuts $specific $gvt"
    #echo "index=$index"
    echo "$index" >> "$findex"
  done
  
  echo ""
  echo "INFO: index summary: size / path"
  wc -l $findex
  
  echo ""
  
    if [[ "$HTMLDIR" == "" ]] ; then
      HTMLDIR="html"
    fi
    local workd_html="$workd/$HTMLDIR"
    
  if [[ "$MKHTML" == "1" ]] ; then
    # prepare html page with plots of all produced histograms
    echo "INFO: preparing summary page..."
    
    # Note the `yes | ...` is to bypass "LaTeX Error: Type <RETURN> to proceed" problem
    yes "" | rivet-mkhtml -s $tmp_yoda -o $workd_html
    echo "INFO: summary html path = $workd_html"
    echo ""
  fi
  
  if [[ "$PRINTLIST" != "" ]] ; then
    local printlist=$workd_html/printlist.txt
    {
      echo "Date: $(date)"
      echo "Run parameters: $@"
      cat "$tmpd/printlist.txt"
      echo ""
    } > $printlist
    
    echo "INFO: histograms summary file = $printlist"
    echo ""
  fi
  
  if [[ "$nSkip" != "0" ]] ; then
    echo "ERROR: $nSkip histograms were not processed due to errors (see WARNINGs above)"
    echo "       find unprocessed histograms here: $tmpd_flat"
    echo ""
    echo "       to cure the error add skipped histograms to:"
    echo "         configuration/rivet-histograms.map"
    exit 1
  fi
  
  # check for missing histograms:
  local allHistograms=$(echo $producedHistograms | tr ' ' '\n' ; echo "$histogramsNames")
  local missingHistograms=$(echo "$allHistograms" | sort | uniq -u)
  if [[ "$missingHistograms" != "" ]] ; then
    echo "ERROR: following histograms should be produced according to run parameters,"
    echo "       but missing from Rivet output:"
    echo "$missingHistograms" | sed 's,^,         ,'
    echo ""
    echo "       check mapping of above histograms in configuration file:"
    echo "         configuration/rivet-histograms.map"
    exit 1
  fi
  
  # Change status to "completed"
  echo "completed" > $tmpd_statusflag
  
  # print disk usage for lxbatch or boinc mode:
  if [[ "$mode" == "lxbatch" || "$mode" == "boinc" ]] ; then
    local disk=$(du -sk $workd | cut -f 1)
    echo $disk > $workd/diskusage
    echo "Disk usage: $disk Kb"
    echo ""
  fi
  
  # print CPU usage
  times > $tmpd/cputimes
  local cpu=$(cat $tmpd/cputimes | xargs | sed -e 's,m,*60+,g' -e 's,s ,+,g' -e 's,s$,+0.5,' | bc | cut -d . -f 1)
  echo $cpu > $workd/cpuusage
  echo "CPU usage: $cpu s"
  echo ""
  
  # copy calibration histograms
  if [[ "$HICAL" == "1" ]] ; then
    make -B -C $workd/rivetvm yoda2calibration.exe YODA=$YODA
    if [[ "$?" != "0" ]] ; then
      echo "ERROR: fail to compile yoda2calibration"
      exit 1
    fi
    
    echo "Copying calibration files..."
    local calibration_output="$workd/calibration/$beam/$process/$energy/$generator/$version/$tune"
    mkdir -p $calibration_output
    for i in $analysesNames ; do
      $workd/rivetvm/yoda2calibration.exe -i $tmp_yoda -o "$calibration_output/$i.yoda" -a $i
      
      if [[ "$?" != "0" ]] ; then
        echo "ERROR: fail to extract calibration data for $i"
        exit 1
      fi
    done
    
    echo "INFO: calibration data extracted"
  fi
  
  echo "Clean tmp ..."
  cd $workd
  rm -rf $tmpd
  echo ""
  
  echo "Run finished successfully"
}


# === main ===

echo "===> [runRivet] $(date) [$@]"
echo ""

# run self-test
if [[ "${1/:*}" == "check" ]] ; then
  set_environment ${1/*:}
  
  # remove lib with private analyses
  rm -f analyses/*.so
  
  # analyses info and check
  check_analyses
  echo ""
  
  echo "Rivet LCG installations:"
  gpath="/cvmfs/sft.cern.ch/lcg/releases/LCG_*/MCGenerators/rivet/*"
  echo "$gpath:"
  echo -n "  "
  ls -1d $gpath 2>&- | cut -d / -f 9 | sort -u | xargs
  echo ""
  
  echo "Generators check:"
  ./rungen.sh check
  echo ""
  
  # TODO: indicate problems with proper exit code
  exit 0
fi

# load environment
if [[ "$1" == "load" ]]; then
  set_environment local
  return
fi

# print usage info
if [[ "$#" != "11" ]] ; then
  echo "runRivet.sh: tool for MC production"
  echo "Usage:"
  echo "  ./runRivet.sh [mode] [beam] [process] [energy] [params] [specific] [generator] [version] [tune] [nevts] [seed]"
  echo "        [mode] - local lxbatch boinc check:local check:boinc"
  echo "        [beam] - $(cat configuration/rivet-histograms.map | weed | cut -d ' ' -f 1 | sort -u | xargs)"
  echo "     [process] - $(cat configuration/rivet-histograms.map | weed | cut -d ' ' -f 2 | sort -u | xargs)"
  echo "      [energy] - $(cat configuration/rivet-histograms.map | weed | cut -d ' ' -f 3 | sort -unr | xargs)"
  echo "      [params] - generator settings (generator-level cuts)"
  echo "    [specific] - generator-specific settings (for example, jet bins for alpgen)"
  echo "   [generator] - $(ls -1 configuration/*.params | cut -d / -f 2 | cut -d - -f 1 | sort -u | xargs)"
  echo "     [version] - generator version"
  echo "        [tune] - generator tune"
  echo "       [nevts] - number of events in run"
  echo "        [seed] - initial seed of random number generator"
  echo ""
  echo "Environment (heavy-ion calibration data):"
  echo "  export HICAL=1  - generate local calibration data and output to calibration/ subdirectory"
  echo "  export HICAL=0  - use local calibration data form calibration/ subdirectory"
  echo "  export HICAL=   - use Rivet integrated calibration data (default)"
  echo ""
  echo "Environment (debug context):"
  echo "  export MKHTML=1     - generate histograms pictures"
  echo "  export HTMLDIR=path - path to pictures output directory (default is 'html')"
  echo "  export ANALYSIS=xxx - narrow the running analyses list to only xxx"
  echo "  export RIVETLOGLEVEL=xxx - set Rivet log level to: TRACE, DEBUG, INFO, WARN, ERROR"
  echo "  export PRINTLIST=1  - print the list of output histograms"
  echo "  export USECORES=n   - event generation parallelization, use n cores, default n=1"
  echo ""
  echo "Environment (optimisation):"
  echo "  export CACHE=path   - set path and activate the cache of generator grids"
  echo ""
  echo "Examples:"
  echo "  ./runRivet.sh local ee    zhad         91.2 -  -     pythia6           6.424        p0            100 123"
  echo "  ./runRivet.sh local ee    zhad         91.2 -  -     pythia8           8.145        hoeth         100 895"
  echo "  ./runRivet.sh local ee    zhad         91.2 -  -     vincia            1.0.24_8.142 jeppsson2     100 123"
  echo "  ./runRivet.sh local ee    zhad         91.2 -  -     vincia            1.0.25_8.150 jeppsson3     100 123"
  echo "  ./runRivet.sh local pp    mb-inelastic 7000 -  -     pythia6           6.424        ambt1         100 234"
  echo "  ./runRivet.sh local ppbar mb-inelastic 1800 -  -     pythia8           8.145        tune-4c       100 345"
  echo "  ./runRivet.sh local ppbar mb-inelastic  630 -  -     herwig++          2.4.2        default       100 456"
  echo "  ./runRivet.sh local ppbar mb-inelastic 1800 -  -     sherpa            1.2.3        default       100 456"
  echo "  ./runRivet.sh local pp    mb-inelastic 7000 -  -     phojet            1.12a        default       100 123"
  echo "  ./runRivet.sh local pp    zinclusive   7000 -,-,50,130 - herwig++powheg 2.7.1       default       100 456"
  echo "  ./runRivet.sh local pp    winclusive   7000 10 0,1   alpgenpythia6     2.1.4_6.426  350-CTEQ5L    100 123"
  echo "  ./runRivet.sh local pp    jets         7000 10 2,0   alpgenherwigjimmy 2.1.3e_6.520 default-CTEQ6L1   100 123"
  echo "  ./runRivet.sh local pp    ue           7000 -  -     epos              1.99.crmc.0.v3400 default    100 123"
  echo "  ./runRivet.sh local pp    zinclusive   7000 -,-,50,130 -  madgraph5amc 2.6.2.atlas  lo            100 1"
  echo "  ./runRivet.sh local pp    z1j          8000     70  - pythia8          8.240        tune-4c       100 345"
  echo "  ./runRivet.sh local PbPb  heavyion-mb  2760 - - pythia8 8.308 default  100 1"
  exit 1
fi

if [[ "$HICAL" != "" ]]; then
  echo "WARNING: HICAL modes is not yet implemented"
fi

set_environment $1
run "$@"
