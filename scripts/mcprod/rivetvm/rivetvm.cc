#include "Rivet/AnalysisHandler.hh"
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/RivetYODA.hh"
#include "Rivet/Tools/Logging.hh"

#include "HepMC/GenEvent.h"
#include <fstream>
#include "tools.h"

using namespace std;

int main(int argc, char* argv[])
{
  // Vector with Rivet analyses
  vector<string> analyses;
  
  // HepMC input file
  string input_file = "input.hepmc";
  
  // .dat histograms output directory
  string output_dat_dir = ".";
  
  // YODA output file
  string output_yoda = "output.yoda";
  
  // dump directory for screensaver
  string output_dump_dir = "";
  
  // preload YODA files (in use for load of calibration data for heavy-ion analyses)
  vector<string> preload_files;
  
  // Rivet log level
  string loglevel = "";
  
  // print list of final histograms
  bool printlist = false;
  
  // Iterate over command-line arguments
  for (int i = 1; i < argc; i++) {
    const string par = argv[i];
    const string val = (i < argc - 1) ? argv[i+1] : "";
    
    if (par == "--list-analyses") {
      listAvailableAnalyses();
      return 0;
    }
    else if (par == "-a") {
      analyses.push_back(val);
    }
    else if (par == "-i") {
      input_file = val;
    }
    else if (par == "-o") {
      output_dat_dir = val;
    }
    else if (par == "-H") {
      output_yoda = val;
    }
    else if (par == "-d") {
      output_dump_dir = val;
    }
    else if (par == "-p") {
      preload_files.push_back(val);
    }
    else if (par == "-loglevel") {
      loglevel = val;
    }
    else if (par == "-printlist") {
      printlist = true;
    }
    
    // TODO: exit with error:
    //else {
    //  cerr << "ERROR: (rivetvm) unknown parameter" << par << endl;
    //  return 1;
    //}
  }
  
  // Check if analyses have been supplied
  if (analyses.size() == 0) {
    cerr << "ERROR: no analysis specified" << endl;
    return 1;
  }
  
  // Check analyses names
  for (const string& i : analyses) {
    if (!checkAnalysis(i)) {
      cerr << "ERROR: requested analysis " << i << " is not available" << endl;
      return 1;
    }
  }
  
  // activate debug logging (domain, level)
  // https://gitlab.com/hepcedar/rivet/-/blob/main/include/Rivet/Tools/Logging.hh#L13
  // level: 0="TRACE", 10="DEBUG"
  //Rivet::Log::setLevel("Rivet", 10);
  //Rivet::Log::setLevel("Rivet.Analysis.Handler", 0);
  
  if (loglevel != "") {
    cout << "INFO: (rivetvm.cc) set Rivet log level = " << loglevel << endl;
    Rivet::Log::setLevel("Rivet", Rivet::Log::getLevelFromName(loglevel));
  }
  
  // Rivet
  Rivet::AnalysisHandler rivet;
  
  // explicitly skipping all multi-weights handling to avoid multiple output histograms[i]
  // (was not neccesary in 3.1.0, but have effect in 3.1.8)
  // https://indico.cern.ch/event/944962/contributions/3997245/attachments/2100421/3531136/cg_rivetWeights.pdf
  rivet.skipMultiWeights(true);
  
  rivet.addAnalyses(analyses);
  
  // open input file
  ifstream is(input_file.c_str());
  
  if (!is) {
    cerr << "ERROR: failed to open input file " << input_file << endl;
    return 1;
  }
  
  // read preload files
  for (auto i : preload_files) {
    cout << "INFO: (rivetvm.cc) read preload (calibration) file " << i << endl;
    rivet.readData(i);
  }
  
  // create an empty event
  HepMC::GenEvent evt;

  // loop over the input stream
  while (is) {
    // read the event
    evt.read(is);
    
    if (is.bad()) {
      cerr << "ERROR: failed to read events from input file" << endl;
      return 1;
    }
    
    // skip invalid event (no particles or vertices)
    if (! evt.is_valid()) continue;
    
    /*
    cout << "INFO: hepmc event units"
         << " energy=" << HepMC::Units::name(evt.momentum_unit())
         << " length=" << HepMC::Units::name(evt.length_unit())
         << endl;
    */
    // explicit units to work-around rivet [3.0.0 - 3.1.0] units handling issue
    // TODO: to be removed after fix in rivet, use pythia6 428 for test (produce events with MEV,MM units)
    evt.use_units(HepMC::Units::GEV, HepMC::Units::MM);
    
    // work-around for rivet 3.1.10 bug, crash on empty event weights:
    // https://gitlab.com/hepcedar/rivet/-/issues/441
    if (evt.weights().empty()) {
      //cout << "evt.weights().size()=" << evt.weights().size() << endl;
      // set single weight=1.0
      const std::vector<double> w(1, 1.0);
      evt.weights() = w;
    }
    
    // analyze the event
    rivet.analyze(evt);
    
    // print progress
    const int nevt = rivet.numEvents();
    if (nevt % 100 == 0) {
      cout << nevt << " events processed" << endl;
    }
    
    // dump histograms for display every 100 up to 1000 and then every 1000
    if ((output_dump_dir != "") && 
        (nevt > 0) &&
        ( (nevt % 1000 == 0) ||
          ((nevt < 1000) && (nevt % 100 == 0)) )
       ) {
      cout << "dumping histograms..." << endl;
      
      // reduce verbosity to suppress warnings due to low-statistics,
      // also these warnings are not really important as
      // the only purpose of `dump` is a progress indication
      const int levelsave = Rivet::Log::getLog("Rivet.Analysis").getLevel();
      Rivet::Log::setLevel("Rivet.Analysis", Rivet::Log::WARN+1);
      
      // prepare intermediate "final" histograms
      rivet.finalize();
      
      // restore verbosity
      Rivet::Log::setLevel("Rivet.Analysis", levelsave);
      
      //printState(rivet);
      dumpState(output_dump_dir, rivet);
    }
  }
  
  // finalize a run
  rivet.finalize();
  
  // print the list of histograms
  if (printlist) printState(rivet);
  
  // write out histograms
  dumpState(output_dat_dir, rivet);
  
  // dump all histograms to .yoda file
  rivet.writeData(output_yoda);
  
  {
  // dump metadata into the file output.yoda.meta
  const string output_meta = output_yoda + ".meta";
  const int numEvents = rivet.numEvents();
  const double crossSection = rivet.nominalCrossSection();
  ofstream fmeta(output_meta.c_str());
  writeHeader(fmeta, numEvents, crossSection, "");
  
  cout << "INFO: rivet analysis finished:"
       << " numEvents=" << numEvents
       << " crossSection=" << crossSection
       << endl;
  }
  
  return 0;
}
