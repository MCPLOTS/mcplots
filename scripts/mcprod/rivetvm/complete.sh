#!/bin/bash

# attach plot description data to histogram .dat file from corresponding .plot file from Rivet data directory

fname="$1"

if [[ "$fname" == "" ]] ; then
  echo "ERROR: missing file name"
  exit 1
fi

if grep -q "# BEGIN PLOT" $fname ; then
  echo "ERROR: file $fname already completed"
  exit 1
fi

# extract histogram 'path'
histo=$(grep -m 1 Path= $fname)
histo=${histo#*=}
histo=${histo// }
histo=${histo#/REF}

# get analysis name
analysis=${histo#/}
analysis=${analysis%%/*}

# strip options suffix substring
analysis=${analysis%%:*}

# TODO: work-around stray '_WJET' inside data path, to be fixed in rivet > 3.1.2
analysis=${analysis%_WJET}

# find corresponding .plot file
data=""
for i in ${RIVET_DATA_PATH//:/ } ; do
  data=$i/$analysis.plot
  if test -e $data ; then break ; fi
  data=""
done
if [[ "$data" == "" ]] ; then
  echo "ERROR: failed to find .plot file for $analysis"
  exit 1
fi

#echo "fname = $fname"
#echo "histo = $histo"
#echo "analysis = $analysis"
#echo "data = $data"

# parse .plot file and extract all blocks which correspond to $histo
{
# remove option substring ":key=value"
histobase=${histo/:*\//\/}

#{ echo "histo=$histo"
#  echo "histobase=$histobase"
#} >&2

echo "# BEGIN PLOT $histo"

# the `tr` and emply line skip is for "CRLF -> LF" correction in some of .plot files

cat $data | tr '\r' '\n' | while read line ; do
  if [[ "$line" == "" ]] ; then continue ; fi
  
  if [[ "$line" =~ "BEGIN PLOT" ]] ; then
    # current line is PLOT block start
    pattern=${line#*BEGIN PLOT}
    pattern=${pattern// }
    
    # fix the '*' misuse: replace '*' (any number of repeats, including zero) -> '+' (one or more repeats)
    # TODO: inform Rivet team
    pattern=${pattern/\*/+}
    
    #{ echo "line=$line"
    #  echo "pattern=$pattern"
    #} >&2
    
    if [[ "$histobase" =~ $pattern ]] ; then
      # block match histogram $histo, enable output of content
      print=1
      continue
    fi
  fi
  
  if [[ "$line" =~ "END PLOT" ]] ; then
    print=0
    continue
  fi
  
  if [[ "$print" == "1" ]] ; then
    echo "$line"
  fi
done

echo "# END PLOT"
} >> $fname
