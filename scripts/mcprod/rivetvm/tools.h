#ifndef TOOLS_H
#define TOOLS_H

#include "Rivet/Rivet.hh"
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/RivetYODA.hh"

using namespace std;

int numEntries(const YODA::AnalysisObjectPtr obj)
{
  const string type = obj->type();
  
  if (type == "Profile1D") {
    const YODA::Profile1D& x = dynamic_cast<const YODA::Profile1D&>(*obj);
    return x.numEntries();
  }
  
  if (type == "Histo1D") {
    const YODA::Histo1D& x = dynamic_cast<const YODA::Histo1D&>(*obj);
    return x.numEntries();
  }
  
  if (type == "Histo2D") {
    const YODA::Histo2D& x = dynamic_cast<const YODA::Histo2D&>(*obj);
    return x.numEntries();
  }
  
  if (type == "Counter") {
    const YODA::Counter& x = dynamic_cast<const YODA::Counter&>(*obj);
    return x.numEntries();
  }
  
  // unknown histogram type or
  // object is not capable for number of entries (for example Scatter2D)
  return -1;
}


void printState(const Rivet::AnalysisHandler& rivet)
{
  ofstream fout("printlist.txt");
  
  const int numEvents = rivet.numEvents();
  const double crossSection = rivet.nominalCrossSection();
  const vector<YODA::AnalysisObjectPtr> data = rivet.getYodaAOs();
  
  fout << "Total events processed = " << numEvents << ", "
       << "cross-section = " << crossSection << " pb. "
       << "Histograms list:"
       << endl;
  
  // header
  fout << left << setw(12) << "Type"
       << right << setw(8) << "Entries" << "  "
       << "Path" << endl;
  
  for (auto ao : data) {
    fout << left << setw(12) << ao->type()
         << right << setw(8) << numEntries(ao) << "  "
         << ao->path() << endl;
  }
}

string stripOptions(string x)
{
  const size_t p = x.find(":");
  return x.substr(0, p);
}

bool checkAnalysis(const string a)
{
  static const vector<string> analyses = Rivet::AnalysisLoader::allAnalysisNames();
  return (count(analyses.cbegin(), analyses.cend(), stripOptions(a)) != 0);
}

void listAvailableAnalyses()
{
  static const vector<string> analyses = Rivet::AnalysisLoader::allAnalysisNames();
  cout << "Available analyses: " << endl;
  for (const string& a : analyses) {
    cout << a << endl;
  }
}

// create the output filename based on a histogram name
string getOutputFilename(string path)
{
  // replace '/' -> '_'
  while (true) {
    const size_t p = path.find("/");
    if (p == string::npos) break;
    path.replace(p, 1, "_");
  }
  
  return path.substr(1) + ".dat";
}

string getAnalysisName(const string path)
{
  const size_t p1 = path.find("/");
  if (p1 != 0) return "";
  
  const size_t p2 = path.find("/", p1 + 1);
  if (p2 == string::npos) return "";
  
  return path.substr(p1 + 1, p2 - (p1 + 1));
}

// get analysis name for histogram object
string getAnalysisName(const Rivet::AnalysisHandler& rivet, const Rivet::MultiweightAOPtr obj)
{
  for (const Rivet::AnaHandle a : rivet.analyses()) {
    for (const Rivet::MultiweightAOPtr ao : a->analysisObjects()) {
      if (obj == ao) {
        return a->name();
      }
    }
  }
  
  return "";
}

// Write some header meta data
void writeHeader(ostream &file, const int numEvents, const double crosssection, const string status)
{
  // increase precision for floats:
  const streamsize prec0 = file.precision(15);
  
  file << "# BEGIN METADATA\n"
       << "beam=%beam%\n"
       << "process=%process%\n"
       << "energy=%energy%\n"
       << "params=%params%\n"
       << "specific=%specific%\n"
       << "generator=%generator%\n"
       << "version=%version%\n"
       << "tune=%tune%\n"
       << "nevts=" << numEvents << "\n"
       << "seed=%seed%\n"
       << "crosssection=" << crosssection << "\n"
       << "rivet=" << Rivet::version() << "\n"
       << "status=" << status << "\n"
       << "revision=%revision%\n"
       << "observable=%observable%\n"
       << "cuts=%cuts%\n"
       << "# END METADATA\n\n";
  
  // restore precision
  file.precision(prec0);
}

// Write Histogram1D to file
void writeHisto(ostream& file, const YODA::AnalysisObjectPtr obj)
{
  const string hpath = obj->path();
  const YODA::Histo1D& histo = dynamic_cast<const YODA::Histo1D&>(*obj);
  const size_t nBins = histo.numBins();
  
  file << "# BEGIN HISTOGRAM " << hpath << "\n"
       << "Path=" << hpath << "\n";
  
  for (size_t i = 0; i < nBins; ++i) {
    const YODA::HistoBin1D& bin = histo.bin(i);
    
    file << bin.xMin()   << "\t"
         << bin.xMid()  << "\t"
         << bin.xMax()  << "\t"
         << bin.height()    << "\t"
         << bin.heightErr() << "\t"
         << bin.heightErr() << "\n";
  }
  file << "# END HISTOGRAM\n\n";
  
  // increase precision for floats:
  const streamsize prec0 = file.precision(15);
  
  // Also write the Histogram1D distributions so that we can add weigthed things afterwards
  file << "# BEGIN HISTOSTATS " << hpath << "\n"
       << "Path=" << hpath << "\n";
  
  for (size_t i = 0; i < nBins; ++i) {
    const YODA::HistoBin1D& bin = histo.bin(i);
    
    file << bin.xMin()    << "\t"
         << bin.xMid()   << "\t"
         << bin.xMax()   << "\t"
         << bin.numEntries() << "\t"
         << bin.sumW()    << "\t"
         << bin.sumW2()   << "\t"
         << bin.sumWX()   << "\t"
         << bin.sumWX2()  << "\n";
  }
  file << "# END HISTOSTATS\n\n";
  
  // restore precision
  file.precision(prec0);
}

double getBinMean(const YODA::ProfileBin1D& bin)
{
  double y;
  try {
    y = bin.mean();
  } catch (const YODA::LowStatsError& lse) {
    y = 0.0;
  }
  return y;
}

double getBinError(const YODA::ProfileBin1D& bin)
{
  double e;
  try {
    e = bin.stdErr();
  } catch (const YODA::LowStatsError& lse) {
    e = 0.0;
  }
  return e;
}

// Write a Profile1D object to file
void writeProfile(ostream &file, const YODA::AnalysisObjectPtr obj)
{
  const string hpath = obj->path();
  const YODA::Profile1D& prof = dynamic_cast<const YODA::Profile1D&>(*obj);
  const size_t nBins = prof.numBins();
  
  file << "# BEGIN HISTOGRAM " << hpath << "\n"
       << "Path=" << hpath << "\n";
  
  for (int i = 0; i < nBins; ++i) {
    const YODA::ProfileBin1D& bin = prof.bin(i);
    
    file << bin.xMin()    << "\t"
         << bin.xMid()   << "\t"
         << bin.xMax()   << "\t"
         << getBinMean(bin)  << "\t"
         << getBinError(bin) << "\t"
         << getBinError(bin) << "\n";
  }
  file << "# END HISTOGRAM\n\n";
  
  // increase precision for floats:
  const streamsize prec0 = file.precision(15);
  
  file << "# BEGIN PROFILESTATS " << hpath << "\n"
       << "Path=" << hpath << "\n";
  
  for (int i = 0; i < nBins; ++i) {
    const YODA::ProfileBin1D& bin = prof.bin(i);
    
    file << bin.xMin()    << "\t"
         << bin.xMid()   << "\t"
         << bin.xMax()   << "\t"
         << bin.numEntries() << "\t"
         << bin.sumW()    << "\t"
         << bin.sumW2()   << "\t"
         << bin.sumWX()   << "\t"
         << bin.sumWX2()  << "\t"
         << bin.sumWY()   << "\t"
         << bin.sumWY2()  << "\t"
         << 0 << "\n";
    
    // Note: the last output value was LWH::Profile1D.getSumY2W2(i) (Rivet1/AIDA)
    // which is not available in Rivet2/YODA (which provide different quantity
    // YODA::Profile1D.bin(i).sumWXY() instead).
    // The Y2W2 or WXY are not necessary for our applications: calculations of error or mean and
    // merging of histograms and could be ommited.
    // The field is set to ZERO to keep compatibility with parser from merge code (mcprod/merge),
    // this also allows to use same merge program for both Rivet1 and Rivet2 output.
  }
  file << "# END PROFILESTATS\n\n";
  
  // restore precision
  file.precision(prec0);
}

// Write DataPointSet to file
void writeDPS(ostream& file, const YODA::AnalysisObjectPtr obj)
{
  const string hpath = obj->path();
  const YODA::Scatter2D& dps = dynamic_cast<const YODA::Scatter2D&>(*obj);
  
  file << "# BEGIN HISTOGRAM " << hpath << "\n"
       << "Path=" << hpath << "\n";
  
  for (size_t i = 0; i < dps.numPoints(); ++i) {
    const YODA::Point2D& p = dps.point(i);
    
    file << p.xMin() << "\t"
         << p.x()    << "\t"
         << p.xMax() << "\t"
         << p.y()    << "\t"
         << p.yErrMinus() << "\t"
         << p.yErrPlus()  << "\n";
  }
  file << "# END HISTOGRAM\n\n";
}

// A small function that calls either writeHisto or writeProfile, depending on the observable
bool writeFinal(ostream& file, const YODA::AnalysisObjectPtr obj)
{
  const string type = obj->type();
  const string path = obj->path();
  
  if (type == "Profile1D") {
    writeProfile(file, obj);
    return true;
  }
  
  if (type == "Histo1D") {
    writeHisto(file, obj);
    return true;
  }
  
  if (type == "Scatter2D") {
    writeDPS(file, obj);
    return true;
  }
  
  cout << "WARNING: skipping output of unsupported type=" << type << " path=" << path << endl;
  return false;
}

void dumpState(const string& root, const Rivet::AnalysisHandler& rivet)
{
  const int numEvents = rivet.numEvents();
  const double crossSection = rivet.nominalCrossSection();
  const vector<YODA::AnalysisObjectPtr> data = rivet.getYodaAOs();
  
  //cout << "rivet.numWeights()=" << rivet.numWeights() << endl;
  //cout << "rivet.defaultWeightIndex()=" << rivet.defaultWeightIndex(); << endl;
  //cout << "rivet.getYodaAOs().size()=" << data.size() << endl;
  
  // query analyses statuses (VALIDATED, UNVALIDATED, etc)
  map<string, string> name_to_status;
  for (const auto& i : rivet.analysesMap())
    name_to_status[i.second->name()] = i.second->status();
  
  for (size_t i = 0; i < data.size(); ++i) {
    //cout << "i=" << i << endl;
    const YODA::AnalysisObjectPtr obj = data[i];
    
    const string type = obj->type();
    const string path = obj->path();
    const string name = getAnalysisName(path);
    //const AOPath aop(obj->path());
    //const string name = path.analysis();
    const string status = name_to_status[name];
    const string fname = root + "/" + getOutputFilename(path);
    //cout << "name = " << name << " type = " << type << " status = " << status << " path = " << path << " fname = " << fname << endl;
    
    // skip several auxially counter histograms introduced in rivet 2.4.0
    // (path starts with "/_" for such histograms, so far /_EVTCOUNT and /_XSEC exist)
    if (path.substr(0, 2) == "/_") continue;
    
    // skip temp. histograms (introduced in rivet 3.x)
    if (path.find("/TMP/") != string::npos) continue;
    
    // skip all internal state variables
    if (type == "Counter") continue;
    
    if (name == "") {
      // something is wrong with histogram path
      cout << "ERROR: dumpState() skipping histogram due to strange path string "
           << " path=" << path
           //<< " analysis=" << getAnalysisName(rivet, obj)
           << endl;
      continue;
    }
    
    ostringstream buf;
    writeHeader(buf, numEvents, crossSection, status);
    const bool ok = writeFinal(buf, obj);
    
    if (ok) {
      ofstream file(fname.c_str());
      file << buf.str();
      
      if (!file) {
        cout << "ERROR: failed to write output file " << fname << endl;
        return;
      }
    }
  }
}

#endif
