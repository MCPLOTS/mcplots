#!/bin/bash

# path to software packages:
EXTERNAL=/cvmfs/sft.cern.ch/lcg/releases/LCG_104d_ATLAS_10

# set platform name:
local osver="centos7"
local LCG_BASE=$(uname -m)-$osver
LCG_PLATFORM=$LCG_BASE-gcc11-opt

# set compiler
source $EXTERNAL/../gcc/11.2.0/$LCG_BASE/setup.sh

if [[ "$?" != "0" ]] ; then
  echo "ERROR: fail to set environment (gcc)"
  exit 1
fi

local MCGENERATORS=$EXTERNAL/MCGenerators
RIVET=$MCGENERATORS/rivet/3.1.10/$LCG_PLATFORM
YODA=$MCGENERATORS/yoda/1.9.10/$LCG_PLATFORM
HEPMC=$EXTERNAL/HepMC/2.06.11/$LCG_PLATFORM
local PYTHON=$EXTERNAL/Python/3.9.12/$LCG_PLATFORM
local FASTJET=$EXTERNAL/fastjet/3.4.1/$LCG_PLATFORM
local FJCONTRIB=$EXTERNAL/fjcontrib/1.052/$LCG_PLATFORM
local GSL=$EXTERNAL/GSL/2.7/$LCG_PLATFORM

local PYPKG=lib/python3.9/site-packages
export PYTHONPATH=$RIVET/$PYPKG:$YODA/$PYPKG:$PYTHONPATH
export LD_LIBRARY_PATH=$HEPMC/lib:$RIVET/lib:$YODA/lib:$PYTHON/lib:$FASTJET/lib:$FJCONTRIB/lib:$GSL/lib:$LD_LIBRARY_PATH
# the libffi is needed for "ctypes" module load by Rivet
export LD_LIBRARY_PATH=$EXTERNAL/libffi/3.4.2/$LCG_PLATFORM/lib64:$LD_LIBRARY_PATH
export PATH=$PYTHON/bin:$RIVET/bin:$PATH
# subdirectory for local user analyses
local MCPLOTS_ANALYSIS_PATH=$(pwd)/analyses
export RIVET_ANALYSIS_PATH=$RIVET/lib/Rivet:$MCPLOTS_ANALYSIS_PATH
export RIVET_DATA_PATH=$RIVET/share/Rivet:$MCPLOTS_ANALYSIS_PATH

