#!/bin/bash

# this function strip comments, empty lines and double spaces from input stream
weed () {
  sed -e '/^#.*/ d' -e '/^ *$/ d' -e 's,  *, ,g'
}

# generate a list of strings by inserting $args to $template
mul () {
  local template="$1"
  local args="$2"
  
  for i in $args ; do echo ${template/@/$i} ; done
}

# split jobs into multiple subjobs
multijob () {
  local nevtperjob=$1
  
  while read -a job; do
    local ntotal=${job[9]}
    local seed0=${job[10]}
    local nsingle=$nevtperjob
    local nsubjobs=$((ntotal/nsingle))
    
    if (( ntotal < nsingle )); then
      nsingle=$ntotal
      nsubjobs=1
    fi
    
    for isubjob in $(seq $nsubjobs); do
      # patch nevents and seed
      job[9]=$nsingle
      job[10]=$((seed0+isubjob-1))
      if [[ "$USEJOBIDSEED" == "1" ]] ; then job[10]="JOBID" ; fi
      
      echo "${job[@]}"
    done
  done
}

# generate list of all supported combinations of <$beam $process $energy $params>
list_conf () {
  cat configuration/rivet-histograms.map | weed | grep -v ' - -$' | cut -d ' ' -f 1-4 | sort | uniq
}

# list all supported combinations of <$generator $process>
list_gen_proc () {
  # - list all .params files in configuration/
  # - truncate suffix `~xxx` (card customization tag) from file name
  # - work-around for `-` char in the "powheg-box" string
  
  find configuration/ -name '*.params' | cut -d/ -f2- | \
  sed -e 's,\.params$,,' \
      -e 's,~.*$,,' \
      -e 's,powheg-box-,powhegbox-,' \
      -e 's,-, ,' \
      -e 's,powhegbox ,powheg-box ,' | \
  sort -u
}

# list tunes specified for production
# the first parameter could be in the form "name:tag", the ":tag" is stripped from output
list_gen_tunes () {
  local gen="$1"
  cat configuration/tunes-prod.map | weed | grep "^$gen " | sed -e 's,:[^ ]*,,'
}

# generate list of all runs
list_runs () {
  local mode="$1"
  local nevt="$2"
  local seed="$3"
  local filter="$4"
  
  local genproc=$(list_gen_proc)
  
  list_conf | while read conf ; do
    local vars=( $conf )
    local beam=${vars[0]}
    local process=${vars[1]}
    local energy=${vars[2]%%.*} # get integer part of beam energy
    
    # heavy-ion runs
    if [[ "$process" == "heavyion-mb" ]]; then
      # limit events number, epos/pythia8 PbPb performance is ~3s/event on lxplus7
      local maxevt="1000000000"
      if [[ "$beam" == "PbPb" ]] ; then
        maxevt="5000"
      fi
      
      #mul "$mode $conf - epos 1.99.crmc.1.5.4-hi @ $nevt $seed" "lhc" | multijob $maxevt
      mul "$mode $conf - epos 1.99.crmc.1.7.0-hi @ $nevt $seed" "lhc" | multijob $maxevt
      
      mul "$mode $conf - pythia8 @ default $nevt $seed" "8.230 8.235 8.240 8.243 8.244 8.301 8.302 8.303 8.304 8.305 8.306 8.307 8.308" | multijob $maxevt
      
      continue
    fi
    
    # powheg-box
    if [[ $(echo "$genproc" | grep -c "^powheg-box $process\$") != "0" ]] ; then
      list_gen_tunes powheg-box | while read g v tunelist ; do
        # "jets" process is not available in r3043
        if [[ "$v" == "r3043" && "$process" == "jets" ]] ; then continue ; fi
        
        mul "$mode $conf - $g $v @ $nevt $seed" "$tunelist"
      done
    fi
    
    # Pythia8:
    if [[ $(echo "$genproc" | grep -c "^pythia8 $process\$") != "0" ]] ; then
      local pyname="pythia8"
      if [[ "$beam" == "ee" ]] ; then pyname="pythia8:ee" ; fi
      
      list_gen_tunes $pyname | while read g v tunelist ; do
        # exclude top-mc runs with old versions (generate a lot of log output due to HepMC interface peculiarity)
        if [[ "$v" == "8.108" || "$v" == "8.130" || "$v" == "8.135" ]] ; then
          if [[ "$process" == "top-mc" ]] ; then continue ; fi
        fi
        
        mul "$mode $conf - $g $v @ $nevt $seed" "$tunelist"
      done
    fi
    
    # Vincia:
    if [[ $(echo "$genproc" | grep -c "^vincia $process\$") != "0" ]] ; then
      list_gen_tunes vincia | while read g v tunelist ; do
        mul "$mode $conf - $g $v @ $nevt $seed" "$tunelist"
      done
    fi
    
    # Pythia6:
    if [[ $(echo "$genproc" | grep -c "^pythia6 $process\$") != "0" ]] ; then
      list_gen_tunes pythia6 | while read g v tunelist ; do
        # skip some runs due to "Error: no requested process has non-vanishing cross-section"
        if [[ "$conf" == "pp winclusive 13000 -" || "$conf" == "pp winclusive 7000 -" || "$conf" == "pp zinclusive 13000 -" ]] ; then
          continue
        fi
        
        mul "$mode $conf - $g $v @ $nevt $seed" "$tunelist"
      done
    fi
    
    # Herwig++:
    
    # herwig++ and herwig7 (at least up to 7.1.4) stop with error "... cross-section for the selected sub-processes was zero" for ee <= 22 GeV
    local isEE22=0
    if [[ "$beam" == "ee" ]] && (( energy <= 22 )) ; then
      isEE22=1
    fi
    
    if [[ $(echo "$genproc" | grep -c "^herwig++ $process\$") != "0" && "$isEE22" != "1" ]] ; then
      list_gen_tunes herwig++ | while read g v tunelist ; do
        # some tunes are energy-limited
        if (( $energy >= 1000 )) ; then
          tunelist=${tunelist/LHC-MU900-2}
          tunelist=${tunelist/LHC-UE-EE-2-900}
          tunelist=${tunelist/LHC-UE-EE-3-900}
        fi
        if (( $energy >= 3000 )) ; then
          tunelist=${tunelist/LHC-UE-EE-3-1800}
          tunelist=${tunelist/LHC-UE-EE-3-2760}
        fi
        
        # 2.5.1 and 2.5.2 @ 13TeV fail to calc. total crossection and error exit
        # (GSL error: endpoints do not straddle)
        if [[ "$v" == "2.5.1" || "$v" == "2.5.2" ]] ; then
          if (( $energy >= 13000 )) ; then continue ; fi
        fi
        
        mul "$mode $conf - $g $v @ $nevt $seed" "$tunelist"
      done
    fi
    
    # herwig7 (at least up to 7.1.4) stop with error "MPIHandler: slope of soft pt spectrum couldn't be determined" for mb-inelastic process with beam energy <= 63 GeV
    local isMBINEL63=0
    if [[ "$process" == "mb-inelastic" ]] && (( energy <= 63 )) ; then
      isMBINEL63=1
    fi
    
    # Herwig7:
    # (with new soft model, can lift some/all restrictions on process class?)
    if [[ $(echo "$genproc" | grep -c "^herwig7 $process\$") != "0" && "$isEE22" != "1" && "$isMBINEL63" != "1" ]] ; then
      list_gen_tunes herwig7 | while read g v tunelist ; do
        # "mb-nsd" process is not available now for ver < 7.1
        if [[ "$v" < "7.1.0" && "$process" == "mb-nsd" ]] ; then continue ; fi
        
        # nlo tune is now available only in winclusive,zinclusive
        if [[ "$process" != "winclusive" && "$process" != "zinclusive" && "$process" != "z1j" && "$process" != "w1j" ]] ; then
          tunelist=${tunelist/nlo-pw-dipole/}
          tunelist=${tunelist/nlo-pw/}
          tunelist=${tunelist/nlo-dipole/}
          tunelist=${tunelist/nlo/}
        fi
        
        mul "$mode $conf - $g $v @ $nevt $seed" "$tunelist"
      done
    fi
    
    # Herwig++ with POWHEG (only for Drell-Yan)
    if [[ $(echo "$genproc" | grep -c "^herwig++powheg $process\$") != "0" ]] ; then
      list_gen_tunes herwig++powheg | while read g v tunelist ; do
        # some tunes are energy-limited
        if (( $energy >= 1000 )) ; then
          tunelist=${tunelist/LHC-MU900-2}
          tunelist=${tunelist/LHC-UE-EE-2-900}
          tunelist=${tunelist/LHC-UE-EE-3-900}
        fi
        if (( $energy >= 3000 )) ; then
          tunelist=${tunelist/LHC-UE-EE-3-1800}
          tunelist=${tunelist/LHC-UE-EE-3-2760}
        fi
        
        mul "$mode $conf - $g $v @ $nevt $seed" "$tunelist"
      done
    fi
    
    # Herwig7 with POWHEG (or other matching/merging/Matchbox) ?
    # ... to discuss with Herwig7 authors ... 

    # Sherpa
    if [[ $(echo "$genproc" | grep -c "^sherpa $process\$") != "0" ]] ; then
      list_gen_tunes sherpa | while read g v tunelist ; do
        # Sherpa 1.2.3 uemb-soft 53 and 63 GeV runs lead to
        # "Determination of <\tilde{O}> failed" critical error.
        # This is a know bug and fixed in next version.
        # Skip such runs.
        if [[ "$v" == "1.2.3" && "$process" == "uemb-soft" ]] && (( $energy < 100 )) ; then continue ; fi
        
        # there is a "fatal error: No vertex in z mode 0" bug which is
        # fixed in version >= 1.4.0
        if [[ "$v" == "1.3.0" || "$v" == "1.3.1" ]] ; then
          if [[ "$conf" == "pp b 7000 -" ]] ; then continue ; fi
        fi
        
        # 2.1.* hang for CM = 53 and 63 GeV
        if [[ "$v" == "2.1.0" || "$v" == "2.1.1" ]] ; then
          if (( $energy == 53 || $energy == 63 )) ; then continue ; fi
        fi
        
        mul "$mode $conf - $g $v @ $nevt $seed" "$tunelist"
      done
    fi
    
    # MadGraph5_aMC@NLO
    if [[ $(echo "$genproc" | grep -c "^madgraph5amc $process\$") != "0" ]] ; then
      list_gen_tunes madgraph5amc | while read g v tunelist ; do
        mul "$mode $conf - $g $v @ $nevt $seed" "$tunelist"
      done
    fi

    # Phojet:
    if [[ $(echo "$genproc" | grep -c "^phojet $process\$") != "0" ]] ; then
      list_gen_tunes phojet | while read g v tunelist ; do
        mul "$mode $conf - $g $v @ $nevt $seed" "$tunelist"
      done
    fi

    # Epos:
    if [[ $(echo "$genproc" | grep -c "^epos $process\$") != "0" ]] ; then
      list_gen_tunes epos | while read g v tunelist ; do
        # ver. v3400 - only pp is supported
        if [[ "$v" == "1.99.crmc.0.v3400" && "$beam" != "pp" ]] ; then continue ; fi
        
        mul "$mode $conf - $g $v @ $nevt $seed" "$tunelist"
      done
    fi

    # Whizard:
    if [[ $(echo "$genproc" | grep -c "^whizard $process\$") != "0" ]] ; then
      list_gen_tunes whizard | while read g v tunelist ; do
        mul "$mode $conf - $g $v @ $nevt $seed" "$tunelist"
      done
    fi
    
    # Alpgen:
    if [[ "$process" == "winclusive" ]]; then
      # Pythia
      for v in 2.1.3e_6.426 2.1.4_6.426 ; do
        mul "$mode $conf 0,1 alpgenpythia6 $v @ 50000 $seed" "350-CTEQ5L 351-CTEQ5L 352-CTEQ5L 356-CTEQ6L1 pro-q2o-CTEQ5L z1-CTEQ5L z2-CTEQ6L1 z2-lep-CTEQ6L1"
        mul "$mode $conf 1,1 alpgenpythia6 $v @ 10000 $seed" "350-CTEQ5L 351-CTEQ5L 352-CTEQ5L 356-CTEQ6L1 pro-q2o-CTEQ5L z1-CTEQ5L z2-CTEQ6L1 z2-lep-CTEQ6L1"
        mul "$mode $conf 2,1 alpgenpythia6 $v @ 5000 $seed" "350-CTEQ5L 351-CTEQ5L 352-CTEQ5L 356-CTEQ6L1 pro-q2o-CTEQ5L z1-CTEQ5L z2-CTEQ6L1 z2-lep-CTEQ6L1"
        mul "$mode $conf 3,0 alpgenpythia6 $v @ 1000 $seed" "350-CTEQ5L 351-CTEQ5L 352-CTEQ5L 356-CTEQ6L1 pro-q2o-CTEQ5L z1-CTEQ5L z2-CTEQ6L1 z2-lep-CTEQ6L1"
      done
      
      # fortran Herwig + Jimmy
      for v in 2.1.3e_6.520 2.1.4_6.520 ; do
        echo "$mode $conf 0,1 alpgenherwigjimmy $v default-CTEQ6L1 50000 $seed"
        echo "$mode $conf 1,1 alpgenherwigjimmy $v default-CTEQ6L1 10000 $seed"
        echo "$mode $conf 2,1 alpgenherwigjimmy $v default-CTEQ6L1 5000 $seed"
        echo "$mode $conf 3,0 alpgenherwigjimmy $v default-CTEQ6L1 1000 $seed"
      done
    fi

    # skip alpgen jets runs if ptMin = 0 (as 'mb-inelastic' process is not implemented for alpgen)
    local ptMin=$(echo $conf | cut -d ' ' -f 4 | cut -d , -f 1)
    
    if [[ "$process" == "jets" && "$ptMin" != "-" ]]; then
      # Pythia
      for v in 2.1.3e_6.426 2.1.4_6.426 ; do
        mul "$mode $conf 2,1 alpgenpythia6 $v @ 20000 $seed" "350-CTEQ5L 351-CTEQ5L 352-CTEQ5L 356-CTEQ6L1 pro-q2o-CTEQ5L z1-CTEQ5L z2-CTEQ6L1 z2-lep-CTEQ6L1"
        mul "$mode $conf 3,1 alpgenpythia6 $v @ 5000 $seed" "350-CTEQ5L 351-CTEQ5L 352-CTEQ5L 356-CTEQ6L1 pro-q2o-CTEQ5L z1-CTEQ5L z2-CTEQ6L1 z2-lep-CTEQ6L1"
        mul "$mode $conf 4,0 alpgenpythia6 $v @ 1000 $seed" "350-CTEQ5L 351-CTEQ5L 352-CTEQ5L 356-CTEQ6L1 pro-q2o-CTEQ5L z1-CTEQ5L z2-CTEQ6L1 z2-lep-CTEQ6L1"
      done
      
      # fortran Herwig + Jimmy
      for v in 2.1.3e_6.520 2.1.4_6.520 ; do
        echo "$mode $conf 2,1 alpgenherwigjimmy $v default-CTEQ6L1 20000 $seed"
        echo "$mode $conf 3,1 alpgenherwigjimmy $v default-CTEQ6L1 5000 $seed"
        echo "$mode $conf 4,0 alpgenherwigjimmy $v default-CTEQ6L1 1000 $seed"
      done
    fi

  done | grep -E "$filter"
}


# === main ===

# this script is intended to run all supported generators
# to produce all supported tunes/observables/cuts

# print usage info
if [[ "$#" < "2" || "$#" > "4" ]] ; then
  echo "runAll.sh: tool for MC production"
  echo "Usage:"
  echo "  ./runAll.sh [mode] [nevt] {filter} {duration}"
  echo "      [mode]   - local, lxbatch, lxbatch-resub, lxbatch-continue, list, summary"
  echo "      [nevt]   - number of events per run, can be specified with 'k' or 'M' suffix"
  echo "      {filter} - filter string, optional, default is no filtering"
  echo "      {duration} - batch job max runtime duration (seconds), optional, default is 5000"
  echo ""
  echo "    Environment variables:"
  echo "      ANALYSIS MKHTML HTMLDIR PRINTLIST - see the runRivet.sh for the meaning"
  echo "      USEJOBIDSEED=1   - to use JOBID as seed for splitted jobs"
  
  exit 1
fi

mode="$1"         # run mode
nevt="$2"         # number of events per run
filter="$3"
duration=${4-"5000"}  # batch jobs runtime max duration

# take into account 'k' or 'M' suffix
nevt=${nevt/%[kK]/000}
nevt=${nevt/%[mM]/000000}

# generate initial seed for random number generator
seed=$((100000 + RANDOM))

case "$mode" in
  "local" )
    log="mcprod.log"  # log file
    rm -f $log
    
    echo "Running histograms generation ..."
    echo "Total runs = $(list_runs | wc -l)"
    echo "Mode = $mode"
    echo "Events per run = $nevt"
    echo "Seed = $seed"
    echo "Seed type = $seedtype"
    echo "Log = $log"
    echo "Output = dat/"
    echo "USEJOBIDSEED=$USEJOBIDSEED"
    
    list_runs $mode $nevt $seed "$filter" | while read line ; do
      echo "$(date) => $line"
      # make run and save logs
      ./runRivet.sh $line >> $log 2>&1
      
      # check run success:
      if [[ "$?" != "0" ]] ; then
        echo "WARNING: fail to run [$line]"
      fi
    done
    
    echo "All runs finished"
    echo "Total number of generated histograms: $(find dat/ -type f | wc -l)"
    ;;
  
  "lxbatch" )
    # --- master mode ---
    # (this part is going on local machine)
    
    # check the submission directory situated on AFS to avoid job failures
    if ! fs whereis >& /dev/null ; then
      echo "ERROR: current directory is situated on local disk drive"
      echo "       which is not accessible from LXBATCH nodes."
      echo "       Move files to any AFS directory (for example"
      echo "       home directory on lxplus) and try again."
      exit 1
    fi
    
    # get code revision
    #codeRevision=$(git log -1 --date=iso --format=format:'%H %ad')
    codeRevision=$( (git log -1 ; git status .) | nl -ba )
    
    # prepare job submission directory
    batchname="batch-$(date +%Y%m%d-%H%M%S)"
    batchdir="$(pwd)/$batchname"
    echo "batchdir=$batchdir"
    
    jobfiles="configuration/ pythia8/ alpgen/ phojet/ rivetvm/ analyses/ heavyion/ runRivet.sh rungen.sh"
    
    # TODO: implement local calibration for heavy-ion
    if [[ "$HICAL" != "" ]] ; then
      echo "ERROR: HICAL=$HICAL - heavy-ion local calibration is not implemented yet (unset variable HICAL)"
      exit 1
    fi
    
    # package local calibration data (needed for HI runs)
    #if [[ "$HICAL" == "0" ]] ; then
    #  jobfiles="$jobfiles calibration/"
    #fi
    
    mkdir $batchdir
    cp runAll.sh $batchdir/
    tar -z -c --exclude-vcs --exclude='*.exe' --exclude='*.o' -f $batchdir/mcprod.tgz $jobfiles
    
    if [[ "$?" != "0" ]] ; then
      echo "ERROR: fail to copy job files to batch directory"
      echo "       probably, job files do not situated in current directory"
      echo "         job files   = $jobfiles"
      echo "         current dir = $(pwd)"
      echo "         batch dir   = $batchdir"
      exit 1
    fi
    
    echo "Preparing the tasks list ..."
    list_runs $mode $nevt $seed "$filter" > $batchdir/tasks.txt
    
    # enter to the batch directory
    cd $batchdir
    
    # submit jobs:
    { echo "Submitting jobs to LXBATCH ..."
      echo "Batch directory = $(pwd)"
      echo "MaxRuntime = $duration"
      echo "Events per run = $nevt"
      echo "Seed = $seed"
      echo "Filter = $filter"
      echo "USEJOBIDSEED=$USEJOBIDSEED"
      echo "Code revision:"
      echo "$codeRevision"
    } > info.txt
    
    # documentation: https://batchdocs.web.cern.ch/
    
    # split jobs list to subsets to respect the limits:
    #   AFS dir size limit = max 64k files per directory
    #   htcondor: jobs cluster max size at least 20k
    #   htcondor: total jobs limit ~90k+, "Number of submitted jobs would exceed MAX_JOBS_PER_OWNER"
    setsize=10000
    
    # prepare sub-sets
    split -l$setsize -d tasks.txt tasks-
    
    for j in tasks-* ; do
      setid=${j#*-}
      setdir=set-$setid
      mkdir $setdir
      cd $setdir
      mv ../$j tasks.txt
      cp ../runAll.sh .
      ln -s ../mcprod.tgz
      
      njobs=$(wc -l tasks.txt | awk '{print $1}')
      echo "INFO: $setdir, njobs=$njobs"
      
      # generate list of job tasks
      i="0"
      mkdir tasks/ results/
      
      cat tasks.txt | while read line ; do
        echo $line > tasks/$i
        let "i++"
      done
      
      jobcmd="$(pwd)/$0"
      
      {
      echo "JobBatchName = $batchname"
      echo "+MaxRuntime = $duration"
      echo "executable  = $jobcmd"
      echo "arguments   = slave" $(pwd) '$(ClusterId)' '$(ProcId)'
      # propagate env. variables
      echo 'getenv      = ANALYSIS MKHTML HTMLDIR PRINTLIST USECORES'
      echo 'output      = task-$(ProcId).out'
      echo 'error       = task-$(ProcId).out'
      echo 'log         = condor.log'
      echo 'transfer_output_files = ""'
      #echo "environment  = HIMODE=$HIMODE"
      echo "queue $njobs"
      echo ""
      } > condor.sub
      
      condor_submit condor.sub
      
      if [[ "$?" != "0" ]] ; then
        echo "WARNING: submit failed, skip for the next set"
        # no stop here to continue the preparation of sub-sets set-NNN/
        #break
      fi
      
      cd $batchdir
    done
    
    setntot=$(ls -d1 set-* | wc -l)
    setnsub=$(ls -d1 set-*/condor.log | wc -l)
    
    echo "Jobs submitted: $setnsub of $setntot sets"
    echo ""
    echo "Jobs progress monitoring:"
    echo "   condor_q"
    echo "   condor_q -nobatch"
    echo "   condor_tail -f <job-id>"
    echo "   condor_ssh_to_job <job-id>"
    echo "Jobs termination:"
    echo "   condor_rm"
    
    ;;
  
  "lxbatch-continue" )
    # master mode, work-around for htcondor max jobs limit to complete the submission of remaining jobs
    
    bdir=$2
    cd $bdir
    
    for i in set-* ; do
      echo "INFO: setdir=$i"
      
      if [[ -e $i/condor.log ]] ; then continue ; fi
      
      cd $i
      condor_submit condor.sub
      
      if [[ "$?" != "0" ]] ; then
        echo "ERROR: submit failed, stop"
        break
      fi
      
      cd -
    done
    
    ;;
  
  "lxbatch-analyse" )
    bdir=$2
    cd $bdir
    echo "batch=$bdir"
    
    ntask=$(cat tasks.txt | wc -l)
    echo "total tasks = $ntask"
    
    find set-*/condor.log | sed -e 's,condor.log,tasks.txt,' | xargs cat > list-submit.txt
    nsub=$(cat list-submit.txt | wc -l)
    echo "total submit = $nsub"
    
    find . -name "*.out" > list-completed-out.txt
    ntot=$(cat list-completed-out.txt | wc -l)
    echo "total completed = $ntot"
    
    find . -name "*.out" -size +1000c | xargs grep ^jobspec > list-fail-out.txt
    cat list-fail-out.txt | cut -d= -f2- > list-fail.txt
    echo "total failed = $(wc -l list-fail.txt)"
    
    cat list-fail.txt | cut -d' ' -f2-5 | sort | uniq -c | sort -n > list-fail-summary1.txt
    echo "summary file = $(wc -l list-fail-summary1.txt)"
    
    cat list-fail.txt | cut -d' ' -f7-9 | sort | uniq -c | sort -n > list-fail-summary2.txt
    echo "summary file = $(wc -l list-fail-summary2.txt)"
    
    ;;
  
  "lxbatch-resub" )
    # --- master mode ---
    # (this part is going on local machine)
    
    echo "ERROR: resubmit is not yet implemented for HTCondor"
    exit 1
    
    # check the script run from batch-* directory
    if ! [[ -d tasks/ && -d results/ && -d configuration/ && -f mcprod.tgz && -f runAll.sh ]] ; then
      echo "ERROR: script should be run from batch- directory"
      exit 1
    fi
    
    # submit jobs:
    { echo "Submitting jobs to LXBATCH ..."
      echo "Batch directory = $(pwd)"
      echo "MaxRuntime = $duration"
      #echo "Events per run = $nevt"
    } | tee info-resub-$(date +%F-%H.%M.%S).txt
    
    jobcmd="$(pwd)/$0"
    
    find tasks -type f | while read i ; do
      line=$(cat $i)
      echo -n "$i [$line] "
      
      # submit job
      # require to have at least 1000 Mb RAM on execution host (option -R)
      bsub -q $duration -R "rusage[mem=1000]" -r -J "$line" $jobcmd slave $i
    done
    
    echo "All jobs resubmitted"
    ;;
  
  "slave" )
    # --- slave mode ---
    # (this part is going on "lxbatch" server)
    src=$2
    clusterid=$3
    procid=$4
    
    jobid=$clusterid.$procid
    jobidnum=${jobid/.}
    taskf=$src/tasks/$procid
    jobspec="$(cat $taskf)"
    dst=$src/results/$jobid.tgz
    
    echo "src=$src"
    echo "jobid=$jobid"
    echo "jobidnum=$jobidnum"
    echo "taskf=$taskf"
    echo "jobspec=$jobspec"
    echo "dst=$dst"
    
    # patch seed JOBID string with current job ID
    jobspec=${jobspec/" JOBID"/" $jobidnum"}
    echo "(patch JOBID) jobspec=$jobspec"
    
    # copying job files
    echo "$(date): Prepare job files..."
    tar zxf $src/mcprod.tgz
    
    # run generator and rivet
    echo "$(date): Running MC production..."
    ./runRivet.sh $jobspec &> runRivet.log
    
    if [[ "$?" != "0" ]] ; then
      echo "ERROR: run fail"
      echo "       log-file runRivet.log:"
      echo "=================================================================="
      cat runRivet.log
      echo "=================================================================="
      exit 1
    fi
    
    # truncate log file to 1 Mb:
    logsize=$(du -k runRivet.log | cut -f 1)
    if (( $logsize > 1000 )) ; then
      # take head and tail part, drop middle
      ( head -c 500k runRivet.log ; tail -c 500k runRivet.log ) > runRivet.log.new
      mv runRivet.log.new runRivet.log
    fi
    
    # and copy results back
    echo "$(date): Copying results to $dst"
    tar zcf $dst dat/ runRivet.log $HTMLDIR
    if [[ "$?" != "0" ]] ; then
      echo "ERROR: fail to copy results"
      echo "         src = $(pwd)/"
      echo "         dst = $dst"
      exit 1
    fi
    
    # remove task file to indicate success
    rm $taskf
    
    echo "$(date): Job finished successfully"
    ;;
  
  "list" )
    list_runs $mode $nevt $seed "$filter"
    ;;
  
  "summary" )
    echo "Runs list and totals calculation..."
    tmpd=$(mktemp -d)
    cat configuration/rivet-histograms.map | weed | grep -v ' - -$' | cut -d ' ' -f 1-4 | sort | uniq -c > $tmpd/list-conf-nhist.txt
    list_runs "0" 0 0 "" | cut -d' ' -f 2-9 | sort > $tmpd/list-runs-all.txt
    cd $tmpd
    cat list-runs-all.txt | cut -d' ' -f 6-8 | sort -u > list-gvt.txt
    cat list-runs-all.txt | cut -d' ' -f 1-4,6-8 | sort -u | cut -d' ' -f 1-4 | sort | uniq -c > list-conf-ngvt.txt
    nhist=$(paste list-conf-ngvt.txt list-conf-nhist.txt | \
      awk 'BEGIN {s=0;} {v1=$1; k1=$2$3$4$5; v2=$6; k2=$7$8$9$10; if (k1!=k2) print "ERROR"; s+=v1*v2;} END {print s}' )
    
    # TODO: integrity checks
    
    echo "Summary:"
    echo "Runs = $(wc -l < list-runs-all.txt)"
    echo "Configurations (beam-energy-process-cuts) = $(wc -l < list-conf-ngvt.txt)"
    echo "Tunes (generator-version-tune) = $(wc -l < list-gvt.txt)"
    echo "Histograms = $nhist"
    
    rm -rf "$tmpd"
    
    ;;
  
  * )
    echo "ERROR: unknown mode: $mode"
    exit 1
    ;;
esac

