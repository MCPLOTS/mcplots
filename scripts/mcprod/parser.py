# parameters to adjust for each .plot file
rivetAnalysisName = 'CMS_2022_I2079374'
genParameters = 'pp    zinclusive    13000    '
mcplotsAnalysisRef = '    cms2022-dymass'

# other parameters to adjust
# to parse a part of the .plot file choose the start and end lines
startLine = 0
endLine = -1 # number of the last *END PLOT* line to parse (-1 to parse all the file)

# if there are global XLabel and/or gen cut put them here
xGlobal = ''
cutGlobal = '-'

# if various pT cuts are necessary the following parameters should be adjusted, otherwise leave as is
needPtCut = False   # True or False
cutPos = 7          # How to find the pT cut in the Title in case it is separate by spaces
                    # The code should be changed in other cases 
dictCuts = {'50': 25, '65': 35, '88':45, '120':75, '150':90, '186':110, '254':160, '326': 220,
            '408': 260, '481': 260, '614': 320, '800': 430, '1000': 580}

# reg exp symbols
re = '[]*.'

# the routine                                
inFile = open(rivetAnalysisName + '.plot', 'r', encoding='utf8')
outFile = open(rivetAnalysisName + '-1.txt', 'w', encoding='utf8')
paramSet = {'Title', 'XLabel', 'YLabel'}  # necessary parameters to read from the .plot file
dictX = {}  # mcplots variables

nPlots = 0
nLine = 0
if xGlobal != '':
    dictX[xGlobal] = xGlobal

for i in range (1, max(startLine+1,2)):
    line = inFile.readline()
    nLine += 1

while line:
    if len(line) == 1:
        line = inFile.readline()
        nLine += 1
        continue

    splittedLine = line.split()
    
    # a new plot is found : processing
    if 'BEGIN' in splittedLine[-3] and splittedLine[-2] == 'PLOT':
        histRef = splittedLine[-1][1:].replace('/', '_')
        histNum = splittedLine[-1].split('/')[2]
        if any(c in re for c in histNum):
            print('Regular expressions are not supported: check the .plot file')
            break
        if '-' not in histNum:
            histRef = histRef + '-x01-y01'
        nPlots += 1
        info = ''
        xlabel = xGlobal
        cut = cutGlobal
        line = inFile.readline()
        nLine += 1
        splittedLine = line.split()
        
        # read the parameters until the end of the current plot
        while not (len(splittedLine) >= 2 and 'END' in splittedLine[-2] and splittedLine[-1] == 'PLOT'):
            param = line.split('=')[0]
            if param in paramSet:
                info += line
            if param == 'XLabel':
                xlabel = line.split('=')[1][:-1]
            elif param == 'Title' and needPtCut:
                cut = dictCuts[splittedLine[cutPos]]
            line = inFile.readline()
            nLine += 1
            splittedLine = line.split()

        info = '\n' + info[:-1] #cosmetics

        # name a mcplot variable of this plot
        if xlabel == '':
            print(info)
            var = input('Enter the variable: ')
        elif xlabel not in dictX :
            print(info)
            dictX[xlabel] = input('Enter the variable: ')
            var = dictX[xlabel]
        else:
            var = dictX[xlabel]

        # the plots is parsed, print its mcplots description
        print(genParameters, cut, '    ', histRef, '    ', var, mcplotsAnalysisRef, file=outFile)

    if nPlots == 0:
        print('Incorrect file or startLine parameter')
        break

    if nLine == endLine:
        print('Line number', endLine, ': stop here.')
        break

    line = inFile.readline()
    nLine += 1

inFile.close()
outFile.close()
print('\n', nPlots, 'plot(s) parsed.\n')

