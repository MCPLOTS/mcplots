#!/bin/bash -e
anref=CMS_2019_I1719955
# output dir name
dname=cms2019-jsep
beam=pp
prc=jets
ecm=13000

#initial cut in rivet-histograms
c0=100 
c1=$c0
echo

echo "CUTLOOP: initial cut = $c1"
ANALYSIS=$anref MKHTML=1 HTMLDIR=$dname-$c1-10K PRINTLIST=1 \
    ./runRivet.sh local $beam $prc $ecm $c1 - pythia8 8.244 default 10000 1

# loop
for c2 in 95,100 ; do
    echo
    echo "CUTLOOP: cut = $c2"
  
    sed "/ $c1 *$anref/ s;\($beam *$prc *$ecm *\)$c1\(.*\);\1$c2\2;" \
      -i configuration/rivet-histograms.map
  
    # print actual change for verification
    # git diff -U0 configuration/rivet-histograms.map

    c2name=$(echo $c2 | sed "s/,/-/")

    # do the run
    ANALYSIS=$anref MKHTML=1 HTMLDIR=$dname-$c2name-10K PRINTLIST=1 \
    ./runRivet.sh local $beam $prc $ecm $c2 - pythia8 8.244 default 10000 1
    c1=$c2
    echo
  
done

# back to the initial cut
sed "/ $c1 *$anref/ s;\($beam *$prc *$ecm *\)$c1\(.*\);\1$c0\2;" -i configuration/rivet-histograms.map
