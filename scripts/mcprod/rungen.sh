#!/bin/bash

# setup environment to run generators from MCGenerators_hepmc2.06.05 tree
set_environment_hepmc20605 () {
  local mode=$1
  local generator=$2
  local version=$3

  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/external

  # set SLC5 platform name:
  local LCG_BASE=$(uname -m)-slc5
  LCG_PLATFORM=$LCG_BASE-gcc43-opt
  
  # EL9 compatibility
  if grep -q "release 9" /etc/redhat-release ; then
    source $EXTERNAL/gcc/4.8.4/x86_64-centos7/setup.sh $EXTERNAL
  else
    source $EXTERNAL/gcc/4.3.6/$LCG_BASE/setup.sh $EXTERNAL
  fi
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to set environment (gcc)"
    exit 1
  fi
  
  # work-around for `uselocale was not defined` error due to build and runtime environment mismatch of LCG gcc
  # reference: https://gcc.gnu.org/ml/gcc-help/2011-01/msg00313.html
  
  local gpporig=$(which g++)
  local gppdir=$(mktemp -d)
  {
    echo '#!/bin/bash'
    echo $gpporig ' -D__USE_XOPEN2K8 $@'
  } > $gppdir/g++
  chmod +x $gppdir/g++
  export PATH=$gppdir:$PATH
  
  #ls -l $gppdir
  #cat $gppdir/g++
  
  MCGENERATORS=$EXTERNAL/MCGenerators_hepmc2.06.05
  HEPMC=$EXTERNAL/HepMC/2.06.05/$LCG_PLATFORM
  local AGILE=$MCGENERATORS/agile/1.4.0/$LCG_PLATFORM
  local PYTHON=$EXTERNAL/Python/2.6.5/$LCG_PLATFORM
  local FASTJET=$EXTERNAL/fastjet/2.4.4/$LCG_PLATFORM
  local GSL=$EXTERNAL/GSL/1.10/$LCG_PLATFORM
  
  export PYTHONPATH=$AGILE/lib/python2.6/site-packages:$PYTHONPATH
  export LD_LIBRARY_PATH=$HEPMC/lib:$AGILE/lib:$PYTHON/lib:$FASTJET/lib:$GSL/lib:$LD_LIBRARY_PATH
  export PATH=$PYTHON/bin:$AGILE/bin:$PATH
  export AGILE_GEN_PATH=$MCGENERATORS
  
  # explicitly specify path to latest LHAPDF to be able to do
  # LHAPDF-dependent runs of Pythia 6 and Sherpa on BOINC
  # (if the variable do not exported the LHAPDF package
  #  picks up the path specified inside libLHAPDF, which
  #  points to /afs location and lead to fail in BOINC environment)
  LHAPDF=$MCGENERATORS/lhapdf/5.8.9/$LCG_PLATFORM
  export LHAPATH=$LHAPDF/../share/PDFsets
  export LD_LIBRARY_PATH=$LHAPDF/lib:$LD_LIBRARY_PATH
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "g++ = $(which g++)"
  echo "g++ version = $(g++ -dumpversion)"
  echo "g++ orig = $gpporig"
  echo "AGILE=$AGILE"
  echo "HEPMC=$HEPMC"
  echo "AGILE_GEN_PATH=$AGILE_GEN_PATH"
  echo "LHAPDF=$LHAPDF"
  #echo "PATH=$PATH"
  #echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
  echo ""
  
  # check paths to essential parts of machinery:
  for f in $MCGENERATORS $AGILE $HEPMC $PYTHON $LHAPDF $AGILE/bin/agile-runmc ; do
    if [[ ! -e "$f" ]] ; then
      echo "ERROR: fail to set environment, no access to $f"
      exit 1
    fi
  done
}

# setup environment to run generators from MCGenerators_lcgcmt61c tree
set_environment_lcgcmt61c () {
  local mode=$1
  local generator=$2
  local version=$3
  
  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/external
  
  # set SLC5 platform name:
  LCG_PLATFORM=$(uname -m)-slc5-gcc43-opt
  
  # use gcc 4.3.2 on SLC5 and system default gcc 4.4 on SLC6 / gcc 4.8 on CC7
  if grep -q "release 5" /etc/redhat-release ; then
    source $EXTERNAL/gcc/4.3.2/$LCG_PLATFORM/setup.sh $EXTERNAL
  fi
  
  # system gcc on EL9 is c++11 by default, avoid to compile old packages
  if grep -q "release 9" /etc/redhat-release ; then
    source $EXTERNAL/gcc/4.8.4/x86_64-centos7/setup.sh $EXTERNAL
  fi
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to set environment (gcc)"
    exit 1
  fi
  
  MCGENERATORS=$EXTERNAL/MCGenerators_lcgcmt61c
  HEPMC=$EXTERNAL/HepMC/2.06.05/$LCG_PLATFORM
  local AGILE=$MCGENERATORS/agile/1.4.0/$LCG_PLATFORM
  local PYTHON=$EXTERNAL/Python/2.6.5/$LCG_PLATFORM
  local FASTJET=$EXTERNAL/fastjet/2.4.3/$LCG_PLATFORM
  local GSL=$EXTERNAL/GSL/1.10/$LCG_PLATFORM
  local SQLITE=$EXTERNAL/sqlite/3.6.22/$LCG_PLATFORM
  
  export PYTHONPATH=$AGILE/lib/python2.6/site-packages:$PYTHONPATH
  export LD_LIBRARY_PATH=$HEPMC/lib:$AGILE/lib:$PYTHON/lib:$FASTJET/lib:$GSL/lib:$SQLITE/lib:$LD_LIBRARY_PATH
  export PATH=$PYTHON/bin:$AGILE/bin:$PATH
  export AGILE_GEN_PATH=$MCGENERATORS
  
  # explicitly specify LHAPDF to override the (/afs) path specified inside libLHAPDF library
  LHAPDF=$MCGENERATORS/lhapdf/5.8.9/$LCG_PLATFORM
  export LHAPATH=$LHAPDF/../share/PDFsets
  export LD_LIBRARY_PATH=$LHAPDF/lib:$LD_LIBRARY_PATH
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "gcc = $(which gcc)"
  echo "gcc version = $(gcc -dumpversion)"
  echo "AGILE=$AGILE"
  echo "HEPMC=$HEPMC"
  echo "AGILE_GEN_PATH=$AGILE_GEN_PATH"
  echo "LHAPDF=$LHAPDF"
  echo ""
  
  # check paths to essential parts of machinery:
  for f in $MCGENERATORS $AGILE $HEPMC $PYTHON $LHAPDF $AGILE/bin/agile-runmc ; do
    if [[ ! -e "$f" ]] ; then
      echo "ERROR: fail to set environment, no access to $f"
      exit 1
    fi
  done
}

# setup environment to run generators from MCGenerators_lcgcmt65 tree
set_environment_lcgcmt65 () {
  local mode=$1
  
  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/external
  
  # get OS version
  local osver="slc6"
  if grep -q "release 5" /etc/redhat-release ; then
    osver="slc5"
  fi
  
  # set platform name:
  LCG_PLATFORM=$(uname -m)-$osver-gcc47-opt
  
  # set compiler
  source $EXTERNAL/gcc/4.7.2/$LCG_PLATFORM/setup.sh $EXTERNAL
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to set environment (gcc)"
    exit 1
  fi
  
  MCGENERATORS=$EXTERNAL/MCGenerators_lcgcmt65
  HEPMC=$EXTERNAL/HepMC/2.06.08/$LCG_PLATFORM
  local AGILE=$MCGENERATORS/agile/1.4.0/$LCG_PLATFORM
  local PYTHON=$EXTERNAL/Python/2.7.3/$LCG_PLATFORM
  local FASTJET=$EXTERNAL/fastjet/3.0.3/$LCG_PLATFORM
  local GSL=$EXTERNAL/GSL/1.10/$LCG_PLATFORM
  
  export PYTHONPATH=$AGILE/lib/python2.7/site-packages:$PYTHONPATH
  export LD_LIBRARY_PATH=$HEPMC/lib:$AGILE/lib:$PYTHON/lib:$FASTJET/lib:$GSL/lib:$LD_LIBRARY_PATH
  export PATH=$PYTHON/bin:$AGILE/bin:$PATH
  export AGILE_GEN_PATH=$MCGENERATORS
  
  # explicitly specify LHAPDF to override the (/afs) path specified inside libLHAPDF library
  LHAPDF=$MCGENERATORS/lhapdf/5.9.1/$LCG_PLATFORM
  export LHAPATH=$LHAPDF/../share/PDFsets
  export LD_LIBRARY_PATH=$LHAPDF/lib:$LD_LIBRARY_PATH
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "gcc = $(which gcc)"
  echo "gcc version = $(gcc -dumpversion)"
  echo "AGILE=$AGILE"
  echo "HEPMC=$HEPMC"
  echo "AGILE_GEN_PATH=$AGILE_GEN_PATH"
  echo "LHAPDF=$LHAPDF"
  echo ""
  
  # check paths to essential parts of machinery:
  for f in $MCGENERATORS $AGILE $HEPMC $PYTHON $LHAPDF $AGILE/bin/agile-runmc ; do
    if [[ ! -e "$f" ]] ; then
      echo "ERROR: fail to set environment, no access to $f"
      exit 1
    fi
  done
}

# setup environment to run generators from MCGenerators_lcgcmt67c tree
set_environment_lcgcmt67c () {
  local mode=$1
  
  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/external
  
  # set platform name:
  LCG_PLATFORM=$(uname -m)-slc6-gcc47-opt
  
  # set compiler
  source $EXTERNAL/gcc/4.7.2/$LCG_PLATFORM/setup.sh $EXTERNAL
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to set environment (gcc)"
    exit 1
  fi
  
  MCGENERATORS=$EXTERNAL/MCGenerators_lcgcmt67c
  HEPMC=$EXTERNAL/HepMC/2.06.08/$LCG_PLATFORM
  local AGILE=$MCGENERATORS/agile/1.4.0/$LCG_PLATFORM
  local PYTHON=$EXTERNAL/Python/2.7.3/$LCG_PLATFORM
  local FASTJET=$EXTERNAL/fastjet/3.0.3/$LCG_PLATFORM
  local GSL=$EXTERNAL/GSL/1.10/$LCG_PLATFORM
  local BOOST=$EXTERNAL/Boost/1.53.0_python2.7/$LCG_PLATFORM
  local ROOT=$EXTERNAL/../app/releases/ROOT/5.34.19/$LCG_PLATFORM/root
  
  export PYTHONPATH=$AGILE/lib/python2.7/site-packages:$PYTHONPATH
  export LD_LIBRARY_PATH=$HEPMC/lib:$AGILE/lib:$PYTHON/lib:$FASTJET/lib:$GSL/lib:$BOOST/lib:$ROOT/lib:$LD_LIBRARY_PATH
  export PATH=$PYTHON/bin:$AGILE/bin:$PATH
  export AGILE_GEN_PATH=$MCGENERATORS
  
  # explicitly specify LHAPDF to override the (/afs) path specified inside libLHAPDF library
  LHAPDF=$MCGENERATORS/lhapdf/5.9.1/$LCG_PLATFORM
  export LHAPATH=$LHAPDF/../share/PDFsets
  export LD_LIBRARY_PATH=$LHAPDF/lib:$LD_LIBRARY_PATH
  
  # use LHAPDF6 for herwig++, Herwig7
  if [[ "$generator $version" == "herwig++ 2.7.1" || "$generator $version" == "herwig++powheg 2.7.1" ||
        "$generator" == "herwig7" ]] ; then
    LHAPDF=$MCGENERATORS/lhapdf/6.1.4/$LCG_PLATFORM
    export LD_LIBRARY_PATH=$LHAPDF/lib:$LD_LIBRARY_PATH
    local PDFDATA=$EXTERNAL/lhapdfsets/current
    export LHAPDF_DATA_PATH=$PDFDATA:$LHAPDF/share/LHAPDF
  fi
  
  # the rpath is removed from herwig++ 2.7.1 installation and
  # it doesn't know where to find ThePEG dependency
  # set path to ThePEG explicitly
  if [[ "$generator $version" == "herwig++ 2.7.1" || "$generator $version" == "herwig++powheg 2.7.1" ]] ; then
    local THEPEG=$MCGENERATORS/thepeg/1.9.2p1/$LCG_PLATFORM
    export LD_LIBRARY_PATH=$THEPEG/lib/ThePEG:$LD_LIBRARY_PATH
  fi
  
  # Herwig7: Set ThePEG and other shared-library dependency paths
  if [[ "$generator" == "herwig7" ]] ; then
    if [[ "$version" == "7.0.0" ]]; then
      local THEPEG=$MCGENERATORS/thepeg/2.0.0/$LCG_PLATFORM
    elif [[ "$version" == "7.0.1" ]]; then
      local THEPEG=$MCGENERATORS/thepeg/2.0.1/$LCG_PLATFORM
    fi
    export LD_LIBRARY_PATH=$THEPEG/lib/ThePEG:$LD_LIBRARY_PATH
    local HW7=$MCGENERATORS/herwig++/$version/$LCG_PLATFORM
    export LD_LIBRARY_PATH=$HW7/lib/Herwig:$LD_LIBRARY_PATH
  fi
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "gcc = $(which gcc)"
  echo "gcc version = $(gcc -dumpversion)"
  echo "AGILE=$AGILE"
  echo "HEPMC=$HEPMC"
  echo "AGILE_GEN_PATH=$AGILE_GEN_PATH"
  echo "LHAPDF=$LHAPDF"
  echo "LHAPDF_DATA_PATH=$LHAPDF_DATA_PATH"
  echo ""
  
  # check paths to essential parts of machinery:
  for f in $MCGENERATORS $AGILE $HEPMC $PYTHON $LHAPDF $AGILE/bin/agile-runmc ; do
    if [[ ! -e "$f" ]] ; then
      echo "ERROR: fail to set environment, no access to $f"
      exit 1
    fi
  done
}

# setup environment to run generators from releases/LCG_87 tree
set_environment_LCG_87 () {
  local mode=$1
  local generator=$2
  local version=$3
  
  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/releases/LCG_87
  
  # set gcc version number and platform name:
  local LCG_BASE=$(uname -m)-slc6
  LCG_PLATFORM=$LCG_BASE-gcc49-opt
  
  # set compiler et
  source $EXTERNAL/../gcc/4.9.3/$LCG_BASE/setup.sh $EXTERNAL/..
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to set environment (gcc)"
    exit 1
  fi
  
  local PYTHON=$EXTERNAL/Python/2.7.10/$LCG_PLATFORM
  local GSL=$EXTERNAL/GSL/2.1/$LCG_PLATFORM
  local BOOST=$EXTERNAL/Boost/1.62.0/$LCG_PLATFORM
  local ROOT=$EXTERNAL/../../app/releases/ROOT/5.34.19/$LCG_PLATFORM/root

  HEPMC=$EXTERNAL/HepMC/2.06.09/$LCG_PLATFORM
  local FASTJET=$EXTERNAL/fastjet/3.2.0/$LCG_PLATFORM

  MCGENERATORS=$EXTERNAL/MCGenerators
  local AGILE=$MCGENERATORS/agile/1.4.1/$LCG_PLATFORM
  
  export PYTHONPATH=$AGILE/lib/python2.7/site-packages:$PYTHONPATH
  export LD_LIBRARY_PATH=$HEPMC/lib:$AGILE/lib:$PYTHON/lib:$FASTJET/lib:$GSL/lib:$BOOST/lib:$ROOT/lib:$LD_LIBRARY_PATH
  export PATH=$PYTHON/bin:$AGILE/bin:$PATH
  export AGILE_GEN_PATH=$MCGENERATORS
  
  # explicitly specify LHAPDF to override the (/afs) path specified inside libLHAPDF library
  LHAPDF=$MCGENERATORS/lhapdf/5.9.1/$LCG_PLATFORM
  export LHAPATH=$LHAPDF/../share/PDFsets
  export LD_LIBRARY_PATH=$LHAPDF/lib:$LD_LIBRARY_PATH
  
  # TODO: make LHAPDF6 the default for LCG_87?
  
  # use LHAPDF6 for pythia8, Sherpa 2.x, Herwig7
  if [[ "$generator" == "pythia8" || "$generator" == "vincia" ||
        "$generator" == "herwig7" || "$generator" == "sherpa" ]] ; then
    LHAPDF=$MCGENERATORS/lhapdf/6.1.6.cxxstd/$LCG_PLATFORM
    export LD_LIBRARY_PATH=$LHAPDF/lib:$LD_LIBRARY_PATH
    local PDFDATA=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current
    export LHAPDF_DATA_PATH=$PDFDATA:$LHAPDF/share/LHAPDF
  fi

  # Herwig7: Set ThePEG and other shared-library dependency paths
  if [[ "$generator" == "herwig7" ]] ; then
    if [[ "$version" == "7.1.1" ]]; then
      local THEPEG=$MCGENERATORS/thepeg/2.1.1/$LCG_PLATFORM
    elif [[ "$version" == "7.1.0" ]]; then
      local THEPEG=$MCGENERATORS/thepeg/2.1.0/$LCG_PLATFORM
    elif [[ "$version" == "7.0.4" ]]; then
      local THEPEG=$MCGENERATORS/thepeg/2.0.4/$LCG_PLATFORM
    elif [[ "$version" == "7.0.3" ]]; then
      local THEPEG=$MCGENERATORS/thepeg/2.0.3/$LCG_PLATFORM
    elif [[ "$version" == "7.0.2" ]]; then
      local THEPEG=$MCGENERATORS/thepeg/2.0.2/$LCG_PLATFORM
    fi
    export LD_LIBRARY_PATH=$THEPEG/lib/ThePEG:$LD_LIBRARY_PATH
    local HW7=$MCGENERATORS/herwig++/$version/$LCG_PLATFORM
    export LD_LIBRARY_PATH=$HW7/lib/Herwig:$LD_LIBRARY_PATH
    
    # work-around: Herwig7 in LCG_87 was build also vs. system GSL library (version 1.13)
    local GSL1=$EXTERNAL/../LCG_83/GSL/1.10/$LCG_PLATFORM
    export LD_LIBRARY_PATH=$GSL1/lib:$LD_LIBRARY_PATH
  fi
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "gcc = $(which gcc)"
  echo "gcc version = $(gcc -dumpversion)"
  echo "AGILE=$AGILE"
  echo "HEPMC=$HEPMC"
  echo "AGILE_GEN_PATH=$AGILE_GEN_PATH"
  echo "LHAPDF=$LHAPDF"
  echo "LHAPATH=$LHAPATH"
  echo "LHAPDF_DATA_PATH=$LHAPDF_DATA_PATH"
  echo "GSL=$GSL"
  echo ""
  
  # check paths to essential parts of machinery:
  for f in $MCGENERATORS $AGILE $HEPMC $PYTHON $LHAPDF $AGILE/bin/agile-runmc ; do
    if [[ ! -e "$f" ]] ; then
      echo "ERROR: fail to set environment, no access to $f"
      exit 1
    fi
  done
}

# setup environment to run generators from releases/LCG_88b tree
set_environment_LCG_88b () {
  local mode=$1
  local generator=$2
  local version=$3
  local tag=$4
  
  # $tag optionally contents actual name/version at LCG releases installation area
  local gname=$generator
  local gver=$version
  if [[ "$tag" != "" ]] ; then
    gname=${tag%%/*}
    gver=${tag##*/}
  fi
  
  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/releases/LCG_88b
  MCGENERATORS=$EXTERNAL/MCGenerators
  
  # get OS version
  local osver="centos7"
  if grep -q "release 6" /etc/redhat-release ; then
    osver="slc6"
  fi
  
  # set gcc version number and platform name:
  local LCG_BASE=$(uname -m)-$osver
  LCG_PLATFORM=$LCG_BASE-gcc62-opt
  
  # setup compiler
  source $EXTERNAL/../gcc/6.2.0/$LCG_BASE/setup.sh $EXTERNAL/..
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to set environment (gcc)"
    exit 1
  fi
  
  # work-around for herwig env script name mismatch: replace herwig++ --> herwig3
  local ename=$gname
  ename=${ename/herwig++/herwig3}
  
  local envscript=$MCGENERATORS/$gname/$gver/$LCG_PLATFORM/${ename}env-genser.sh
  
  # work-around for herwig7 7.x env setup, fixed in some next LCG release
  #if [[ "$generator" == "herwig7" ]] ; then
  #  # call low-level env. setup machinery directly
  #  # /cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv -p /cvmfs/sft.cern.ch/lcg/releases/LCG_96/ herwig3 7.1.5 x86_64-centos7-gcc8-opt
  #  
  #  local newenv=$(mktemp)
  #  local lcgenv="/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv"
  #  $lcgenv -p $EXTERNAL $ename $version $LCG_PLATFORM > $newenv
  #  echo 'LHAPDF_HOME=$LHAPDF__HOME' >> $newenv
  #  envscript=$newenv
  #fi
  
  echo "envscript=$envscript"
  source $envscript
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to set environment"
    exit 1
  fi
  
  HEPMC=$EXTERNAL/HepMC/2.06.11/$LCG_PLATFORM
  LHAPDF=$MCGENERATORS/lhapdf/6.2.3/$LCG_PLATFORM
  # work-around missing LHAPDF6 data pointer in `xxx-genser.sh`
  local PDFDATA=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current
  export LHAPDF_DATA_PATH=$PDFDATA:$LHAPDF/share/LHAPDF
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "g++ = $(which g++)"
  echo "g++ version = $(g++ -dumpversion)"
  echo "HEPMC=$HEPMC"
  echo "LHAPDF=$LHAPDF"
  echo "LHAPDF_DATA_PATH=$LHAPDF_DATA_PATH"
  #echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
  echo ""
}

# setup environment to run generators from releases/LCG_94 tree
set_environment_LCG_94 () {
  local mode=$1
  local generator=$2
  local version=$3
  
  # LCG_94 is CVMFS-only
  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/releases/LCG_94
  
  # set gcc version number and platform name:
  local LCG_BASE=$(uname -m)-slc6
  LCG_PLATFORM=$LCG_BASE-gcc62-opt
  
  # setup compiler
  source $EXTERNAL/../gcc/6.2.0/$LCG_BASE/setup.sh $EXTERNAL/..
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to set environment (gcc)"
    exit 1
  fi
  
  # setup newer linker (ld, binutils package) to work-around the bug in standard SLC6 ld version
  # /usr/bin/ld: BFD version internal error, aborting at reloc.c line 443 in bfd_get_reloc_size
  source /cvmfs/sft.cern.ch/lcg/releases/binutils/2.28-19981/x86_64-slc6-gcc62-opt/setup.sh
  #source /cvmfs/sft.cern.ch/lcg/releases/binutils/2.30-70db8/x86_64-slc6-gcc62-opt/setup.sh
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to set environment (binutils)"
    exit 1
  fi
  
  local PYTHON=$EXTERNAL/Python/2.7.15/$LCG_PLATFORM
  local GSL=$EXTERNAL/GSL/2.5/$LCG_PLATFORM
  local BOOST=$EXTERNAL/Boost/1.66.0/$LCG_PLATFORM
  local ROOT=$EXTERNAL/ROOT/6.14.04/$LCG_PLATFORM

  HEPMC=$EXTERNAL/HepMC/2.06.09/$LCG_PLATFORM
  local FASTJET=$EXTERNAL/fastjet/3.3.0/$LCG_PLATFORM

  MCGENERATORS=$EXTERNAL/MCGenerators
  local AGILE=$MCGENERATORS/agile/1.4.1/$LCG_PLATFORM
  
  export PYTHONPATH=$AGILE/lib/python2.7/site-packages:$PYTHONPATH
  export LD_LIBRARY_PATH=$HEPMC/lib:$AGILE/lib:$PYTHON/lib:$FASTJET/lib:$GSL/lib:$BOOST/lib:$ROOT/lib:$LD_LIBRARY_PATH
  export PATH=$PYTHON/bin:$AGILE/bin:$PATH
  export AGILE_GEN_PATH=$MCGENERATORS
  
  # EL9 compat.: work-around for missing ancient libssl.so.10 (needed for default LCG_94/ROOT/6.14.04)
  local isEL9="0"
  if grep -q "release 9" /etc/redhat-release ; then
    isEL9="1"
  fi
  if [[ "$isEL9" == "1" && "$generator" == "epos" ]] ; then
    # this installation depends on libssl.so.1.1 still available on EL9
    source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.14.04/x86_64-fedora28-gcc81-opt/root/bin/thisroot.sh
  fi
  
  # explicitly specify LHAPDF to override the (/afs) path specified inside libLHAPDF library
  
  # TODO: lhapdf5 is deprecated and is not available in LCG_94
  #       pointing to LCG_88 installation, but commented to pick up
  #       possible generator versions in LCG_94 which still need it
  #LHAPDF=/cvmfs/sft.cern.ch/lcg/releases/LCG_88/MCGenerators/lhapdf/5.9.1/$LCG_PLATFORM
  #export LHAPATH=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/5.9.1/share/PDFsets
  #export LD_LIBRARY_PATH=$LHAPDF/lib:$LD_LIBRARY_PATH
  
  # LHAPDF6 is the default
  LHAPDF=$MCGENERATORS/lhapdf/6.2.1/$LCG_PLATFORM
  export LD_LIBRARY_PATH=$LHAPDF/lib:$LD_LIBRARY_PATH
  local PDFDATA=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current
  export LHAPDF_DATA_PATH=$PDFDATA:$LHAPDF/share/LHAPDF
  
  # Herwig7: Set ThePEG and other shared-library dependency paths
  if [[ "$generator" == "herwig7" ]] ; then
    case "$version" in
      "7.1.1" )
        local THEPEG=$MCGENERATORS/thepeg/2.1.1/$LCG_PLATFORM
        ;;
      
      "7.1.3" )
        local THEPEG=$MCGENERATORS/thepeg/2.1.3/$LCG_PLATFORM
        ;;
      
      "7.1.4" )
        local THEPEG=$MCGENERATORS/thepeg/2.1.4/$LCG_PLATFORM
        ;;
      
      * )
        echo "ERROR: set_environment_LCG_94() not implemented: $generator $version"
        exit 1
        ;;
    esac
    
    export LD_LIBRARY_PATH=$THEPEG/lib/ThePEG:$LD_LIBRARY_PATH
    local HW7=$MCGENERATORS/herwig++/$version/$LCG_PLATFORM
    export LD_LIBRARY_PATH=$HW7/lib/Herwig:$LD_LIBRARY_PATH
  fi
  
  # CRMC
  local TBB=$EXTERNAL/tbb/2018_U1/$LCG_PLATFORM
  export LD_LIBRARY_PATH=$TBB/lib:$LD_LIBRARY_PATH
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "g++ = $(which g++)"
  echo "g++ version = $(g++ -dumpversion)"
  echo "ld version = $(ld -version | head -n 1)"
  echo "AGILE=$AGILE"
  echo "HEPMC=$HEPMC"
  echo "AGILE_GEN_PATH=$AGILE_GEN_PATH"
  echo "LHAPDF=$LHAPDF"
  echo "LHAPATH=$LHAPATH"
  echo "LHAPDF_DATA_PATH=$LHAPDF_DATA_PATH"
  echo "GSL=$GSL"
  echo ""
  
  # check paths to essential parts of machinery:
  for f in $MCGENERATORS $AGILE $HEPMC $PYTHON $LHAPDF $AGILE/bin/agile-runmc ; do
    if [[ ! -e "$f" ]] ; then
      echo "ERROR: fail to set environment, no access to $f"
      exit 1
    fi
  done
}

el9pycompat () {
  # EL9 compat.: create 'python' executable in PATH
  if ! which python 2>&- ; then
    local pydir=$(mktemp -d)
    ln -s -T /usr/bin/python3 $pydir/python
    export PATH=$pydir:$PATH
    echo "INFO: python = "
    ls -l $(which python)
  fi
}

# setup environment to run generators from releases/LCG_96 tree
set_environment_LCG_96 () {
  local mode=$1
  local generator=$2
  local version=$3
  local tag=$4
  
  # $tag optionally contents actual name/version at LCG releases installation area
  local gname=$generator
  local gver=$version
  if [[ "$tag" != "" ]] ; then
    gname=${tag%%/*}
    gver=${tag##*/}
  fi
  
  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/releases/LCG_96
  MCGENERATORS=$EXTERNAL/MCGenerators
  
  # get OS version
  local osver="centos7"
  if grep -q "release 6" /etc/redhat-release ; then
    osver="slc6"
  fi
  
  LCG_PLATFORM=$(uname -m)-$osver-gcc8-opt
  
  # work-around for herwig env script name mismatch: replace herwig++ --> herwig3
  local ename=$gname
  ename=${ename/herwig++/herwig3}
  
  local envscript=$MCGENERATORS/$gname/$gver/$LCG_PLATFORM/${ename}env-genser.sh
  
  # work-around for herwig7 7.x env setup, fixed in some next LCG release
  if [[ "$generator" == "herwig7" ]] ; then
    # call low-level env. setup machinery directly
    # /cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv -p /cvmfs/sft.cern.ch/lcg/releases/LCG_96/ herwig3 7.1.5 x86_64-centos7-gcc8-opt
    
    local newenv=$(mktemp)
    local lcgenv="/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv"
    $lcgenv -p $EXTERNAL $ename $version $LCG_PLATFORM > $newenv
    echo 'LHAPDF_HOME=$LHAPDF__HOME' >> $newenv
    envscript=$newenv
  fi
  
  el9pycompat
  
  echo "envscript=$envscript"
  source $envscript
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to set environment"
    exit 1
  fi
  
  # EL9 compat.: work-around for missing ancient libssl.so.10 (needed for default LCG_96/ROOT/6.18.00)
  local isEL9="0"
  if grep -q "release 9" /etc/redhat-release ; then
    isEL9="1"
  fi
  if [[ "$isEL9" == "1" && "$generator" == "epos" ]] ; then
    # this installation depends on libssl.so.1.1 still available on EL9
    source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.18.04/x86_64-fedora29-gcc83-opt/bin/thisroot.sh
  fi
  
  HEPMC=$HEPMC_HOME
  LHAPDF=$LHAPDF_HOME
  # work-around missing LHAPDF6 data pointer in `xxx-genser.sh`
  local PDFDATA=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current
  export LHAPDF_DATA_PATH=$PDFDATA:$LHAPDF/share/LHAPDF
  
  # explicitly set HepMC for madgraph5amc
  if [[ "$HEPMC" == "" ]] ; then
    HEPMC=$EXTERNAL/HepMC/2.06.09/$LCG_PLATFORM
  fi
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "g++ = $(which g++)"
  echo "g++ version = $(g++ -dumpversion)"
  echo "HEPMC=$HEPMC"
  echo "LHAPDF=$LHAPDF"
  echo "LHAPDF_DATA_PATH=$LHAPDF_DATA_PATH"
  #echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
  echo ""
}

# setup environment to run generators from releases/LCG_97a_FCC_4 tree
set_environment_LCG_97a_FCC_4 () {
  local mode=$1
  local generator=$2
  local version=$3
  local tag=$4
  
  # $tag optionally contents actual name/version at LCG releases installation area
  local gname=$generator
  local gver=$version
  if [[ "$tag" != "" ]] ; then
    gname=${tag%%/*}
    gver=${tag##*/}
  fi
  
  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/releases/LCG_97a_FCC_4
  MCGENERATORS=$EXTERNAL/MCGenerators
  
  # get OS version
  local osver="centos7"
  if grep -q "release 6" /etc/redhat-release ; then
    osver="slc6"
  fi
  
  LCG_PLATFORM=$(uname -m)-$osver-gcc8-opt
  
  # work-around for herwig env script name mismatch: replace herwig++ --> herwig3
  local ename=$gname
  ename=${ename/herwig++/herwig3}
  
  local envscript=$MCGENERATORS/$gname/$gver/$LCG_PLATFORM/${ename}env-genser.sh
  
  # work-around for herwig7 7.x env setup, fixed in some next LCG release
  if [[ "$generator" == "herwig7" ]] ; then
    # call low-level env. setup machinery directly
    # /cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv -p /cvmfs/sft.cern.ch/lcg/releases/LCG_96/ herwig3 7.1.5 x86_64-centos7-gcc8-opt
    
    local newenv=$(mktemp)
    local lcgenv="/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv"
    $lcgenv -p $EXTERNAL $ename $version $LCG_PLATFORM > $newenv
    echo 'LHAPDF_HOME=$LHAPDF__HOME' >> $newenv
    envscript=$newenv
  fi
  
  el9pycompat
  
  echo "envscript=$envscript"
  source $envscript
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to set environment"
    exit 1
  fi
  
  # EL9 compat.: work-around for missing ancient libssl.so.10 (needed for default LCG_96/ROOT/6.18.00)
  local isEL9="0"
  if grep -q "release 9" /etc/redhat-release ; then
    isEL9="1"
  fi
  if [[ "$isEL9" == "1" && "$generator" == "epos" ]] ; then
    # this installation depends on libssl.so.1.1 still available on EL9
    source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.18.04/x86_64-fedora29-gcc83-opt/bin/thisroot.sh
  fi
  
  HEPMC=$HEPMC_HOME
  LHAPDF=$LHAPDF_HOME
  # work-around missing LHAPDF6 data pointer in `xxx-genser.sh`
  local PDFDATA=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current
  export LHAPDF_DATA_PATH=$PDFDATA:$LHAPDF/share/LHAPDF
  
  # explicitly set HepMC for madgraph5amc
  if [[ "$HEPMC" == "" ]] ; then
    HEPMC=$EXTERNAL/HepMC/2.06.09/$LCG_PLATFORM
  fi
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "g++ = $(which g++)"
  echo "g++ version = $(g++ -dumpversion)"
  echo "HEPMC=$HEPMC"
  echo "LHAPDF=$LHAPDF"
  echo "LHAPDF_DATA_PATH=$LHAPDF_DATA_PATH"
  #echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
  echo ""
}

# setup environment to run generators from releases/LCG_101 tree
set_environment_LCG_101 () {
  local mode=$1
  local generator=$2
  local version=$3
  local tag=$4
  
  # $tag optionally contents actual name/version at LCG releases installation area
  local gname=$generator
  local gver=$version
  if [[ "$tag" != "" ]] ; then
    gname=${tag%%/*}
    gver=${tag##*/}
  fi
  
  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/releases/LCG_101
  MCGENERATORS=$EXTERNAL/MCGenerators
  
  # platform name
  local osver="centos7"
  LCG_PLATFORM=$(uname -m)-$osver-gcc8-opt
  
  # work-around for herwig env script name mismatch: replace herwig++ --> herwig3
  local ename=$gname
  ename=${ename/herwig++/herwig3}
  
  local envscript=$MCGENERATORS/$gname/$gver/$LCG_PLATFORM/${ename}env-genser.sh
  
  # work-around for herwig7 7.x env setup, fixed in some next LCG release
  #if [[ "$generator" == "herwig7" ]] ; then
  #  # call low-level env. setup machinery directly
  #  # /cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv -p /cvmfs/sft.cern.ch/lcg/releases/LCG_96/ herwig3 7.1.5 x86_64-centos7-gcc8-opt
  #  
  #  local newenv=$(mktemp)
  #  local lcgenv="/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv"
  #  $lcgenv -p $EXTERNAL $ename $version $LCG_PLATFORM > $newenv
  #  echo 'LHAPDF_HOME=$LHAPDF__HOME' >> $newenv
  #  envscript=$newenv
  #fi
  
  el9pycompat
  
  echo "envscript=$envscript"
  source $envscript
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to set environment"
    exit 1
  fi
  
  HEPMC=$HEPMC__HOME
  LHAPDF=$LHAPDF__HOME
  # work-around missing LHAPDF6 data pointer in `xxx-genser.sh`
  local PDFDATA=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current
  export LHAPDF_DATA_PATH=$PDFDATA:$LHAPDF/share/LHAPDF
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "g++ = $(which g++)"
  echo "g++ version = $(g++ -dumpversion)"
  echo "HEPMC=$HEPMC"
  echo "LHAPDF=$LHAPDF"
  echo "LHAPDF_DATA_PATH=$LHAPDF_DATA_PATH"
  #echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
  echo ""
}

# setup environment to run generators from releases/LCG_102b_ATLAS_10 tree
set_environment_LCG_102b_ATLAS_10 () {
  local mode=$1
  local generator=$2
  local version=$3
  local tag=$4
  
  # $tag optionally contents actual name/version at LCG releases installation area
  local gname=$generator
  local gver=$version
  if [[ "$tag" != "" ]] ; then
    gname=${tag%%/*}
    gver=${tag##*/}
  fi
  
  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/releases/LCG_102b_ATLAS_10
  MCGENERATORS=$EXTERNAL/MCGenerators
  
  # get OS version
  local osver="centos7"
  if grep -q "release 9" /etc/redhat-release ; then
    # note here unusual os name "centos9" instead of "el9"
    osver="centos9"
  fi
  
  LCG_PLATFORM=$(uname -m)-$osver-gcc11-opt
  
  # work-around for herwig env script name mismatch: replace herwig++ --> herwig3
  local ename=$gname
  ename=${ename/herwig++/herwig3}
  
  local envscript=$MCGENERATORS/$gname/$gver/$LCG_PLATFORM/${ename}env-genser.sh
  
  # work-around for herwig7 7.x env setup, fixed in some next LCG release
  #if [[ "$generator" == "herwig7" ]] ; then
  #  # call low-level env. setup machinery directly
  #  # /cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv -p /cvmfs/sft.cern.ch/lcg/releases/LCG_96/ herwig3 7.1.5 x86_64-centos7-gcc8-opt
  #  
  #  local newenv=$(mktemp)
  #  local lcgenv="/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv"
  #  $lcgenv -p $EXTERNAL $ename $version $LCG_PLATFORM > $newenv
  #  echo 'LHAPDF_HOME=$LHAPDF__HOME' >> $newenv
  #  envscript=$newenv
  #fi
  
  echo "envscript=$envscript"
  source $envscript
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to set environment"
    exit 1
  fi
  
  HEPMC=$HEPMC__HOME
  LHAPDF=$LHAPDF__HOME
  # work-around missing LHAPDF6 data pointer in `xxx-genser.sh`
  local PDFDATA=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current
  export LHAPDF_DATA_PATH=$PDFDATA:$LHAPDF/share/LHAPDF
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "g++ = $(which g++)"
  echo "g++ version = $(g++ -dumpversion)"
  echo "HEPMC=$HEPMC"
  echo "LHAPDF=$LHAPDF"
  echo "LHAPDF_DATA_PATH=$LHAPDF_DATA_PATH"
  #echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
  echo ""
}

# setup environment to run generators from releases/LCG_104 tree
set_environment_LCG_104 () {
  local mode=$1
  local generator=$2
  local version=$3
  local tag=$4
  
  # $tag optionally contents actual name/version at LCG releases installation area
  local gname=$generator
  local gver=$version
  if [[ "$tag" != "" ]] ; then
    gname=${tag%%/*}
    gver=${tag##*/}
  fi
  
  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/releases/LCG_104
  MCGENERATORS=$EXTERNAL/MCGenerators
  
  # get OS version
  local osver="centos7"
  if grep -q "release 9" /etc/redhat-release ; then
    osver="el9"
  fi
  
  LCG_PLATFORM=$(uname -m)-$osver-gcc11-opt
  
  # work-around for herwig env script name mismatch: replace herwig++ --> herwig3
  local ename=$gname
  ename=${ename/herwig++/herwig3}
  
  local envscript=$MCGENERATORS/$gname/$gver/$LCG_PLATFORM/${ename}env-genser.sh
  
  # work-around for herwig7 7.x env setup, fixed in some next LCG release
  #if [[ "$generator" == "herwig7" ]] ; then
  #  # call low-level env. setup machinery directly
  #  # /cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv -p /cvmfs/sft.cern.ch/lcg/releases/LCG_96/ herwig3 7.1.5 x86_64-centos7-gcc8-opt
  #  
  #  local newenv=$(mktemp)
  #  local lcgenv="/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv"
  #  $lcgenv -p $EXTERNAL $ename $version $LCG_PLATFORM > $newenv
  #  echo 'LHAPDF_HOME=$LHAPDF__HOME' >> $newenv
  #  envscript=$newenv
  #fi
  
  echo "envscript=$envscript"
  source $envscript
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to set environment"
    exit 1
  fi
  
  #HEPMC=$HEPMC__HOME
  HEPMC=$EXTERNAL/HepMC/2.06.11/$LCG_PLATFORM
  LHAPDF=$LHAPDF__HOME
  # work-around missing LHAPDF6 data pointer in `xxx-genser.sh`
  local PDFDATA=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current
  export LHAPDF_DATA_PATH=$PDFDATA:$LHAPDF/share/LHAPDF
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "g++ = $(which g++)"
  echo "g++ version = $(g++ -dumpversion)"
  echo "HEPMC=$HEPMC"
  echo "LHAPDF=$LHAPDF"
  echo "LHAPDF_DATA_PATH=$LHAPDF_DATA_PATH"
  #echo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
  echo ""
}

# setup environment to run heavy-ion generators
set_environment_higenerators () {
  local mode=$1
  
  local EXTERNAL=/cvmfs/sft.cern.ch/lcg/external
  
  # set platform name:
  local LCG_BASE=$(uname -m)-slc6
  LCG_PLATFORM=$LCG_BASE-gcc47-opt
  
  # set compiler
  source $EXTERNAL/gcc/4.9.1/$LCG_BASE/setup.sh $EXTERNAL
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to set environment (gcc)"
    exit 1
  fi
  
  MCGENERATORS=$EXTERNAL/MCGenerators_lcgcmt67c
  HEPMC=/cvmfs/alice.cern.ch/el6-x86_64/Packages/HepMC/v2.06.09-15
  local AGILE=$MCGENERATORS/agile/1.4.0/$LCG_PLATFORM
  local PYTHON=$EXTERNAL/Python/2.7.3/$LCG_PLATFORM
  local FASTJET=$EXTERNAL/fastjet/3.0.3/$LCG_PLATFORM
  local GSL=$EXTERNAL/GSL/1.10/$LCG_PLATFORM
  local BOOST=$EXTERNAL/Boost/1.53.0_python2.7/$LCG_PLATFORM
  local ROOT=$EXTERNAL/../app/releases/ROOT/5.34.19/$LCG_PLATFORM/root
  
  export PYTHONPATH=$AGILE/lib/python2.7/site-packages:$PYTHONPATH
  export LD_LIBRARY_PATH=$HEPMC/lib:$AGILE/lib:$PYTHON/lib:$FASTJET/lib:$GSL/lib:$BOOST/lib:$ROOT/lib:$LD_LIBRARY_PATH
  export PATH=$PYTHON/bin:$AGILE/bin:$PATH
  export AGILE_GEN_PATH=$MCGENERATORS
  
  # explicitly specify LHAPDF to override the (/afs) path specified inside libLHAPDF library
  LHAPDF=$MCGENERATORS/lhapdf/5.9.1/$LCG_PLATFORM
  export LHAPATH=$LHAPDF/../share/PDFsets
  export LD_LIBRARY_PATH=$LHAPDF/lib:$LD_LIBRARY_PATH
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "gcc = $(which gcc)"
  echo "gcc version = $(gcc -dumpversion)"
  echo "AGILE=$AGILE"
  echo "HEPMC=$HEPMC"
  echo "AGILE_GEN_PATH=$AGILE_GEN_PATH"
  echo "LHAPDF=$LHAPDF"
  echo ""
  
  # check paths to essential parts of machinery:
  for f in $MCGENERATORS $AGILE $HEPMC $PYTHON $LHAPDF $AGILE/bin/agile-runmc ; do
    if [[ ! -e "$f" ]] ; then
      echo "ERROR: fail to set environment, no access to $f"
      exit 1
    fi
  done  
}

# setup environment to packages from ALICE repository (for now heavy-ion generators)
# interface (example):
# - list available Rivet versions:
#     $ /cvmfs/alice.cern.ch/bin/alienv -l avail Rivet
#     $ /cvmfs/alice.cern.ch/bin/alienv display Rivet/xxx
# - source the environment:
#     $ eval $(/cvmfs/alice.cern.ch/bin/alienv printenv Rivet-hi/2.6.0-alice1-3)
#     $ echo $HEPMC_ROOT
#     $ echo $RIVETHI_ROOT
set_environment_alice () {
  local tag=$4
  local alienv=/cvmfs/alice.cern.ch/bin/alienv
  
  echo "INFO: loading ALICE environment for $tag"
  $alienv display $tag
  
  # source ALICE environment setup
  eval $($alienv printenv $tag)
  
  LCG_PLATFORM=undefined_edit_set_environment_alice
  MCGENERATORS=undefined_edit_set_environment_alice
  HEPMC=$HEPMC_ROOT
  
  echo "MCGENERATORS=$MCGENERATORS"
  echo "LCG_PLATFORM=$LCG_PLATFORM"
  echo "g++ = $(which g++)"
  echo "g++ version = $(g++ -dumpversion)"
  echo "HEPMC=$HEPMC"
  echo ""
  
  # check paths to essential parts of machinery:
  #for f in $MCGENERATORS $AGILE $HEPMC $PYTHON $LHAPDF $AGILE/bin/agile-runmc ; do
  #  if [[ ! -e "$f" ]] ; then
  #    echo "ERROR: fail to set environment, no access to $f"
  #    exit 1
  #  fi
  #done
}

# EL9/CC7 compat.: work-around for missing ancient libs
el9cc7compat () {
  local generator=$1
  
  # check for EL9, assume the CC7 by default
  local osver=7
  if grep -q "release 9" /etc/redhat-release ; then
    osver=9
  fi
  
  # these libraries are in use only by interactive mode of herwig7 / herwig++
  if [[ "$generator" == "herwig7" ||
        "$generator" == "herwig++" || "$generator" == "herwig++powheg" ]] ; then
    local rldir=$(mktemp -d)
    cd $rldir
    touch empty.c
    gcc -o empty.so -shared -fPIC empty.c
    if (( osver > 7 )) ; then
      ln -s empty.so libreadline.so.6
    fi
    ln -s empty.so libreadline.so.5
    ln -s empty.so libtermcap.so.2
    export LD_LIBRARY_PATH=$PWD:$LD_LIBRARY_PATH
    echo "INFO: EL9/CC7 compat: $generator - added work-around for missing libraries:"
    ls -l *.so*
    cd -
    echo ""
  fi
}

# setup execution environment based on generator version
set_environment () {
  local mode=$1
  local generator=$2
  local version=$3
  
  echo "Setting environment for $generator $version ..."
  
  local tree
  tree=$(map_get_value "configuration/locations.map" "^$generator $version " 3) || exit 1
  
  local tag=$(map_get_value "configuration/locations.map" "^$generator $version " ~4)
  
  echo "tree = $tree"
  echo "tag = $tag"
  echo ""
  
  tree=${tree//.} # delete dots from tree name
  
  # call function corresponding to "$tree"
  set_environment_$tree $mode $generator $version $tag || exit 1
  
  el9cc7compat $generator
}


# this function strip comments, empty lines and double spaces from input stream
weed () {
  sed -e '/^#.*/ d' -e '/^ *$/ d' -e 's,  *, ,g'
}

# print file contents
print_file () {
  fname="$1"

  echo "=> $fname :"
  cat $fname
  echo "--------------------------------------"
  echo ""
}

# return beam energy = ECM / 2
calc_beam_energy () {
  local ecm=$1
  # calc "ECM / 2" and strip trailing zeros
  echo "$ecm / 2" | bc -l | sed 's,0*$,,'
}


# lookup $mapf file for $key string and extract value at position $fieldn
map_get_value () {
  local mapf=$1
  local key=$2
  local fieldn=$3
  
  # leading '~' means the field is optional and could be missing
  local isReq=${fieldn:0:1}
  isReq=${isReq/\~}
  fieldn=${fieldn/\~}
  
  #echo "isReq=$isReq" >&2
  #echo "fieldn=$fieldn" >&2
  
  local value=$(cat $mapf | weed | grep "$key" | cut -d ' ' -f $fieldn)
  
  if [[ "$value" == "" && "$isReq" != "" ]] ; then
    echo "ERROR: cannot find data in map file" >&2
    echo "         file  = $mapf" >&2
    echo "         key   = '$key'" >&2
    echo "         field = $fieldn" >&2
    return 1
  fi
  
  echo $value
}

# preprocessor for configuration/pythia8-tunes.map
# to process @copy command
pythia8_tunes_map_preprocessor () {
  local fin="$1"
  local fout="$2"
  local fbuf="$fout.buf"
  
  cp "$fin" "$fout"
  
  while true ; do
    # search for @copy command
    local acopy=$(grep -n "^@copy " "$fout" | head -n1)
    if [[ "$acopy" == "" ]] ; then break ; fi
    
    # extract parameters
    local n=$(echo "$acopy" | cut -d: -f1)
    local versrc=$(echo "$acopy" | awk '{print $2}')
    local verdst=$(echo "$acopy" | awk '{print $3}')
    
    #echo "n=$n versrc=$versrc verdst=$verdst"
    
    # prepare copy text
    grep "^$versrc " "$fout" > "$fbuf"
    sed -e "s,^$versrc ,$verdst ," -i "$fbuf"
    
    # check to missing 'versrc'
    if ! test -s "$fbuf" ; then
      echo "ERROR: pythia8_tunes_map_preprocessor() missing $versrc in $fin"
      rm -f "$fbuf"
      exit 1
    fi
    
    # delete @copy command line
    sed -e "$n d" -i "$fout"
    # insert @copy result
    sed -e "$((n-1)) r $fbuf" -i "$fout"
    rm -f "$fbuf"
  done
}

# => cache service functions

# init cache root directory $CACHE
cache_init () {
  if [[ "$CACHE" == "" ]] ; then
    echo "INFO: cache is not active, CACHE=$CACHE"
    return 0
  fi
  
  # ensure the cache path exists
  mkdir -p "$CACHE"
  CACHE=$(cd "$CACHE"; pwd)
  echo "INFO: cache is active, CACHE=$CACHE"
}

# prepare cache key file "key.txt"
cache_make_key () {
  if [[ "$CACHE" == "" ]] ; then
    return 0
  fi
  
  local finput="$1"
  local extra="$2"
  local fkey="key.txt"
  
  {
    echo $extra
    
    # strip non-key data of powheg-box steering file
    #  - number of events
    #  - seed of random generator
    sed -e '/numevts/ d' \
        -e '/iseed/ d' \
         $finput
  } > $fkey
  
  echo "INFO: cache file is ready $fkey"
}

# extract data corresponding to key file "key.txt"
cache_get_data () {
  if [[ "$CACHE" == "" ]] ; then
    return 0
  fi
  
  local fkey="key.txt"
  
  echo "INFO: cache attempt to extract the data of key $fkey"
  
  local keycode=$(md5sum $fkey | awk '{print $1}')
  echo "keycode=$keycode"
  
  local datapath="$CACHE/$keycode.tgz"
  echo "datapath=$datapath"
  
  if [[ ! -e "$datapath" ]] ; then
    echo "INFO: cache - no data available"
    return 0
  fi
  
  mv key.txt key.txt.here
  tar zxfv "$datapath"
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: cache - fail to unpack the data"
    return 1
  fi
  
  # TODO: check `key.txt == key.txt.here`
  
  echo "INFO: cache - data is extracted"
}

# update cache with missing data
cache_set_data () {
  if [[ "$CACHE" == "" ]] ; then
    return 0
  fi
  
  local fkey="key.txt"
  
  echo "INFO: cache - update for key file $fkey"
  
  local keycode=$(md5sum $fkey | awk '{print $1}')
  echo "keycode=$keycode"
  
  local datapath="$CACHE/$keycode.tgz"
  echo "datapath=$datapath"
  
  if [[ -e "$datapath" ]] ; then
    echo "INFO: cache - data exists already"
    return 0
  fi
  
  echo "INFO: cache - package data"
  
  tar zcfv "$datapath" key.txt $*
  if [[ "$?" != "0" ]] ; then
    echo "WARNING: cache - fail to prepare the data file"
    rm -f "$datapath"
    return 0
  fi
  
  echo "INFO: cache - data file OK"
}


# run all machinery and produce histograms
run () {
  # input parameters
  local mode=$1
  local beam=$2
  local process=$3
  local energy=$4
  local params=$5
  local specific=$6
  local generator=$7
  local version=$8
  local tune=$9
  local nevts=${10}
  local seed=${11}
  local outfile=${12}

  echo "Input parameters:"
  echo "mode=$mode"
  echo "beam=$beam"
  echo "process=$process"
  echo "energy=$energy"
  echo "params=$params"
  echo "specific=$specific"
  echo "generator=$generator"
  echo "version=$version"
  echo "tune=$tune"
  echo "nevts=$nevts"
  echo "seed=$seed"
  echo "outfile=$outfile"
  echo ""

  # check mode:
  if [[ "$mode" != "local" && "$mode" != "lxbatch" && "$mode" != "boinc" ]] ; then
    echo "ERROR: unknown mode: $mode"
    exit 1
  fi

  # paths to temporary directories and files
  echo "Prepare temporary directories and files ..."
  local workd=$(pwd)
  local tmpd=$(dirname $outfile)
  local tmp_params="$tmpd/generator.params"
  
  echo "workd=$workd"
  echo "tmpd=$tmpd"
  echo "tmp_params=$tmp_params"
  echo ""
  
  # decode params:
  echo "Decoding parameters of generator..."
  local vect=( ${params//,/ } )   # replace ',' by spaces and prepare array

  # 1st parameter is 'pTmin'
  local pTmin=${vect[0]}
  if [[ "$pTmin" == "" || "$pTmin" == "-" ]] ; then
    # set default value if parameter omitted
    pTmin="0"
  fi

  # 2nd parameter is 'pTmax'
  local pTmax=${vect[1]}
  if [[ "$pTmax" == "" || "$pTmax" == "-" ]] ; then
    # set default value if parameter omitted
    pTmax="$energy"
  fi

  # 3rd parameter is 'mHatMin'
  local mHatMin=${vect[2]}
  if [[ "$mHatMin" == "" || "$mHatMin" == "-" ]] ; then
    mHatMin="0"
  fi

  # 4th parameter is 'mHatMax'
  local mHatMax=${vect[3]}
  if [[ "$mHatMax" == "" || "$mHatMax" == "-" ]] ; then
    mHatMax="$energy"
  fi

  echo "  pTmin  = $pTmin"
  echo "  pTmax  = $pTmax"
  echo "  mHatMin = $mHatMin"
  echo "  mHatMax = $mHatMax"
  echo ""

  # here is a trick to run soft QCD process instead of hard QCD
  # if required pTmin == 0 to avoid infrared divergency
  processCode="$process"
  if [[ "$pTmin" == "0" ]] ; then
    if [[ "$process" == "jets" || "$process" == "ue" ]] ; then
      processCode="mb-inelastic"
    fi
  fi
  echo "processCode=$processCode"
  echo ""

  local beam1
  beam1=$(map_get_value "configuration/beams.map" "^${generator} ${beam} " 3) || exit 1
  echo "beam1=$beam1"
  
  local beam2
  beam2=$(map_get_value "configuration/beams.map" "^${generator} ${beam} " 4) || exit 1
  echo "beam2=$beam2"

  local beamEn="$(calc_beam_energy $energy)"
  echo "beam energy = $beamEn"

  # steering file template
  local templatef="configuration/$generator-$processCode.params"
  
  # Special treatment for Herwig7 soft processes; incompatible changes between 7.0 and 7.1
  if [[ "$generator" == "herwig7" && "$version" < "7.1.0" && "$processCode" == "mb-inelastic" ]]; then
    templatef="configuration/$generator-$processCode~v70.params"
  fi
  
  # sherpa < 1.4: use old card for zhad process
  if [[ "$generator" == "sherpa" && "$version" < "1.4.0" && "$processCode" == "zhad" ]]; then
    templatef="configuration/$generator-$processCode~v13.params"
  fi
  
  # check for tune-specific steering template:
  local templateftune="${templatef%.params}~${tune}.params"
  
  if [[ -e $templateftune ]] ; then
    echo "INFO: using tune-specific steering file template"
    templatef=$templateftune
  fi
  
  echo "INFO: steering file template = $templatef"
  
  cp $templatef $tmp_params
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to copy config file:"
    echo "         $templatef"
    exit 1
  fi

  cache_init
  
  echo "Prepare $generator $version parameters ..."
  local generatorExecString=""

  case "$generator" in
    "pythia6" )

      if [[ "${version:0:2}" != "6." ]] ; then
        echo "ERROR: incorrect Pythia6 version: $version"
        echo "       should be in form 6.xxx"
        exit 1
      fi

      # version -> versionCode
      local versionCode=${version/6./}  # remove "6." from version string
      echo "versionCode=$versionCode"

      # tune -> tuneCode (convert tune string to tune code)
      local tuneCode
      tuneCode=$(map_get_value "configuration/pythia6-tunes.map" "^${version} ${tune} " 3) || exit 1
      echo "tuneCode=$tuneCode"

      sed -e "s,%beam1%,$beam1,"        \
          -e "s,%beam2%,$beam2,"        \
          -e "s,%beamEn%,$beamEn,"      \
          -e "s,%seed%,$seed,"          \
          -e "s,%nevts%,$nevts,"        \
          -e "s,%tuneCode%,$tuneCode,"  \
          -e "s,%pTmin%,$pTmin,"        \
          -e "s,%pTmax%,$pTmax,"        \
          -e "s,%mHatMin%,$mHatMin,"    \
          -e "s,%mHatMax%,$mHatMax,"    \
          -i $tmp_params
      
      # cut tune definition line from param file in case of "default" tune
      if [[ "$tune" == "default" ]] ; then
        sed -e "s,^MSTP(5).*,," -i $tmp_params
      fi
      
      # print contents of parameters file
      print_file "$tmp_params"

      # the `agile-runmc` explicitly loads the libHepMCfio from the set of hard-coded locations
      # which is problematic in case of local system has newer HepMC version installation
      # work-around: create a link to actual HepMC library to ensure the proper version will be in use
      # the work-around is possible as the very first location in the set is the current directory path "./"
      # reference: AGILe-1.4.0/src/Core/Loader.cc:72, function getLibSearchDirs()
      (
        # the '$tmps' will be the agile-runmc execution directory
        cd $tmpd
        pwd
        ln -s $HEPMC/lib/libHepMCfio.so
        ls -l libHepMCfio.so
      )
      
      #debug options:
      #agile-runmc --list-gens
      #agile-runmc -l TRACE
      
      # form generator execution command
      generatorExecString="agile-runmc Pythia6:$versionCode --paramfile=$tmp_params --out=$outfile"
      ;;

    "pythia8" )
      # documentation:
      #   http://home.thep.lu.se/Pythia/  ->  "online HTML manual"

      if [[ "${version:0:2}" != "8." ]] ; then
        echo "ERROR: incorrect Pythia8 version: $version"
        echo "       should be in form 8.xxx"
        exit 1
      fi

      # version -> versionCode
      local versionCode=${version/8./}  # remove "8." from version string
      echo "versionCode=$versionCode"

      local py8tunes=$(mktemp --tmpdir pythia8-tunes.map.XXXXXX)
      pythia8_tunes_map_preprocessor "configuration/pythia8-tunes.map" "$py8tunes"
      #cp "$py8tunes" $workd/pythia8-tunes.map.preproc
      
      # tune -> tuneCode (convert tune string to tune code)
      # note: pythia8 share same tune codes for pp and ppbar beams
      local tuneCode
      tuneCode=$(map_get_value "$py8tunes" "^${version} ${beam/ppbar/pp} ${tune} " 4) || exit 1
      echo "tuneCode=$tuneCode"

      sed -e "s,%nevts%,$nevts," \
          -e "s,%seed%,$seed," \
          -e "s,%beam1%,$beam1," \
          -e "s,%beam2%,$beam2," \
          -e "s,%energy%,$energy," \
          -e "s,%tuneCode%,$tuneCode," \
          -e "s,%pTmin%,$pTmin,"        \
          -e "s,%pTmax%,$pTmax,"        \
          -e "s,%mHatMin%,$mHatMin,"    \
          -e "s,%mHatMax%,$mHatMax,"    \
          -i $tmp_params

      # cut tune definition line from param file in case of "default*" tune
      if [[ "${tune:0:7}" == "default" ]] ; then
        sed -e "s,^Tune:.*,," -i $tmp_params
      fi
      
      # complete tune by possible additional parameters
      if test -e "configuration/$generator-$tune.tune" ; then
        cat "configuration/$generator-$tune.tune" >> $tmp_params
      fi
      
      # Pythia8 >= 180 flag renamed: SoftQCD:minBias -> SoftQCD:nonDiffractive
      if [[ ! "$versionCode" < "180" ]] ; then
        sed -e "s,SoftQCD:minBias,SoftQCD:nonDiffractive," -i $tmp_params
      fi

      # Pythia8 >= 200 flag renamed: BeamRemnants:reconnectColours -> ColourReconnection:reconnect
      if [[ ! "$versionCode" < "200" ]] ; then
        sed -e "s,BeamRemnants:reconnectColours,ColourReconnection:reconnect," -i $tmp_params
      fi
      
      # prefer LHAPDF6 starting from version Pythia8 >= 219
      if [[ ! "$versionCode" < "219" ]] ; then
        sed -e "/Tune:pp/i Tune:preferLHAPDF=2" -i $tmp_params
      fi
      
      # renames: Diffraction:xxx -> SigmaDiffractive:xxx starting from version Pythia8 >= 235
      if [[ ! "$versionCode" < "235" ]] ; then
        sed -e "s,Diffraction:PomFlux,SigmaDiffractive:PomFlux," -i $tmp_params
      fi
      
      # renames (version>=302): "Vincia:tuneFile = path" -> "Vincia:Tune = code"
      if [[ "$versionCode" > "301" && "$tune" == "vincia-default" ]] ; then
        sed -e "s,Vincia:tuneFile.*,Vincia:Tune = 0," -i $tmp_params
      fi

      # Inject additional liveq tune configuration to the parameters file
      if [[ ! -z "$LIVEQ_TUNE" && -f "$LIVEQ_TUNE" ]]; then
        cat $LIVEQ_TUNE >> $tmp_params
      fi
      
      # print contents of parameters file
      print_file "$tmp_params"

      # generator location
      local genver=$(map_get_value "configuration/locations.map" "^$generator $version " ~4)
      if [[ "$genver" == "" ]] ; then genver="$generator/$versionCode" ; fi
      local PYTHIA8=$MCGENERATORS/$genver/$LCG_PLATFORM
      
      # hook for local custom installation
      if [[ $versionCode =~ custom ]] ; then
        PYTHIA8=$workd/local/pythia8-$versionCode
      fi

      if [ ! -d $PYTHIA8 ] ; then
        echo "ERROR: cannot find $generator $versionCode at"
        echo "       PYTHIA8 = $PYTHIA8"
        exit 1
      fi

      echo "PYTHIA8=$PYTHIA8"

      # compile generator wrapper
      make -B -C pythia8 HEPMC=$HEPMC PYTHIA8=$PYTHIA8 LHAPDF=$LHAPDF VERSION=${versionCode%%.*} VIVERSION=0

      if [[ "$?" != "0" ]] ; then
        echo "ERROR: fail to compile pythia8 wrapper code"
        exit 1
      fi

      if [[ "$versionCode" < "200" ]] ; then
        export PYTHIA8DATA=$PYTHIA8/xmldoc
      else
        # Pythia8 >= 200 new data path
        export PYTHIA8DATA=$PYTHIA8/share/Pythia8/xmldoc
      fi

      # prepare command to execute pythia8
      generatorExecString="$workd/pythia8/pythia8.exe $tmp_params $outfile"
      ;;

    "vincia" )
      # documentation:
      #   http://skands.web.cern.ch/skands/vincia/current/htmldoc/

      # Vincia version is combination of two versions:
      #   [vincia]_[pythia8]
      # Decode versions:
      local versionVincia=${version%_8.*}   # get Vincia version (remove '_8.*' from the back of string)
      local codeVincia=${versionVincia//.}  # get Vincia version without dots (replace X.Y.ZZ -> XYZZ)
      local versionPythia8=${version#*_8.}  # get Pythia 8 version
      
      # the 1.0.28 version which is installed at GENSER area
      #   /afs/cern.ch/sw/lcg/external/MCGenerators_hepmc2.06.05/vincia/1.0.28/x86_64-slc5-gcc43-opt
      # is compiled against pythia8 165 which is not binary
      # compatible with 170 and gives the segfault
      # re-point the 1.0.28_8.170 combination to the proper installation
      if [[ "$version" == "1.0.28_8.170" ]] ; then
        versionVincia=$version
      fi
      
      # build with pythia8 210:
      if [[ "$version" == "1.2.02_8.210" ]] ; then
        versionVincia="1.2.02_210"
      fi
      
      echo "versionVincia=$versionVincia"
      echo "versionPythia8=$versionPythia8"

      local VINCIA=$MCGENERATORS/vincia/$versionVincia/$LCG_PLATFORM

      if [ ! -d $VINCIA ] ; then
        echo "ERROR: cannot find $generator $versionVincia at"
        echo "       VINCIA = $VINCIA"
        exit 1
      fi

      echo "VINCIA=$VINCIA"
      
      local shareDir=$VINCIA/../share
      if [[ "$versionVincia" > "1.1.00" ]] ; then
        # new installation layout in versions > 1.1.00
        shareDir=$VINCIA
      fi
      if [[ ! "$versionVincia" < "1.2.00" ]] ; then
        # new installation layout in versions >= 1.2.00
        shareDir=$VINCIA/share/Vincia
      fi
      
      echo "shareDir=$shareDir"
      
      local tuneFile=""
      if [[ "$tune" == "default" ]] ; then
        # remove tune definition to let default tune run
        sed -e '/^Vincia:tuneFile/ d' -i $tmp_params
        tuneFile="[$tune]"
        
        # Vincia < 1.2.00 assume default tune files situated in tunes/ subdirectory
        if [[ "$versionVincia" < "1.2.00" ]] ; then
          ln -s $shareDir/tunes $tmpd/tunes
        fi
      else
        # tune -> tuneFile
        tuneFile=$shareDir/tunes/$tune.cmnd
        
        if [[ ! -s $tuneFile ]] ; then
          echo "ERROR: cannot find tune file:"
          echo "         $tuneFile"
          exit 1
        fi
      fi
      
      echo "tuneFile=$tuneFile"
      
      sed -e "s,%nevts%,$nevts," \
          -e "s,%seed%,$seed," \
          -e "s,%beam1%,$beam1," \
          -e "s,%beam2%,$beam2," \
          -e "s,%energy%,$energy," \
          -e "s,%tuneFile%,$tuneFile," \
          -e "s,%pTmin%,$pTmin,"        \
          -e "s,%pTmax%,$pTmax,"        \
          -e "s,%mHatMin%,$mHatMin,"    \
          -e "s,%mHatMax%,$mHatMax,"    \
          -i $tmp_params

      # print contents of parameters file
      print_file "$tmp_params"

      # compile generator wrapper
      local PYTHIA8=$MCGENERATORS/pythia8/$versionPythia8/$LCG_PLATFORM

      if [ ! -d $PYTHIA8 ] ; then
        echo "ERROR: cannot find pythia8 $versionPythia8 at"
        echo "       PYTHIA8 = $PYTHIA8"
        exit 1
      fi

      echo "PYTHIA8=$PYTHIA8"

      make -B -C pythia8 vincia.exe HEPMC=$HEPMC PYTHIA8=$PYTHIA8 LHAPDF=$LHAPDF VINCIA=$VINCIA VERSION=$versionPythia8 VIVERSION=$codeVincia

      if [[ "$?" != "0" ]] ; then
        echo "ERROR: fail to compile vincia wrapper code"
        exit 1
      fi

      if [[ "$versionPythia8" < "200" ]] ; then
        export PYTHIA8DATA=$PYTHIA8/xmldoc
      else
        # Pythia8 >= 200 new data path
        export PYTHIA8DATA=$PYTHIA8/share/Pythia8/xmldoc
      fi
      
      export VINCIADATA=$shareDir/xmldoc
      echo "VINCIADATA=$VINCIADATA"

      # prepare command to execute Vincia
      generatorExecString="$workd/pythia8/vincia.exe $tmp_params $outfile"
      ;;

    "herwig++" | "herwig++powheg" )
      # manual:
      #   http://arxiv.org/abs/0803.0883

      # check tune
      local tuneString
      tuneString=$(map_get_value "configuration/${generator}-tunes.map" "^${version} ${tune}" 2) || exit 1

      sed -e "s,%nevts%,$nevts," \
          -e "s,%seed%,$seed," \
          -e "s,%energy%,$energy," \
          -e "s,%beamA%,$beam1," \
          -e "s,%beamB%,$beam2," \
          -e "s,%outfile%,$outfile," \
          -e "s,%pTmin%,$pTmin,"        \
          -e "s,%pTmax%,$pTmax,"        \
          -e "s,%mHatMin%,$mHatMin,"    \
          -e "s,%mHatMax%,$mHatMax,"    \
          -e "s,%tune%,$tune,"    \
          -i $tmp_params

      # include tune file:
      sed -e "/#%tuneFile%/ r configuration/herwig++-${tune}.tune" -i $tmp_params

      # apply xsec fix (possible starting from version >= 2.6.0):
      if [[ ! "$version" < "2.6.0" ]] ; then
        sed -e "/#%xsecfix%/ r configuration/herwig++-xsec.fix" -i $tmp_params
      fi
      
      # print contents of steering file
      print_file "$tmp_params"

      local HERWIGPP=$MCGENERATORS/herwig++/$version/$LCG_PLATFORM

      if [ ! -d $HERWIGPP ] ; then
        echo "ERROR: cannot find $generator $version at"
        echo "       HERWIGPP = $HERWIGPP"
        exit 1
      fi
      
      echo "HERWIGPP=$HERWIGPP"
      
      # prepare command to execute herwig++
      local repo_opt="--repo"
      if [[ "$version" < "2.6.1" ]] ; then
        repo_opt="-r"
      fi
      
      generatorExecString="$HERWIGPP/bin/Herwig++ read $repo_opt $HERWIGPP/share/Herwig++/HerwigDefaults.rpo $tmp_params"
      ;;

#######################################################
    "herwig7" | "herwig7powheg" )

      # check tune
      local tuneString
      tuneString=$(map_get_value "configuration/${generator}-tunes.map" "^${version} ${tune}" 2) || exit 1

      sed -e "s,%nevts%,$nevts," \
          -e "s,%seed%,$seed," \
          -e "s,%energy%,$energy," \
          -e "s,%beamA%,$beam1," \
          -e "s,%beamB%,$beam2," \
          -e "s,%outfile%,$outfile," \
          -e "s,%pTmin%,$pTmin,"        \
          -e "s,%pTmax%,$pTmax,"        \
          -e "s,%mHatMin%,$mHatMin,"    \
          -e "s,%mHatMax%,$mHatMax,"    \
          -e "s,%tune%,$tune,"    \
          -i $tmp_params

      # include tune file:
      sed -e "/#%tuneFile%/ r configuration/herwig7-${tune}.tune" -i $tmp_params

      # restructuring of input files from version >= 7.1.0:
      # https://herwig.hepforge.org/trac/changeset/362f0d442736
      # https://arxiv.org/abs/1705.06919
      # https://herwig.hepforge.org/tutorials/faq/pdf.html#how-do-i-switch-off-isr-in-e-e-collisions
      local beamsetup71=$(map_get_value "configuration/beams.map" "^${generator} ${beam} " 5) || exit 1
      if [[ ! "$version" < "7.1.0" ]] ; then
        sed -e "/#%beamsetup71%/ a read $beamsetup71" \
            -e "s,LEPGenerator,EventGenerator," \
            -e "s,LHCGenerator,EventGenerator," \
            -e "s,LHCHandler:,EventHandler:," \
            -e "s,SimpleEE:,SubProcess:," \
            -e "s,SimpleQCD:,SubProcess:," \
            -e "s,QCDCuts:,MinBiasCuts:," \
            -e "/SplittingGenerator:ISR/ d" \
            -i $tmp_params
     fi
      
      # locate madgraph5 for matrix element computation
      local mg5ver=$(map_get_value "configuration/locations.map" "^$generator $version " 5 | cut -d= -f2)
      local mg5path=$MCGENERATORS/madgraph5amc/$mg5ver/$LCG_PLATFORM
      echo "mg5path=$mg5path"
      sed -e "s,%mg5path%,$mg5path," -i $tmp_params
      #TODO: check the `$mg5path` path exists if in use
      
      # print contents of steering file
      print_file "$tmp_params"

      local HERWIG7=$MCGENERATORS/herwig++/$version/$LCG_PLATFORM

      if [ ! -d $HERWIG7 ] ; then
        echo "ERROR: cannot find $generator $version at"
        echo "       HERWIG7 = $HERWIG7"
        exit 1
      fi
      
      echo "HERWIG7=$HERWIG7"
      
      # this is a fix to set correct paths
      # (installed settings database file contains local paths of build system)
      local repofix=$tmpd/HerwigDefaults-pathfix.rpo
      cp $HERWIG7/share/Herwig/HerwigDefaults.rpo $repofix
      sed -e "s,/build/jenkins/.*/MCGenerators,$MCGENERATORS," -i $repofix
      
      # fix for afs paths in cvmfs installations (seen in 7.0.0 and 7.0.1 installed to lcgcmt67c)
      if [[ "$HERWIG7" =~ cvmfs ]] ; then
        sed -e "s,/afs/cern.ch/sw/,/cvmfs/sft.cern.ch/," -i $repofix
      fi
      
      # prepare things for parallel execution
      
      # patch: run -> saverun
      sed -e 's,^run *[^ ]*,saverun generator,' -i $tmp_params
      cd $tmpd
      
      njobs=1
      if [[ "$USECORES" != "" ]] ; then njobs=$USECORES ; fi
      lastid=$((njobs-1))
      
      echo "INFO: njobs=$njobs, build step..."
      $HERWIG7/bin/Herwig build --maxjobs=$njobs --repo $repofix $tmp_params
      if [[ "$?" != "0" ]] ; then
        echo "ERROR: fail to do Herwig build step"
        exit 1
      fi
      
      for i in $(seq 0 $lastid) ; do
        echo "INFO: integrate step #$i..."
        $HERWIG7/bin/Herwig integrate --jobid=$i --repo $repofix generator.run &
      done
      
      # wait the completion of all integration jobs
      wait
      
      #TODO: check exit code of integrate
      
      # normal mode:
      # read .in
      # run .run
      
      # parallel mode:
      # build --maxjobs=n .in
      # integrate --jobid=k .run
      # mergegrids .run
      # run .run
      
      # prepare command to execute herwig7
      generatorExecString="$HERWIG7/bin/Herwig run --repo $repofix generator.run"
      ;;

    #######################################################
    "phojet" )
      # PHOJET reference:
      #   https://github.com/DPMJET/DPMJET/blob/master/docs/phoman_112.pdf
      #   (old) /cvmfs/sft.cern.ch/lcg/external/MCGenerators/phojet/1.10/share/doc/phoman5c.ps
      #
      # The code is now part of DPMJET:
      #   https://github.com/DPMJET/DPMJET
      #   https://cds.cern.ch/record/2115393/

      # check tune
      if [[ "$tune" != "default" ]] ; then
        echo "ERROR: Phojet unknown tune specification: $tune"
        echo "       only 'default' is now supported"
        exit 1
      fi

      # compile generator wrapper
      local PHOJET=$MCGENERATORS/phojet/$version/$LCG_PLATFORM
      echo "PHOJET=$PHOJET"

      if [ ! -d $PHOJET ] ; then
        echo "ERROR: $generator $versionCode - incorrect path"
        exit 1
      fi

      local PYTHIA6=$MCGENERATORS/pythia6/115a/$LCG_PLATFORM

      make -B -C phojet PYTHIA6=$PYTHIA6 HEPMC=$HEPMC PHOJET=$PHOJET

      if [[ "$?" != "0" ]] ; then
        echo "ERROR: fail to compile phojet wrapper code"
        exit 1
      fi

      # prepare seeds
      # allowed values:
      #  - ISD1,2,3: 2 <= seed <= 178
      #  - ISD4    : 2 <= seed <= 168
      local ISD1=$((2 + $seed % 160))
      local ISD2=$((2 + $seed / 160 % 160))
      local ISD3=$((2 + $seed / 160 / 160 % 160))
      local ISD4=$((2 + $seed / 160 / 160 / 160 % 160))
      echo "seed numbers [1,2,3,4] =[$ISD1, $ISD2, $ISD3, $ISD4]"

      sed -e "s,%nevts%,$nevts," \
          -e "s,%ISD1%,$ISD1," \
          -e "s,%ISD2%,$ISD2," \
          -e "s,%ISD3%,$ISD3," \
          -e "s,%ISD4%,$ISD4," \
          -e "s,%beam1%,$beam1," \
          -e "s,%beam2%,$beam2," \
          -e "s,%energy%,$energy," \
          -i $tmp_params

      # print contents of parameters file
      print_file "$tmp_params"

      # prepare command to execute phojet
      generatorExecString="$workd/phojet/phojet.exe $outfile < $tmp_params"
      ;;
#######################################################

    "epos" )
      # EPOS reference:
      #   http://arxiv.org/abs/1006.2967
      # CRMC:
      #   (website)      https://web.ikp.kit.edu/rulrich/crmc.html
      #   (source code)  https://devel-ik.fzk.de/wsvn/mc/crmc/   (user: download, password: download)
      
      # check beam support
      if [[ "$version" == "1.99.crmc.0.v3400" && "$beam" != "pp" ]] ; then
        echo "ERROR: $version of EPOS supports only pp beam"
        exit 1
      fi
      
      # get tune code
      local tuneCode
      tuneCode=$(map_get_value "configuration/epos-tunes.map" "^${tune} " 2) || exit 1
      echo "tuneCode=$tuneCode"
      
      local versionCode=${version/1.99.crmc./}
      versionCode=${versionCode#0.}
      echo "versionCode = $versionCode"

      local EPOS=$MCGENERATORS/crmc/$versionCode/$LCG_PLATFORM
      
      if [[ "$version" == "1.99.crmc.1.7.0-hi" ]]; then
        # $CRMC_ROOT is set in set_environment_alice()
        EPOS=$CRMC_ROOT
      fi
      
        # 1.5.4-hi is installed in private area
        if [[ "$version" == "1.99.crmc.1.5.4-hi" ]] ; then
          # TODO: Jan2019 started afs phaseout, repoint to some /cvmfs area
          EPOS=/afs/cern.ch/work/p/pkarczma/public/mcplots/software/crmc
        fi
      
      if [ ! -d $EPOS ] ; then
        echo "ERROR: cannot find $generator $version at"
        echo "       EPOS = $EPOS"
        exit 1
      fi

      echo "EPOS=$EPOS"
      
      # calculate beam momentum
      local mass
      mass=$(map_get_value "configuration/beams.map" "^${generator} ${beam} " 5) || exit 1
      local beamMom=$(echo $energy $mass | awk '{ecm=$1; mass=$2; e=ecm/2; mom=sqrt(e*e-mass*mass); print mom}')
      echo "ECM = $energy"
      echo "beam particle mass = $mass"
      echo "beam momentum = $beamMom"
      
      # prepare seed (minimum value is 1)
      local seedA=$((1 + $seed))

      sed -e "s,%nevts%,$nevts," \
          -e "s,%seedA%,$seedA," \
          -e "s,%beam1%,$beam1," \
          -e "s,%beam2%,$beam2," \
          -e "s,%beamMom%,$beamMom," \
          -e "s,%ecm%,$energy," \
          -e "s,%tune%,$tuneCode," \
          -e "s,%EPOS%,$EPOS," \
          -i $tmp_params

      # print contents of steering file
      print_file "$tmp_params"

      local beamOptions
      if [[ "$version" == "1.99.crmc.0.v3400" ]] ; then
        beamOptions="-b $beam1 -t $beam2"
      else
        beamOptions="-i $beam1 -I $beam2"
      fi
      
      export LD_LIBRARY_PATH=$EPOS/lib:$LD_LIBRARY_PATH
      
      #ldd $EPOS/bin/crmc
      #echo "INFO: actual CRMC version = $($EPOS/bin/crmc -v)"
      
      # prepare command to execute generator
      generatorExecString="$EPOS/bin/crmc -c $tmp_params -s $seedA -m $tuneCode -n $nevts -p $beamMom -P -$beamMom $beamOptions -f $outfile"
      ;;

    "sherpa" )
      # Sherpa reference:
      #   http://projects.hepforge.org/sherpa/doc/SHERPA-MC-1.2.2.html

      # TODO: check 0 <= $seed < 900 000 000

      # prepare seeds
      local seed1=$((1 + $seed % 30000))
      local seed2=$((1 + $seed / 30000))
      local seeds="$seed1 $seed2"
      
      # Sherpa >= 2.0.0 new random seeds format (four values instead of two)
      if [[ ! "$version" < "2.0.0" ]] ; then
        seeds="$seed1 $seed2 1 1"
      fi
      
      echo "seed numbers = $seeds"

      sed -e "s,%nevts%,$nevts," \
          -e "s,%seeds%,$seeds," \
          -e "s,%beam1%,$beam1," \
          -e "s,%beam2%,$beam2," \
          -e "s,%beamEn%,$beamEn," \
          -e "s,%pTmin%,$pTmin,"        \
          -e "s,%pTmax%,$pTmax,"        \
          -e "s,%mHatMin%,$mHatMin,"    \
          -e "s,%mHatMax%,$mHatMax,"    \
          -e "s,%tune%,$tune,"    \
          -i $tmp_params
      
      # tune file path
      local tunef=configuration/sherpa-${tune}.tune
      if [ ! -e $tunef ] ; then
        echo "ERROR: cannot find tune file $tunef"
        exit 1
      fi
      
      # include parameter (tune) file:
      sed -e "/#%tuneFile%/ r $tunef" -i $tmp_params

      # Sherpa >= 2.0.0 new parameter name for output file name and format
      if [[ ! "$version" < "2.0.0" ]] ; then
        sed -e "s,HEPMC2_GENEVENT_OUTPUT.*,EVENT_OUTPUT = HepMC_GenEvent[sherpa]," -i $tmp_params
      fi

      if [[ "$version" < "2.0.0" ]] ; then
        sed -e "/Switch off ME weight/d" -i $tmp_params
        sed -e "/CSS_MEWMODE/d" -i $tmp_params
      fi

      # Sherpa >= 2.2.0 new 'Order' syntax - example: Order_EW 0 -> Order (*,0)
      if [[ ! "$version" < "2.2.0" ]] ; then
        sed -e 's;Order_EW *\([0-9]*\);Order (*,\1);' -i $tmp_params
      fi
      
      # using lhapdf6 staring from 2.1.1, correct pdf set name
      if [[ "$version" > "2.1.0" ]] ; then
        sed -e 's,cteq6ll.LHpdf,cteq6l1,' -i $tmp_params
      fi
      
      # print contents of steering file
      print_file "$tmp_params"

      local SHERPA=$MCGENERATORS/sherpa/$version/$LCG_PLATFORM

      if [ ! -d $SHERPA ] ; then
        echo "ERROR: cannot find $generator $version at"
        echo "       SHERPA = $SHERPA"
        exit 1
      fi

      echo "SHERPA=$SHERPA"

      # make symlink to connect sherpa output HepMC file with pipe
      # this neccessary as there is no possibility to explicitly
      # specify full output file name in Sherpa
      ln -s $outfile $tmpd/sherpa.hepmc2g

      export LD_LIBRARY_PATH=$SHERPA/lib/SHERPA-MC:$LD_LIBRARY_PATH
      
      # prepare command to execute generator
      generatorExecString="$SHERPA/bin/Sherpa -f $tmp_params"
      
      # debug: print list of available pdf sets
      #generatorExecString="$SHERPA/bin/Sherpa SHOW_PDF_SETS=1"

      # do initialization run if Sherpa is run using AMEGIC++ matrix element generator
      if [[ "$(cat $tmp_params | weed | grep 'ME_SIGNAL_GENERATOR.*Amegic')" != "" ]] ; then
        echo "AMEGIC++ detected, do Sherpa initialization run ..."
        cd $tmpd

        $generatorExecString
        ./makelibs

        if [[ "$?" != "0" ]] ; then
          echo "ERROR: fail to do Sherpa initialization run"
          exit 1
        fi

        cd $workd
      fi
      ;;

    "alpgenpythia6" | "alpgenherwig" | "alpgenherwigjimmy" ) 
      # documentation:
      # http://mlm.home.cern.ch/mlm/alpgen/

      # which PS generator:
      local isPythia6=$(echo "$generator" | grep pythia | wc -l)
      local isHerwig=$(echo "$generator" | grep herwig | wc -l)
      local isJimmy=$(echo "$generator" | grep jimmy | wc -l)

      # version is combination of:
      #   [alpgen]_[PSgenerator]
      local versionAlpgenCode=$(echo $version | cut -d '_' -f 1)
      local fullversionPSCode=$(echo $version | cut -d '_' -f 2)
      
      # strip leading 6. from Pythia version
      local versionPSCode=$fullversionPSCode
      if [[ "$isPythia6" == "1" ]]; then
        versionPSCode=${versionPSCode/6./};
      fi
      
      # running behind Agile => check if version comb. is supported
      # Agile specs are e.g.: 
      #   AlpGenPythia6:422:2.1.3d
      #   AlpGenHerwig:6.510:2.1.3d
      #   AlpGenHerwigJimmy:6.510:2.1.3d

      # check Alpgen version: [when using agile genser libs, update otherwise...]
      # Alpgen:
      local tmp_alp_mever=$( agile-runmc --list-gens | grep AlpGen | cut -s -d ':' -f 3 | sort -u)
      if [[ $(echo $tmp_alp_mever | grep -wc $versionAlpgenCode) != "1" ]]; then
        echo "WARNING: Parton Shower cannot be added with the Alp. version $versionAlpgenCode"
        echo $(agile-runmc --list-gens | grep AlpGen ) 
        echo "The Alpgen version ${versionAlpgenCode} is not supported; please use: ${tmp_alp_mever}."
        exit 1
      fi
      
      local tmp_alp_psver=$( agile-runmc --list-gens | grep AlpGen | cut -s -d ':' -f 2 | sort -u)
      if [[ $(echo $tmp_alp_psver | grep -wc $versionPSCode) != "1" ]]; then
        echo "The PS generator version ${versionPSCode} is not supported; please use: ${tmp_alp_psver}."
        exit 1
      fi

      local AgilePSString="Pythia6"
      if [[ "$isHerwig" == "1" ]] ; then
        AgilePSString="Herwig"
      fi
      if [[ "$isJimmy" == "1" ]] ; then
        AgilePSString="HerwigJimmy"
      fi
      
      # check supplied tune string
      local tunePS
      tunePS=$(map_get_value "configuration/alpgen-tunes.map" "^${generator} ${tune} " 3) || exit 1
      local tuneME
      tuneME=$(map_get_value "configuration/alpgen-tunes.map" "^${generator} ${tune} " 4) || exit 1
      
      # check if tunePS supported
      if [[ "$isPythia6" == "1" ]]; then
        # try to resolve digital code for pythia6 tune name
        local tuneCode
        tuneCode=$(map_get_value "configuration/pythia6-tunes.map" "^${fullversionPSCode} ${tunePS} " 3) || exit 1
        
        # rebuild tune string:
        tune="${tuneCode}:${tuneME}"
      else
        # only 'default' tune is supported for alpgenherwig* generators
        if [[ "$tunePS" != "default" ]] ; then
          echo "ERROR: only 'default' tune is supported for $generator"
          exit 1
        fi
        
        # rebuild tune string:
        tune="${tunePS}:${tuneME}"
      fi
      
      local work_alpgen_dir=$workd/alpgen
      
      # fill the process card:
      $work_alpgen_dir/utils_alp/fill_alpgen_params.sh \
          $tmp_params \
          $beam1 $beamEn $seed $tune $specific $nevts \
          $pTmin $pTmax || exit 1
      
      # print contents of parameters file
      print_file "$tmp_params"
      
      # generate Alpgen unweighted events
      cd $tmpd
      $work_alpgen_dir/mcrun_alp/unweighted_driver.sh \
        $MCGENERATORS/alpgen/$versionAlpgenCode/$LCG_PLATFORM \
        $tmp_params \
        $work_alpgen_dir || exit 1
      
      # alpgen parameter parsing for the PS step
      source $work_alpgen_dir/utils_alp/section.sh
      local tmp_params_ps="$tmpd/alpgen_ps.params"
      section $tmp_params steerPS > $tmp_params_ps
      
      # running Agile+Rivet with the generated unweighted events:
      # form generator execution command: 1) Alpgen param. reading | 2) agile -> FIFO
      
      # TODO: fix problem with events vetoing with Alpgen version > 2.1.3d
      
      echo "WARNING: parton showering will run with Alpgen 2.1.3d"
      generatorExecString="section $tmp_params addPS | agile-runmc AlpGen$AgilePSString:$versionPSCode:2.1.3d --paramfile=$tmp_params_ps --out=$outfile"

      ;;

    "madgraph5amc" )
      # documentation:
      #   https://launchpad.net/mg5amcnlo
      #   http://madgraph.phys.ucl.ac.be/
      
      # check tune support
      local tuneCode
      tuneCode=$(map_get_value "configuration/madgraph5amc-tunes.map" "^${tune}\$" 1) || exit 1
      
      sed -e "s,%nevts%,$nevts," \
          -e "s,%seed%,$seed," \
          -e "s,%beam1%,$beam1," \
          -e "s,%beam2%,$beam2," \
          -e "s,%beamEn%,$beamEn," \
          -e "s,%pTmin%,$pTmin,"        \
          -e "s,%pTmax%,$pTmax,"        \
          -e "s,%mHatMin%,$mHatMin,"    \
          -e "s,%mHatMax%,$mHatMax,"    \
          -i $tmp_params
      
      # print contents of parameters file
      print_file "$tmp_params"
      
      # generator location
      local MG5AMC=$MCGENERATORS/$generator/$version/$LCG_PLATFORM
      echo "MG5AMC=$MG5AMC"
      
      if [ ! -d $MG5AMC ] ; then
        echo "ERROR: cannot find $generator $version at"
        echo "       MG5AMC = $MG5AMC"
        exit 1
      fi

      local pyver=244
      if [[ "$version" < "2.6.1" ]] ; then
        pyver=230
      elif [[ "$version" < "2.6.6" ]] ; then
        pyver=235
      elif [[ "$version" < "2.7.0" ]] ; then
        pyver=243
      fi
      
      # compile pythia8 generator wrapper (for hadronization)
      local PYTHIA8=$MCGENERATORS/pythia8/$pyver/$LCG_PLATFORM
      make -B -C pythia8 HEPMC=$HEPMC PYTHIA8=$PYTHIA8 LHAPDF=$LHAPDF VERSION=$pyver VIVERSION=0

      if [[ "$?" != "0" ]] ; then
        echo "ERROR: fail to compile pythia8 wrapper code"
        exit 1
      fi

      # Pythia8 >= 200 new data path
      export PYTHIA8DATA=$PYTHIA8/share/Pythia8/xmldoc
      
      
      # setup environment
      source $MG5AMC/madgraph5amcenv-genser.sh
      source $workd/alpgen/utils_alp/section.sh
      
      # run MG5:
      cd $tmpd
      section $tmp_params madgraph5amc > $tmp_params.mg
      mg5_aMC $tmp_params.mg
      
      # uncompress LHE
      local lhef="$(pwd)/MG5RUN/Events/run_01/unweighted_events.lhe"
      if [[ ${tune:0:3} == "nlo" ]] ; then
        lhef="$(pwd)/MG5RUN/Events/run_01/events.lhe"
      fi
      
      ls -l $(dirname $lhef)
      ls -l $lhef.gz
      gunzip $lhef.gz
      ls -l $lhef
      
      if [[ ! -e "$lhef" ]] ; then
        echo "ERROR: missing LHE output file: $lhef"
        exit 1
      fi
      
      # prepare command to execute pythia8 hadronization
      sed -e "s,%lhef%,$lhef," -i $tmp_params
      section $tmp_params pythia8 > $tmp_params.py8
      generatorExecString="$workd/pythia8/pythia8.exe $tmp_params.py8 $outfile"
      ;;
      
    "powheg-box" )
      # documentation:
      #   http://powhegbox.mib.infn.it
      
      sed -e "s,%nevts%,$nevts," \
          -e "s,%seed%,$seed," \
          -e "s,%beam1%,$beam1," \
          -e "s,%beam2%,$beam2," \
          -e "s,%beamEn%,$beamEn," \
          -e "s,%pTmin%,$pTmin,"        \
          -e "s,%pTmax%,$pTmax,"        \
          -e "s,%mHatMin%,$mHatMin,"    \
          -e "s,%mHatMax%,$mHatMax,"    \
          -e "s,%tune%,$tune,"    \
          -i $tmp_params
      
      # tune file path
      local tunef=configuration/powheg-box-${tune}.tune
      if [ ! -e $tunef ] ; then
        echo "ERROR: cannot find tune file $tunef"
        exit 1
      fi
      
      # include parameter (tune) file:
      sed -e "/#%tuneFile%/ r $tunef" -i $tmp_params
      
      # print contents of parameters file
      print_file "$tmp_params"
      
      # generator location
      local tag=$(map_get_value "configuration/locations.map" "^$generator $version " ~4)
      local PWB=$MCGENERATORS/$tag/$LCG_PLATFORM
      echo "PWB=$PWB"
      
      if [ ! -d $PWB ] ; then
        echo "ERROR: cannot find $generator $version at"
        echo "       PWB = $PWB"
        exit 1
      fi
      
      # compile pythia8 generator wrapper (for hadronization)
      local pyver=$(map_get_value "configuration/locations.map" "^$generator $version " 5 | cut -d= -f2)
      local PYTHIA8=$MCGENERATORS/pythia8/$pyver/$LCG_PLATFORM
      export PYTHIA8DATA=$PYTHIA8/share/Pythia8/xmldoc
      make -B -C pythia8 HEPMC=$HEPMC PYTHIA8=$PYTHIA8 LHAPDF=$LHAPDF VERSION=$pyver VIVERSION=0
      if [[ "$?" != "0" ]] ; then
        echo "ERROR: fail to compile pythia8 wrapper code"
        exit 1
      fi
      
      # setup environment
      source $PWB/powheg-box-v2env-genser.sh
      source $workd/alpgen/utils_alp/section.sh
      # export LHAPDF_DATA_PATH=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current:/cvmfs/sft.cern.ch/lcg/releases/LCG_96/MCGenerators/lhapdf/6.2.3/x86_64-centos7-gcc8-opt/share/LHAPDF
      
      # run Powheg-Box:
      cd $tmpd
      
      section $tmp_params powheg-box > powheg.input
      local pwgproc=$(section $tmp_params pw-process)
      local pwgbin=$PWB/bin/$pwgproc
      local pwgrun=$pwgbin
      if [[ "$version" != "r3043" ]] ; then
        # new installations have all binaries in the form of `lib/lib*.so`
        pwgbin=$PWB/lib/lib$pwgproc.so
        pwgrun=$(pwd)/run-$pwgproc.exe
        gfortran -o $pwgrun $pwgbin
      fi
      echo "pwgbin=$pwgbin"
      echo "pwgrun=$pwgrun"
      if [[ ! -e "$pwgbin" ]] ; then
        echo "ERROR: powheg process executable does not exist"
        exit 1
      fi
      
      # prepare subruns list
      section $tmp_params subruns > subruns.txt
      
      if [[ ! -s "subruns.txt" ]] ; then
        # "subruns" section is missing, set single subrun entry without parameters
        echo "main" > subruns.txt
      fi
      
      subrunsN=$(wc -l subruns.txt | awk '{print $1}')
      echo "INFO: total subruns = $subrunsN"
      
      # sanity check
      subrunsListNonUniq=$(cat subruns.txt | awk '{print $1}' | sort | uniq -d)
      if [[ "$subrunsListNonUniq" != "" ]] ; then
        echo "ERROR: duplicated names in subruns section: " $subrunsListNonUniq
        exit 1
      fi
      
      # prepare subruns cards
      cat subruns.txt | while read runname runvars ; do
        rundir=run-$runname
        #echo "subrun $rundir"
        mkdir $rundir
        cp powheg.input $rundir/
        
        for i in $runvars ; do
          key=${i%%=*}
          val=${i#*=}
          #echo "$rundir >>> $key >>> $val"
          sed -e "s,$key,$val," -i $rundir/powheg.input
        done
        
        #diff -u powheg.input $rundir/powheg.input
      done
      
      # perform subruns
      for i in run-*/powheg.input ; do
        d=$(dirname $i)
        cd $d
        echo "INFO: powheg subrun name=$d path=$(pwd)"
        
        cache_make_key powheg.input "$pwgbin"
        cache_get_data
        
        $pwgrun
        if [[ "$?" != "0" ]] ; then
          echo "ERROR: powheg exit with error exit code"
          exit 1
        fi
        
        # powheg-box break with "0" error code in case of problematic input steering file
        # verify the run was good by the check of output file presence
        if [[ ! -e "pwgevents.lhe" ]] ; then
          echo "ERROR: missing LHE output file: pwgevents.lhe"
          exit 1
        fi
        
        echo "INFO: powheg run finished"
        
        cache_set_data *.dat
        
        cd $tmpd
      done
      
      echo ""
      
      # list generated LHEF files and create array
      local lheFiles=( $(find $(pwd)/run-*/pwgevents.lhe -type f) )
      local lheFilesN="${#lheFiles[*]}"
      
      echo "INFO: subruns output list, total files = $lheFilesN"
      for i in ${lheFiles[*]} ; do
        $workd/pythia8/readlhe.sh $i
      done
      echo ""
      
      if [[ "$lheFilesN" != "$subrunsN" ]] ; then
        echo "ERROR: mismatch: lheFilesN=$lheFilesN, subrunsN=$subrunsN"
        exit 1
      fi
      
      # add LHEF files paths to steering file
      local lhef=${lheFiles[0]}
      sed -e "s,%lhef%,$lhef," -i $tmp_params
      
      for (( i=0; i < $lheFilesN; i++ )) ; do
        local lhef_s="lhef$((i+1))"
        local lhef_i=${lheFiles[i]}
        sed -e "s,%$lhef_s%,$lhef_i," -i $tmp_params
      done
      
      # TODO: check all .lhe files paths are inserted into pythia8 card
      
      # prepare command to execute pythia8 hadronization
      section $tmp_params pythia8 > $tmp_params.py8
      generatorExecString="$workd/pythia8/pythia8.exe $tmp_params.py8 $outfile"
      ;;
      
      "whizard" )
      # documentation:
      #   https://whizard.hepforge.org/
      
      sed -e "s,%nevts%,$nevts," \
          -e "s,%seed%,$seed," \
          -e "s,%beam1%,$beam1," \
          -e "s,%beam2%,$beam2," \
          -e "s,%beamEn%,$beamEn," \
          -e "s,%pTmin%,$pTmin,"        \
          -e "s,%pTmax%,$pTmax,"        \
          -e "s,%mHatMin%,$mHatMin,"    \
          -e "s,%mHatMax%,$mHatMax,"    \
          -e "s,%tune%,$tune,"    \
          -i $tmp_params
      
      # tune file path
      local tunef=configuration/whizard-${tune}.tune
      if [ ! -e $tunef ] ; then
        echo "ERROR: cannot find tune file $tunef"
        exit 1
      fi
      
      # include parameter (tune) file:
      sed -e "/#%tuneFile%/ r $tunef" -i $tmp_params
      
      # print contents of parameters file
      print_file "$tmp_params"
      
      # generator location
      local WHIZ=$MCGENERATORS/$generator/$version/$LCG_PLATFORM
      echo "WHIZ=$WHIZ"
      
      if [ ! -d $WHIZ ] ; then
        echo "ERROR: cannot find $generator $version at"
        echo "       WHIZ = $WHIZ"
        exit 1
      fi
      
      # make symlink to connect whizard output HepMC file with pipe
      # this neccessary as there is no possibility to explicitly
      # specify full output file name

      ln -s $outfile $tmpd/whizard.hepmc
      
      # setup environment
      # source $WHIZ/whizardenv-genser.sh
      # source $workd/alpgen/utils_alp/section.sh
      # export LHAPDF_DATA_PATH=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current:/cvmfs/sft.cern.ch/lcg/releases/LCG_96/MCGenerators/lhapdf/6.2.3/x86_64-centos7-gcc8-opt/share/LHAPDF
      cd $tmpd
      /cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv -p /cvmfs/sft.cern.ch/lcg/releases/LCG_97a_FCC_4/ whizard 2.8.5 x86_64-centos7-gcc8-opt > env.sh
      source env.sh

      # run Whizard:
      cd $tmpd
      wbin=$WHIZ/bin/whizard
      generatorExecString="$wbin $tmp_params"
      ;;

    * )
      echo "ERROR: unsupported generator: $generator"
      exit 1
      ;;
  esac


  # run generator
  # .param -> .hepmc
  echo "Run $generator $version ..."
  echo "generatorExecString = $generatorExecString"

  cd $tmpd

  eval $generatorExecString
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to run $generator $version"
    exit 1
  fi
  
  echo ""
  echo "Generator run finished successfully"
}


# === main ===

# run self-test
if [[ "$1" == "check" ]] ; then
  # check configuration files
  for i in configuration/*.map ; do
    echo "Checking for duplicate entries in $i ..."
    cat $i | weed | sort | uniq -d
  done
  
  # check the list of available generators versions
  for g in $(cat configuration/locations.map | weed | cut -d ' ' -f 1 | sort -u) ; do
    greal=${g/alpgen*/alpgen}
    greal=${greal/epos/crmc}
    greal=${greal/herwig++powheg/herwig++}
    greal=${greal/herwig7/herwig++}
    gpath="/cvmfs/sft.cern.ch/lcg/releases/LCG_*/MCGenerators/$greal*/*"
    
    echo "$g - $gpath:"
    echo -n "  "
    ls -1d $gpath 2>&- | cut -d / -f 9 | sort -u | xargs
  done
  
  exit 0
fi


# print usage info
if [[ "$#" != "12" ]] ; then
  echo "rungen.sh: tool for MC production"
  echo "Usage:"
  echo "  rungen.sh [mode] [beam] [process] [energy] [params] [specific] [generator] [version] [tune] [nevts] [seed] [hepmc] [outfile]"
  echo "        [mode] - local lxbatch boinc check"
  echo "        [beam] - $(cat configuration/rivet-histograms.map | weed | cut -d ' ' -f 1 | sort -u | xargs)"
  echo "     [process] - $(cat configuration/rivet-histograms.map | weed | cut -d ' ' -f 2 | sort -u | xargs)"
  echo "      [energy] - $(cat configuration/rivet-histograms.map | weed | cut -d ' ' -f 3 | sort -unr | xargs)"
  echo "      [params] - generator settings (generator-level cuts)"
  echo "    [specific] - generator-specific settings (for example, jet bins for alpgen)"
  echo "   [generator] - $(ls -1 configuration/*.params | cut -d / -f 2 | cut -d - -f 1 | sort -u | xargs)"
  echo "     [version]"
  echo "        [tune]"
  echo "       [nevts] - number of events in run"
  echo "        [seed] - initial seed of random number generator"
  echo "     [outfile] - output file of data generation in hepmc format"

  echo ""
  echo "Examples:"
  echo "  ./rungen.sh local ee    zhad         91.2 - - pythia6  6.424  p0      100 1234 /tmp/out.hepmc"
  echo "  ./rungen.sh local ee    zhad         91.2 - - pythia8  8.145  hoeth   100 8956 /tmp/out.hepmc"
  echo "  ./rungen.sh local ee    zhad         91.2 - - vincia   1.0.25_8.150 default 100 1234 /tmp/out.hepmc"
  echo "  ./rungen.sh local pp    mb-inelastic 7000 - - pythia6  6.424  ambt1   100 2345 /tmp/out.hepmc"
  echo "  ./rungen.sh local ppbar mb-inelastic 1800 - - pythia8  8.145  tune-4c 100 3456 /tmp/out.hepmc"
  echo "  ./rungen.sh local ppbar mb-inelastic 630  - - herwig++ 2.4.2  default 100 4567 /tmp/out.hepmc"
  echo "  ./rungen.sh local ppbar mb-inelastic 1800 - - sherpa   1.2.3  default 100 4567 /tmp/out.hepmc"
  echo "  ./rungen.sh local pp    mb-inelastic 7000 - - phojet   1.12a              default     100 123 /tmp/out.hepmc"
  echo "  ./rungen.sh local pp    winclusive   7000 10 2,5,1 alpgenpythia6      2.1.4_6.425  351-CTEQ5L 100 123 /tmp/out.hepmc"
  echo "  ./rungen.sh local pp    winclusive   7000 10 2,5,1 alpgenpythia6      2.1.3e_6.425 z2-CTEQ6L1 100 123 /tmp/out.hepmc"
  echo "  ./rungen.sh local pp    jets         7000 10 2,9,1 alpgenherwigjimmy  2.1.3e_6.520  default-CTEQ6L1 100 123 /tmp/out.hepmc"
  echo "  ./rungen.sh local pp    ue           7000 - - epos     1.99.crmc.0.v3400  default     100 123 /tmp/out.hepmc"
  echo "  ./rungen.sh local pp    zinclusive   7000 -,-,50,130 - madgraph5amc 2.6.2.atlas lo  100 1 /tmp/out.hepmc"
  exit 1
fi

echo "===> [rungen] $(date) [$@]"
echo ""

# setup execution environment
set_environment $1 $7 $8

run "$@"
