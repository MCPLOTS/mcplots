# Usage :
# python yodareader.py [file.yoda] [cmd]
# [file.yoda] - path to the yoda file to read
# [cmd] - xsec for the cross-section, nevts for the number of events, analyses for the list of analyses

import yoda
import sys

if  len(sys.argv) != 3:
  sys.exit("yodareader.py needs 2 parameters")

fpath = sys.argv[1]
cmd = sys.argv[2]

if cmd == 'xsec':
    aos = yoda.read(fpath, asdict=False, patterns='/RAW/_XSEC')
    print(aos[0].points()[0].x())
elif cmd == 'nevts':
    aos = yoda.read(fpath, asdict=False, patterns='/RAW/_EVTCOUNT')
    print(int(aos[0].val()))
elif cmd == 'analyses':
    #aos = yoda.read(fpath, asdict=False, patterns='/RAW/', unpatterns='/RAW/_XSEC,/RAW/_EVTCOUNT')
    aos = yoda.read(fpath, asdict=False, patterns='/RAW/')
    ans = set()
    for i in aos:
      a = i.path().split("/")[2]
      a = a.split(":")[0] #remove options from the analysis name
      ans.add(a)
    #TODO: add /raw/_xsec and /raw/_evtcount to unpatterns, it doesn't work for yoda1.9 as is
    ans.discard('_XSEC')
    ans.discard('_EVTCOUNT')
    for a in ans:
      print(a)
else:
    sys.exit("Error in the yodareader parameter")

