#!/bin/bash -e

if [[ "$#" != "3" ]] ; then
  echo "Usage:"
  echo "  ./merger.sh [yoda1] [yoda2] [destination]"
  echo " [yoda1,yoda2] the yoda files to be merged"
  echo " [destination] destination yoda file"
  exit 1
fi

mergeyodas () {
  # check metadata : these parameters must be the same
  for mcconfig in beam process energy params specific generator version tune rivet ; do
    if [[ $(cat $1 | grep "# MCPLOTS $mcconfig") != $(cat $2 | grep "# MCPLOTS $mcconfig") ]] ; then
      echo "ERROR : not possible to merge ; different MC configurations. Check MCPLOTS METADATA section."
      return 1
    fi
  done
  echo "MCPLOTS MCCONFIG OK"

  # check seeds
  s_source=$(cat $1 | grep "# MCPLOTS seed" | cut -d= -f2)
  s_dest=$(cat $2 | grep "# MCPLOTS seed" | cut -d= -f2)

  for i in $s_dest ; do
    if [[ $s_source == $i ]] ; then
      echo "ERROR : not possible to merge ; yodas share the same seed $i"
      return 1
    fi
  done
  echo "Seeds OK"
  
  s_rivet=$(cat $1 | grep "# MCPLOTS rivet" | cut -d= -f2)
  rivetenv="../rivetenv-$s_rivet.sh"
  echo "MCPLOTS rivetenv=$rivetenv"
  # sanity check, version string should be less than 10 chars
  if (( ${#s_rivet} >= 10 )) ; then
    echo "ERROR: rivet version check failed"
    return 1
  fi
  source $rivetenv

  # merge to the destination file
  rivet-merge -e $1 $2 -o $3

  # copy the MCPLOTS CONFIG section to the destination file
  # (all entries except of nevts,crosssection,seed)
  cat $1 | grep "^# MCPLOTS " | grep -vwE "nevts|crosssection|seed" >> $3
  
  # add nevts and xsec from the merged yoda file, and seeds
  echo "# MCPLOTS nevts=$(python yodareader.py $3 nevts)" >> $3
  echo "# MCPLOTS crossection=$(python yodareader.py $3 xsec)" >> $3
  echo "# MCPLOTS seed=$s_dest $s_source" >> $3
}

echo "SOURCE1 : $1"
echo "SOURCE2 : $2"
echo "DESTINATION : $3"

tmp_yoda=$(mktemp).yoda
mergeyodas $1 $2 $tmp_yoda

# check analyses in the merged yoda
scans=$(python yodareader.py $1 analyses)
dsans=$(python yodareader.py $tmp_yoda analyses)
if [[ $(echo $scans | wc -w) != $(echo $dsans | wc -w) ]] ; then
  echo "ERROR : the number of analyses in source and merged yodas don't match. Probably not reentrant-safe analysis is used. Check yodas."
  rm $tmp_yoda
  exit 1
fi

mv $tmp_yoda $3

echo "DONE"
