#!/bin/bash -e

if [[ "$#" != "2" ]] ; then
  echo "Usage:"
  echo "  ./run-multirun.sh [yoda] [destination]"
  echo " [yoda] the yoda files to be tested and merged if possible"
  echo " [destination] destination dat dir "
  exit 1
fi

merge () {

echo "SOURCE : $1"
echo "DESTINATION DIR : $2"

yname=$1
dest_dir=$2
cfile="../configuration/multirun-beam.map"

# setup rivet environment
s_rivet=$(cat $1 | grep "# MCPLOTS rivet" | cut -d= -f2)
rivetenv="../rivetenv-$s_rivet.sh"
echo "MCPLOTS rivetenv=$rivetenv"
# sanity check, version string should be less than 10 chars
if (( ${#s_rivet} >= 10 )) ; then
  echo "ERROR: rivet version check failed"
  return 1
fi
source $rivetenv

# source yoda analyses
an_yoda=$(python yodareader.py $yname analyses)
echo "Analyses in the source yoda : "$an_yoda

# delete comments, empty lines, double spaces, heading and trailing spaces from the input stream
map_parse () {
  sed -e '/^#.*/ d' -e '/^ *$/ d' -e 's,  *, ,g' -e 's,^ ,,g' -e 's, $,,g'
}

# list of analyses supported by the multirun merge
an_multirun=$(cat $cfile | map_parse | cut -d' ' -f5 | cut -d':' -f1 | sort | uniq)
echo "Analyses in the merging config file : "$an_multirun

# intersection of lists
an_merge=$(printf "%s\n" $an_yoda $an_multirun | sort | uniq -d)

if [[ $an_merge != '' ]] ; then
  echo "To be merged : "$an_merge
else
  echo "No multirun needed"
  exit 0
fi

# read mc config parameters from the file name
#read MC config parameters
beam=$(cat $1 | grep "# MCPLOTS beam" | cut -d= -f2)
energy=$(cat $1 | grep "# MCPLOTS energy" | cut -d= -f2)
process=$(cat $1 | grep "# MCPLOTS process" | cut -d= -f2)
cuts=$(cat $1 | grep "# MCPLOTS params" | cut -d= -f2)
specific=$(cat $1 | grep "# MCPLOTS specific" | cut -d= -f2)
generator=$(cat $1 | grep "# MCPLOTS generator" | cut -d= -f2)
version=$(cat $1 | grep "# MCPLOTS version" | cut -d= -f2)
tune=$(cat $1 | grep "# MCPLOTS tune" | cut -d= -f2)

#construct the name of the second yoda necessary for the merging (pp if PbPb arrived, PbPb if pp arrived)

beams="PbPb pp"
mergebeam=$(printf "%s\n" $beam $beams | sort | uniq -u)
mbeams="ppPbPb"

yname2=$dest_dir'/yoda-mc/'$mergebeam'-'$energy'/'$process'/'$generator'-'$version'-'$tune'/'$mergebeam'_'$energy'_'$process'_'$cuts'_'$specific'_'$generator'_'$version'_'$tune'.yoda'

echo "Looking for : $yname2"
if [ ! -f $yname2 ]; then
  echo "$mergebeam results are not ready yet; multirun merging not possible."
  exit 0
fi

#construct the name of the merged yoda and do the merging!
mergeyname="$dest_dir/yoda-merge/$mbeams-$energy/$process/$generator-$version-$tune/${mbeams}_${energy}_${process}_${cuts}_${specific}_${generator}_${version}_${tune}.yoda"
mkdir -p $(dirname $mergeyname)

tmp_yoda=${mergeyname}_tmp
echo

# check if it's better to add -m option
rivet-merge $yname $yname2 -o $tmp_yoda
mv $tmp_yoda $mergeyname

# create (rewrite) the index file
indexname=$dest_dir'/index/'$mbeams'_'$energy'_'$process'_'$cuts'_'$specific'_'$generator'_'$version'_'$tune'.txt'
rm -f $indexname
mkdir -p $(dirname $indexname)

echo "MULTIRUN MERGING IS DONE, PROCESSING INDEX"
for an in $an_merge ; do
  echo "Processing $an"
  cat $cfile | grep "$an" | while read l
  do
    hparams=( ${l} )
    if [[ ${#hparams[*]} != 8 ]] ; then
      continue
    fi
    if [[ ${hparams[0]} != $mbeams || ${hparams[1]} != $process || ${hparams[2]} != $energy || ${hparams[3]} != $cuts ]] ; then
      echo "ERROR : configuration in the map file doesn't match merged files: check ${hparams[4]}"
      exit 1
    fi
    # construct all parameters to be written in the index file (for mc and data hists)
    mcpath=${mergeyname#*$dest_dir/}
    analysis_id="$an"
    datapath="yoda-data/$analysis_id.yoda"

    # extract some parameters from the full hist ID
    opt=$(echo ${hparams[4]} | cut -d ':' -f2 -s)
    if [[ "$opt" == "" ]] ; then
      opt="-"
    fi
    hist_id=${hparams[5]}
    hist_observable=${hparams[6]}
    hist_cuts=${hparams[7]}

    # write everything to the index file
    echo "$mcpath mc $analysis_id $opt $hist_id $mbeams $energy $process $hist_observable $hist_cuts $specific $generator $version $tune" >> "$indexname"
    echo "$datapath data $analysis_id - $hist_id $mbeams $energy $process $hist_observable $hist_cuts  - - - -" >> "$indexname"
  done
done

}

merge $*
