#!/bin/bash -e

if [[ "$#" != "2" ]] ; then
  echo "Usage:"
  echo "  ./run-multirun.sh [yoda] [destination]"
  echo " [yoda] the yoda files to be tested and merged if possible"
  echo " [destination] destination dat dir "
  exit 1
fi

merge () {

echo "SOURCE : $1"
echo "DESTINATION DIR : $2"

yname=$1
dest_dir=$2
cfile="../configuration/multirun-energy.map"

# setup rivet environment
s_rivet=$(cat $1 | grep "# MCPLOTS rivet" | cut -d= -f2)
rivetenv="../rivetenv-$s_rivet.sh"
echo "MCPLOTS rivetenv=$rivetenv"
# sanity check, version string should be less than 10 chars
if (( ${#s_rivet} >= 10 )) ; then
  echo "ERROR: rivet version check failed"
  return 1
fi
source $rivetenv

# source yoda analyses
an_yoda=$(python yodareader.py $yname analyses)
echo "Analyses in the source yoda : "$an_yoda

# delete comments, empty lines, double spaces, heading and trailing spaces from the input stream
map_parse () {
  sed -e '/^#.*/ d' -e '/^ *$/ d' -e 's,  *, ,g' -e 's,^ ,,g' -e 's, $,,g'
}

# list of analyses supported by the multirun merge
an_multirun=$(cat $cfile | map_parse | cut -d' ' -f5 | cut -d':' -f1 | sort | uniq)
echo "Analyses in the merging config file : "$an_multirun

# intersection of lists
an_merge=$(printf "%s\n" $an_yoda $an_multirun | sort | uniq -d)

if [[ $an_merge != '' ]] ; then
  echo "To be merged : "$an_merge
else
  echo "No multirun needed"
  exit 0
fi

# read mc config parameters from the file name
#read MC config parameters
beam=$(cat $1 | grep "# MCPLOTS beam" | cut -d= -f2)
energy=$(cat $1 | grep "# MCPLOTS energy" | cut -d= -f2)
process=$(cat $1 | grep "# MCPLOTS process" | cut -d= -f2)
cuts=$(cat $1 | grep "# MCPLOTS params" | cut -d= -f2)
specific=$(cat $1 | grep "# MCPLOTS specific" | cut -d= -f2)
generator=$(cat $1 | grep "# MCPLOTS generator" | cut -d= -f2)
version=$(cat $1 | grep "# MCPLOTS version" | cut -d= -f2)
tune=$(cat $1 | grep "# MCPLOTS tune" | cut -d= -f2)

# create an array with different merging schemes (sqrts) as keys and corresponding analyses
declare -A enan
for an in $an_merge ; do
echo "Processing $an"
  while read str
    do
    if [[ $(echo $str | cut -c 1) != "#" ]] && { [[ $(echo $str | grep "$an ") != '' || $(echo $str | grep "$an:") != '' ]] ; } ; then
      en=$(echo $str | cut -d' ' -f 3)
      enan[$en]=${enan[$en]}"$an "
      break
    fi
    done < $cfile
done

# separate merging for each merging scheme
# and index!
for i in ${!enan[*]} ; do
  ens=$(echo $i | sed 's/:/ /g')
  echo "MERGING ENERGIES $ens : ${enan[$i]}"
  mergeens=$(printf "%s\n" $ens $energy | sort | uniq -u)
  myodas=''
  for men in $mergeens ; do
    myoda=$dest_dir/yoda-mc/$beam-$men/$process/$generator-$version-$tune/$beam\_$men\_$process\_$cuts\_$specific\_$generator\_$version\_$tune.yoda
    echo "Looking for : $myoda"
    if [ ! -f $myoda ]; then
      echo "(At least) $men GeV results are not ready yet; multirun merging not possible."
      myodas=''
      break
    fi
    myodas="$myodas $myoda"
  done
  echo $myodas
  if [[ $myodas == '' ]] ; then
    continue
  fi

  #construct the name of the merged yoda and do the merging!
  mergeyname="$dest_dir/yoda-merge/$beam-$i/$process/$generator-$version-$tune/${beam}_${i}_${process}_${cuts}_${specific}_${generator}_${version}_${tune}.yoda"
  mkdir -p $(dirname $mergeyname)

  tmp_yoda=${mergeyname}_tmp
  echo $tmp_yoda

  # today the following rivet-merge works. However, it is necessary to test it with more analyses (at the moment being only one is available)
  rivet-merge $yname $myodas -o $tmp_yoda -m $(echo ${enan[$i]} | sed 's/ /,/g')
  mv $tmp_yoda $mergeyname

  indexname=$dest_dir'/index/'$beam'_'$i'_'$process'_'$cuts'_'$specific'_'$generator'_'$version'_'$tune'.txt'
  rm -f $indexname
  mkdir -p $(dirname $indexname)

  echo "MULTIRUN MERGING $ens IS DONE, PROCESSING INDEX"
  for an in ${enan[$i]} ; do
  echo "Processing $an"
  cat $cfile | grep "$an" | while read l   # here better to check $an_ or an: (otherwise it may grep $an1... fortunately this probability is quite low)
  do
    hparams=( ${l} )
    if [[ ${#hparams[*]} != 8 || $(echo ${hparams[0]} | cut -c 1) == "#" ]] ; then
      continue
    fi
    if [[ ${hparams[0]} != "$beam" || ${hparams[1]} != "$process" || ${hparams[2]} != $i || ${hparams[3]} != $cuts ]] ; then
      echo "ERROR : configuration in the map file doesn't match merged files: check ${hparams[4]}"
      exit 1
    fi
    # construct all parameters to be written in the index file (for mc and data hists)
    mcpath=${mergeyname#*$dest_dir/}
    analysis_id="$an"
    datapath="yoda-data/$analysis_id.yoda"

    # extract some parameters from the full hist ID
    opt=$(echo ${hparams[4]} | cut -d ':' -f2 -s)
    if [[ "$opt" == "" ]] ; then
      opt="-"
    fi
    hist_id=${hparams[5]}
    hist_observable=${hparams[6]}
    hist_cuts=${hparams[7]}

    # write everything to the index file
    echo "$mcpath mc $analysis_id $opt $hist_id $beam $i $process $hist_observable $hist_cuts $specific $generator $version $tune" >> "$indexname"
    echo "$datapath data $analysis_id - $hist_id $beam $i $process $hist_observable $hist_cuts  - - - -" >> "$indexname"
  done
  echo "DONE"

done
done

}

merge $*
