// Pythia UserHook to look for events with two (or more) W and/or Z decays,
// and vetoing any that do not have one vector boson decaying hadronically
// and one decaying leptonically. (If the event contains more than two vector
// bosons, then the requirement is still that there be at least one
// leptonically decaying one and at least one hadronically decaying one.)
//
// Authors: Peter Skands <peter.skands@monash.edu>

class SemileptonicHook: public Pythia8::UserHooks {
  
 public:

  // Constructor.
  SemileptonicHook() {};
  
  // Destructor.
  ~SemileptonicHook() {}
  
  // Allow to veto event at process-level (after inspection of res decays).
  virtual bool canVetoProcessLevel() {return true;} 
  
  // compatibility with version < 140 (...const...)
  // TODO: verify if it works as expected
  //virtual bool doVetoProcessLevel(const Pythia8::Event& process) {
  //  std::cout << "SemileptonicHookConst" << std::endl;
  //  // call non-const function as defined starting from version 140
  //  return doVetoProcessLevel((Pythia8::Event&) process);
  //}
  
  virtual bool doVetoProcessLevel(Pythia8::Event& process) {
    //std::cout << "SemileptonicHook" << std::endl;
    bool hasLeptons = false;
    bool hasQuarks = false;
    // Find particles whose mothers are W (or Z) particles.
    for (int i=5; i<process.size(); ++i) {
      int iMot = process[i].mother1();
      if (process[iMot].idAbs() == 24 || process[iMot].idAbs() == 23) {
        if (process[i].isLepton()) hasLeptons = true;
        if (process[i].isQuark()) hasQuarks = true;        
      }
      // If we found (at least) one of each, do not veto. 
      if (hasQuarks && hasLeptons) return false;
    }
    // Else veto.
    return true;
  }
  
};
