#!/bin/bash

# $ find powheg.lhe/run-*/pwgevents.lhe | xargs -L1 ./readlhe.sh

lhef=$1

# LHE format: https://arxiv.org/abs/hep-ph/0609017

nevt=$(grep -c "<event" $lhef)
proclist=$(sed -n -e "/<init/,/<\/init/ p" $lhef | grep -v init | (read ; cat))
nproc=$(echo $proclist | wc -l)
xs1=$(echo $proclist | awk '{print $1}')

if (( $nproc != 1 )) ; then
  echo "ERROR: lhe file nproc != 1 - unsupproted!"
  exit 1
fi

echo "$nevt $xs1 $lhef"
