// Pythia8 wrapper (based on example main32.cc from Pythia8 distribution)
// Vincia wrapper (based on vincia01.cc example from Vincia distribution)

#ifdef VINCIA
#if VIVERSION < 2000
#include "Vincia.h"
#elif VIVERSION < 2300
#include "Vincia/Vincia.h"
using namespace Vincia;
#else
#include "Pythia8/Vincia.h"
#endif
#endif

#if VERSION < 180
#include "Pythia.h"
#include "HepMCInterface.h"
#elif VERSION < 200
#include "Pythia8/Pythia.h"
#include "Pythia8/Pythia8ToHepMC.h"
#elif VERSION < 230
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#else
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include "Pythia8/HeavyIons.h"
#define PY8_HAS_HI
#endif

#include "HepMC/GenEvent.h"
#include "HepMC/IO_GenEvent.h"
#ifdef HEPMC_HAS_UNITS
#include "HepMC/Units.h"
#endif

#include <memory>

#include "SemileptonicHook.h"
#if VERSION > 199
#include "Pythia8Plugins/PowhegHooks.h"
#endif

using namespace Pythia8;

#ifdef PY8_HAS_HI
// propagate heavy-ion record to hepmc event
// extracted from pythia8244\include\Pythia8Plugins\Pythia8Rivet.h:108, Pythia8Rivet::operator()
void hepmc_add_hiinfo(const Pythia& pythia, HepMC::GenEvent* evt)
{
#if VERSION < 300
    const Pythia8::HIInfo* hi = pythia.info.hiinfo;
#else
    const Pythia8::HIInfo* hi = pythia.info.hiInfo;
#endif
    
    if (hi) {
      HepMC::HeavyIon ion;
      ion.set_Ncoll_hard(hi->nCollNDTot());
      ion.set_Ncoll(hi->nAbsProj() +
                    hi->nDiffProj() +
                    hi->nAbsTarg() +
                    hi->nDiffTarg() -
                    hi->nCollND() -
                    hi->nCollDD());
      ion.set_Npart_proj(hi->nAbsProj() +
                         hi->nDiffProj());
      ion.set_Npart_targ(hi->nAbsTarg() +
                         hi->nDiffTarg());
      ion.set_impact_parameter(hi->b());
      
      evt->set_heavy_ion(ion);
    }
}
#endif


int main(int argc, char* argv[])
{
  // Check that correct number of command-line arguments
  if (argc != 3) {
    cerr << "ERROR: Unexpected number of command-line arguments.\n"
         << "       Expected one input and one output file name" << endl;
    return 1;
  }
  
  // Print command-line parameters
  cout << "Input steering file = " << argv[1] << "\n"
       << "Output HepMC file = " << argv[2] << endl;
  
  // Check that the input steering file exists
  ifstream is(argv[1]);
  if (!is) {
    cerr << "ERROR: input steering file does not found" << endl;
    return 1;
  }
  
  // Interface for conversion from Pythia8::Event to HepMC one
#if VERSION < 180
  HepMC::I_Pythia8 ToHepMC;
#else
  HepMC::Pythia8ToHepMC ToHepMC;
#endif

  // HepMC: do not complain about mother-daughter mismatches.
  // (As of 8.302, Vincia's antenna-like relationships not yet supported.)
  // TODO: rewise versions range once the issue is fixed in the next Pythia8/Vincia
#if VERSION > 299
  ToHepMC.set_print_inconsistency(false);
#endif
  
  //  ToHepMC.set_crash_on_problem();

  // Specify file where HepMC events will be stored
  HepMC::IO_GenEvent ascii_io(argv[2], std::ios::out);
  
  // Generator
  Pythia pythia;
  
  // add several custom commands to activate hooks from generator streering card file
  pythia.settings.addFlag("mcplots:loadSemileptonicHook", false);
  pythia.settings.addFlag("mcplots:loadPowhegHooks", false);
  
#ifdef VINCIA
  // get location of the Vincia configuration data
  // the VINCIADATA variable is supported by Vincia itself starting from version 1.0.25
  // and this part of code is neccessary for older versions < 1.0.25
  const char* xmldir = getenv("VINCIADATA");
  if (!xmldir) {
    cerr << "ERROR: VINCIADATA environment variable (path to xmldoc/) is not set" << endl;
    return 1;
  }
  
  // define plug-in
#if VIVERSION < 2205
  VinciaPlugin vincia(&pythia, argv[1], xmldir);
#else
  VinciaPlugin vincia(&pythia, xmldir);
  pythia.readFile(argv[1]);
#endif
  
  // activate debug print
  //pythia.readString("Vincia:verbose = 9");
#else
  
  // Read in commands from external file
  pythia.readFile(argv[1]);
#endif
  
  if (pythia.info.errorTotalNumber() > 0) {
    cerr << "ERROR: error proccessing steering file" << endl;
#if VERSION < 219
    pythia.info.errorStatistics(cerr);
#else 
    pythia.info.errorStatistics();
#endif
    return 1;
  }
  
  // Extract settings to be used in the main program
  int    nEvent    = pythia.mode("Main:numberOfEvents");
  bool   isLHE     = (pythia.mode("Beams:frameType") == 4);
  int    nShow     = 0;
  int    nAbort    = nEvent;
  bool   showA     = false;
  bool   showC     = true;
  
  // Set up plugin: SemileptonicHook
  if (pythia.flag("mcplots:loadSemileptonicHook")) {
    cout << "INFO: loading plugin SemileptonicHook" << endl;
#if VERSION < 300
    SemileptonicHook* hook1 = new SemileptonicHook;
#else
    std::shared_ptr<SemileptonicHook> hook1(new SemileptonicHook);
#endif
    pythia.setUserHooksPtr(hook1);
  }
  
  // Set up plugin: PowhegHooks
  if (pythia.flag("mcplots:loadPowhegHooks")) {
    cout << "INFO: loading plugin PowhegHooks" << endl;
#if VERSION < 200
    UserHooks* hook1 = 0; // dummy definition to avoid compilation error
    cout << "ERROR: Pythia8 version < 200, the plugin PowhegHooks is not available." << endl;
    return 1;
#elif VERSION < 300
    PowhegHooks* hook1 = new PowhegHooks;
#else
    std::shared_ptr<PowhegHooks> hook1(new PowhegHooks);
#endif
    pythia.setUserHooksPtr(hook1);
  }
  
  // initialization
#ifdef VINCIA
#if VIVERSION < 2000
  if (! pythia.init()) return 1;
#elif VIVERSION < 2300
  // TODO: check the init status
  vincia.init();
#else
  if (! vincia.init()) return 1;
#endif
#else
  if (! pythia.init()) return 1;
#endif
  
  // List settings
  if (showC) pythia.settings.listChanged();
  if (showA) pythia.settings.listAll();
  
  // List particle data
  // suppress changed particle data printing for LHE input
  if (showC && !isLHE) pythia.particleData.listChanged();
  if (showA) pythia.particleData.listAll();
  
  // for now, make use of subruns capability only for LHEF
  int nSubruns = pythia.mode("Main:numberOfSubruns");
  if (nSubruns < 1) nSubruns = 1;
  if (!isLHE) nSubruns = 1;
  
  // LHEF and subruns processing:
  //   https://pythia.org/latest-manual/MainProgramSettings.html#section5
  //   https://pythia.org/latest-manual/BeamParameters.html#section0
  //   https://pythia.org/latest-manual/LHEF.html#section3
  //   https://gitlab.com/Pythia8/releases/-/blob/master/examples/main43.cc
  //   https://gitlab.com/Pythia8/releases/-/blob/master/examples/main43.cmnd
  
  // subruns loop
  for (int s = 1; s <= nSubruns; ++s) {
    if (isLHE && (nSubruns > 1)) {
      // Read in subrun-specific data from external file.
      pythia.readFile(argv[1], s);
      
      // Initialization.
      pythia.init();
      
      // Print name of Les Houches Event File.
      string lheFile = pythia.settings.word("Beams:LHEF");
      cout << "INFO: begin subrun " << s << " of " << nSubruns
           << " lheFile = " << lheFile
           << endl;
    }
  
  // Begin event loop
  int nPace  = max(1, nEvent / max(1, nShow) );
  int iAbort = 0;
  
  for (int i = 0; i < (nEvent + iAbort); ++i) {
    if (nShow > 0 && (i % nPace == 0))
      cout << " Now begin event " << i << endl;
    
    // Generate event
    if (!pythia.next()) {
      
      if (pythia.info.atEndOfFile()) {
        // end of input LHEF file is reached
        cout << "INFO: stop event generation since reached end of Les Houches Event File" << endl;
        break;
      }
      
      // record failure and skip the event
      iAbort++;
      
      // First few failures write off as "acceptable" errors, then quit.
      if (iAbort < nAbort) continue;
      
      cerr << "ERROR: abort event generation due to high number of errors" << endl; 
      return 1;
    }
    
    // Construct new empty HepMC event
    #ifdef HEPMC_HAS_UNITS
      HepMC::GenEvent* hepmcevt = new HepMC::GenEvent(HepMC::Units::GEV, HepMC::Units::MM);
    #else
      HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
    #endif
    
    // fill HepMC event
    ToHepMC.fill_next_event(pythia, hepmcevt);
    
    #ifdef PY8_HAS_HI
    hepmc_add_hiinfo(pythia, hepmcevt);
    #endif
    
    // write the HepMC event to file
    ascii_io << hepmcevt;
    delete hepmcevt;
  }
  }
  
  // print event generation statistics
#if VERSION < 200
  pythia.statistics();
#else
  pythia.stat();
#endif
  
#ifdef VINCIA
  vincia.printInfo();
#endif
  
  return 0;
}
