#!/bin/bash

function dosql () {
  mysql --skip-column-names -u mcplots mcplots
}

function prepare_table () {
  # NOTE:
  #   prepare_histograms_2023() format: new field 'histid'
  #   prepare_histograms() format: the histid is always empty string (compatibility with the 2023 format)
  
  # clean table
  echo "DROP TABLE IF EXISTS histograms;"
  echo "CREATE TABLE histograms"
  echo "("
  echo "  id         MEDIUMINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,"
  echo "  fname      CHAR(200) NOT NULL,"
  echo "  "
  echo "  type       CHAR(4)  NOT NULL,"
  echo "  process    CHAR(20) NOT NULL,"
  echo "  observable CHAR(40) NOT NULL,"
  echo "  tune       CHAR(30) NOT NULL,"
  echo "  experiment CHAR(20) NOT NULL,"
  echo "  reference  CHAR(40) NOT NULL,"
  echo "  optid      CHAR(40) NOT NULL,"
  echo "  histid     CHAR(40) NOT NULL,"
  echo "  beam       CHAR(20) NOT NULL,"
  echo "  energy     SMALLINT UNSIGNED NOT NULL,"
  echo "  cuts       CHAR(40) NOT NULL,"
  echo "  generator  CHAR(20) NOT NULL,"
  echo "  version    CHAR(20) NOT NULL,"
  echo "  INDEX (process, observable)"
  echo ");"
}

# TODO: obsolete, old db file layout, to be removed
# this function iterates over histograms directory structure
# and prints SQL commands to build the table which reflect
# the directory structure
function prepare_histograms() {
  local src=$1
  
  # records counter
  local i=0
  
  echo "LOCK TABLES histograms WRITE;"
  echo ""
  
  # iterate over all *.dat files to add new records to the table
  find $src -type f -name '*.dat' | while read fname ; do
    # extract all metadata fields from file path/name
    vals=( ${fname//// } )         # replace all '/' by ' ' in $fname and prepare array
    
    local nvals="${#vals[*]}"
    
    # do check on file path structure:
    if [[ "$nvals" != "9" && "$nvals" != "7" ]] ; then
      if [[ "$nvals" == "10" && ("${fname}" =~ "alpgen") ]] ; then
        # Alpgen NpX histogram, skip
        continue
      fi
      
      { echo "ERROR: incorrect path to histogram:"
        echo "         $fname"
        echo ""
        echo "       should be in form:"
        echo "         [data] dat/beam/process/observable/cuts/energy/reference.dat"
        echo "         [mc]   dat/beam/process/observable/cuts/energy/generator/version/tune.dat"
        echo "         [mc]   dat/beam/process/observable/cuts/energy/alpgen*/version/specific/tune.dat"
      } 1>&2
      exit 1
    fi
    
    beam="${vals[1]}"
    process="${vals[2]}"
    observable="${vals[3]}"
    cuts="${vals[4]}"
    energy="${vals[5]}"
    
    if [[ "$nvals" == "9" ]] ; then
      # this is MC .dat file
      type="mc"
      reference=""
      experiment=""
      generator="${vals[6]}"
      version="${vals[7]}"
      tune="${vals[8]%.dat}"         # remove '.dat' from the back of the string
    else
      # this is DATA .dat file
      type="data"
      reference="${vals[6]%.dat}"
      experiment="${reference%%_*}"  # remove first '_' character and everything after
      generator=""
      version=""
      tune=""
    fi
    
    # the '' is empty string for histid field, compatibility with 2023 layout
    echo "INSERT INTO histograms VALUES " \
         "( NULL, '$fname', '$type', '$process', '$observable', '$tune', '$experiment'," \
         " '$reference', '', '', '$beam', $energy, '$cuts', '$generator', '$version' );"
    
    # print progress and increase counter
    let i++
    if (( i % 10000 == 0 )) ; then
      echo "[$(date)] ${i%000}k: $fname" >&2
    fi
  done || return 1
  
  echo "UNLOCK TABLES;"
}

# new file layout format 2023
# there is subdirectory index/ with full record for each histogram .dat file
function prepare_histograms_2023() {
  local src=$1
  
  # records counter
  local i=0
  
  echo "LOCK TABLES histograms WRITE;"
  echo ""
  
  # iterate over all index/*.txt files to add new records to the table,
  # example:
  #CMS_2021_I1847230-pp-8000/z1j-jj.dR-cms2021-zj-d12-x01-y01/pythia8-8.240-tune-4c.dat mc CMS_2021_I1847230 MODE=ZJet d12-x01-y01 pp 8000 z1j jj.dR cms2021-zj - pythia8 8.240 tune-4c
  #CMS_2021_I1847230-pp-8000/z1j-j.pt_j.pt-cms2021-zj-d09-x01-y01/CMS_2021_I1847230.dat data CMS_2021_I1847230 - d09-x01-y01 pp 8000 z1j j.pt_j.pt cms2021-zj - - - -

  find $src/index -type f -name '*.txt' | xargs cat | while read line ; do
    vals=( $line )
    local nvals="${#vals[*]}"
    fname="$src/${vals[0]}"
    type="${vals[1]}"
    reference="${vals[2]}"
    experiment="${reference%%_*}"  # remove first '_' character and everything after
    optid="${vals[3]}"
    histid="${vals[4]}"
    beam="${vals[5]}"
    energy="${vals[6]}"
    process="${vals[7]}"
    observable="${vals[8]}"
    cuts="${vals[9]}"
    specific="${vals[10]}"
    generator="${vals[11]}"
    version="${vals[12]}"
    tune="${vals[13]}"
    
    # the values '-' means absent value, set parameter to empty string
    if [[ "$optid" == "-" ]] ; then optid="" ; fi
    if [[ "$specific" == "-" ]] ; then specific="" ; fi
    if [[ "$generator" == "-" ]] ; then generator="" ; fi
    if [[ "$version" == "-" ]] ; then version="" ; fi
    if [[ "$tune" == "-" ]] ; then tune="" ; fi
  
    # do check on file path structure:
    if [[ "$nvals" != "14" ]] ; then
      { echo "ERROR: incorrect index entry size=$nvals, while expected size is 14:"
        echo "         $line"
      } 1>&2
      
      # TODO: enforce the error exit once an empty 'histid' is solved
      continue
      #exit 1
    fi
    
    if [[ "$specific" != "" && ("$generator" =~ "alpgen") ]] ; then
      # Alpgen NpX histogram, skip
      continue
    fi
    
    # TODO: for now only alpgen subsamples are present in the index,
    #       populate the index with extra entry on the merge of alpgen subsamples
    
    echo "INSERT INTO histograms VALUES " \
         "( NULL, '$fname', '$type', '$process', '$observable', '$tune', '$experiment'," \
         " '$reference', '$optid', '$histid', '$beam', $energy, '$cuts', '$generator', '$version' );"
    
    # print progress and increase counter
    let i++
    if (( i % 10000 == 0 )) ; then
      echo "[$(date)] ${i%000}k: $fname" >&2
    fi
  done || return 1
  
  echo "UNLOCK TABLES;"
}

# each MC run produces it's own copy of data histograms
# which gives multiple duplicated entries after the accumulation
# = the duplicated entries are to be deleted
function drop_dup_2023 () {
  # - query the list of all data histograms
  #     the histogram path key is "fname,histid"
  #     as the .yoda output contents multiple histograms per single .yoda file
  # - drop first fname component (ex.: dat/ dat.2773/) and resort with priority of lower id value (was inserted into table earlier)
  # - show the list of duplicated histogram IDs
  echo "SELECT fname,histid,id FROM histograms WHERE type = 'data' ORDER BY 1,2,3" |  dosql | \
    cut -d/ -f2- | sort -k1,1 -k2,2 -k3n,3n | \
      awk 'BEGIN{s="";} {if(s!=$1$2) s=$1$2;  else print $3;}' > listdbdatadup.txt
  
  echo "INFO: [$(date)] total duplicates: $(wc -l listdbdatadup.txt)"
  echo -n "Running cleanup "
  
  # TODO: group with `id IN (list,)` to speedup the cleanup
  cat listdbdatadup.txt | while read i ; do
    echo "DELETE FROM histograms WHERE id = $i;"
    let "n+=1"
    if (( n%10000 == 0 )) ; then echo -n "." >&2 ; fi
  done | dosql
  
  rm -f listdbdatadup.txt
  
  echo "INFO: [$(date)] cleanup done"
}

# optimize `histograms` table structure by
# parsing output of MySQL ANALYSE() procedure to
# convert most of fields from CHAR() to ENUM() type
# and reduce disk usage/improve access speed
function optimize_db () {
  echo "SELECT * FROM histograms PROCEDURE ANALYSE()" | \
  dosql | \
  while read line ; do
    # field name
    local f=$(echo "$line" | cut -f 1)
    f=$(echo $f | cut -d '.' -f 3)
    
    # optimised type
    local t=$(echo "$line" | cut -f 10)
    
    # skip `id` and `energy` columns
    if [[ "$f" == "id" || "$f" == "energy" ]] ; then
      continue
    fi
    
    # keep `fname` column as CHAR type
    if [[ "$f" == "fname" ]] ; then
      t=${t/VARCHAR/CHAR}
    fi
    
    echo "MODIFY $f $t"
  done | \
  (
    echo -n "ALTER TABLE histograms "
    paste -s -d ,
  ) | \
  dosql
}


# path to histograms root directory
# default is dat/
dirlist=${*:-"dat"}
echo "Input path list: $dirlist"

# path to SQL script:
sql=$(mktemp)
echo "[$(date)] Generating SQL script ($sql)..."
# re-create new table
prepare_table > $sql

for histdir in $dirlist ; do
echo "Input path: $histdir"

if ! test -d $histdir ; then
  echo "ERROR: input path directory $histdir does not exist"
  exit 1
fi

# check for new file layout format
if ! test -d $histdir/index ; then
  echo "ERROR: index directory $histdir/index does not exist"
  exit 1
fi

# generate SQL script:
prepare_histograms_2023 $histdir >> $sql

# check success:
if [[ "$?" != "0" ]] ; then
  echo "ERROR: fail to prepare SQL script" >&2
  exit 1
fi
done

echo "[$(date)] SQL script ready, size= $(du -h $sql)"

# update database:
echo "[$(date)] Updating database..."
dosql < $sql

# and check the update is successful:
if [[ "$?" != "0" ]] ; then
  echo "ERROR: fail to update database" >&2
  exit 1
fi

# database updated, remove SQL script
rm -f $sql


echo "[$(date)] Drop duplicates..."
drop_dup_2023

# optimize database
echo "[$(date)] Optimizing database..."
optimize_db

if [[ "$?" != "0" ]] ; then
  echo "ERROR: fail to optimize database" >&2
  exit 1
fi

# clear cache:
echo "[$(date)] Clearing cache..."
# ensure cache/ could be written
mkdir -p cache
chmod a+w cache
# rename plots cache directory and run remove in background
cache0="cache/plots.obsolete.$RANDOM"
mkdir -p cache/plots
mv cache/plots $cache0
rm -rf $cache0 &
# create new empty plots cache
mkdir -p cache/plots
chmod a+w cache/plots

echo "[$(date)] Update finished successfully"
