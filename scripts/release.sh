#!/bin/bash -ev
# -e: stop script execution on command error exit code
# -v: print commands before execution to see the progress

pool="/home/mcplots/pool"
release="/home/mcplots/release"

echo "[$(date)] Building release ..."

# ===== prepare dev release
reldate="2022-10-12"
rel1="$release/dev.$reldate"
mkdir $rel1
cd $rel1

# copy "base" dataset (BOINC production)
cp -al --remove-destination $pool/2390/dat .

# add runs which are missig in BOINC production:
#  - Alpgen runs (segfault on 32 bits nodes)
# merge.sh $release/2390.alpgen/dat $release/2390.alpgen.combined/dat
cp -al --remove-destination $release/2390.alpgen.combined/dat .

# fix for rivet-histograms.map names typo of ATLAS_2014_I1282441:
mv dat/pp/mb-inelastic/phi_y/atlas2014-phiKK tmp_pt
mv dat/pp/mb-inelastic/phi_pt/atlas2014-phiKK tmp_y
mv tmp_pt dat/pp/mb-inelastic/phi_pt/atlas2014-phiKK
mv tmp_y dat/pp/mb-inelastic/phi_y/atlas2014-phiKK

#exit

# ===== public release
rel2="$release/pub.$reldate"
mkdir $rel2
cd $rel2

# the public release is based on rel1:
cp -al --remove-destination $rel1/dat .

# heavyion postponed to the next release (due to issues with rivet 3.1.0)
rm -rf dat/PbPb

# MC_TTBAR_FBA (remove debug plot with various cuts)
rm -rf dat/{pp,ppbar}/top-mc/xsec/mc-cut

# Alpgen+Herwig/AGILe has an issue with XS
rm -rf dat/*/*/*/*/*/alpgenherwigjimmy

# the highest jets multiplicity subsample ("4,0") is failed and absent in some of "jets" runs
# which leads to histograms with incomplete data
# remove such histograms
find dat/*/jets/*/*/*/alpgen*/*/*.dat | while read f ; do
  dn=$(dirname $f)
  bn=$(basename $f)
  f40="$dn/4,0/$bn"
  if [[ ! -e "$f40" ]] ; then
    rm $f
  fi
done
