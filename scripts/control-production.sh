#!/bin/bash -e

function show_usage () {
  echo "Usage: control-production.sh <command>"
  echo "  commands:"
  echo "    state          - show production status"
  echo "    set <revision> - enable #revision for production"
  echo "    inputq         - show boinc input queue state"
  echo "    delinq <limit> - clean boinc input queue, with <limit> is the maximum amount of jobs to remove"
}

function dosql () {
  mysql --skip-column-names -u mcplots mcplots
}

function show_state () {
  echo -n "Current production revision: "
  echo "select revision from production where boinc = 1" | dosql | xargs
  
  echo -n "Revisions available for production (last 100): $available"
  echo "select revision from production order by 1 desc limit 100" | dosql | xargs
}

function set_prod () {
  declare -i revision=$1
  
  echo "Setting BOINC production revision to $revision ..."
  
  echo "update production set boinc = 0 where boinc != 0" | dosql
  echo "update production set boinc = 1 where revision = $revision" | dosql
}

function boinc_input_queue_status () {
  local inputQueue="/t4t/input"
  local inputQueueList=$(find $inputQueue -ignore_readdir_race -name '*.run')
  local inputQueueLen=$(echo "$inputQueueList" | wc -w)
  
  echo "T4T input queue:"
  echo "  path = $inputQueue"
  echo "  size = $inputQueueLen"
  
  # breakdown by job revision number
  echo "$inputQueueList" | sed -e 's,^.*/,,' | cut -d- -f1 | sort | uniq -c | while read njobs revision ; do
    echo "  revision=$revision njobs=$njobs"
  done
}

function boinc_input_queue_clean() {
  declare -i nlimit=$1
  echo "Cleaning input queue, nlimit = $nlimit"
  
  if (( nlimit == 0 )) ; then return 0 ; fi
  
  # prepare list of files in the input queue
  local inputQueue="/t4t/input"
  local inputQueueList=$(find $inputQueue -ignore_readdir_race -name '*.run')
  local inputQueueLen=$(echo "$inputQueueList" | wc -w)
  
  echo "T4T input queue:"
  echo "  path = $inputQueue"
  echo "  size = $inputQueueLen"
  
  # remove files from the queue
  local ntot=0
  
  for f in $inputQueueList ; do
    if (( ntot == nlimit )) ; then echo "" ; break ; fi
    
    #echo "next item = $f"
    if rm -f $f ; then
      echo $f >> droplist1.txt
    fi
    
    let ++ntot
    if (( ntot % 100 == 0 )) ; then echo -n "." ; fi
  done
  
  if (( ntot == 0 )) ; then return 0 ; fi
  
  # record the number of removed jobs
  # (with breakdown by job revision number)
  cat droplist1.txt | sed -e 's,^.*/,,' | cut -d- -f1 | sort | uniq -c | while read nremoved revision ; do
    echo "revision = $revision, nremoved = $nremoved"
    echo "INSERT INTO telemetry VALUES ( NULL, NOW(), $revision, 'submit-to-boinc', -$nremoved );" | dosql
  done
  
  cat droplist1.txt >> droplist2.txt
  rm -f droplist1.txt
  
  # TODO: remove deleted jobs from DB 'jobs' and 'runs' tables
  #
  #
  
  
  # print last records of jobs submits telemetry
  echo "select * from telemetry where id > 1560000 and param = 'submit-to-boinc'" | mysql -u mcplots mcplots --table
}


case $1 in
  "state" ) show_state ;;
  "set"   ) set_prod $2 ;;
  "inputq") boinc_input_queue_status ;;
  "delinq") boinc_input_queue_clean $2 ;;
  *       ) show_usage ;;
esac
