#!/bin/bash -e

# purpose of this script is to aid identification and metadata correction of jobs with anomalous 'cpuusage' values:
#   cpu_time > 10*real_time

function dosql() {
  mysql --no-defaults --skip-column-names -u mcplots mcplots $@
}

function guisql() {
  dosql --table --column-names $@
}

function histo() {
  local binw=1000
  sort -n | awk -v binw=$binw '{h[int($1/binw)]++} END {for (i in h) {print binw*i, "=", h[i]}}' | sort -n
}

function median() {
  local ordlist=$(sort -n)
  local n=$(echo "$ordlist" | wc -w)
  local midn=$(( (n+1)/2 ))
  local midr=$(( 2-(n%2) ))
  local midlim=$(( midn + midr - 1 ))
  local midlist=$(echo "$ordlist" | head -n $midlim | tail -n $midr | xargs)
  
  #echo "ordlist=" $ordlist
  #echo "n=$n"
  #echo "midn=$midn"
  #echo "midr=$midr"
  #echo "midlim=$midlim"
  #echo "midlist=$midlist"
  
  echo "$midlist" | awk '{if(NF==1) print $1; else print ($1+$2)/2;}'
}


#echo "1 5 3 9 3 4" | tr ' ' '\n' | median

#echo "select * from jobs where cpuusage > 1000000" | mysql -umcplots mcplots > cpuusage1M.txt

#echo "select *, cpuusage/timestampdiff(second,subdate,retdate) from jobs where cpuusage > 10*timestampdiff(second,subdate,retdate)" | mysql -umcplots mcplots --table


ids=$(echo "select id from jobs where cpuusage > 100*timestampdiff(second,subdate,retdate)" | dosql)
idslist=$(echo "$ids" | paste -s -d ,)

echo "idslist=$idslist"

if [[ "$idslist" == "" ]] ; then
  echo "INFO: no problems detected, exit"
  exit 0
fi

echo "select *, cpuusage/timestampdiff(second,subdate,retdate) from jobs where id in ($idslist)" | guisql
echo ""

rm -f fixjobcpu-proposal.sql

for i in $ids ; do
  # get job details
  d=$(echo "select retdate from jobs where id=$i" | dosql)
  rid=$(echo "select runid from jobs where id=$i" | dosql)
  rev=$(echo "select revision from jobs where id=$i" | dosql)
  exitcode=$(echo "select exitcode from jobs where id=$i" | dosql)
  echo "===> id=$i runid=$rid revision=$rev retdate=$d exitcode=$exitcode"
  
  # get telemetry data
  telid=$(echo "select id from telemetry where date >= '$d' order by id limit 1" | dosql)
  telstart=$(echo "select date from telemetry where id=$telid-1" | dosql)
  telstop=$(echo "select date from telemetry where id=$telid" | dosql)
  teltotname=""
  if [[ "$exitcode" == "0" ]] ; then teltotname="good-cpuusage" ; else teltotname="bad-cpuusage" ; fi
  teltotid=$(echo "select id from telemetry where id>$telid and revision=$rev and param='$teltotname' order by id limit 1" | dosql)
  echo "telid=$telid"
  echo "telstart=$telstart"
  echo "telstop=$telstop"
  echo "teltotname=$teltotname"
  echo "teltotid=$teltotid"
  echo "select * from telemetry where id between $telid-10 and $telid+15" | guisql
  
  # estimate true cpu time usage
  echo "select count(1),sum(cpuusage) from jobs where revision=$rev and retdate between '$telstart' and '$telstop' and exitcode=0 and id not in ($idslist)" | guisql
  cpunew=$(echo "select sum(cpuusage) from jobs where revision=$rev and retdate between '$telstart' and '$telstop' and exitcode=0 and id not in ($idslist)" | dosql)
  echo "cpunew=$cpunew"
  
  echo "select * from runs where id=$rid" | guisql
  echo "select runid, count(1), min(cpuusage), max(cpuusage), avg(cpuusage), stddev(cpuusage) from jobs where runid=$rid and exitcode=0 and id not in ($idslist)"  | guisql
  
  cpulist=$(echo "select cpuusage from jobs where runid=$rid and exitcode=0 and id not in ($idslist)"  | dosql)
  median=$(echo "$cpulist" | median | sed -e 's,\..*$,,')
  
  echo "median = $median"
  echo "$cpulist" | histo
  
  teltotnew=$((cpunew+median))
  echo "teltotnew = $cpunew + $median = $teltotnew"
  
  # sudgest the correction
  echo "=> FIX PROPOSAL:"
  echo "select * from jobs where id=$i" | guisql
  echo "update jobs set cpuusage = $median where id=$i" | tee -a fixjobcpu-proposal.sql
  echo "select * from telemetry where id=$teltotid" | guisql
  echo "update telemetry set value = $teltotnew where id=$teltotid" | tee -a fixjobcpu-proposal.sql
  
  echo ""
  echo ""
  echo ""
done

sed -e 's,$,;,' -i fixjobcpu-proposal.sql
echo "INFO: output fix job sql script = fixjobcpu-proposal.sql"
