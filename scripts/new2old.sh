#!/bin/bash -e

# convert new layout format into old format
function convert_new2old() {
  local src=$1
  local dst=$2
  
  # iterate over all index/*.txt files to add new records to the table,
  # example:
  #CMS_2021_I1847230-pp-8000/z1j-jj.dR-cms2021-zj-d12-x01-y01/pythia8-8.240-tune-4c.dat mc CMS_2021_I1847230 MODE=ZJet d12-x01-y01 pp 8000 z1j jj.dR cms2021-zj - pythia8 8.240 tune-4c
  #CMS_2021_I1847230-pp-8000/z1j-j.pt_j.pt-cms2021-zj-d09-x01-y01/CMS_2021_I1847230.dat data CMS_2021_I1847230 - d09-x01-y01 pp 8000 z1j j.pt_j.pt cms2021-zj - - - -

  find $src/index -type f -name '*.txt' | xargs cat | while read line ; do
    vals=( $line )
    local nvals="${#vals[*]}"
    fname="${vals[0]}"
    type="${vals[1]}"
    reference="${vals[2]}"
    experiment="${reference%%_*}"  # remove first '_' character and everything after
    optid="${vals[3]}"
    histid="${vals[4]}"
    beam="${vals[5]}"
    energy="${vals[6]}"
    process="${vals[7]}"
    observable="${vals[8]}"
    cuts="${vals[9]}"
    specific="${vals[10]}"
    generator="${vals[11]}"
    version="${vals[12]}"
    tune="${vals[13]}"
    
    
    dstdbase="$dst/dat/$beam/$process/$observable/$cuts/$energy"
    if [[ "$type" == "mc" ]] ; then
      dstd="$dstdbase/$generator/$version"
      mkdir -p "$dstd"
      cp -a "$src/$fname" "$dstd/$tune.dat"
      cp -a "$src/${fname/.dat/.params}" "$dstd/$tune.params"
    elif [[ "$type" == "data" ]] ; then
      dstd="$dstdbase"
      mkdir -p "$dstd"
      cp -a "$src/$fname" "$dstd/$reference.dat"
    fi
    
    
    # TODO: alpgen ???
  done
}

convert_new2old $1/dat $2
