#!/bin/bash

# path to GIT repository of MC production machinery:
giturl="https://gitlab.cern.ch/MCPLOTS/mcplots.git"
gitmcprod="scripts/mcprod"
gitjob="scripts/job.run"
gitbatchcontrol="scripts/control.txt"
gitplotter="plotter"
gitmerge="scripts/mcprod/merge"

# path to MC production machinery cache on CVMFS:
# (used to decrease job size)
cvmfsrepo="/cvmfs/sft.cern.ch/lcg/external/mcplots"

# paths to copilot shared directories:
# jobs input queue:
inputQueue="/t4t/input"
# jobs output queue:
outputQueue="/t4t/output"

# threshould on queue length in order to submit new jobs (for LXBATCH and BOINC):
minQueueLen="2500"

# place to put results:
pool="/home/mcplots/pool"

# number of events per job:
maxEventsPerJob="100000"
minEventsPerJob="1000"
eventsDropRate="7"

# parameter to specify how often to submit runs with the 'latest' version of
# generator as compared with the 'older' versions of a generator to collect
# statistics faster for the newest one
latestVersionRate="2"


# performance profiler: output text "@ timestamp label"
#function logstamp () { echo "@ $(date +%s.%N) $1"; }
# performance profiler: off
function logstamp () { true; }

# ==============================================================================

# wrapper function to make queries to local database
function dosql() {
  mysql --no-defaults --skip-column-names -u mcplots mcplots
}

# check all input parameters are unsigned integers
function test_uint() {
  for x in "$@" ; do
    if ! test "$x" -ge 0 2>/dev/null ; then
      return 1
    fi
  done
  
  return 0
}

# query T4T API to resolve BOINC USERID by AUTHENTICATOR
function query_t4t_userid() {
  local auth="$1"
  local api="http://lhcathome2.cern.ch/vLHCathome/show_user.php?format=xml&auth"
  
  xmlstarlet sel --net -t -v /user/id "$api=$auth" 2>/dev/null
}

function init_db() {
  {
    # NOTE: revision=2327 is the last SVN revision before final migration to GIT
    #       next revision numbers (from 2328) is just indices for
    #       actual commits IDs in `sha1` column
    
    # table of revisions of production machinery:
    echo "CREATE TABLE IF NOT EXISTS production"
    echo "("
    echo "  revision     INT      NOT NULL PRIMARY KEY AUTO_INCREMENT,"   # machinery revision number
    echo "  seed         INT      NOT NULL,"               # current seed of random number generator
    echo "  lxbatch      INT      NOT NULL,"               # flag to run production on LXBATCH
    echo "  boinc        INT      NOT NULL,"               # flag to run production on BOINC
    echo "  sha1         CHAR(40) NOT NULL UNIQUE KEY,"    # git commit SHA1
    echo "  added        DATETIME NOT NULL"                # time of revision date, local time
    echo ");"
    
    # table of runs specifications
    echo "CREATE TABLE IF NOT EXISTS runs"
    echo "("
    echo "  id           INT       NOT NULL PRIMARY KEY AUTO_INCREMENT,"
    echo "  revision     INT       NOT NULL,"
    echo "  run          CHAR(200) NOT NULL,"
    echo "  events       BIGINT    NOT NULL,"
    echo "  attempts     INT       NOT NULL,"
    echo "  success      INT       NOT NULL,"
    echo "  failure      INT       NOT NULL,"
    echo "  UNIQUE INDEX (revision, run)"
    echo ");"
    
    # jobs db
    echo "CREATE TABLE IF NOT EXISTS jobs"
    echo "("
    echo "  id        INT       UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,"  # job ID
    echo "  revision  SMALLINT  UNSIGNED NOT NULL,"    # job revision
    echo "  runid     INT       UNSIGNED NOT NULL,"    # run ID
    echo "  subdate   DATETIME           NOT NULL,"    # job submission date
    echo "  retdate   DATETIME,"                       # job return date
    echo "  system    TINYINT   UNSIGNED NOT NULL,"    # batch system: 0 = Unknown, 1 = vLHC@home, 2 = vLHCdev@home, 3 = LHC@home
    echo "  cpuusage  INT       UNSIGNED,"             # job CPU usage, seconds
    echo "  diskusage INT       UNSIGNED,"             # job disk usage, kilobytes
    echo "  events    MEDIUMINT UNSIGNED NOT NULL,"    # number of events
    echo "  seed      MEDIUMINT UNSIGNED NOT NULL,"    # initial seed of random number generator
    echo "  exitcode  TINYINT   UNSIGNED,"             # job exit code
    echo "  userid    MEDIUMINT UNSIGNED,"             # BOINC user ID
    echo "  hostid    INT       UNSIGNED,"             # BOINC host ID
    echo "  INDEX (revision, runid),"
    echo "  INDEX (system, userid, hostid)"
    echo ");"
    
    # telemetry log:
    echo "CREATE TABLE IF NOT EXISTS telemetry"
    echo "("
    echo "  id        INT      NOT NULL PRIMARY KEY AUTO_INCREMENT,"  # event ID
    echo "  date      DATETIME NOT NULL,"                             # event date and time, local
    echo "  revision  INT      NOT NULL,"                             # revision to which the event belongs to
    echo "  param     CHAR(50) NOT NULL,"                             # parameter name
    echo "  value     INT      NOT NULL,"                             # parameter value
    echo "  INDEX (revision, param)"
    echo ");"
    
  } | dosql
}

function record_telemetry() {
  local revision="$1"
  local key="$2"
  local value="$3"
  
  echo "INSERT INTO telemetry VALUES ( NULL, NOW(), $revision, '$key', $value );" | dosql
}


# this function syncronize 'production' table with revisions
# of MC production machinery from GIT
function fetch_new_git_revisions() {
  echo "[$(date)] -> fetch_new_git_revisions()"
  
  echo "Quering $giturl ..."
  local pwdd=$(pwd)
  local repod=$(mktemp -d)
  git clone -q $giturl "$repod" || return 1
  cd "$repod"
  
  # get list of commits from GIT:
  git log --reverse --format="%H %at %ai %ae %s" $gitmcprod > gitlog.txt
  echo "Total git revisions = $(wc -l gitlog.txt)"
  
  # log format output example:
  # SHA1, unix time, ISO 8601 time, author email, commit message
  # 18ad1a8a6a337388d5799f5785f3192500478dd8 1551871838 2019-03-06 12:30:38 +0100 anton.karneyeu@cern.ch Added ALICE info
  
  echo "Updating DB ..."
  
  {
    echo "LOCK TABLES production WRITE;"
    
    cat gitlog.txt | while read sha1 utime xxx ; do
      # convert unix time to local time - example `1289079402` => `2010-11-06 22:36:42`
      local loctime=$(date -d @$utime "+%Y-%m-%d %H:%M:%S")
      
      # add new revision to table (skip if revision is already here):
      echo "INSERT IGNORE INTO production VALUES ( NULL, 0, 0, 0, '$sha1', '$loctime' );"
    done
    
    echo "UNLOCK TABLES;"
  } > gitlog.sql
  
  cat gitlog.sql | dosql
  
  # cleanup
  cd "$pwdd"
  rm -rf "$repod"
  
  echo ""
}


# process batch production control
function process_batch_control () {
  echo "[$(date)] -> process_batch_control()"
  
  echo "Quering $giturl ..."
  local pwdd=$(pwd)
  local repod=$(mktemp -d)
  git clone -q $giturl "$repod" || return 1
  cd "$repod"
  
  if [[ ! -f $gitbatchcontrol ]] ; then
    # control file is missing, no action
    
    # cleanup
    cd "$pwdd"
    rm -rf "$repod"
    echo ""
    
    return 0
  fi
  
  # get the list of revisions for production
  # `sed`: strip comments, empty lines
  # `while`: print revision
  rev=$(cat $gitbatchcontrol | sed -e '/^#.*/ d' -e '/^ *$/ d' | while read r f ; do echo $r ; done)
  
  echo "Setting BOINC production revision to $rev"
  
  echo "update production set boinc = 0 where boinc != 0" | dosql
  for i in $rev ; do
    echo "update production set boinc = 1 where revision = $i" | dosql
  done
  
  # cleanup
  cd "$pwdd"
  rm -rf "$repod"
  
  echo ""
}


# this function updates 'runs' table with run specifications of
# revisions selected for production
function prepare_runs_list () {
  echo "[$(date)] -> prepare_runs_list()"
  
  # get list of revisions marked for production:
  local rev=$(echo "SELECT revision FROM production WHERE (lxbatch = 1 OR boinc = 1) AND revision NOT IN (SELECT DISTINCT revision FROM runs)" | dosql | xargs)
  
  if [[ "$rev" == "" ]] ; then
    echo "No new revisions to prepare runs list"
    echo ""
    
    return
  fi
  
  echo "Preparing runs list for revisions: $rev"
  echo "Updating DB ..."
  
  for i in $rev ; do
    local pwdd=$(pwd)
    local repod=$(mktemp -d)
    git clone -q $giturl "$repod" || continue
    cd $repod
    
    local sha1=$(echo "SELECT sha1 FROM production WHERE revision = $i" | dosql)
    git checkout $sha1
    cd $gitmcprod
    
    # TODO: revision is no longer commit id since migration to GIT
    
    # take into account increased number of parameters starting from revision #761
    local specf="2-8"  # before #761, run spec. situated in fields 2-8
    if (( "$i" >= 761 )) ; then
      specf="2-9"      # starting from #761, run spec. in 2-9
    fi
    
    { echo "LOCK TABLES runs WRITE;"
      
      # make list of all runs, cut 'run specification' part of each entry
      ./runAll.sh list 1 | cut -d ' ' -f "$specf" | while read run ; do
        # and add run specification to the 'runs' table:
        echo "INSERT IGNORE INTO runs VALUES ( NULL, $i, '$run', 0, 0, 0, 0 );"
      done
      
      echo "UNLOCK TABLES;"
    } > runs.sql
    
    cat runs.sql | dosql
    
    record_telemetry $i "runs-list-added" 1
    
    cd "$pwdd"
    rm -rf $repod
  done
  
  echo ""
}


# function check copilot input queue and return:
#  0 - queue is full or not reachable - no need to refill
#  1 - queue is empty and reachable   - to be refilled
function check_copilot_input_queue_len () {
  echo "Checking copilot public input queue..."
  
  # get list of jobs in the input queue
  local pubQueueList
  pubQueueList=$(find $inputQueue -ignore_readdir_race -type f -name '*.run')
  local pubEnumCode=$?
  
  # length of copilot public jobs queue:
  local pubQueueLen
  
  if [[ "$pubQueueList" != "" ]] ; then
    pubQueueLen=$(echo "$pubQueueList" | wc -l)
  else
    # work-around to correctly calculate size of empty queue
    pubQueueLen="0"
  fi
  
  if [[ "$pubEnumCode" != "0" ]] ; then
    # sometimes `find` prints error messages and exit with non-zero exit code while enumeration the input queue directory
    #   find: `/opt/copilot/input/2279-800864-38.run': No such file or directory
    # probably the reason is the removal of job files by batch system at the time of enumeration
    # still, the file list looks correct, so just issue the warning and keep the processing
    
    echo "WARNING: listing of copilot input queue $inputQueue returns non-zero exit code = $pubEnumCode"
    #return 0
  fi
  
  echo "length ($inputQueue) = $pubQueueLen"
  record_telemetry 0 "copilot-jobs-queue-len" $pubQueueLen
  
  # TODO: in past the `copilot-jobs-queue-total` telemetry was a total size of public queue and copilot-internal jobs buffer,
  #       the value is in use by update-production.sh and production.php and kept for compatibility for the moment
  record_telemetry 0 "copilot-jobs-queue-total" $pubQueueLen
  
  if (( "$pubQueueLen" > "$minQueueLen" )) ; then
    echo "Copilot public queue is full ($pubQueueLen > $minQueueLen)"
    echo ""
    return 0
  fi
  
  # queue is empty, return 1 to trigger refill
  return 1
}

# check the recent jobs fail or lost rate are below threshold
function check_recent_jobs_problem_rate() {
  echo "Checking recent problem rate..."
  
  # TODO: add error checking for DB queries
  
  local interval="4:05"  # 4 hours (+5 minutes to accomodate script startup jitter)
  local threshold="50"   # 50%
  echo "interval = $interval"
  echo "threshold = $threshold %"
  
  # get initial lenght of the copilot input queue
  local len=$(echo "SELECT value FROM telemetry WHERE param='copilot-jobs-queue-total' AND date < (NOW() - interval '$interval' hour_minute) ORDER BY id DESC" | dosql | head -n 1)
  echo "len = $len"
  
  # get number of submitted and received (good and bad) jobs
  local q=$(echo "SELECT SUM((param = 'submit-to-boinc')*value), SUM((param = 'good-jobs')*value), SUM((param = 'bad-jobs')* value) FROM telemetry WHERE date > (NOW() - interval '$interval' hour_minute)" | dosql)
  local a=( $q )
  local nsub=${a[0]}
  local ngood=${a[1]}
  local nbad=${a[2]}
  echo "nsub = $nsub"
  echo "ngood = $ngood"
  echo "nbad = $nbad"
  
  # get current lenght of the copilot input queue
  local len0=$(echo "SELECT value FROM telemetry WHERE param='copilot-jobs-queue-total' ORDER BY id DESC LIMIT 1" | dosql)
  echo "len0 = $len0"
  
  local nrcv=$((ngood + nbad))
  local ndrain=$((nsub + len - len0))
  local nlost=$((ndrain - nrcv))
  echo "nrcv = $nrcv"
  echo "ndrain = $ndrain"
  echo "nlost = $nlost"
  
  # calculate fail and lost rate
  local failrate="0"
  if [[ "$nrcv" != "0" ]] ; then
    failrate=$((100 * nbad / nrcv))
  fi
  
  local lostrate="0"
  if [[ "$ndrain" != "0" ]] ; then
    lostrate=$((100 * nlost / ndrain))
  fi
  
  echo "failrate = $failrate %"
  echo "lostrate = $lostrate %"
  
  if (( "$failrate" > "$threshold" )) ; then
    echo "WARNING: high fail rate"
    return 1
  fi
  
  if (( "$lostrate" > "$threshold" )) ; then
    echo "WARNING: high lost rate"
    return 1
  fi
  
  # problem rate is normal
  return 0
}

# swap fields 1 and 2 in the input stream
function swap12 () {
  while read a b ; do
    echo $b $a
  done
}

# check disk space
function check_disk_space () {
  local path="$1"
  local limitMb="$2"
  local spaceMb=$(df --direct -B M "$path" | awk 'FNR==2 {print 0+$4}')
  
  echo "INFO: path = $path, free space = $spaceMb Mb"
  
  if (( spaceMb < limitMb )) ; then
    echo "WARNING: insufficient disk space: $spaceMb < $limitMb"
    return 1
  fi
  
  return 0
}

function submit_boinc_jobs() {
  echo "[$(date)] -> submit_boinc_jobs()"
  
  # check input queue and exit if queue is full
  check_copilot_input_queue_len && return 0
  
  # set default limit on max jobs to avoid overflow in the input queue
  # input queue max capacity = 25 Gb / 150k jobs
  local jobsLimit="5000"
  
  # TODO: take into account current length
  
  # reduce the limit threshold if current fail or lost rate are too high
  if ! check_recent_jobs_problem_rate ; then
    jobsLimit="1000"
  fi
  
  echo "INFO: job limit is set to $jobsLimit"
  
  # get revisions selected for production on BOINC:
  local rev=$(echo "SELECT revision FROM production WHERE boinc = 1" | dosql | xargs)
  
  echo "Revisions to submit to BOINC: $rev"
  
  for i in $rev ; do
    logstamp "start"
    echo "Submitting revision $i ..."
    
    # get and increase SEED counter
    local seed=$(echo "SELECT seed FROM production WHERE revision = $i" | dosql)
    echo "UPDATE production SET seed = seed + 1 WHERE revision = $i" | dosql
    
    echo "seed = $seed"
    
    # prepare paths to temporary directories and files
    local pwdd=$(pwd)
    local tmpd="$(mktemp -d)"
    local repod="$tmpd/repo"
    local prodd="$tmpd/$i"
    local pack="$tmpd/$i.tgz"
    local tmplJob="$tmpd/job.run"
    local srcJob="$tmpd/job0.run"
    local runslist="$tmpd/runslist.txt"
    local runslist1="$tmpd/runslist1.txt"
    
    check_disk_space "$tmpd" 1000 || break
    
    # download codes
    git clone -q $giturl "$repod" || continue
    cd $repod
    
    # latest version of job template:
    cp -a $gitjob $tmplJob
    
    # extract runs list filter strings (`grep -E` regular expression)
    # `sed`: strip comments, empty lines
    # `while`: print filter string corresponding to revision `$i`
    runsFilter=$(cat $gitbatchcontrol | sed -e '/^#.*/ d' -e '/^ *$/ d' | while read r f ; do if (( r == i )) ; then echo -n $f ; fi ; done)
    echo "runsFilter = $runsFilter"
    
    # init job file
    touch $srcJob
    chmod a+x $srcJob
    
    local sha1=$(echo "SELECT sha1 FROM production WHERE revision = $i" | dosql)
    git checkout -q $sha1
    
    # create tarball with production machinery:
    cp -a $gitmcprod $prodd
    cp -a $gitplotter $prodd/plotter
    
    local packdist="$cvmfsrepo/$i.tgz"
    local packsrc=""
    
    # check if tarball is cached already in CVMFS
    # and it was created/modified more than 1 day (24*60 minutes) ago
    # (enough time to propagate to all CVMFS clients)
    # TODO: obsolete and not in use, to be removed?
    if [[ -f $packdist && "$(find $packdist -mmin +1440)" != "" ]] ; then
      # check cached tarball contents really matches to exported GIT contents
      local cvmfsprodd="$(mktemp -d)"
      tar -xzf $packdist -C $cvmfsprodd
      
      if diff -rq $prodd $cvmfsprodd >& /dev/null ; then
        # cache is valid
        packsrc=$packdist
      else
        echo "WARNING: $packdist does not match GIT revision $i / sha1 = $sha1"
      fi
      
      rm -rf $cvmfsprodd
    fi
    
    echo "packsrc = $packsrc"
    
    if [[ "$packsrc" == "" ]] ; then
      # remove unnecessary files to decrease job size
      ( cd $prodd && rm -rf merge runAll.sh plotter/data configuration/rivet-histograms-todo.map )
      
      # the `--xz` vs. `-z` (gzip): less size by ~30%
      tar -c --xz -f $pack -C $prodd . || continue
    else
      # integrate empty binary into job
      echo -n > $pack
    fi
    
    echo "Uploading jobs ..."
    echo ""
    
    # get total number of already submitted jobs:
    local totalAttempts1=$(echo "SELECT SUM(attempts) FROM runs WHERE revision = $i" | dosql)
    
    # get average number of lost jobs per run:
    local lost_avg=$(echo "SELECT AVG(attempts - success - failure) from runs where revision = $i and attempts > 0" | dosql)
    
    logstamp "sub_init"
    
    # extract runs list
    # the list is sorted according to "attempts, RAND()"
    echo "SELECT attempts, success, success + failure, id, run FROM runs WHERE revision = $i ORDER BY attempts, RAND()" | dosql > $runslist
    echo "Runs list, total = $(wc -l $runslist)"
    
    # check if this iteration is "latest versions only"
    if (( seed % latestVersionRate != 0 )) ; then
      # get list of the latest versions of generators
      local filter=$(cat $runslist | cut -f5- | cut -d ' ' -f 6,7 | sort -ru | swap12 | uniq -f 1 | swap12)
      cat "$runslist" | grep -F "$filter" > "$runslist1"
      mv -f "$runslist1" "$runslist"
      echo "Runs list, latest-versions = $(wc -l $runslist)"
    fi
    
    # exclude following runs:
    #   "alpgenpythia6" gives "Segmentation fault" on i686
    #   "alpgenherwigjimmy" does not run (some problem with mcprod machinery)
    # TODO: resolve alpgen* problems
    cat "$runslist" | grep -v "alpgen" > "$runslist1"
    mv -f "$runslist1" "$runslist"
    echo "Runs list, no-alpgen = $(wc -l $runslist)"
    
    # mask phojet (to hepmc conversion issue), pythia6 w/zinc w/o cuts
    # TODO: remove the mask on the next production version
    cat "$runslist" | grep -v "phojet" > "$runslist1"
    mv -f "$runslist1" "$runslist"
    echo "Runs list, no-phojet = $(wc -l $runslist)"
    cat "$runslist" | grep -v "pp winclusive 13000 - - pythia6 " > "$runslist1"
    mv -f "$runslist1" "$runslist"
    cat "$runslist" | grep -v "pp winclusive 7000 - - pythia6 " > "$runslist1"
    mv -f "$runslist1" "$runslist"
    cat "$runslist" | grep -v "pp zinclusive 13000 - - pythia6 " > "$runslist1"
    mv -f "$runslist1" "$runslist"
    echo "Runs list, no-py6wznocut = $(wc -l $runslist)"
    
    # filter runs list according to the control
    if [[ "$runsFilter" != "" ]] ; then
      cat "$runslist" | grep -E "$runsFilter" > "$runslist1"
      mv -f "$runslist1" "$runslist"
      echo "Runs list, runsFilter = $(wc -l $runslist)"
    fi
    
    # respect jobs limit per submit
    cat "$runslist" | head -n $jobsLimit > "$runslist1"
    mv -f "$runslist1" "$runslist"
    echo "Runs list, for-submit = $(wc -l $runslist)"
    
    cat $runslist | while read nsub ngood nrcv runid run ; do
      logstamp "start"
      
      # $nsub  - number of submited jobs
      # $ngood - number of successful jobs
      # $nrcv  - number of received jobs
      # $runid - run ID
      # $run   - run specification
      
      # exclude runs which don't have any return (all jobs lost, probably due to endless loop or hang)
      # TODO: compute nsub limit based on stats
      if (( nsub > 20 && ngood == 0 )) ; then
        echo "Skipping '$run' (HANG)"
        continue
      fi
      
      logstamp "sub_job_filtr"
      
      # Estimate number of events to submit according to amount of lost jobs.
      # Use $maxEventsPerJob events if #lost jobs below threshould:
      #   lost_jobs < sqrt(submitted_jobs) + <lost_jobs>
      #     lost_jobs = attempts - (success + failure)
      #     <lost_jobs> - average number of lost jobs over all runs
      #
      # , else:
      #     index = lost_jobs - (sqrt(submitted_jobs) + <lost_jobs>)
      #     nevt = min_jobs + (max_jobs - min_jobs) * exp(- rate * index)
      
      local nevt="$maxEventsPerJob"
      
      if [[ "$nsub" != "0" ]] ; then  # check submitted_jobs != 0
        local ind=$(echo "($nsub - $nrcv - sqrt($nsub) - $lost_avg)/$nsub" | bc -l)
        
        if [[ ${ind:0:1} != "-" ]] ; then  # check do we above threshould
          nevt=$(echo "$minEventsPerJob + ($maxEventsPerJob - $minEventsPerJob) * e(- $eventsDropRate * $ind)" | bc -l)
          
          # round to 1000
          nevt=$(echo "$nevt / 1000 * 1000" | bc)
          
          if (( "$nevt" < "$minEventsPerJob" )) ; then
            nevt="$minEventsPerJob"
          fi
        fi
      fi
      
      # limit events number for PbPb collisions
      if [[ "${run/ PbPb /}" != "$run" ]] ; then
        # info: lxplus7 performance for epos and pythia8 is 3s/event
        nevt="5000"
      fi
      
      logstamp "sub_job_init"
      
      local system="0"
      
      # create new job record in DB and extract job ID
      local jobid=$(echo "INSERT INTO jobs VALUES ( NULL, $i, $runid, NOW(), NULL, $system, NULL, NULL, $nevt, $seed, NULL, NULL, NULL ); SELECT LAST_INSERT_ID()" | dosql)
      
      if [[ "$jobid" == "" ]] ; then
        echo "ERROR: failed to add job record to DB, exit from job submission loop"
        break
      fi
      
      logstamp "sub_job_db_ins"
      
      # cook job file:
      local runspec="boinc $run $nevt $seed"
      { sed -e "s/%runspec%/$runspec/" \
            -e "s/%run%/$run/" \
            -e "s,%jobid%,$jobid," \
            -e "s,%revision%,$i," \
            -e "s,%runid%,$runid," \
            -e "s,%system%,$system," \
            -e "s,%events%,$nevt," \
            -e "s,%seed%,$seed," \
            -e "s,%packsrc%,$packsrc," \
            $tmplJob
        cat $pack
      } > $srcJob
      
      logstamp "sub_job_file"
      
      # put job to copilot input queue:
      local dstJob="$inputQueue/$i-$runid-$seed.run"
      echo "Submitting '$runspec' ($dstJob)"
      cp $srcJob "$dstJob"
      jobcopycode="$?"
      
      logstamp "sub_job_queue"
      
      if [[ "$jobcopycode" == "0" ]] ; then
        # increase number of submits
        echo "UPDATE runs SET attempts = attempts + 1 WHERE id = $runid" | dosql
      else
        # remove job record
        echo "DELETE FROM jobs WHERE id = $jobid" | dosql
        
        echo "ERROR: failed to submit job, possible network connection to copilot is lost"
        break # exit from job submission loop
      fi
      
      logstamp "sub_job_db_done"
    done
    
    logstamp "start"
    
    # get new total number of submitted jobs:
    local totalAttempts2=$(echo "SELECT SUM(attempts) FROM runs WHERE revision = $i" | dosql)
    
    # record number of just submitted jobs:
    record_telemetry $i "submit-to-boinc" $((totalAttempts2-totalAttempts1))
    
    cd "$pwdd"
    rm -rf $tmpd
    
    logstamp "sub_fini"
  done
  
  # re-check input queue after submit to record queue length telemetry
  check_copilot_input_queue_len
  
  echo ""
}


function get_boinc_results() {
  echo "[$(date)] -> get_boinc_results()"
  
  echo "Listing finished jobs..."
  
  # list all files in results directory:
  local jobsList
  jobsList=$(find $outputQueue -type f -name '*.tgz')
  
  if [[ "$?" != "0" ]] ; then
    echo "WARNING: failed to access copilot output queue $outputQueue"
  fi
  
  # record output queue size
  local finishedLen=$(echo "$jobsList" | wc -l)
  
  # work-around to correctly calculate size of empty queue
  if [[ "$jobsList" == "" ]] ; then
    finishedLen="0"
  fi
  
  echo "Current number of copilot done jobs = $finishedLen"
  record_telemetry 0 "copilot-done-queue-len" $finishedLen
  
  # checkout and build merge tool
  echo "Preparing merge tool..."
  local repod="$(mktemp -d)"
  git clone -q $giturl "$repod" || return 1
  local mergedir="$repod/$gitmerge"
  make -s -B -C $mergedir
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to prepare merge tool. Skipping jobs processing."
    return 1
  fi
  
  # check enough disk space in the output area
  check_disk_space "$pool" 1000 || return 1
  
  echo "Downloading finished jobs..."
  
  # counters for numbers of succesfull and failed jobs:
  # indeces (or keys) correspond to revisions, and values correspond to counters
  local n_good=( )
  local n_bad=( )
  local cpu_good=( )
  local cpu_bad=( )
  local n_corrupt="0"
  local n_dup="0"
  
  # limit number of jobs to process to have better progress indication on production page
  # TODO: fix hardcoded number
  jobsList=$(echo "$jobsList" | head -n 2700)
  
  # copy/move jobs to local machine:
  for i in $jobsList ; do
    logstamp "start"
    echo "Processing $i"
    
    local tmpd="$(mktemp -d)"
    local fname=$(basename $i)
    
    # move job from copilot to local host
    mv $i $tmpd/$fname
    
    if [[ "$?" != "0" ]] ; then
      echo "WARNING: failed to get the job. Skip current job."
      rm -rf $tmpd
      continue
    fi
    
    logstamp "move"
    
    # job integrity flag, zero if no problems detected
    local integrityStatus="0"
    
    # TODO: support .txz (created by tar --xz), gives smaller files size by ~30%
    
    # unpack job
    # option '--exclude $fname' is a protection to prevent
    # over-writting of original job file if file with the
    # same name exists inside archive $fname
    tar zxmf $tmpd/$fname -C $tmpd --exclude $fname
    integrityStatus="$?"
    
    logstamp "unpack"
    
    # process job metadata:
    # line-terminating characters in BOINC_* strings could be CRLF / LF / CR
    # depending on the volonteer's system
    # replace CR by LF to fix strings procesing on mcplots-dev (SLC)
    sed 's,\r,\n,' -i $tmpd/jobdata
    
    # debug output:
    echo -n "jobdata: "
    xargs < $tmpd/jobdata
    
    # read metadata
    local jobid=$(sed -n "s,^jobid=\(.*\)$,\1,p" $tmpd/jobdata)
    local rev=$(sed -n "s,^revision=\(.*\)$,\1,p" $tmpd/jobdata)
    local runid=$(sed -n "s,^runid=\(.*\)$,\1,p" $tmpd/jobdata)
    local cpuusage=$(sed -n "s,^cpuusage=\(.*\)$,\1,p" $tmpd/jobdata)
    local diskusage=$(sed -n "s,^diskusage=\(.*\)$,\1,p" $tmpd/jobdata)
    local nevt=$(sed -n "s,^events=\(.*\)$,\1,p" $tmpd/jobdata)
    local exitcode=$(sed -n "s,^exitcode=\(.*\)$,\1,p" $tmpd/jobdata)
    local userid=$(sed -n "s,^BOINC_USERID=\(.*\)$,\1,p" $tmpd/jobdata)
    local hostid=$(sed -n "s,^BOINC_HOSTID=\(.*\)$,\1,p" $tmpd/jobdata)
    
    # sanity check (unsigned integer)
    if ! test_uint "$jobid" "$rev" "$runid" "$cpuusage" "$diskusage" "$nevt" "$exitcode" ; then
      integrityStatus="1"
    fi
    
    # sanity check (unsigned integer)
    if ! test_uint "$userid" ; then userid="NULL" ; fi
    if ! test_uint "$hostid" ; then hostid="NULL" ; fi
    
    # the default is copilot/vLHC system (the BOINC_PROJECT metadata is not present)
    local system="1"
    
    # condor jobs have BOINC_PROJECT metadata to distinguish between various @home projects
    if grep -q "^BOINC_PROJECT=" $tmpd/jobdata ; then
      local project=$(sed -n "s,^BOINC_PROJECT=\(.*\)$,\1,p" $tmpd/jobdata)
      
      case "$project" in
        "vLHC"    ) system="1" ;;
        "vLHCdev" ) system="2" ;;
        "LHC"     ) system="3" ;;
        *         ) system="0" ;;
      esac
      
      echo "condor-job: jobid=$jobid project=$project -> system=$system"
    fi
    
    logstamp "metadata"
    
    # TODO: recovery works now only for vLHC project
    if [[ "$system" == "1" && "$userid" == "0" ]] ; then
      # BOINC clients 6.10.x always return USERID = 0
      # try to recover USERID based on AUTHENTICATOR
      
      local auth=$(sed -n "s,^BOINC_AUTHENTICATOR=\(.*\)$,\1,p" $tmpd/jobdata)
      userid=$(query_t4t_userid "$auth")
      
      # check the value is valid (unsigned integer)
      if ! test_uint "$userid" ; then userid="0" ; fi
      
      # debug output:
      echo "recovery-userid: 0 -> $userid"
      
      logstamp "uidrecov"
    fi
    
    if [[ "$integrityStatus" != "0" ]] ; then
      echo "WARNING: job is corrupted."
      
      # copy full job file for investigation
      mkdir -p $pool/failed/unknown
      cp -f $tmpd/$fname $pool/failed/unknown/
      
      # count corrupted job
      let n_corrupt+=1
      
      rm -rf $tmpd
      
      logstamp "corrupt"
      
      continue
    fi
    
    # check for anomalous cpuusage
    if (( "$cpuusage" > 100000000 && "$nevt" == 100000 )) ; then
      echo "WARNING: anomalous cpuusage."
      
      # copy full job file for investigation
      mkdir -p $pool/failed/cpuusage
      cp -f $tmpd/$fname $pool/failed/cpuusage/
      
      rm -rf $tmpd
      
      logstamp "cpuusage"
      
      continue
    fi
    
    # at this point the job integrity is ok
    
    if [[ "$exitcode" == "0" ]] ; then
      # job finished successfully, copy results (histograms) to pool:
      mkdir -p $pool/$rev/dat
      
      if (( "$rev" >= 857 )) ; then
        # merge new histograms into central repository
        ( cd $mergedir && ./merge.sh $tmpd/dat $pool/$rev/dat ) > /dev/null
      else
        # old revisions do not support merging, just do a simple copy
        cp -rf $tmpd/dat $pool/$rev/
      fi
      
      logstamp "goodmerge"
      
      # increase number of successes and events
      echo "UPDATE runs SET success = success + 1, events = events + $nevt WHERE id = $runid AND revision = $rev" | dosql
      
      logstamp "gooddb"
      
      # increase good jobs counter and CPU usage:
      let n_good[rev]+=1
      let cpu_good[rev]+=cpuusage
      
      #echo "Job succeeded: $fname (see results in $pool/$rev/)"
    
    else
      # job failed, make a copy of log file for investigation:
      mkdir -p $pool/failed/$rev
      cp -f $tmpd/runRivet.log $pool/failed/$rev/$fname.log
      
      logstamp "badmerge"
      
      # increase number of failures
      echo "UPDATE runs SET failure = failure + 1 WHERE id = $runid AND revision = $rev" | dosql
      
      logstamp "baddb"
      
      # increase bad jobs counter and CPU usage:
      let n_bad[rev]+=1
      let cpu_bad[rev]+=cpuusage
      
      echo "Job failed"
    fi
    
    logstamp "jobdone"
    
    # update job record
    local nchanged=$(echo "UPDATE jobs SET retdate = NOW(), cpuusage = $cpuusage, diskusage = $diskusage, exitcode = $exitcode, system = $system, userid = $userid, hostid = $hostid WHERE id = $jobid AND retdate IS NULL; SELECT ROW_COUNT()" | dosql)
    
    if [[ "$nchanged" == "0" ]] ; then
      echo "WARNING: duplicate job"
      let n_dup+=1
    fi
    
    logstamp "jobdb"
    
    rm -rf $tmpd
    
    logstamp "clean"
  done
  
  rm -rf $repod
  
  # --- record counters of good and bad jobs:
  # (iterate over revisions)
  for i in ${!n_good[@]}; do record_telemetry $i "good-jobs" ${n_good[$i]} ; done
  for i in ${!n_bad[@]};  do record_telemetry $i "bad-jobs"  ${n_bad[$i]}  ; done
  
  # no good jobs at this check:
  if [[ "${#n_good[@]}" == "0" ]] ; then
    record_telemetry 0 "good-jobs" 0
  fi
  
  # no bad jobs at this check:
  if [[ "${#n_bad[@]}" == "0" ]] ; then
    record_telemetry 0 "bad-jobs" 0
  fi
  
  # --- record jobs CPU usage:
  for i in ${!cpu_good[@]}; do record_telemetry $i "good-cpuusage" ${cpu_good[$i]} ; done
  for i in ${!cpu_bad[@]};  do record_telemetry $i "bad-cpuusage"  ${cpu_bad[$i]}  ; done
  [[ "${#cpu_good[@]}" == "0" ]] && record_telemetry 0 "good-cpuusage" 0
  [[ "${#cpu_bad[@]}"  == "0" ]] && record_telemetry 0 "bad-cpuusage"  0
  
  # corrupted and duplicated jobs
  record_telemetry 0 "corrupt-jobs" $n_corrupt
  record_telemetry 0 "dup-jobs" $n_dup
  
  # maintenance: remove old and empty folders from copilot output directory
  find $outputQueue -mindepth 1 -type d -mtime +3 -empty -delete
  
  echo ""
}


#
function refresh_api_cache() {
  echo "[$(date)] -> refresh_api_cache()"
  
  {
    # create new api cache data table
    echo "DROP TABLE IF EXISTS api_new;"
    echo "CREATE TABLE api_new"
    echo "("
    echo "  date1G  DATE,"
    echo "  date10G DATE,"
    echo "  UNIQUE INDEX (system, userid, hostid)"
    echo ")"
    echo "SELECT"
    echo "  system,"
    echo "  userid,"
    echo "  hostid,"
    echo "  SUM(cpuusage) AS cpu_time,"
    echo "  SUM(diskusage) AS disk_usage,"
    echo "  SUM((exitcode != 0)*cpuusage) AS cpu_bad,"
    echo "  SUM((exitcode = 0)*events) AS n_events,"
    echo "  COUNT(*) AS n_jobs,"
    echo "  SUM(exitcode = 0) AS n_good_jobs,"
    echo "  SUM(retdate > (NOW() - INTERVAL 1 MONTH)) AS n_jobs_1m,"
    echo "  SUM((exitcode = 0)*(retdate > (NOW() - INTERVAL 1 MONTH))) AS n_good_jobs_1m"
    echo "FROM jobs"
    echo "WHERE retdate IS NOT NULL"
    echo "GROUP BY 1, 2, 3;"
    
    echo "CREATE TABLE IF NOT EXISTS api LIKE api_new;"
    
    # copy 1G and 10G achievements dates to new cache
    # (these dates remain constant once reached, thus no need to recalculate)
    echo "UPDATE"
    echo "  api_new, api"
    echo "SET"
    echo "  api_new.date1G = api.date1G,"
    echo "  api_new.date10G = api.date10G"
    echo "WHERE"
    echo "  api_new.system <=> api.system AND api_new.userid <=> api.userid;"
    
    # NOTE: to invalidate the date1G/date10G cache run manually:
    # $ echo "UPDATE api SET date1G = NULL, date10G = NULL" | mysql -u mcplots mcplots
    
    # swap new cache with old cache
    echo "RENAME TABLE api TO api_old, api_new TO api;"
    
    # drop old cache
    echo "DROP TABLE IF EXISTS api_old;"
  } | dosql
  
  # calculate date1G and date10G
  for i in 1 10 ; do
    local f="date${i}G"
    local n="${i}000000000"
    
    # get the list of users who just reach the achievement
    local users=$(echo "SELECT system, userid, $f, SUM(n_events) as total FROM api GROUP BY 1, 2 HAVING total >= $n AND $f <=> NULL" | dosql | cut -f 1,2 --output-delimiter=: | xargs)
    
    echo "New ${i}G users: $users"
    
    for user in $users ; do
      local s=${user%%:*}
      local u=${user##*:}
      
      echo "Processing $user (system=$s, userid=$u)..."
      
      {
        echo "UPDATE api, ("
        echo "  SELECT"
        echo "    *"
        echo "  FROM ("
        echo "    SELECT"
        echo "      *, (@s := @s + events) AS total"
        echo "    FROM ("
        echo "      SELECT @s := 0"
        echo "    ) AS vars,"
        echo "    ("
        echo "      SELECT"
        echo "        DATE(retdate) AS date,"
        echo "        SUM((exitcode = 0)*events) AS events"
        echo "      FROM"
        echo "        jobs"
        echo "      WHERE"
        echo "        system <=> $s AND userid <=> $u"
        echo "      GROUP BY"
        echo "        1"
        echo "    ) AS q1"
        echo "    WHERE"
        echo "      date IS NOT NULL"
        echo "  ) AS q2"
        echo "  WHERE"
        echo "    total >= $n"
        echo "  LIMIT"
        echo "    1"
        echo ") AS achi"
        echo "SET"
        echo "  api.$f = achi.date"
        echo "WHERE"
        echo "  api.system <=> $s AND api.userid <=> $u"
      } | dosql
    done
  done
  
  echo ""
}


# record script busy time
function record_busy_time() {
  echo "[$(date)] -> record_busy_time()"
  
  # calculate and record spent CPU time:
  local tmpf=$(mktemp)
  times > $tmpf
  local busyCPUTime=$(cat $tmpf | xargs | sed -e 's,m,*60+,g' -e 's,s ,+,g' -e 's,s$,+0.5,' | bc | cut -d . -f 1)
  rm -f $tmpf
  
  # work-around if busyCPUTime < 1 second
  if [[ "$busyCPUTime" == "" ]] ; then
    busyCPUTime="0"
  fi
  
  echo "Script busy time (CPU) = $busyCPUTime s"
  record_telemetry 0 "busy-time" $busyCPUTime
  
  # elapsed wall clock time:
  local startTime="$1"
  
  if [[ "$startTime" != "" ]] ; then
    local stopTime=$(date +%s)
    local busyWallTime=$((stopTime - startTime))
    
    echo "Script busy time (wall) = $busyWallTime s"
    record_telemetry 0 "busy-time-wall" $busyWallTime
  fi
  
  echo ""
}

# make production page more responsive for first visitor by triggering cache renew
function update_prod_page_cache() {
  echo "[$(date)] -> update_prod_page_cache()"
  
  wget -q --spider -T 60 \
    http://localhost/production.php?view=status \
    http://localhost/production.php?view=control \
    http://localhost/production.php?view=contrib
  
  echo ""
}

# === main ===

# TODO: implement reporting to know that the script is working or not

# lock in order to prevent execution of several copies of script
mkdir -p $pool/
lockFile=$pool/lock

# check the lock file exists and process created the file is running
if [[ -e $lockFile ]] && kill -0 $(cat $lockFile) ; then
  echo "Script $0 already running. Skipping execution and exit."
  exit 0
fi

echo $$ > $lockFile

# set timeout to avoid `git clone` hang due to network connection problems
# the hang definition: speed < 1000 bytes/s over the interval of 30 s
export GIT_HTTP_LOW_SPEED_LIMIT=1000
export GIT_HTTP_LOW_SPEED_TIME=30

startTime=$(date +%s)

init_db

fetch_new_git_revisions
process_batch_control
prepare_runs_list

submit_boinc_jobs

get_boinc_results

refresh_api_cache

record_busy_time $startTime

update_prod_page_cache

echo "[$(date)] Update finished"
echo ""
echo ""

# release lock:
rm -f $lockFile
