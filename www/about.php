<?php
  $abarr = [
    "about" => "About",
    "uguide" => "User's guide",
    "dguide" => "Developer's guide",
    "doc" => "Documentation",
    "status" => "Current status",
    "history" => "Update history"
  ];
  
  $q_info = $_GET["info"];
  // sanity
  $q_info = array_key_exists($q_info, $abarr) ? $q_info : "about";

  print_about_menu();

  echo "<div class=\"rightpage\">\n";
  echo "<h2 class=\"d2\"><span>".$abarr[$q_info]."</span></h2>\n";
  echo "\n";
  if ($q_info == "status") include($q_info.".php");
  else readfile($q_info.".html");



  echo "</div>";


  function print_about_menu() {
    global $q_info, $abarr;
    echo "<div class=\"leftside\">\n";
    echo "<div class=\"sidenav\">\n";
    echo "<h2>MCPLOTS</h2>\n";

    foreach (array_keys($abarr) as $pmenu){
      $curobs = ($q_info == $pmenu) ? " class=\"active\"" : "";
      printf("<a%s href=\"?query=about&info=%s\">$abarr[$pmenu]</a>\n",
                  $curobs,
                  $pmenu);
    }
    echo "</div>";
    echo "</div>";
  }
?>
