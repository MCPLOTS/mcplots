#!/bin/bash -e

# input .script file
fscript=$1

if [[ "$#" != "1" ]] ; then
  echo "Usage: ./mk_plotter_pkg.sh path/to/file.script"
  exit 1
fi

if ! test -f $fscript ; then
  echo "ERROR: the input parameter is not a file"
  exit 1
fi

# separate directory to cook all files for plot
tmpd=$(mktemp -d)

# run cleanup on exit
trap "rm -rf $tmpd" EXIT

scriptname=${fscript##*/}
pkgd=$tmpd/$scriptname
mkdir -p $pkgd
dsts=$pkgd/$scriptname
list=$tmpd/datlist.txt

# prepare the list of .dat files for plot
grep ^filename= $fscript | cut -d = -f 2- > $list

# copy .dat files
while read f ; do
  dstf=$pkgd/${f//\//-}
  cp -a $f $dstf
done < $list

# copy .script file
cp -a $fscript $dsts

# patch .script file
sed -e '/^filename=/ s,/,-,g' -i $dsts
sed -e '/^outputFileName=/ s,=.*/,=,' -i $dsts

# prepare tarball with all files
# place tarball near input .script file
tar zcf $fscript.tgz -C $tmpd $scriptname

echo "INFO: $fscript.tgz"
