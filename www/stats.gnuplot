# jobs rates
# hourly plot:
set terminal png medium size 850,200
set output "cache/stats/stats-jobs-hourly.png"
set xdata time
set timefmt "%Y-%m-%d.%H"
set format x "%b %d"
#set xlabel "date"
set ylabel "#jobs/hour"
#set logscale y
set grid xtics ytics
set xtics "2010-01-01.0", 86400

# change legend position to top left:
set key left top

plot "cache/stats/stats-jobs-hourly.txt" using 1:4 with linespoints pt 4 ps 0.2 title "received", \
     "cache/stats/stats-jobs-hourly.txt" using 1:2 with points pt 1 title "succeeded", \
     "cache/stats/stats-jobsdrain-hourly.txt" using 1:2 with linespoints pt 4 ps 0.2 title "drain"


# daily plot:
set terminal png size 850,200
set output "cache/stats/stats-jobs-daily.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%b %d"
#set xlabel "date"
set ylabel "#jobs/day"
#set logscale y
set grid xtics ytics
set xtics autofreq

plot "cache/stats/stats-jobs-daily.txt" using 1:4 with linespoints pt 4 ps 0.2 title "received", \
     "cache/stats/stats-jobs-daily.txt" using 1:2 with points pt 1 title "succeeded", \
     "cache/stats/stats-jobsdrain-daily.txt" using 1:2 with linespoints pt 4 ps 0.2 title "drain"


# weekly plot:
set terminal png size 850,200
set output "cache/stats/stats-jobs-weekly.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%b"
set ylabel "#jobs/week"
set grid xtics ytics

plot "cache/stats/stats-jobs-weekly.txt" using 1:4 with linespoints pt 4 ps 0.2 title "received", \
     "cache/stats/stats-jobs-weekly.txt" using 1:2 with points pt 1 title "succeeded", \
     "cache/stats/stats-jobsdrain-weekly.txt" using 1:2 with linespoints pt 4 ps 0.2 title "drain"


# monthly plot:
set terminal png size 850,200
set output "cache/stats/stats-jobs-monthly.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%Y"
set ylabel "#jobs/month"
set grid xtics ytics

plot "cache/stats/stats-jobs-monthly.txt" using 1:4 with linespoints pt 4 ps 0.2 title "received", \
     "cache/stats/stats-jobs-monthly.txt" using 1:2 with points pt 1 title "succeeded", \
     "cache/stats/stats-jobsdrain-monthly.txt" using 1:2 with linespoints pt 4 ps 0.2 title "drain"


# jobs rates ratios
# hourly plot:
set terminal png size 850,200
set output "cache/stats/stats-jobsratios-hourly.png"
set xdata time
set timefmt "%Y-%m-%d.%H"
set format x "%b %d"
#set xlabel "date"
set ylabel "%"
#set logscale y
set grid xtics ytics
set xtics "2010-01-01.0", 86400

# change legend position to top left:
set key left top

plot [] [0:] \
     "cache/stats/stats-jobsratios-hourly.txt" using 1:(100 * ($5 - $4) / $5) with linespoints pt 4 ps 0.2 lt 3 title "lost ratio", \
     "cache/stats/stats-jobsratios-hourly.txt" using 1:(100 * ($4 - $2) / $4) with linespoints pt 4 ps 0.2 lt 1 title "fail ratio"

# daily plot:
set terminal png size 850,200
set output "cache/stats/stats-jobsratios-daily.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%b %d"
#set xlabel "date"
set ylabel "%"
#set logscale y
set grid xtics ytics
set xtics autofreq

plot [] [0:] \
     "cache/stats/stats-jobsratios-daily.txt" using 1:(100 * ($5 - $4) / $5) with linespoints pt 4 ps 0.2 lt 3 title "lost ratio", \
     "cache/stats/stats-jobsratios-daily.txt" using 1:(100 * ($4 - $2) / $4) with linespoints pt 4 ps 0.2 lt 1 title "fail ratio"


# weekly plot:
set terminal png size 850,200
set output "cache/stats/stats-jobsratios-weekly.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%b"
set ylabel "%"
set grid xtics ytics

plot [] [0:] \
     "cache/stats/stats-jobsratios-weekly.txt" using 1:(100 * ($5 - $4) / $5) with linespoints pt 4 ps 0.2 lt 3 title "lost ratio", \
     "cache/stats/stats-jobsratios-weekly.txt" using 1:(100 * ($4 - $2) / $4) with linespoints pt 4 ps 0.2 lt 1 title "fail ratio"


# monthly plot:
set terminal png size 850,200
set output "cache/stats/stats-jobsratios-monthly.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%Y"
set ylabel "%"
set grid xtics ytics

plot [] [0:] \
     "cache/stats/stats-jobsratios-monthly.txt" using 1:(100 * ($5 - $4) / $5) with linespoints pt 4 ps 0.2 lt 3 title "lost ratio", \
     "cache/stats/stats-jobsratios-monthly.txt" using 1:(100 * ($4 - $2) / $4) with linespoints pt 4 ps 0.2 lt 1 title "fail ratio"


# contributed CPU time
# hourly
set terminal png size 850,200
set output "cache/stats/stats-cpu-hourly.png"
set xdata time
set timefmt "%Y-%m-%d.%H"
set format x "%b %d"
#set xlabel "date"
set ylabel "CPU*hours per hour"
#set logscale y
set grid xtics ytics
set xtics "2010-01-01.0", 86400

# change legend position to top left:
set key left top

# TODO: yrange max is explicitly set to 2000 to work-around sometimes horribly scaled axis due to
#       incorrectly reported job cputime
#       proper fix would be to automatically locate and fix metadata of such jobs

# TODO: use axis autoscaling limit after update to cc7 (gnuplot 4.6)
#       https://stackoverflow.com/questions/11269877/gnuplot-minimum-and-maximum-boundaries-for-autoscaling/20518066

#plot [] [:2000]
plot \
     "cache/stats/stats-cpu-hourly.txt" using 1:($4/3600) with linespoints pt 4 ps 0.2 title "total", \
     "cache/stats/stats-cpu-hourly.txt" using 1:($2/3600) with points pt 1 title "succeeded"

# daily
set terminal png size 850,200
set output "cache/stats/stats-cpu-daily.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%b %d"
#set xlabel "date"
set ylabel "CPU*days per day"
#set logscale y
set grid xtics ytics
set xtics autofreq

#plot [] [:2000]
plot \
     "cache/stats/stats-cpu-daily.txt" using 1:($4/86400) with linespoints pt 4 ps 0.2 title "total", \
     "cache/stats/stats-cpu-daily.txt" using 1:($2/86400) with points pt 1 title "succeeded"

# weekly
set terminal png size 850,200
set output "cache/stats/stats-cpu-weekly.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%b"
set ylabel "CPU*weeks per week"
set grid xtics ytics

plot "cache/stats/stats-cpu-weekly.txt" using 1:($4/604800) with linespoints pt 4 ps 0.2 title "total", \
     "cache/stats/stats-cpu-weekly.txt" using 1:($2/604800) with points pt 1 title "succeeded"

# monthly
set terminal png size 850,200
set output "cache/stats/stats-cpu-monthly.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%Y"
set ylabel "CPU*months per month"
set grid xtics ytics

plot "cache/stats/stats-cpu-monthly.txt" using 1:($4/2592000) with linespoints pt 4 ps 0.2 title "total", \
     "cache/stats/stats-cpu-monthly.txt" using 1:($2/2592000) with points pt 1 title "succeeded"



# queues sizes plot:
set terminal png size 850,200
set output "cache/stats/stats-queues.png"
set xdata time
set timefmt "%Y-%m-%d %H:%M:%S"
set format x "%b %d"
#set xlabel "date"
set ylabel "#jobs"
set logscale y
set grid xtics ytics
set xtics "2010-01-01.0", 86400

plot "cache/stats/stats-queue-in-total.txt" using 1:3 with linespoints pt 4 ps 0.2 title "input total", \
     "cache/stats/stats-queue-in.txt" using 1:3 with linespoints pt 4 ps 0.2 title "input queue", \
     "cache/stats/stats-queue-out.txt" using 1:3 with linespoints pt 4 ps 0.2 title "output"


# busytime per hour plot:
set terminal png size 850,200
set output "cache/stats/stats-busytime-hourly.png"
set xdata time
set timefmt "%Y-%m-%d.%H"
set format x "%b %d"
set ylabel "busy time, s/hour"
unset logscale y
set grid xtics ytics
set xtics "2010-01-01.0", 86400

plot "cache/stats/stats-busytime-hourly.txt" using 1:3 with linespoints lt 3 pt 4 ps 0.2 title "busy time (wall)", \
     "cache/stats/stats-busytime-hourly.txt" using 1:2 with linespoints lt 1 pt 4 ps 0.2 title "busy time (CPU)"


# busytime per day plot:
set terminal png size 850,200
set output "cache/stats/stats-busytime-daily.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%b %d"
set ylabel "busy time, s/day"
unset logscale y
set grid xtics ytics
set xtics autofreq
plot "cache/stats/stats-busytime-daily.txt" using 1:3 with linespoints lt 3 pt 4 ps 0.2 title "busy time (wall)", \
     "cache/stats/stats-busytime-daily.txt" using 1:2 with linespoints lt 1 pt 4 ps 0.2 title "busy time (CPU)"

# busytime per week plot:
set terminal png size 850,200
set output "cache/stats/stats-busytime-weekly.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%b"
set ylabel "busy time, s/week"
unset logscale y
set grid xtics ytics
plot "cache/stats/stats-busytime-weekly.txt" using 1:3 with linespoints lt 3 pt 4 ps 0.2 title "busy time (wall)", \
     "cache/stats/stats-busytime-weekly.txt" using 1:2 with linespoints lt 1 pt 4 ps 0.2 title "busy time (CPU)"

# busytime per month plot:
set terminal png size 850,200
set output "cache/stats/stats-busytime-monthly.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%Y"
set ylabel "busy time, s/month"
unset logscale y
set grid xtics ytics
plot "cache/stats/stats-busytime-monthly.txt" using 1:3 with linespoints lt 3 pt 4 ps 0.2 title "busy time (wall)", \
     "cache/stats/stats-busytime-monthly.txt" using 1:2 with linespoints lt 1 pt 4 ps 0.2 title "busy time (CPU)"


# next plots are obsolete
exit

# connected machines plot:
set terminal png size 850,200
set output "cache/stats/stats-machines-hourly.png"
set xdata time
set timefmt "%Y-%m-%d %H:%M:%S"
set format x "%b %d"
#set xlabel "date"
set ylabel "#machines"
unset logscale y
set grid xtics ytics
set xtics "2010-01-01.0", 86400
plot "cache/stats/stats-machines-hourly.txt" using 1:3 with linespoints pt 4 ps 0.2 title "#machines"

# connected <#machines>/day plot:
set terminal png size 850,200
set output "cache/stats/stats-machines-daily.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%b %d"
#set xlabel "date"
set ylabel "<#machines>/day"
unset logscale y
set grid xtics ytics
set xtics autofreq
plot "cache/stats/stats-machines-daily.txt" using 1:2 with linespoints pt 4 ps 0.2 title "<#machines>"

# connected <#machines>/week plot:
set terminal png size 850,200
set output "cache/stats/stats-machines-weekly.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%b"
set ylabel "<#machines>/week"
unset logscale y
set grid xtics ytics
plot "cache/stats/stats-machines-weekly.txt" using 1:2 with linespoints pt 4 ps 0.2 title "<#machines>"

# connected <#machines>/month plot:
set terminal png size 850,200
set output "cache/stats/stats-machines-monthly.png"
set xdata time
set timefmt "%Y-%m-%d"
set format x "%Y"
set ylabel "<#machines>/month"
unset logscale y
set grid xtics ytics
plot "cache/stats/stats-machines-monthly.txt" using 1:2 with linespoints pt 4 ps 0.2 title "<#machines>"
