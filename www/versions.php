<?php
  $lurl2obj = unpackStr($q_gen_version);

  //get all generators and versions
  $query = $db->query("SELECT DISTINCT generator, version FROM histograms WHERE type = 'mc' ORDER BY 1, 2 DESC");
  $generators = array();
  
  while ($row = $query->fetch_row()) {
    $generator=$row[0];
    $version=$row[1];
    $generators[$generator][]=$version;
  }

  echo "<form action=\"index.php\" id=\"genform\">\n";
  echo "<input type=\"hidden\" name=\"query\" value=\"allplots\">\n";
  echo "<input type=\"hidden\" name=\"parse_form\" value=\"1\">\n";
  echo "<div class=\"genline\">\n";
  foreach (array_keys($generators) as $generator) {
    echo "<div class=\"genblock\">\n";
    $checked = array_key_exists($generator, $lurl2obj) ? "" : "selected";
    echo "<div><label for=\"$generator\">$generator</label></div>\n";
    echo "<select class=\"genselect\" name=\"$generator\" id=\"$generator\">\n";
    echo "<option $checked value=\"\">latest</option>\n";
    foreach ($generators[$generator] as $version) {
      $checked = (array_key_exists($generator, $lurl2obj) && ($lurl2obj[$generator][0] == $version)) ? "selected" : "";
      echo "<option $checked value=\"$version\">$version</option>\n";
    }
    echo "</select>\n";
    echo "</div>\n";
  }
  echo "</div>\n";
  echo "<br>\n";
  echo "<input type=\"submit\" value=\"Confirm\">\n";
  echo "</form>\n";
?>
