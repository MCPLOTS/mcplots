#!/bin/bash -e

# load `section` command for mcplots.conf parsing
source ../scripts/mcprod/alpgen/utils_alp/section.sh

# copy of ../scripts/mcprod/runRivet.sh:244 weed()
weed () {
  sed -e '/^#.*/ d' -e '/^ *$/ d' -e 's,  *, ,g' -e 's,^ ,,g' -e 's, $,,g'
}


fconf="mcplots.conf"
ferr=$(mktemp)

# 1) check the definition syntax
# the `|| true` is to avoid the error exit in the no-match (no syntax error) case
section $fconf abbreviations | (grep -v '=.*!.*!' || true) > $ferr
if test -s $ferr ; then
  echo "ERROR: $fconf incorrect syntax, expected format is 'name = xxx ! yyy ! zzz':"
  cat $ferr
  exit 1
fi


# 2) check for duplicates
section $fconf abbreviations | cut -d = -f 1 | sort | uniq -d > $ferr
if test -s $ferr ; then
  echo "ERROR: $fconf contains duplicate definitions for:" $(cat $ferr | xargs)
  
  cat $ferr | while read line ; do
    grep -n "$line" mcplots.conf
  done
  
  exit 1
fi


# 3) check for missing definitions
# TODO: upgrade to be the error
# TODO: take into account the possible "process.observable" in mcplots.conf
echo "INFO: the list of observables name missing in the mcplots.conf:"
{
# extract process name, observable name, cut name
#cat ../scripts/mcprod/configuration/rivet-histograms.map | weed | cut -d ' ' -f 2,6,7 | grep -v " - -$" | tr ' ' '\n' | sort -u
# extract process name, observable name
cat ../scripts/mcprod/configuration/rivet-histograms.map | weed | cut -d ' ' -f 2,6 | grep -v " -$" | tr ' ' '\n' | sort -u

# print all definitions from mcplots.conf
section $fconf abbreviations | cut -d = -f 1 | awk '{print $1}' | sort -u
section $fconf abbreviations | cut -d = -f 1 | awk '{print $1}' | sort -u
} | sort | uniq -u
