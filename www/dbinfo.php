<html>

<head>
  <title>Databases and tables</title>
</head>

<body>

<h1>Databases and tables</h1>

<?php
  // function returns the list of databases and tables
  function list_tables() {
    global $db;
    $tables = array();
    
    // list all available databases
    $query = $db->query("SHOW DATABASES");
    while ($row = $query->fetch_row()) {
      $name = $row[0];
      $tables[$name] = array();
    }
    
    // list tables in databases
    foreach (array_keys($tables) as $name) {
      $db->select_db($name);
      $query = $db->query("SHOW TABLES");
      while ($row = $query->fetch_row()) {
        $tables[$name][] = $row[0];
      }
    }
    
    return $tables;
  }
  
  // display contents of the table
  function display_table($table) {
    global $db;
    $filter = "";
    switch ($table) {
      case "production": $filter = "ORDER BY revision DESC LIMIT 100"; break;
      case "runs"      : $filter = "ORDER BY id DESC LIMIT 100"; break;
      case "telemetry" : $filter = "ORDER BY id DESC LIMIT 100"; break;
      default:           $filter = "LIMIT 100"; break;
    }
    
    $query = $db->query("SELECT COUNT(*) FROM $table $filter");
    $row = $query->fetch_row();
    echo "<p>Total entries: $row[0]<p>\n";
    
    echo "<table border=1>\n";
    
    $query = $db->query("SELECT * FROM $table $filter");
    
    // print columns names
    echo "<tr>\n";
    $fields = $query->fetch_fields();  // extract the fields in result
    foreach ($fields as $i) {
      $name = $i->name;
      echo "  <th>$name</th>\n";
    }
    echo "</tr>\n";
    
    // print table contents
    while ($row = $query->fetch_row()) {
      echo "<tr>\n";
      foreach ($row as $field) {
        echo "  <td><code>$field</code></td>\n";
      }
      echo "</tr>\n";
    }
    
    echo "</table>\n";
  }
  
  
  // open database
  $db = new mysqli("localhost", "mcplots", "");
  
  // print list of databases and tables
  $list = list_tables();
  
  echo "<ul>\n";
  foreach ($list as $name => $tables) {
    echo "  <li>" . $name;
    
    // display list of tables only for 'mcplots' database
    if ($name == "mcplots") {
      echo "<br>\n";
      
      echo "  <ul>\n";
      foreach ($tables as $i) {
        echo "    <li><a href=\"?table=$i\">$i</a></li>\n";
      }
      echo "  </ul>";
    }
    
    echo "</li>\n";
  }
  echo "</ul>\n";
  echo "\n";
  
  // print table content
  $table = $_GET["table"];
  
  // check the table is exists to avoid hacks
  if (in_array($table, $list["mcplots"])) {
    echo "<h3>Table $table</h3>\n";
    $db->select_db("mcplots");
    display_table($table);
    echo "\n";
  }
  
  $db->close();
?>

</body>

</html>
