<?php
  // convert seconds to milliseconds
  function ms($seconds) {
    return round(1000 * $seconds);
  }
  
  // get total number of executed SQL queries
  function totalSqlQueries() {
    global $db;
    $query = $db->query("SHOW SESSION STATUS LIKE 'Questions'");
    if (!$query) return 0;
    
    static $ncalls = 0;
    $ncalls++;
    
    $row = $query->fetch_assoc();
    
    // substraction of ($ncalls + 1) is to compensate for 'SHOW STATUS' and initial `new mysqli()` connect
    return $row["Value"] - ($ncalls + 1);
  }
  
  // performance profiler: timing, memory and SQL usage
  class Profiler
  {
    // time stamps storage
    private $stamps = array();
    
    public function __construct() {
      $this->start();
    }
    
    public function start() {
      $this->stamp("start");
    }
    
    public function stamp($tag) {
      $this->stamps[] = array("tag" => $tag, "time" => microtime(true), "mem" => memory_get_usage(false));
    }
    
    public function totalInterval() {
      $n = count($this->stamps);
      if ($n < 2) return 0;
      
      return $this->stamps[$n - 1]["time"] - $this->stamps[0]["time"];
    }
    
    public function elapsedFromStart() {
      $this->stamp("stop");
      return $this->totalInterval();
    }
    
    public function maxMemoryUsage() {
      $max = 0;
      foreach ($this->stamps as $stamp)
        $max = max($stamp["mem"], $max);
      
      return $max;
    }
    
    public function summarize() {
      $sum = array();
      $t0 = 0;
      
      foreach ($this->stamps as $stamp) {
        $tag = $stamp["tag"];
        $t = $stamp["time"];
        
        if ($tag != "start") {
          $sum[$tag] += $t - $t0;
        }
        
        $t0 = $t;
      }
      
      return $sum;
    }
    
    public function summaryLine() {
      $sum = $this->summarize();
      $line = array();
      
      foreach ($sum as $k => $v)
        $line[] = ms($v) . " ms (" . $k . ")";
      
      $line[] = "mem = " . round($this->maxMemoryUsage() / 1e6, 1) . " MB";
      $line[] = "sql = " . totalSqlQueries() . " queries";
      
      return implode(", ", $line);
    }
  }
?>
