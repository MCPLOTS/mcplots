#!/bin/bash -v

pstree -a

systemctl --no-pager status mariadb
systemctl --no-pager status php-fpm
systemctl --no-pager status httpd

echo "show full processlist" | mysql -u mcplots mcplots

tail -n 20 /var/log/httpd/error_log

tail -n 20 /var/log/php-fpm/error.log

tail -n 20 /var/log/php-fpm/www-error.log
