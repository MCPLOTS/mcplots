Script and style to add the xmas mood.

The snow.js is based on the:

  https://github.com/hyperstown/pure-snow.js
  (under the MIT License)

Installation:

- add the style load to the header of index.php:

<head>
  <link rel="stylesheet" href="snow/snow.css">

- add the script load to the first line of main.php:

<script src="./snow/snow.js" defer></script>
<div id="snow" data-count="200"></div>
