<div class="fullpage">
<h1>Numerical comparison</h1>
<div class="valid carrelage">
  <a class="validcarre" href="?query=validgen">
    <h2 class="d3"><span>Tuning validation</span></h2>
    <div class="text-clarif">
    For each implemented generator/version, by selecting its tunes to display from the list, you can compare their mean &chi;<sup>2</sup> (as well as maximum and minimum) for each hard process. A detailed table with &chi;<sup>2</sup> for each observable and corresponding plots are available by clicking on the mean &chi;<sup>2</sup> of interest.
    </div>
    <!--Numerical comparison of different tunes of a generator. Select a generator and its version in the left menu, a table with chi2 for all available tunes will appear. The tunes can be chosen in the above menu.  In the table rows correspond to implemented for this generator/version/tune hard processes and columns to selected tunes. The main number in each cell is a mean chi2 for a given hard process comparing experimental data and a theoretical model of interest. This chi2 is calculated using all available at the moment plots. The minimum value of chi2 is shown below this number, and the maximum value is shown above. The background color of each sub-cell reflects its chi2 value. White cells correspond to a non-calculated chi2, due to different number of bin in theory and data histograms, for example.

    <br>
    To make chi2 values more physically meaningful, the MC predictions are assigned a flat 5% 'theory uncertainty', as a baseline sanity limit for the achievable theoretical accuracy with present-day MC models. A few clear cases of <a href="http://en.wikipedia.org/wiki/Garbage_In,_Garbage_Out">GIGO</a> are excluded, but some problematic cases remain. Thus, e.g., if a calculation returns a too small cross section for a dimensionful quantity, the corresponding chi2 value will be large, even though the shape of the distribution may be well described.
    <br>
    When clicking on the mean chi2 in the table, a new table of chi2s for each observable for a given hard process will be shown. These individual chi2s are also clickable and allow to see the corresponding plot.-->
    <br>
    <img src="figs1/tunes.jpg" alt="tunestables">
  </a>
  <a class="validcarre" href="?query=valid">
    <h2 class="d3"><span>Generator validation</span></h2>
    <div class="text-clarif">
    For each implemented generator/tune by selecting its versions, you can compare their mean &chi;<sup>2</sup> and their changes (&Delta;) calculated for hard processes. A detailed table with &chi;<sup>2</sup> and their &Delta; for each obervable and corresponding plots are also available.
    </div>
    <!--Changes between generator versions. Select a generator and its tune in the left menu, a table with chi2 for all available versions will appear. The versions can be chosen in the above menu. As in the tunes table, rows correspond to hard processes and main columns to selected generator versions, shown in chronological order. In this table, delta columns are added between the version columns, showing the change in the mean, minimum and maximum chi2. Colors in these delta columns reflect this change.
    <br>
    A click on the mean chi2 will result in a new table with individual for each observable chi2s, as in the tunes table. A click on the delta leads a new table with individual chi2 values for each observable for two versions of interest, as well as individual deltas. Individual chi2 values will show corresponding plots with one theoretical curve while an individual deltas will show a plot with two curves corresponding to two generator versions.-->
    <br>
    <img src="figs1/versions.jpg" alt="versionstables">
    <br>
  </a>
</div>
</div>
