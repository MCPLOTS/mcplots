<?php

  list($q_generator, $q_tune, $q_version, $q_ref) = explode($safeDelimiter, $q_valid_gtvr);
  $param = $q_generator.$safeDelimiter.$q_tune.$safeDelimiter.$q_version;
  $tuneval = "";
  if ($_GET["fromtunes"]) {
    print_tunevalidation_menu();
    $tuneval = "&fromtunes=1";
  }
  else print_versvalidation_menu();

  $isData = ($q_ref == "data");
  //$isData variable is false when comparing 2 generators
  $ltext = $isData ? "" : "vs. v.$q_ref";
  
  $gNew=$q_version;
  $gOld=$q_ref; // this doesn't need to be generator

  $shownote=false;
  $shownote2=false;

  function err_handle($chi2) {
    global $shownote;
    global $shownote2;

    //distinguish different bin size error
    if ($chi2 >= 0) return $chi2; // no error
    elseif ($chi2 == -1) {
      $shownote = true;
      return "n/a*";
    }
    elseif($chi2 == -2) {
      $shownote2 = true;
      return "n/a**";
    }
    else {
      return $chi2;
    }
  }
  
  function simplechi2($chi2) {
    if (($chi2 == -1) || ($chi2 == -1)) return "n/a";
    else return $chi2;
  }
  
  function mean_chi2($sumchi2, $nchi2) {

    if (($nchi2 > 0) && ($sumchi2 > 0) ) return $sumchi2/$nchi2;
    else return "n/a";
  }

  $dispproc = $c->name($q_process);
  $dispgen = $c->name($q_generator);
  echo "<div class=\"rightpage\">\n";
  echo "<h2 class=\"d2\"><span> Details for $dispgen $q_version $q_tune $ltext</span></h2>\n";
  echo "\n";

  echo "<h3 class=\"d3\"><span>" . $c->name($q_beamgroup) . " &rarr; $dispproc</span></h3>\n";

  $fname = str_replace(" ", "_", "$q_generator-$q_version-$q_tune-$q_beamgroup-$q_process.txt");
  $fname = str_replace("/", "_", $fname);
  $fname = str_replace("*", "x", $fname);

  //mc1 and mc2 for the old table based on "cuts" (now used for generators), tab1 for the new representation based on "an - histid" (now used for tunes)
  $mc1 = array();
  $mc1_mcid = array();
  $mc1_dataid = array();
  $tab1 = array();
  $tab1_mcid = array();
  $tab1_dataid = array();
  if (file_exists("cache/chi2/$fname")){
          $fchi = fopen("cache/chi2/$fname", 'r');
          while(!feof($fchi))
          {
              $line = fgets($fchi);
              list($p,$chi,$mcid,$dataid) = explode(" ", substr($line, 0, -1));
              if (!is_numeric($chi)) continue;
              list(,$observable,$energy,$hitref,$histid,$cuts ) = explode($safeDelimiter, $p);
              $cuts .= $safeDelimiter . $hitref . $safeDelimiter . $histid;
              $mc1[$observable][$cuts][$energy] = $chi;
              $mc1_mcid[$observable][$cuts][$energy] = $mcid;
              $mc1_dataid[$observable][$cuts][$energy] = $dataid;
              $tab1[$observable][$hitref][$histid]["energy"] = $energy;
              $tab1[$observable][$hitref][$histid]["chi2"] = $chi;
              $tab1_mcid[$observable][$hitref][$histid] = $mcid;
              $tab1_dataid[$observable][$hitref][$histid] = $dataid;
          }
          fclose($fchi);
        }

  //sort the array : by observable, then by cut and then by energy
  ksort($mc1);
  foreach (array_keys($mc1) as $obs){
    ksort($mc1[$obs]);
    foreach (array_keys($mc1[$obs]) as $cut){
      ksort($mc1[$obs][$cut]);
    }
  }
  ksort($tab1);
  foreach (array_keys($tab1) as $obs){
    ksort($tab1[$obs]);
    foreach (array_keys($tab1[$obs]) as $an){
      ksort($tab1[$obs][$an]);
    }
  }

  if (!$isData){
    $fname = str_replace(" ", "_", "$q_generator-$q_ref-$q_tune-$q_beamgroup-$q_process.txt");
    $fname = str_replace("/", "_", $fname);
    $fname = str_replace("*", "x", $fname);

    $mc2 = array();
    $mc2_mcid = array();
    $mc2_dataid = array();
    if (file_exists("cache/chi2/$fname")){
            $fchi = fopen("cache/chi2/$fname", 'r');
            while(!feof($fchi))
            {
                $line = fgets($fchi);
                list($p,$chi,$mcid,$dataid ) = explode(" ", substr($line, 0, -1));
                if (!is_numeric($chi)) continue;
                list(,$observable,$energy,$hitref,$histid,$cuts ) = explode($safeDelimiter, $p);
                $cuts .= $safeDelimiter . $hitref . $safeDelimiter . $histid;
                $mc2[$observable][$cuts][$energy] = $chi;
                $mc2_mcid[$observable][$cuts][$energy] = $mcid;
                $mc2_dataid[$observable][$cuts][$energy] = $dataid;
            }
            fclose($fchi);
          }

    // compare mc1 and mc2 and add absent in mc1 observable/cut/energy from mc2. Unset corresponding chi2.
    // find array of absent in mc1 observables, unset chi2, add it to mc1 and sort the new mc1
    $arrdif = array_diff_key($mc2, $mc1);
    foreach (array_keys($arrdif) as $obs){
      foreach (array_keys($arrdif[$obs]) as $cut){
        foreach (array_keys($arrdif[$obs][$cut]) as $en){
          $arrdif[$obs][$cut][$en] = "";
        }
      }
    }
    $mc1 += $arrdif;
    ksort($mc1);

    //for each observable
    foreach (array_keys($mc2) as $obs){
      //find array of absent in mc1 cuts, unset chi2, add it to mc1 and sort
      $arrdifcut = array_diff_key($mc2[$obs], $mc1[$obs]);
        foreach (array_keys($arrdifcut) as $cut){
          foreach (array_keys($arrdifcut[$cut]) as $en){
            $arrdifcut[$cut][$en] = "";
          }
        }
      $mc1[$obs] += $arrdifcut;
      ksort($mc1[$obs]);

      //for each cut find array of absent in mc1 energies, unset chi2, add it to mc1 and sort
      foreach (array_keys($mc2[$obs]) as $cut){
        $arrdifen = array_diff_key($mc2[$obs][$cut], $mc1[$obs][$cut]);
        foreach (array_keys($arrdifen) as $en){
          $arrdifen[$en] = "";
        }
        $mc1[$obs][$cut] += $arrdifen;
        ksort($mc1[$obs][$cut]);
      }
    }
  }
  
  $histo_total = 0;
  
  //count the total number of histograms in the table
  $nhiststab=count($tab1, COUNT_RECURSIVE)-count($tab1);
  foreach (array_keys($tab1) as $obs){
    $nhiststab -= count($tab1[$obs]);
    foreach (array_keys($tab1[$obs]) as $an){
      $nhiststab -= count($tab1[$obs][$an]);
    }
  }
  //we have energy and chi2 for each hist, so /2:
  $nhiststab /= 2;

  //calculate some auxiliary data for the collapsed table
  $auxtab1 = array();
  $showColChiNote = False;
  foreach (array_keys($tab1) as $obs){
    $auxtab1[$obs]["ncollapsed"] = 0;
    foreach  (array_keys($tab1[$obs]) as $an) {
      if (count($tab1[$obs][$an]) > 1) $auxtab1[$obs]["ncollapsed"] += 1;
      foreach (array_keys($tab1[$obs][$an]) as $histid) {
        if ($tab1[$obs][$an][$histid]["chi2"] > 0) { 
          $auxtab1[$obs][$an]["sumchi2"] += $tab1[$obs][$an][$histid]["chi2"];
          $auxtab1[$obs][$an]["nchi2"] += 1;
        }
        $auxtab1[$obs][$an]["energies"][] = $tab1[$obs][$an][$histid]["energy"];
      }
      if (count(array_count_values($auxtab1[$obs][$an]["energies"])) > 1) $auxtab1[$obs][$an]["energy"] = "MANY";
      else $auxtab1[$obs][$an]["energy"] = $auxtab1[$obs][$an]["energies"][0];
      $auxtab1[$obs][$an]["chi2"] = mean_chi2($auxtab1[$obs][$an]["sumchi2"],$auxtab1[$obs][$an]["nchi2"]);
    }
  }

  //print the tunes table
  if ($isData){
  echo "<table class=\"nodelta\">\n";
  //table Header begin
  echo "  <tr>\n";
  echo "    <th>Observable</th>\n";
  echo "    <th>Reference</th>\n";
  echo "    <th>Energy</th>\n";
  echo "    <th class=\"chicol\">&chi;<span class=\"supsub\"><span>2</span><span>+5%</span></th>\n";
  echo "    <th> </th>\n";
  echo "  </tr>\n";
  //table Header end
  
  foreach (array_keys($tab1) as $obs){
    $dispSub = $c->submenu($obs, $q_process);
    if ($dispSub != "") $dispSub = "<i style=\"color: grey\">$dispSub</i><br>";
    $dispObs = $c->name($obs, $q_process);
    $numobsaccord = (count($tab1[$obs], COUNT_RECURSIVE)-count($tab1[$obs]))/3+$auxtab1[$obs]["ncollapsed"];
    echo "  <tr>\n";
    // some cosmetics for the observable name td because FF has its own way to treat spanned+collapsed tables...
    $divObsCl = "";
    if (($numobsaccord > 1) && ($dispSub == "")) $divObsCl = " class=\"obspad\"";
    echo "  <td class=\"obscol\" rowspan = \"$numobsaccord\"><div$divObsCl>$dispSub $dispObs</div></td>\n";
    $newrow=False;
    foreach (array_keys($tab1[$obs]) as $an){
        if ($newrow) {echo "<tr>\n";}
        echo "  <td> <a href=\"https://rivet.hepforge.org/analyses/$an.html\" target=\"_blank\" rel=\"noopener noreferrer\">$an</a></td>\n";
        echo "  <td> ".$auxtab1[$obs][$an]["energy"]." </td>\n";
        //echo "  <td> ".$chi2val." </td>\n";
        $chi2val = $auxtab1[$obs][$an]["chi2"];
        echo "  <td class=\"chicol " . get_color($chi2val) . "\"> ";
        if (count($tab1[$obs][$an]) > 1) {
          $notesign = "";
          if ((count($tab1[$obs][$an]) > $auxtab1[$obs][$an]["nchi2"]) && (is_numeric($auxtab1[$obs][$an]["chi2"]))) {
            $showColChiNote = True;
            $notesign = "*";
          } 
          echo spec_format($chi2val)."$notesign</td>\n";
          echo "<td><span class=\"toexpand\" onclick=\"expandAN(this, '" . $obs . $an . "')\"></span></td>\n";
          echo "</tr>\n";
          foreach (array_keys($tab1[$obs][$an]) as $histid){
            echo "<tr class=\"collapsed " . $obs . $an . "\">\n";
            echo "<td class=\"histid\"> $histid </td>\n";
            echo "<td> ".$tab1[$obs][$an][$histid]["energy"]." </td> \n";
            $chi2val=simplechi2($tab1[$obs][$an][$histid]["chi2"]);
            $mcid = $tab1_mcid[$obs][$an][$histid];
            $dataid = $tab1_dataid[$obs][$an][$histid];
            $idquery = $dataid.$safeDelimiter.$mcid;
            echo "<td class=\"chicol ".get_color($chi2val) . "\"> ";
            printf("<a href=\"%s\">%s</a></td>\n",
                    prepare_link(array("imgdetail","","","","","",$param))."&idquery=$idquery".$tuneval,
                    spec_format($chi2val));
            echo "<td> </td>\n";
            echo "</tr>\n";
          }
          
        }
        else {
            $hist = key($tab1_mcid[$obs][$an]);
            $mcid = $tab1_mcid[$obs][$an][$hist];
            $dataid = $tab1_dataid[$obs][$an][$hist];
            $idquery = $dataid.$safeDelimiter.$mcid;
            printf("<a href=\"%s\">%s</a></td>\n",
                    prepare_link(array("imgdetail","","","","","",$param))."&idquery=$idquery".$tuneval,
                    spec_format($chi2val));
            echo "  <td> </td>\n";
        }
        echo "</tr>\n";
        $newrow=True;
    }
  }
  echo "</table>\n";
  echo "<br>\n";
  if ($showColChiNote){
   echo "*  several \"n/a\" histograms were excluded from the calculation <br><br/>";
  }
  echo "<br>\n";
  echo "n/a : not applicable because the number of bins in the data histogram differs from the number of bins in the theory histogram<br><br/>\n";
  
  echo "<br>\n";
  echo "The page data is based on $nhiststab histograms.\n";
  }
  //end of the tunes table
  
  else{
  //print the generator versions table - the old one
  //table begin
  echo "<table class=\"validdetails\">\n";

  //table Header begin
  echo "  <tr>\n";
  echo "    <th class=\"dmid medText bott-bord-thick\">Observable</th>\n";
  echo "    <th class=\"dmid medText bott-bord-thick\">Cut</th>\n";
  echo "    <th class=\"dmid medText bott-bord-thick\">Energy</th>\n";
  echo "    <th class=\"dmid medText bott-bord-thick left-bord-thick\">&chi;<span class=\"supsub\"><span>2</span><span>+5%</span></span> ($gNew)</th>\n";
  echo "    <th class=\"mcd dmid medText bott-bord-thick\" >&Delta;</th>\n";
  echo "    <th class=\"dmid medText bott-bord-thick\" >&chi;<span class=\"supsub\"><span>2</span><span>+5%</span></span> ($gOld)</th>\n";
  echo "  </tr>\n";
  //table Header end




    foreach (array_keys($mc1) as $observable ){
    //ksort($mc1[$observable]);
    $numobs = count($mc1[$observable], COUNT_RECURSIVE)-count($mc1[$observable]);
    $dispSub = $c->submenu($observable, $q_process);
    if ($dispSub != "") $dispSub = "<i style=\"color: grey\">$dispSub</i><br>";
    $dispObs = $c->name($observable, $q_process);
    echo "  <tr>\n";
    echo "  <td rowspan = \"$numobs\">$dispSub $dispObs</td>\n";

    foreach (array_keys($mc1[$observable]) as $cuts){
      $numcuts = count($mc1[$observable][$cuts]);
      //if ($numcuts > 1) ksort($mc1[$observable][$cuts]);
      list($dispCut, $histrefid) = explode($safeDelimiter, $cuts);
      $dispCut = $c->name($dispCut);
      //$dispCut .= " " . $histrefid;
      echo "  <td rowspan = \"$numcuts\">$dispCut</td>\n";

      foreach (array_keys($mc1[$observable][$cuts]) as $energy) {
        echo "  <td> $energy </td>\n";
        $chi2val = err_handle($mc1[$observable][$cuts][$energy]);
        $histo_total += 1;

        $mcid = $mc1_mcid[$observable][$cuts][$energy];
        $dataid = $mc1_dataid[$observable][$cuts][$energy];
        $idquery = $dataid.$safeDelimiter.$mcid;
        echo "  <td class=\"mn " . get_color($chi2val) . "\"> ";
        printf("<a class=\"clblack\" href=\"%s\">%s</a></td>\n",
                prepare_link(array("imgdetail","","","","","",$param))."&idquery=$idquery".$tuneval,
                spec_format($chi2val));

        //adding delta and chi2 for the second generator
        if (!$isData){
          $chi2val2 = err_handle($mc2[$observable][$cuts][$energy]);
          $histo_total += 1;
          $mcid2 = $mc2_mcid[$observable][$cuts][$energy];
          $dataid2 = $mc2_dataid[$observable][$cuts][$energy];
          $idquery2 = $dataid2.$safeDelimiter.$mcid2;

          if (!($chi2val2)) $deltaidquery = $idquery;
          elseif (!($chi2val)) $deltaidquery = $dataid2.$safeDelimiter.$mcid2;
          else $deltaidquery = $idquery.$safeDelimiter.$mcid2;

          $dif = "";
          $dchi = "-";
          if ((is_numeric($chi2val)) && (is_numeric($chi2val2))) {
            $dif = $chi2val2 - $chi2val;
            $dchi=((abs($dif) < 1000) ? ((spec_format($chi2val2)) - (spec_format($chi2val))) : ($dif));
          }

          echo "    <td class=\"mcd ".get_deltColor($dchi,true)." \">";
          printf("<a class=\"%s\" href=\"%s\">%s</a></td>\n",
                  get_deltColor($dchi),
                  prepare_link(array("imgdetail","","","","","",$q_valid_gtvr))."&idquery=$deltaidquery".$tuneval,
                  spec_format($dchi,true));

          echo "  <td class=\"mn " . get_color($chi2val2) . "\"> ";
          printf("<a class=\"clblack\" href=\"%s\">%s</a></td>\n",
                prepare_link(array("imgdetail","","","","","",$param))."&idquery=$idquery2".$tuneval,
                spec_format($chi2val2));
        }

        echo "</tr>\n";
      }
    }
  }

  
    echo "</table>\n";
    echo "<br>\n";


  if ($shownote){
   echo "*  not applicable: because number of bins of data histogram is different than number of bins of theory histogram <br><br/>";
  }
  if ($shownote2){
   echo "** not applicable: because there is not any valid bin<br/>";
  }
  
  echo "<p>The page data is based on $histo_total histograms.</p>\n";
  }
  
  //tablebody end <<<<<
  echo "</div>\n";

?>
