<div class="espaceur" id="curstatgen"></div>
<h2 class="d3"><span> Generators </span></h2>
<section >
<?php
    //count and print statistics
    $query = $db->query("SELECT COUNT(DISTINCT generator,version) FROM histograms WHERE type='mc'");
    $genver = $query->fetch_row();
    $query = $db->query("SELECT COUNT(DISTINCT generator,version,tune) FROM histograms WHERE type='mc'");
    $genvertune = $query->fetch_row();
    echo "Implemented are <b>$genver[0]</b> generator-versions, <b>$genvertune[0]</b> generator-version-tunes.<br><br>\n";

    $glist = array();
    $query = $db->query("SELECT DISTINCT generator FROM histograms WHERE type = 'mc' ORDER BY generator");
    while ($row = $query->fetch_row()) $glist[] = $row[0];
    
    //print stats for each generator : number and names of version and of tunes
    foreach ($glist as $generator) {
      $logofile = (substr($generator,0,6) == "pythia")? "figs1/logo_pythia.png" : "figs1/logo_".$generator.".png";
      if (file_exists($logofile)) {
        echo "<img src=$logofile alt=\"$generator logo\" height=50px><br>\n";
      }

      //special addition of the Pythia version
      $pyver = "";
      if ($generator == "pythia6") $pyver = "6";
      else if ($generator == "pythia8") $pyver = "8";

      echo "<b>".$c->name($generator)." $pyver</b><br>\n";

      //print versions
      $vlist = array();
      $query = $db->query("SELECT DISTINCT version FROM histograms WHERE generator = '$generator' ORDER BY version DESC");
      while ($row = $query->fetch_row()) $vlist[] = $row[0];
      
      $nvers=count($vlist);
      echo "<b>$nvers  version" . (($nvers == "1") ? "" : "s") . ":</b> \n";
      echo implode(", ", $vlist) . ".<br>\n";

      //print tunes
      $tlist = array();
      $query = $db->query("SELECT DISTINCT tune FROM histograms WHERE generator = '$generator' ORDER BY tune");
      while ($row = $query->fetch_row()) $tlist[] = $row[0];
      
      $ntunes=count($tlist);
      echo "<b>$ntunes  tune" . (($ntunes == "1") ? "" : "s") . ":</b> \n";
      echo implode(", ", $tlist) . ".<br><br>\n";
    }
?>
</section>
<div class="espaceur" id="curstatan"></div>
<h2 class="d3"><span> Data analyses </span></h2>
<section >
<?php
    // TODO: include MC-only analyses
    $query = $db->query("SELECT COUNT(DISTINCT reference) FROM histograms WHERE type = 'data'");
    $an=$query->fetch_row();
    echo "Implemented are <b>$an[0]</b> data analyses.<br>\n";
    
    $query = $db->query("SELECT COUNT(*) FROM histograms");
    $htot = $query->fetch_row();
    $query = $db->query("SELECT COUNT(*) FROM histograms WHERE type = 'mc'");
    $hmc = $query->fetch_row();
    $query = $db->query("SELECT COUNT(*) FROM histograms WHERE type = 'data'");
    $hdata = $query->fetch_row();
    echo "Total <b>$htot[0]</b> histograms (mc $hmc[0] and data $hdata[0]).<br><br>\n";
    
    $query = $db->query("SELECT experiment, COUNT(DISTINCT reference) FROM histograms WHERE type = 'data' GROUP BY experiment");
    while ($row = $query->fetch_row()) {
      $experiment = $row[0];
      $ndan = $row[1];
      $an = ($ndan == 1) ? "analysis" : "analyses";
      echo "<b>$experiment:</b> $ndan $an<br>\n";
    }
    echo "<br>\n";
?>
</section>
