<?php

  list($q_generator, $q_version, $q_tune, $q_ref) = array_pad(explode($safeDelimiter, $q_valid_gtvr), 4, "");
  // if the gen-version is not specified, choose the first one
  if ($q_version == "") {
    $query = $db->query("SELECT DISTINCT generator, version FROM histograms WHERE type = 'mc' ORDER BY 1, 2");
    $row = $query->fetch_row();
    $q_generator  = $row[0];
    $q_version    = $row[1];
  }

  print_tunevalidation_menu();

  echo "<div class=\"rightpage\">\n";
  echo "<h2 class=\"d2\"><span> $q_generator  $q_version  tunes validation</span></h2>\n";
  echo "\n";
  
  // get list of available tunes
  $query = $db->query("SELECT DISTINCT tune
                        FROM histograms
                        WHERE generator = '$q_generator' AND version = '$q_version' AND type = 'mc'
                        ORDER BY tune");
  $tunes_avail = array();
  while ($row = $query->fetch_assoc()) {
    $tunes_avail[] = $row["tune"];
  }
  
  // re-sort to put '*default*' tunes first
  function  is_default($s) { return (stripos($s, "default") !== false); }
  function not_default($s) { return !is_default($s); }
  $tunes_avail = array_merge(array_filter($tunes_avail, "is_default"), array_filter($tunes_avail, "not_default"));
  
  // sanitize user input for tunes display
  $tunes_sel = array_value("tunes", $_GET, []);
  $tunes_sel = array_intersect($tunes_sel, $tunes_avail);
  
  // by default display 3 first tunes
  if (count($tunes_sel) == 0) {
    $tunes_sel = array_slice($tunes_avail, 0, 3);
  }
  
  // print list of available tunes
  echo "  <form method=\"get\">\n";
  echo "  <input type=hidden name=query value=\"" . $_GET["query"] . "\">\n";
  echo "    <table>\n";
  echo "      <tr>\n";
  echo "        <th>Tunes:</th>\n";
  echo "        <td>\n";
  
  foreach ($tunes_avail as $tune) {
    $chk = in_array($tune, $tunes_sel) ? "checked" : "";
    echo "  <label><input type=checkbox name=tunes[] value=\"$tune\" $chk><span>$tune</span></label>\n";
  }
  
  echo "        </td>\n";
  echo "      </tr>\n";
  echo "      <tr>";
  echo "        <td></td>";
  echo "        <td><input type=\"submit\" value=\"Display\"></td>\n";
  echo "      </tr>\n";
  echo "    </table>\n";
  echo "  </form>\n";
  echo "<br>\n";
  
  // list DATA histograms
  $query = $db->query("SELECT *
                        FROM histograms
                        WHERE type='data'
                        ORDER BY beam, process, observable, energy, cuts, tune DESC");
  $mydata = array();
  while ($row = $query->fetch_assoc()) {
    $beam = $row["beam"];
    if ($beam == "pp" || $beam == "ppbar") $beam = "ppppbar"; // combine pp/ppbar
    $params  = implode($safeDelimiter, [$row["observable"],$row["energy"],$row["cuts"]]);
    $process = $row["process"];
    $mydata[$beam][$process][$params][] = $row;
  }

  // list MC histograms
  $query = $db->query("SELECT *
                        FROM histograms
                        WHERE generator = '$q_generator' AND version = '$q_version' AND tune IN ('" . implode("','", $tunes_sel)  . "')
                        ORDER BY beam, process, observable, energy, cuts, tune DESC");
  $mytable = array();
  while ($row = $query->fetch_assoc()) {
    $beam = $row["beam"];
    if ($beam == "pp" || $beam == "ppbar") $beam = "ppppbar"; // combine pp/ppbar
    $process = $row["process"];
    $tune = $row["tune"];
    $params  = implode($safeDelimiter, [$row["observable"],$row["energy"],$row["cuts"]]);
    $mytable[$beam][$process][$tune][$params][] = $row;
  }

  
  $histo_total = 0;
  
  echo "<table class=\"validation\">\n";

  //table Header begin
  $minrow="";
  $tunerow="";
  $maxrow="";
  foreach ($tunes_sel as $tune) {
    
    $maxrow  .="    <th class=\"dup\">max</th>\n";
    $tunerow .="    <th class=\"dmid\">$tune</th>\n";
    $minrow  .="    <th class=\"ddwn\">min</th>\n";
  }
  echo "  <tr>\n";
  echo "    <th class=\"mn\" rowspan=\"3\">&lt;&chi;<sup>2</sup>&gt;<br>\n";
  echo "      <span class=\"smallText\">incl. 5% \"theory uncertainty\" on all points</span></th>\n";
  echo "$maxrow";
  echo "  </tr>\n";
  echo "  <tr>\n$tunerow  </tr>\n  <tr>\n$minrow  </tr>\n";
  echo "\n";
  //table header end

  //table body begin >>>>>
  //loop through beam and processes (creating rows)
  foreach (array_keys($mytable) as $beam ){
    foreach (array_keys($mytable[$beam]) as $process){
      //validation row begin >>>>

      //initialization of loop
      $minrow   = "";
      $tunerow  = "";
      $maxrow   = "";
      
      //loop through tunes to create columns of a row begin >>>
      foreach ($tunes_sel as $tune) {
        
        //comparison of tune and DATA
        $curr=$tune;
        $ref="data";

        $fname = str_replace(" ", "_", "$q_generator-$q_version-".$curr."-$beam-$process.txt");
        $fname = str_replace("/", "_", $fname);
        $fname = str_replace("*", "x", $fname);
        $cname = "cache/chi2/$fname";

        $allchi2=array();
        if (file_exists($cname)){
          $fchi = fopen($cname, 'r');
          while (($line = fgets($fchi)) !== false)
          {
              $linedata = explode(" ", substr($line, 0, -1));
              list($p,$chi,, ) = $linedata;
              if (($chi < 0) || (!is_numeric($chi))) {
                continue;
              }
              $allchi2[$p] = $chi;
          }
          fclose($fchi);
        }
        $nchi = count($allchi2);
        $histo_total += $nchi;
        if ($nchi > 0) {
          $maxchi=max($allchi2);
          $minchi=min($allchi2);
          $avgchi=array_sum($allchi2)/$nchi;
        }
        else {
          $maxchi = "";
          $minchi = "";
          $avgchi = "?";
        }
        $minrow  .= "    <td class=\"ddwn ".get_color($minchi)."\" >".spec_format($minchi)."</td>\n";
        $param = $q_generator.$safeDelimiter.$curr.$safeDelimiter.$q_version.$safeDelimiter.$ref;
        $tunerow .= "    <td class=\"dmid ".get_color($avgchi)." \">";
        $tunerow .= sprintf("<a class=\"clblack\" href=\"%s\">%s</a>",
                      prepare_link(array("validdetail",$beam,$process,"","","",$param))."&fromtunes=1",
                      spec_format($avgchi));
        $tunerow .= "</td>\n";
        $maxrow  .= "    <td class=\"dup ".get_color($maxchi)." \">".spec_format($maxchi)."</td>\n";


        }
        echo "  <tr>\n";
        echo "    <td class=\"mn right-bord-thick\" rowspan=\"3\" >";
        echo $c->name($beam) . " &rarr; " . $c->name($process);
        echo "</td>\n";
        echo "$maxrow";
        echo "  </tr>\n";
        echo "  <tr>\n$tunerow</tr>\n  <tr>\n$minrow</tr>\n";
        echo "\n";
      }
    }

  echo "</table>\n";
  
  echo "<p>Legend: ";
  echo "<span class=\"" . get_color(0.5) . "\">[ &chi;<sup>2</sup> &lt; 1 ]</span> / ";
  echo "<span class=\"" . get_color(2) . "\"> [ 1 &le; &chi;<sup>2</sup> &lt; 4 ]</span> / ";
  echo "<span class=\"" . get_color(5) . "\"> [ 4 &le; &chi;<sup>2</sup> ]</span>.";
  echo "The '?' means the &chi;<sup>2</sup> computation is not yet complete.<br>\n";
  
  echo "<p>(click on number in the table cell to see individual observables)</p>\n";

  echo "<p>The page data is based on $histo_total histograms.</p>\n";

?>
