#!/bin/bash -e

function dosql () {
  #cat
  mysql --skip-column-names -u mcplots mcplots
}


# 1) remove all powheg plots EXCEPT OF WW and Hard QCD (Jets) processes:
echo "INFO: powheg: keep only ww and jets"
echo "delete from histograms where generator='powheg-box' and process != 'ww' and process != 'jets'; SELECT ROW_COUNT();" | dosql


# 2) observable names upgrade for consistency:

function list_obs_rename () {
cat << END
z.pt  ->  ll.pt
z.pt._z.pt   ->  z.pt_z.pt
z.m  ->  ll.m
z.y  ->  ll.y
z.pt_norm  ->  ll.pt
ll.phiStar_norm  ->  ll.phiStar
z.y_norm  ->  ll.y
softfrop.zg  ->  softdrop.zg
pt-vs-dphi  ->  sumpt-vs-dphi
njets_njets  ->  njetsR
jj.dpt.rel  ->  jj.dpt
END
}

list_obs_rename | while read old x new ; do
  echo "INFO: rename '$old' to '$new'"
  
  # the observable column is "enum" type,
  # check the value of 'new' already exists in the table to avoid corruption
  hasnewval=$(echo "SELECT * FROM histograms WHERE observable='$new' limit 1" | dosql)
  if [[ "$hasnewval" == "" ]] ; then
    echo "WARNING: can't rename the '$old' as the value '$new' does not already exists in DB, skip"
    continue
  fi
  
  echo "UPDATE histograms SET observable='$new' WHERE observable='$old'; SELECT ROW_COUNT();" | dosql
done

# ANALYSIS : CMS_2011_S9120041: Var : pt  -> pt-trns
echo "INFO: rename CMS_2011_S9120041: pt  -> pt-trns"
echo "UPDATE histograms SET observable='pt-trns' WHERE observable='pt' AND reference='CMS_2011_S9120041'; SELECT ROW_COUNT();" | dosql


echo "Done."
