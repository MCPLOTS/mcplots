<?php
  list($q_generator, $q_tune, $q_version,) = explode($safeDelimiter, $q_valid_gtvr);

  if (array_value("fromtunes", $_GET, 0)) print_tunevalidation_menu();
  else print_versvalidation_menu();
  
  $ids = array();
  $ids = explode($safeDelimiter, $_GET["idquery"]);
  
  // sanity
  foreach ($ids as &$i) $i = (int) $i;
  unset($i); // release the reference
  // TODO: sanity
  //if (count($ids) == 0) exit;
  //if (count($ids) > 5) exit;
  
  //retrieve rows from the database by given ids
  $query = $db->query("SELECT * FROM histograms WHERE id IN (" . implode(",", $ids) . ")");
  $rows = array();
  while ($row = $query->fetch_assoc()) {
    $rows[] = $row;
  }
  
  // TODO: sanity
  // if (count($ids) != count($rows)) exit;
  
  $refid = array();
  $type = array();
  foreach ($rows as $r) {
    $refid[$r["reference"].$r["histid"]] = true;
    
    if ($r["type"] == "data")
      $type[] = "data";
    else
      $type[] = $r["generator"] . " " . $r["version"] . " " . $r["tune"];
  }
  
  // TODO: sanity - all plots should be the same histogram
  //if (count($refid) != 1) exit;
  
  echo "<div class=\"rightpage\">\n";
  echo "<h2 class=\"d2\"><span> Details for " . implode(" vs. ", $type) . "</span></h2>\n";
  echo "\n";

  //prepare a steering file for the plotter and produce a plot
  $fname = implode("", array_sub(["beam", "energy", "process", "observable", "reference", "histid"], $rows[0])) . implode("", $type);
  $fname = str_replace(" ", "_", $fname);
  $fname = str_replace("/", "_", $fname);
  $fname = str_replace("*", "x", $fname);

  $fsteer = "cache/plots/$fname.script";
  $img = "cache/plots/$fname";

  prepare_plotter_steer($rows, $fsteer, $img, "valid");

  // skip image generation if it already exists in cache/
  if (! file_exists("$img.small.png")) {
    // execute plotter to prepare .eps and .pdf files
    exec("./plotter.exe " . escapeshellarg($fsteer) . " >> cache/plotter.log 2>&1");
    // and convert .eps to .png
    // the raster (.png) plot looks rough being produced
    // by plotter (ROOT), that is why we deside to use
    // additional step with `convert` utility
    exec("convert -density 100 " . escapeshellarg("$img.eps") . " " . escapeshellarg("$img.png"));
    exec("convert -density  50 " . escapeshellarg("$img.eps") . " " . escapeshellarg("$img.small.png"));

    // optimize .png file size (lossless, by 30-40%)
    exec("optipng -q -zs0 -f0 " . escapeshellarg("$img.png"));
    exec("optipng -q -zs0 -f0 " . escapeshellarg("$img.small.png"));
  }

    
    $dispproc = $c->name($rows[0]["process"]);
    
    //print header for graph
    $eb = $rows[0]["beam"] . " @ " . $rows[0]["energy"] . " GeV";
    $id = str_replace(" @ ", "", $eb);
    $id = str_replace(" GeV", "", $id);
    echo "<div id=\"$id\" style=\"position:relative; top:-45px;\"></div>";
    echo "<h3 class=\"d3 beam\"><span><a href=\"#$id\">$eb ($dispproc)</a></span></h3>\n";
    echo "\n";

    // print cell with one plot
    echo "<div class=\"plot\">\n";
    //echo "<h4>" . $c->name($fcuts,$fproc) . "</h4>\n";

    // print image
    echo "<div><a href=\"$img.png\"><img src=\"$img.small.png\"></a></div>\n";

    // print "caption"
    echo "<div class=\"details\">\n";
    echo "<input type=\"checkbox\" id=\"$img\">";
    echo "<label for=\"$img\">details</label>";

    echo "<ul>\n";
    echo "<li>";
    echo "  Download as: ";
    echo " <a href=\"$img.pdf\">.pdf</a>\n";
    echo " <a href=\"$img.eps\">.eps</a>\n";
    echo " <a href=\"$img.png\">.png</a>\n";
    echo " <a href=\"$fsteer\">.steer</a>\n";
    echo "</li>";
    
    
    foreach($rows as $r) {
    
    if ($r["type"] == "data") {
    //experiment info
    echo "<li>" . $r["experiment"] . " experiment: <a href=\"" . $r["fname"] . "\">data</a> | ";
    echo "<a href=\"". get_reflink($r["reference"]) . "\">article paper</a></li>\n";
    }
    else {
    //mc info
    $lvers = "($q_version)";
    echo "<li>" . $c->name($r["tune"], $r["generator"]) . " " . $r["version"] . ": <a href=\"" . $r["fname"] . "\">data</a> | ";
    // calc the path to generator steering file
    $fcard = $r["fname"];
    $fcard = str_replace(".dat", ".params", $fcard);
    $fcard = str_replace(".yoda", ".params", $fcard);
    echo "<a href=\"" . $fcard . "\">card</a></li>\n";
    }
    }

    echo "</ul>";
    echo "</div>\n";
    echo "</div>\n";

    echo "</div>\n";
?>

<br/>

