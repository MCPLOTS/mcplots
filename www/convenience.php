<?php
  // convenience: array get value by the key with fallback in the default
  function array_value($key, $array, $default = "") {
    if (!array_key_exists($key, $array)) return $default;
    return $array[$key];
  }
  
  // quote each item of array
  function array_quote($qstr, $array) {
    foreach ($array as &$i) {
      $i = $qstr . $i . $qstr;
    }
    
    return $array;
  }
  
  // index array by multiple keys
  function array_sub($keys, $array) {
    $ret = array();
    foreach ($keys as $k)
      $ret[] = $array[$k];
    
    return $ret;
  }
?>
