<!DOCTYPE html>
<html>

<head>
  <title>MCPLOTS</title>
  <link rel="shortcut icon" href="img/logo.png"/>
  <link rel="stylesheet" href="style.css">
  <script async src="mcplots.js"></script>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>

<?php
  include "convenience.php";
  
  // start profiler
  include "profiler.php";
  $t = new Profiler();
  
  // filter all items of the input array
  function mcplots_sanitize_input(&$a) {
    foreach ($a as &$i)
      if (is_array($i)) mcplots_sanitize_input($i);
      else {
        $i = rawurldecode($i);
        $i = str_replace(array("<", ">", "(", ")", "%", "\"", "/"), "", $i);
      }
  }
  
  // sanity
  mcplots_sanitize_input($_GET);
  
  // extract the query to draw the plots
  list($q_page, $q_beamgroup, $q_process, $q_observable, $q_tunegroup, $q_gen_version, $q_valid_gtvr) = array_pad(explode(",", array_value("query", $_GET)), 7, "");
  
  // patch for compatibility with old URLs: remove spaces
  // TODO: redirect to proper URL, or just show the error message
  $q_tunegroup = str_replace(' ', '', $q_tunegroup);
  
  // open the database
  $db = new mysqli("localhost", "mcplots", "", "mcplots");
  if ($db->connect_error) exit;

  // prepare abbreviations maps, styles map, tunegroups map, beamgroups maps
  include "config.php";
  $c = new Config("mcplots.conf");

  //delimiter used in validation pages to divide variables
  $safeDelimiter = "--";

  //if the generators versions form is just submitted, parse it
  if (array_key_exists("parse_form", $_GET)) {
    $query = $db->query("SELECT DISTINCT generator FROM histograms WHERE type = 'mc' ORDER BY 1");
    while ($row = $query->fetch_row()) {
      $g=$row[0];
      $v=array_value($g, $_GET);
      if ($v != "") {
        $q_gen_version .= "--1" . $g . "~" . $v;
      }
    }
    $q_gen_version = rawurldecode(substr($q_gen_version, 3));
  }

  function print_versvalidation_menu() {
    global $db;
    global $q_generator, $q_tune, $safeDelimiter;

    //get all possible generator-tune combinations
    $query = $db->query("SELECT DISTINCT generator, tune FROM histograms WHERE type = 'mc' ORDER BY 1, 2");
    $menu = array();

    while ($row = $query->fetch_row()) {
      $generator  = $row[0];
      $tune       = $row[1];

      $menu[$generator][] = $tune;
    }

    // print the left-side menu
    echo "<div class=\"leftside\">\n";
    echo "<div class=\"sidenav\">\n";
    echo "<h2> Generator / tune </h2>\n";

    foreach (array_keys($menu) as $generator) {
      $curgen = ($generator == $q_generator) ? "checked" : "";
      echo "<div>\n";
      printf("<input %s type=\"checkbox\" id=\"" . $generator . "\">\n", $curgen);
      echo "<label for=\"" . $generator . "\">" . $generator . "</label>\n";
      echo "<ul>\n";

      foreach ($menu[$generator] as $tune) {
        $curtun = (($generator == $q_generator) && ($tune == $q_tune)) ? "class=\"active\"" : "";
        printf("<li><a %s href=\"%s\">%s</a></li>\n",
              $curtun,
              prepare_link(array("valid","","","","","",$generator.$safeDelimiter.$tune)),
              $tune);
      }
      echo "</ul>\n";
      echo "</div>\n";
      echo "\n";
    }
    echo "</div>\n";
    echo "</div>\n";
  }
  
  function print_tunevalidation_menu() {
    global $db;
    global $q_generator, $q_version, $safeDelimiter;

    //get all possible generator-version combinations
    $query = $db->query("SELECT DISTINCT generator, version FROM histograms WHERE type = 'mc' ORDER BY 1, 2");
    $menu = array();

    while ($row = $query->fetch_row()) {
      $generator  = $row[0];
      $version    = $row[1];
      $menu[$generator][] = $version;
    }
    
    // print the left-side menu
    echo "<div class=\"leftside\">\n";
    echo "<div class=\"sidenav\">\n";
    echo "<h2> Generator </h2>\n";

    foreach (array_keys($menu) as $generator) {
      $curgen = ($generator == $q_generator) ? "checked" : "";
      echo "<div>\n";
      printf("<input %s type=\"checkbox\" id=\"" . $generator . "\">\n", $curgen);
      echo "<label for=\"" . $generator . "\">" . $generator . "</label>\n";
      echo "<ul>\n";

      foreach ($menu[$generator] as $version) {
        $curver = (($generator == $q_generator) && ($version == $q_version)) ? "class=\"active\"" : "";
        printf("<li><a %s href=\"%s\">%s</a></li>\n",
              $curver,
              prepare_link(array("validgen","","","","","",$generator.$safeDelimiter.$version)),
              $version);
      }
      echo "</ul>\n";
      echo "</div>\n";
      echo "\n";
    }
    echo "</div>\n";
    echo "</div>\n";
  }

  // menu for plots repository pages
  function print_plots_menu() {
    global $db, $c;
    global $q_process, $q_observable, $q_tunegroup, $q_gen_version;
    $process = $q_process;

    $query = $db->query("SELECT DISTINCT observable FROM histograms WHERE process = '$process'");
    $menu = array();

    while ($row = $query->fetch_row()) {
      $observable = $row[0];
      $submenu    = $c->submenu($observable,$process);
      $menu[$process][$submenu][] = $observable;
    }

    echo "<div class=\"leftside\">\n";
    echo "<div class=\"sidenav\">\n";
    echo "<h2>" . $c->name($process) . "</h2>\n";

    foreach (array_keys($menu[$process]) as $submenu) {
      $isSubmenu = ($submenu != "");

      //prepare a submenu if not empty
      if ($isSubmenu) {
        $curobs = in_array($q_observable, $menu[$process][$submenu]) ? "checked" : "";

        echo "<div>\n";
        printf("<input %s type=\"checkbox\" id=\"" . $submenu . "\">\n", $curobs);
        echo "<label for=\"" . $submenu . "\">" . $submenu . "</label>\n";
        echo "<ul>\n";
      }

      //print a submenu or an observable
      foreach ($menu[$process][$submenu] as $observable) {
        $curobs = ($observable == $q_observable) ? "class=\"active\"" : "";
        $startli = $isSubmenu ? "<li>" : "";
        $endli = $isSubmenu ? "</li>" : "";
        printf("%s<a %s href=\"%s\">%s</a>%s\n",
                  $startli,
                  $curobs,
                  prepare_link(array("plots","",$process,$observable,$q_tunegroup,$q_gen_version,"")),
                  $c->name($observable,$process),
                  $endli);
      }

      //finalize the submenu
      if ($isSubmenu) {
        echo "</ul>\n";
        echo "</div>\n";
      }
    }
    echo "</div>\n";
    echo "</div>\n";
    echo "\n";
  }

  function print_analysis_menu() {
    global $db, $c;
    global $q_process, $q_observable, $q_tunegroup, $q_gen_version, $q_valid_gtvr;
    $analysis = $q_valid_gtvr;

    $query = $db->query("SELECT DISTINCT process, observable
                              FROM histograms
                              WHERE reference='$analysis'
                              ORDER BY 1, 2");
    $menu = array();

    while ($row = $query->fetch_row()) {
      $process    = $row[0];
      $observable = $row[1];
      $submenu    = $c->submenu($observable,$process);

      $menu[$process][$submenu][] = $observable;
    }

    echo "<div class=\"leftside\">\n";

    //print the analysis filter button
    $query = $db->query("SELECT DISTINCT reference FROM histograms ORDER BY 1");
    echo "<ul class=\"gendropdown an\">\n";
    $print_an = str_replace("_", " ", $analysis);
    echo "<li><a class=\"anbutton\">$print_an</a>\n";

    echo "<ul>\n";
    while ($row = $query->fetch_row()) {
      $ref = $row[0];
      if ($ref == "") continue;
      $label = str_replace("_", " ", $ref);
      printf("  <li><a class=\"genmenu anmenu\" href=\"%s\">%s</a></li>\n",
              prepare_link(array("plots","","","",$q_tunegroup,$q_gen_version,$ref),true),
              $label);
    }
    echo "</ul>\n";
    echo "</li>\n";
    echo "</ul>\n";

    echo "<div class=\"sidenav\">\n";

    // print the process and vars menu
    foreach (array_keys($menu) as $process) {
      echo "<div class=\"k1\"><h2>" . $c->name($process) . "</h2></div>\n";

      foreach (array_keys($menu[$process]) as $submenu) {
        $startli = "";
        $endli = "";

        //prepare a submenu if not empty
        if ($submenu != "") {
          $startli = "<li>";
          $endli = "</li>";
          $curobs = "";
          foreach ($menu[$process][$submenu] as $observable) {
            if (($observable == $q_observable) && ($process == $q_process)){
              $curobs = " checked";
            }
          }
          echo "<div>\n";
          printf("<input%s type=\"checkbox\" id=\"" . $process.$submenu . "\">\n", $curobs);
          echo "<label for=\"" . $process.$submenu . "\">" . $submenu . "</label>\n";
          echo "<ul>\n";
        }

        //print a submenu or an observable
        foreach ($menu[$process][$submenu] as $observable) {
          $curobs = "";
          if (($observable == $q_observable) && ($process == $q_process)){
            $curobs = " class=\"active\"";
          }
          printf("%s<a%s href=\"%s\">%s</a>%s\n",
                  $startli,
                  $curobs,
                  prepare_link(array("plots","",$process,$observable,$q_tunegroup,$q_gen_version,$analysis)),
                  $c->name($observable,$process),
                  $endli);
        }

        //finalize the submenu if not empty
        if ($submenu != "") {
          echo "</ul>\n";
          echo "</div>\n";
          echo "\n";
        }
      }
    }

    echo "</div>\n";
    echo "</div>\n";
  }

  // === prepare plots ===

  // this function prepare steering file for plotter tool
  // $key is a customization option to generate slightly different steering files
  // for different sections of the site: index, custom, valid, validgen
  function prepare_plotter_steer(array $rows, $fsteer, $img, $key) {
    // debug
    //error_log("prepare_plotter_steer() call _GET=".json_encode($_GET)."nrows=".count($rows) . " rows=".json_encode($rows));
    
    global $c;
    $row0 = $rows[0];
    $cuts0 = $c->plotter($row0["cuts"]);

    $labels = array();
    foreach ($rows as $row) {
      if ($row["type"] != "mc") continue;

      switch ($key) {
        // collect names and versions of generators
        // now commented to remove the versions list string from the plot (textField1=)
        //case "index": $label = $c->name($row["generator"]) . " " . $row["version"]; break;
        //case "custom": $label = $c->name($row["generator"]) . " " . $row["version"]; break;

        // collect tunes of generators
        case "valid": $label = $c->name($row["tune"], $row["generator"]); break;

        // collect versions of generators
        case "validgen": $label = $c->name($row["version"], $row["generator"]); break;

        default: $label = ""; break;
      }

      $labels[$label] = true;
    }

    $script = "# BEGIN PLOT\n" .
              "Title=" . $c->plotter($row0["observable"], $row0["process"]) . (($cuts0 != "") ? (" (" . $cuts0 . ")") : "") . "\n" .
              "upperLeftLabel=" . $row0["energy"] . " GeV " . $row0["beam"] . "\n" .
              "upperRightLabel=" . $c->plotter($row0["process"]) . "\n" .
              "textField1=" . implode(", ", array_keys($labels)) . "\n" .
              "textField2=" . $row0["reference"] . "\n" .
              "outputFileName=$img\n" .
              "drawRatioPlot=1\n" .
              "# END PLOT\n" .
              "\n";

    $i = 0;

    foreach ($rows as $row) {
      // get plotting style and split it into components
      list($r, $g, $b, $lineStyle, $lineWidth, $markerStyle, $markerSize) = explode(" ", $c->style($row));

      $legend = $row["experiment"];

      if ($row["type"] == "mc") {
        // override color
        if ($key == "valid")
          list($r, $g, $b, ) = explode(" ", $c->styles["line" . $i++], 4);

        // set legend
        switch ($key) {
          // TODO: the name(tune, generator) translation produces string format "generator tune"
          //       which doesn't combine good with "generator version"
          //       the re-wise of mcplots.conf is needed? to remove generator name from name(tune,generator) translation
          //case "index": $legend = $c->name($row["tune"], $row["generator"]); break;
          
          case "index": $legend = $c->name($row["generator"]) . " " . $row["version"] . " " . $row["tune"]; break;
          case "custom": $legend = $c->name($row["generator"]) . " " . $row["version"] . " " . $row["tune"]; break;
          case "valid": $legend = $row["version"]; break;
          case "validgen": $legend = $row["tune"]; break;
          default: $legend = ""; break;
        }
      }
      
      // .yoda file contains multiple histograms inside
      // need to specify the particular histogram
      // the full path format:  path/to/file.yoda:/analysis_id/histogram_id
      $hpath = $row["fname"];
      $isYoda = str_ends_with($hpath, ".yoda");
      if ($isYoda) {
        $apath = $row["reference"];
        $hasOptions = ($row["optid"] != "");
        if ($hasOptions)
          $apath .= ":" . $row["optid"];
        
        $hpath .= ":/" . $apath . "/" . $row["histid"];
      }
      
      $script .= "# BEGIN HISTOGRAM\n" .
                 "filename=" . $hpath . "\n" .
                 "markerStyle=$markerStyle\n" .
                 "markerSize=$markerSize\n" .
                 "lineStyle=$lineStyle\n" .
                 "lineWidth=$lineWidth\n" .
                 "color=$r $g $b\n" .
                 "legend=$legend\n" .
                 "reference=" . (($row["type"] == "mc") ? "0" : "1") . "\n" .
                 "# END HISTOGRAM\n" .
                 "\n";
    }

    file_put_contents($fsteer, $script);
  }
  
  // this function converts reference string to the url of reference article
  function get_reflink($xreference) {
    $matches = array();
    
    // try to get inSPIRE ID (example: ATLAS_2011_I926145)
    preg_match("/_I([0-9]+)$/", $xreference, $matches);
    if (!empty($matches)) {
      $inspireID = $matches[1];
      return "http://inspirehep.net/literature/" . $inspireID;
    }

    // try to get Spires ID (example: ALICE_2010_S8625980)
    preg_match("/_S([0-9]+)$/", $xreference, $matches);
    if (!empty($matches)) {
      $spiresID = $matches[1];
      return "http://inspirehep.net/search?p=" . $spiresID;
    }

    // try to get ATLAS conference ID (example: ATLAS_2010_CONF_2010_031)
    preg_match("/^ATLAS_[0-9]{4}_CONF_([0-9]{4})_([0-9]+)$/", $xreference, $matches);
    if (!empty($matches)) {
      $atlasLink = "ATLAS-CONF-" . $matches[1] . "-" . $matches[2];
      return "http://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/" . $atlasLink;
    }

    // try to get CMS paper ID (example: CMS_EWK_10_012)
    preg_match("/^CMS_([A-Z]{3})_([0-9]{2})_([0-9]{3})$/", $xreference, $matches);
    if (!empty($matches)) {
      $cmsLink = "CMS-" . $matches[1] . "-" . $matches[2] . "-" . $matches[3];
      return "http://cds.cern.ch/search?f=reportnumber&p=" . $cmsLink;
    }

    // try to get CMS paper ID (example: CMS_2012_PAS_QCD_11_010)
    preg_match("/^CMS_([0-9]{4})_([A-Z]{3})_([A-Z]{3})_([0-9]{2})_([0-9]{3})$/", $xreference, $matches);
    if (!empty($matches)) {
      $cmsLink = "CMS-" . $matches[2] . "-" . $matches[3] . "-" . $matches[4] . "-" . $matches[5];
      return "http://cds.cern.ch/search?f=reportnumber&p=" . $cmsLink;
    }

    // try to get TOTEM paper ID (example: TOTEM_2012_002)
    preg_match("/^TOTEM_([0-9]{4})_([0-9]{3})$/", $xreference, $matches);
    if (!empty($matches)) {
      $cmsLink = "TOTEM-" . $matches[1] . "-" . $matches[2];
      return "http://cds.cern.ch/search?f=reportnumber&p=" . $cmsLink;
    }

    // return link to analysis on Rivet page:
    return "http://rivet.hepforge.org/analyses/" . $xreference . ".html";
  }

  function print_groups() {
    global $db, $c;
    global $q_beamgroup, $q_process, $q_observable, $q_tunegroup, $q_gen_version, $q_valid_gtvr;

    echo "<div class=\"settings\">\n";

    // query for all available combinations of generator/tune for selected beam/process/observable:
    $query = "SELECT DISTINCT generator, tune FROM histograms
              WHERE process = '$q_process' AND
                    observable = '$q_observable' AND
                    type = 'mc'";
    $query = $db->query($query);

    $tunes = array();
    while ($row = $query->fetch_assoc()) {
      $generator = $row["generator"];
      $tune = $row["tune"];
      $tunes[] = $generator . "." . $tune;
    }

    // keep only those groups which have at least one of $tunes
    $groups = array();
    $errorMessage = "";
    foreach ($c->tunegroups as $key => $value) {
      if (count(array_intersect($tunes, $value)) == 0 && $q_tunegroup == $key && $q_tunegroup != "Custom")
        $errorMessage = "There are no mc histograms for the selected preset. Please choose another one";
      if (count(array_intersect($tunes, $value)) > 0 || $q_tunegroup == $key) {
        list($menu, $submenu) = explode(".", $key);
        $groups[$menu][$submenu] = $value;

        // by default select first non-empty tune group
        if ($q_tunegroup == "") {
          $q_tunegroup = $key;
        }
      }
    }

    // extract menu.submenu from group name
    list($q_tunegroupm) = explode(".", $q_tunegroup);

    list($maingroup, $subgroup) = explode(".", $c->name($q_tunegroup));
    $gendropname = ($maingroup == "Custom") ? "Choose a preset" : $maingroup . " : " . $subgroup;
    echo "<ul class=\"gendropdown\">\n";
    echo "<li><a class=\"anbutton\">$gendropname</a>\n";
    echo "<ul>\n";
    foreach (array_keys($groups) as $menu) {
      list($submenu0) = array_keys($groups[$menu]); // assign the first element
      $tunegroup = ($submenu0 == "") ? $menu : $menu . "." . $submenu0;
      $sel = ($menu == $q_tunegroupm) ? "" : "";
      if ($menu == "Custom") break;
      // extract full name
      list($name, ) = explode(".", $c->name($tunegroup));
      printf("  <li><a class=\"genmenu\" %s>%s</a>\n",
             $sel,
             $name);
      $submenus = array_keys($groups[$menu]);
      $displaySubmenu = ($submenus != array(""));
      if ($displaySubmenu) {
        echo "<ul>\n";
        foreach ($submenus as $submenu) {
          if ($submenu == "") continue;

          $tunegroup = ($submenu == "") ? $menu : $menu . "." . $submenu;
          $sel = ($tunegroup == $q_tunegroup) ? " class=\"selected\"" : "";

          // extract full name
          list( , $name) = explode(".", $c->name($tunegroup));

          printf("  <li><a href=\"%s\" %s>%s</a></li>\n",
                prepare_link(array("plots","",$q_process,$q_observable,$tunegroup,$q_gen_version,$q_valid_gtvr)),
                $sel,
                $name);
        }

        echo "</ul>\n";
      }

      echo "</li>\n";
    }
    echo "</ul>\n";
    echo "</li>\n";
    echo "</ul>\n";
    echo "\n";

    echo "<div class=\"gendropdown\">\n";
    printf("  <a href=\"%s\">Customize</a>\n",
               prepare_link(array("plots","",$q_process,$q_observable,"Custom",$q_gen_version,$q_valid_gtvr)));
    echo "</div>\n";
    echo "\n";

    echo "<div class=\"tooltip\">\n";
    echo "&#8505;&#65039;<span class=\"tooltiptext\">Choose generators/versions/tunes to plot : either from a pre-defined preset (left button) or from all available MCs (right button)</span>\n";
    echo "</div>\n";
    echo "\n";
    
    echo "</div>\n";
    echo "\n";

    echo $errorMessage;

    $displayCustom = ($q_tunegroup == 'Custom');
    if ($displayCustom) {
      echo "<table id=\"groups\">\n";
      echo "<tr>\n";
      echo "  <td></td>\n";
      echo "  <td>\n";

      echo "<p class=\"hint\">Hint: Alt+click checkbox to select all generators versions for the tune name.";

      echo "  <form id=\"custom\" method=\"get\">\n";
      echo "  <input type=hidden name=query value=\"" . $_GET["query"] . "\">\n";

      // query for all available combinations of generator/tune for selected beam/process/observable:
      /*$query = "SELECT DISTINCT generator, version, tune FROM histograms
                WHERE beam IN ($beamslist) AND
                      process = '$q_process' AND
                      observable = '$q_observable' AND
                      type = 'mc'
                ORDER BY generator, version, tune";*/
      //beams are not used at the moment, however conserve the previous version probably necessary when Pb beam will be added
      $query = "SELECT DISTINCT generator, version, tune FROM histograms
                WHERE process = '$q_process' AND
                      observable = '$q_observable' AND
                      type = 'mc'
                ORDER BY generator, version, tune";
      $query = $db->query($query);

      $options = array();
      while ($row = $query->fetch_assoc()) {
        $gen = $row["generator"];
        $ver = $row["version"];
        $options[$gen][$ver][] = $row["tune"];
      }

      echo "<table id=\"custommenu\">\n";
      $prevgen = "";
      foreach ($options as $gen => $vers) {
        foreach ($vers as $ver => $tunes) {
          $dispgen = ($gen != $prevgen) ? $gen : "";
          $prevgen = $gen;
          echo "<tr>\n";
          echo "<td>$dispgen</td><td>$ver</td>\n";
          echo "<td>\n";

          foreach ($tunes as $tune) {
            $option = $gen . " " . $ver . " " . $tune;
            $chk = in_array($option, array_value("custom", $_GET, [])) ? "checked" : "";

            echo "  <label><input type=checkbox onClick=\"customClick(event, this)\" name=custom[] value=\"$option\" $chk><span>$tune</span></label>\n";
          }
          
          echo "</td>\n";
          echo "</tr>\n";
        }
      }
      echo "  </table>\n";

      echo "  <input type=submit value=\"Display\">\n";

      echo "  </form>\n";
      echo "  </td>\n";
      echo "</tr>\n";
      echo "</table>\n";
      echo "\n";
    }
  }

  function print_plots() {
    global $db, $c, $t;
    global $q_process, $q_observable, $q_tunegroup, $q_valid_gtvr, $q_gen_version;

    //if the analysis is specified, print analysis menu
    //else the usual process menu
    //if the observable is not specified, print the first one by default
    if ($q_valid_gtvr != "") {
      if ($q_observable == ""){
        $query = $db->query("SELECT DISTINCT process, observable
                              FROM histograms
                              WHERE reference='$q_valid_gtvr'
                              ORDER BY 1, 2");
        $row = $query->fetch_row();
        $q_process    = $row[0];
        $q_observable = $row[1];
      }
      print_analysis_menu();
    }
    else {
      if ($q_observable == ""){
        $query = $db->query("SELECT DISTINCT observable FROM histograms WHERE process = '$q_process'");
        $q_observable = $query->fetch_row()[0];
      }
      print_plots_menu();
    }

    echo "<div class=\"rightpage\">\n";
    // print submenu name and colon only if the submenu name is not empty
    $sub = $c->submenu($q_observable,$q_process);
      $sub = ($sub != "") ? $sub . " : " : "";

      echo "<h2 class=\"d2\">" . $c->name($q_process) . " : " . $sub . $c->name($q_observable,$q_process) . "</h2>\n";
      echo "\n";
    
    print_groups();
    
      $submenu0 = array_value($q_tunegroup, $c->tunegroups, []);
          $displayCustom = ($submenu0 == array("@custom"));
    
    if ($displayCustom) {
        $custom = array_value("custom", $_GET, []);

        // return if nothing is selected for display:
        if (count($custom) == 0) return;

        $tunes = array();
        $userGenVers = array();
        foreach ($custom as $option) {
          list($generator, $version, $tune) = explode(" ", $option);

          $tunes[] = $generator . "." . $tune;
          if (!in_array($version, array_value($generator, $userGenVers, [])))
            $userGenVers[$generator][] = $version;
        }
      }
    else {
        $tunes = $submenu0;
      
      // get array of generators from 5th URL value
    $userGenVers = unpackStr($q_gen_version);
    }
    
    $forcerefresh = (array_value("refresh", $_GET, 0) == 1);

    $userGens = array_keys($userGenVers);

    $tuneslist = $tunes;
    $tuneslist[] = "."; // add "empty" tune to select data histograms as well
    $tuneslist = implode(",", array_quote("'", $tuneslist));

    $query = "SELECT * FROM histograms
              WHERE process = '$q_process' AND
                      observable = '$q_observable' AND
                      CONCAT(generator, '.', tune) IN ($tuneslist)
                ORDER BY energy DESC, beam, cuts, type, generator, version, experiment, tune";

      // prepare map of plots
    $query = $db->query($query);

    $plots = array();
      while ($row = $query->fetch_assoc()) {
        $eb     = $row["beam"] . " @ " . $row["energy"] . " GeV";
        $cuts   = $row["cuts"] . "-" . $row["histid"];
        $type   = $row["type"];
        $generator = $row["generator"];
        $version = $row["version"];
        $plots[$eb][$cuts][$type][$generator][$version][] = $row;
      }

      foreach (array_keys($plots) as $eb) {
        //if filter is set - check whether show beam and energy or not
        if ($q_valid_gtvr != "") {
          $skipIt = true;
          foreach (array_keys($plots[$eb]) as $cuts) {
            $myrows  = (array) $plots[$eb][$cuts]["data"][""][""];
            if ($myrows[0]["reference"] == $q_valid_gtvr) {
              $skipIt = false;
              break;
            }
          }
          if ($skipIt) continue;
        }

        $id = str_replace(" @ ", "", $eb);
        $id = str_replace(" GeV", "", $id);
        
        echo "<div class=\"espaceur\" id=\"$id\"></div>\n";
        echo "<h3 class=\"d3 beam\"><span><a href=\"#$id\">$eb</a></span></h3>\n";
        /*echo "<h3 class=\"d3\" id=\"$id\"><span><a href=\"#$id\">$eb</a></span></h3>\n";*/
        echo "<div>\n";
        echo "\n";

        foreach (array_keys($plots[$eb]) as $cuts) {
        $t->start();
        
          //if filter is set then check whether show certain image file
          if ($q_valid_gtvr != "") {
            $myrows  = (array) $plots[$eb][$cuts]["data"][""][""];
            $skipIt = ! ($myrows[0]["reference"] == $q_valid_gtvr);
            if ($skipIt) continue;
          }
          
          $rows = array();
          
          // extract list of DATA histograms:
          if (array_key_exists("data", $plots[$eb][$cuts]))
          $rows  = (array) $plots[$eb][$cuts]["data"][""][""];

          // image file name suffix
          $suffix = "";

          // TODO: temporary work-around for missing MC entries to cure php warnings
          if (!array_key_exists("mc", $plots[$eb][$cuts]))
            $plots[$eb][$cuts] += ["mc" => []];
          
          // extract list of MC histograms:
          foreach (array_keys((array) $plots[$eb][$cuts]["mc"]) as $generator) {
            // array of all histograms of all versions of $generator:
            $versions = $plots[$eb][$cuts]["mc"][$generator];

            if (in_array($generator, $userGens)) {
              // user set the preference on the $generator (in "Generators and Versions")

              // extract selected versions:
              $userVersions = $userGenVers[$generator];

              // prepare image suffix
              $suffix .= $generator . "-" . implode("-", $userVersions) . "-";

              foreach ($userVersions as $version) {
                // check if selected $version of the $generator is amongst histograms:
                if (in_array($version, array_keys($versions))) {
                  // TODO: implement filtering according to tunes
                  //       in case of 'Custom' generator group
                  $rows = array_merge($rows, $versions[$version]);
                }
              }
            }
            else {
              // use the latest available version what we have
              $maxver = max(array_keys($versions));
              $rows  = array_merge($rows, $versions[$maxver]);
            }
          }

          // at this point array $rows contents all histograms which we want
          // to display on the plot

          $energy = $rows[0]["energy"];
          $beam   = $rows[0]["beam"];

          // construct file name
          $fname = str_replace(" ", "_", "$q_process-$q_observable-$q_tunegroup-$cuts-$beam-$energy-$suffix");

          // replace '*' characters by 'x' to prevent globbing by convert tool
          // (to avoid large performance penalty due to enumeration of all files
          //  in cache/plots directory which could have millions of files)
          $fname = str_replace("*", "x", $fname);

          $fsteer = "cache/plots/$fname.script";
          $img = "cache/plots/$fname";

        $t->stamp("plot-prep");
        
          // skip image generation if it already exists in cache/
          if ($forcerefresh || !file_exists("$img.small.png")) {
            // prepare plotter steering file
            prepare_plotter_steer($rows, $fsteer, $img, $displayCustom ? "custom" : "index");
          $t->stamp("plot-steer");

            // prepare tarball with all files for plotter
            exec("./mk_plotter_pkg.sh " . escapeshellarg($fsteer));
          $t->stamp("plot-steer-tgz");

            // execute plotter to prepare .eps and .pdf files
            exec("./plotter.exe " . escapeshellarg($fsteer) . " >> cache/plotter.log 2>&1");
          $t->stamp("plot-plotter");

            // and convert .eps to .png
            // the raster (.png) plot looks rough being produced
            // by plotter (ROOT), that is why we deside to use
            // additional step with `convert` utility
            exec("convert -density 100 " . escapeshellarg("$img.eps") . " " . escapeshellarg("$img.png"));
          $t->stamp("plot-convert-png");

            exec("convert -density  50 " . escapeshellarg("$img.eps") . " " . escapeshellarg("$img.small.png"));
          $t->stamp("plot-convert-png-small");

            // optimize .png file size (lossless, by 30-40%)
            exec("optipng -q -zs0 -f0 " . escapeshellarg("$img.png"));
            exec("optipng -q -zs0 -f0 " . escapeshellarg("$img.small.png"));
          $t->stamp("plot-optipng");
          }

          // print cell with one plot
          /*echo "<div class=\"rasp\" id=\"$id$cuts\" style=\"position:relative; top:-45px;\"></div>";
          echo "<div class=\"plot\">\n";*/
          echo "<div class=\"plot\" id=\"$id$cuts\">\n";
          /*echo "<h4>" . $c->name($cuts) . "</h4>\n";*/

          // alt text for the plot:
          $alt = "Plot of $q_observable in $energy GeV $beam collisions";

          // print image
          echo "<div><a href=\"$img.png\"><img src=\"$img.small.png\" alt=\"$alt\"></a></div>\n";

          // print "caption"

          echo "<div class=\"details\">\n";
          echo "<input type=\"checkbox\" id=\"$img\">\n";
          echo "<label for=\"$img\">details</label>\n";

          echo "<ul>\n";
          echo "<li>";
          echo "Download as: \n";
          echo " <a href=\"$img.pdf\">.pdf</a>\n";
          echo " <a href=\"$img.eps\">.eps</a>\n";
          echo " <a href=\"$img.png\">.png</a>\n";
          echo " <a href=\"$fsteer\">.script</a>\n";
          echo " <a href=\"$fsteer.tgz\">.script.tgz</a>\n";
          echo " <a href=\"#$id$cuts\">#</a>\n";
          echo "</li>\n";

          foreach ($rows as $row) {
            if ($row["type"] == "mc") {
              echo "<li>" . $c->name($row["tune"], $row["generator"]) . ": <a href=\"" . $row["fname"] . "\">data</a> | ";
              // calc the path to generator steering file
              $fcard = $row["fname"];
              $fcard = str_replace(".dat", ".params", $fcard);
              $fcard = str_replace(".yoda", ".params", $fcard);
              echo "<a href=\"" . $fcard . "\">generator card</a></li>\n";
            }
            else {
              echo "<li>" . $row["experiment"] . " experiment: <a href=\"" . $row["fname"] . "\">data</a> | ";
              echo "<a href=\"". get_reflink($row["reference"]) . "\">article paper</a></li>\n";
            }
          }
          echo "</ul>\n";
          echo "</div>\n";
          echo "</div>\n";
          echo "\n";
        
        $t->stamp("plot-html");
        }

        echo "</div>\n";
        echo "\n";
      }

      if (count($plots) == 0) {
        echo "<h4>There are no histograms for selected combination of process/observable/tune.</h4>\n";
      }
  }

  function spec_format($xnum, $xsign = false) {
    $lnum=$xnum;
    if (!is_numeric($xnum)) return $xnum;
    if (number_format(abs($xnum),6)=="0.000000") return 0;
    //branch for 0.XXXX number
    if (floor(abs($lnum))==0){
      $cntdec=1;
      while (floor(abs($lnum))==0){
        $cntdec++;
        $lnum=$lnum*10;
      }
      $ynum=round($xnum,$cntdec);
    }
    //branch for XXX.00 number
    else{
      $cntdec=0;
      while (floor(abs($lnum))!=0){
        $cntdec--;
        $lnum=$lnum/10;
      }
      $cntdec=$cntdec+2;
      $ynum=round($xnum,$cntdec);
    }
    $lsig=($xsign && ($ynum > 0)) ? "+":"";
    return $lsig.number_format($ynum,$cntdec);
  }

  function get_color($chi2value, $b1 = 1, $b2 = 4) {
    if (!is_numeric($chi2value)) return "nocolor";
    if ($chi2value < $b1){
      return "goodchi2"; //OK green class
    }
    elseif ($chi2value < $b2){
      return "midchi2"; //diff orange
    }
    else {
      return "badchi2";    //bad red
    }
  }

  function get_deltColor($delta, $dark = false) {
    if (!is_numeric($delta)) return ($dark ? "midDchi2" : "midDtchi2");
    if ($delta < -0.1) {
      return ($dark ? "goodDchi2" : "goodDtchi2"); //OK dark green (background) : OK green
    }
    elseif ($delta < 0.1) {
      return ($dark ? "midDchi2" : "midDtchi2"); //text white background dark : only text white
    }
    else {
      return ($dark ? "badDchi2" : "badDtchi2"); //bad dark red (background) : bad red
    }
  }

  function prepare_link($inputArray,$force = false){
    /* function to prepare link with proper count of commas and positions
     * [0] - page target
     * [1] - beam (ee, pp/ppbar)
     * [2] - process
     * [3] - observable
     * [4] - tune group
     * [5] - generator and version
     * [6] - analysis filter OR for validation: generator--tune--version--reference or
     *
     * $force - if true then comma will be ther even though value is not there
     */
    $outpuLink = "";
    $cnter = 0;
    $lsize = count($inputArray);
    //array_reverse($inputArray)
    foreach(array_reverse($inputArray) as $parameter){
      $cnter++;
      $comma = ($cnter != $lsize ? "," : "");
      $outpuLink = ($parameter != "") ? ($comma . rawurlencode($parameter) . $outpuLink)
                                      : (($outpuLink != "" || $force) ? $comma.$outpuLink : $outpuLink);
    }
    $outpuLink = ($outpuLink != "") ? "?query=" . $outpuLink : "" ;
    return $outpuLink;
  }
?>

<!-- Top navigation -->
<nav>
<ul>
  <li>
    <a href="https://lhcathome.web.cern.ch/projects/test4theory">LHC@HOME</a>
  </li>
  <li class="dropdown">
    <a href="?query=allvalidations" class="dropbtn">COMPARISON<span class="triangle-down"> </span></a>
    <div class="dropdown-content">
      <a href="?query=validgen">Tunes</a>
      <a href="?query=valid">Generators</a>
    </div>
  </li>
  <li class="dropdown">
    <?php
      printf("<a href=\"%s\" class=\"dropbtn\">PLOTS<span class=\"triangle-down\"> </span></a>\n",
                  prepare_link(array("allplots","","","",$q_tunegroup,$q_gen_version,"")));
    ?>
    <!-- <a href="?query=allplots" class="dropbtn">PLOTS</a>-->
    <div class="dropdown-content">
    <?php
      $query = $db->query("SELECT DISTINCT process FROM histograms");
      while ($row = $query->fetch_row()) {
        printf("<a href=\"%s\">%s</a>\n",
                  prepare_link(array("plots","",$row[0],"",$q_tunegroup,$q_gen_version,"")),
                  $c->name($row[0]));
      }
    ?>
    </div>
  </li>
  <li>
    <a href="?query=about&info=about">ABOUT</a>
  </li>
  <li id="logo">
    <a href="/"><img src="figs1/logo1.png"></a>
  </li>
</ul>
</nav>

<?php
  switch ($q_page) {
    case "main"        : include("main.php");       break;
    case "about"       : include("about.php");       break;
    case "allplots"    : include("plots.php");       break;
    case "plots"       : print_plots();          break;
    case "allvalidations"    : include("validations.php");       break;
    case "valid"       : include("valid.php");         break;
    case "validgen"    : include("validgen.php");      break;
    case "validdetail" : include("validdetail.php");   break;
    case "imgdetail"   : include("imgdetail.php");     break;
    default            : include("main.php");      break;
  }

  //echo "<div id =\"footer\">\n";
  //echo "<p><span title=\"" . $t->summaryLine() . "\">Page generation took " . ms($t->elapsedFromStart()) . " ms</span> | <a href=\"dev.html\">dev</a></p>\n";
  //echo "</div>";
  
  $db->close();
?>

</div>
</body>

</html>
