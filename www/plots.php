<div class="fullpage">
<h1>Plots</h1>

<h2 class="d3"><span>Plots by analyses</span></h2>
<?php
  $query = $db->query("SELECT DISTINCT reference FROM histograms
                          ORDER BY reference");
  echo "<ul class=\"gendropdown an\">\n";
  echo "<li><a class=\"anbutton\">Choose an analysis</a>\n";
  echo "<ul>\n";
  while ($row = $query->fetch_row()) {
    $ref = $row[0];
    if ($ref == "") continue;
    $label = str_replace("_", " ", $ref);
    printf("  <li><a class=\"genmenu anmenu\" href=\"%s\">%s</a></li>\n",
             prepare_link(array("plots","","","",$q_tunegroup,$q_gen_version,$ref),true),
             $label);
  }
  echo "</ul>\n";
  echo "</li>\n";
  echo "</ul>\n";
?>

<h2 class="d3"><span>Plots by beams : pp</span></h2>
<div class="figs-plots-beams">
  <?php
    $query = $db->query("SELECT DISTINCT process FROM histograms WHERE beam='pp' OR beam='ppbar'");
    while ($row = $query->fetch_row()) {
      echo "<div class=\"fig-item\">\n";
      printf("<a href=\"%s\">\n",
                      prepare_link(array("plots","",$row[0],"",$q_tunegroup,$q_gen_version,"")));
      printf("<img src=\"figs1/%s.png\" onerror=\"this.src = 'figs1/noir.png'\" alt=\"%s\" width=\"250\" >\n",
            $row[0],
            $row[0]);
      printf("<div class=\"left\">" . $c->name($row[0]) . "</div>\n");
      echo "</a>\n";
      echo "</div>\n";
    }
  ?>
</div>

<h2 class="d3"><span>Plots by beams : ee</span></h2>
<div class="figs-plots-beams">
<?php
  $query = $db->query("SELECT DISTINCT process FROM histograms WHERE beam='ee'");
  while ($row = $query->fetch_row()) {
    echo "<div class=\"fig-item\">\n";
    printf("<a href=\"%s\">\n",
                    prepare_link(array("plots","",$row[0],"",$q_tunegroup,$q_gen_version,"")));
    printf("<img src=\"figs1/%s.png\" onerror=\"this.src = 'figs1/noir.png'\" alt=\"%s\" width=\"250\" >\n",
           $row[0],
           $row[0]);
    printf("<div class=\"left\">" . $c->name($row[0]) . "</div>\n");
    echo "</a>\n";
    echo "</div>\n";
  }
?>
</div>

<h2 class="d3"><span>Plots by beams : PbPb</span></h2>
<div class="figs-plots-beams">
<?php
  $query = $db->query("SELECT DISTINCT process FROM histograms WHERE beam='PbPb'");
  while ($row = $query->fetch_row()) {
    echo "<div class=\"fig-item\">\n";
    printf("<a href=\"%s\">\n",
                    prepare_link(array("plots","",$row[0],"",$q_tunegroup,$q_gen_version,"")));
    printf("<img src=\"figs1/%s.png\" onerror=\"this.src = 'figs1/noir.png'\" alt=\"%s\" width=\"250\" >\n",
           $row[0],
           $row[0]);
    printf("<div class=\"left\">" . $c->name($row[0]) . "</div>\n");
    echo "</a>\n";
    echo "</div>\n";
  }
?>
</div>

<!-- <input type="checkbox" id="genversions">
<h2 id="headergenver" class="d3"><label for="genversions">Generators versions</label></h2> -->
<h2 id="headergenver" class="d3"><span>Generator versions</span></h2>
<div id="tablegens">
<div class="text-clarif">
Though the default on mcplots is to display the most recent generator versions that have so far been implemented on the site, the results generated with previous versions are stored and you can choose a "default" generator version to be displayed on plots. With the "latest" version selection the most recent generator version available for each plot will be shown. If a specific version is selected it will be shown on plots.
<br>
Note that not all plots may have been generated with all versions, so if a specific (especially older) version is selected, some curves may disappear. This may also occur simply if some tunes are not available in all versions.
</div>
<br><br>
<?php include("versions.php");?>
</div>
</div>
<br>
<br>
