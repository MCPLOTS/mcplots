<div class="head">
  <h1> MCPLOTS </h1>
  <div class="line"> Online repository of Monte Carlo plots compared to experimental data
  </div>
  <?php
    $query = $db->query("SELECT COUNT(*) FROM histograms");
    $total = $query->fetch_row();
    $query = $db->query("SELECT COUNT(DISTINCT reference) FROM histograms WHERE type='data'");
    $an=$query->fetch_row();
    $query = $db->query("SELECT COUNT(DISTINCT generator,version) FROM histograms WHERE type='mc'");
    $gen = $query->fetch_row();
    echo "<div class=\"circles\">";
      echo "<div class=\"circ c1\"> <a href=\"?query=about&info=status#curstatan\"><div class=\"number\">$an[0]</div>data analyses</a></div>";
      echo "<div class=\"circ c2\"> <a href=\"?query=about&info=status#curstatgen\"><div class=\"number\">$gen[0]</div>generators</a></div>";
      //echo "<div class=\"circ c3\"> <a href=\"?query=allplots\"><div class=\"number\">$total[0]</div>plots</a></div>";
      $nplots = round($total[0]/1000000, 1)."M";
      echo "<div class=\"circ c3\"> <a href=\"?query=allplots\"><div class=\"number\">$nplots</div>plots</a></div>";
    echo "</div>";
  ?>
</div>

<div class="carrelage">
  <a class="carre" href="?query=about&info=about">
    <img src="figs1/mcplots.png" alt="mcplots" width=100%>
    <div>
      <h2>About</h2>
      <div class="ctext">Learn more about the project, its history and main updates</div>
    </div>
  </a>
  <a class="carre" href="?query=allplots">
    <img id="imgplot" src="figs1/plot.png" alt="plot" width=100%>
    <div>
      <h2>Plots</h2>
      <div class="ctext">Use our database to see how tune X of version Y of generator Z looks on a distribution D</div>
    </div>
  </a>
  <a class="carre" href="?query=allvalidations">
    <img src="figs1/chi2.png" alt="chi2" width=100%>
    <div>
      <h2>Numerical comparison</h2>
      <div class="ctext">Compare numerically different tunes of a certain generator or its different versions</div>
    </div>
  </a>
  <a class="carre" href="https://lhcathome.web.cern.ch/projects/test4theory">
    <img src="figs1/lhchome.png" alt="plot" width=100%>
    <div>
      <h2>LHC@home</h2>
      <div class="ctext">Join the LHC@home/Test4Theory project to donate the unused CPU cycles to help generate more statistics for mcplots</div>
    </div>
  </a>
</div>

<div class="bottombar">
  <h1> Partners </h1>

  <a href="http://wwwth.cern.ch/"><img src="figs1/logo_cern.png" alt="CERN"></a>
  <a href="http://www.montecarlonet.org/"><img src="figs1/logo_mcnet.png" alt="MCnet"></a>
  <a href="https://lhcathome.web.cern.ch/projects/test4theory/"><img src="figs1/logo_lhchome.png" alt="LHC at home"></a>
  <a href="https://www.monash.edu/science/schools/physics/research/research-areas/particle-physics"><img src="figs1/logo_monash.png" alt="LHC at home"></a>
</div>
