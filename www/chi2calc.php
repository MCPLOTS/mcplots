<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
  <title>Chi2 cache</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>

<h1>Chi2 cache operations</h1>

<p>Cache status:<br>

<?php
  // print cache summary info
  $cpath = "cache/chi2";
  $cpathmtime = filemtime($cpath);
  $now = time();
  $ago = $now-$cpathmtime;
  $isRecalcActive = ($ago < 5*60) ? 1 : 0;
  try {
  $f = new FilesystemIterator($cpath, FilesystemIterator::SKIP_DOTS);
  $nfiles = iterator_count($f);
  }
  catch (Exception $e) {
  $nfiles = 0;
  }
  
  // file format: {idx_now idx_total time_start time_now}
  $progress = explode(" ", trim(file_get_contents("cache/chi2/progress.txt")));
  if (count($progress) == 1) { $progress = array(0, 0, 0, 0); } // `count()==1` is the empty array case
  list($idx_now, $idx_total, $tfirst, $tlast) = $progress;
  $telapsed = $tlast - $tfirst;
  $prograt = ($idx_total != 0) ? $idx_now / $idx_total : 1;
  $testimate = round($telapsed / $prograt);
  $tremaining = $testimate - $telapsed;
  
  echo "path = $cpath<br>\n";
  echo "files = $nfiles<br>\n";
  echo "last update = " . date(DATE_COOKIE, $cpathmtime) . " (" . $ago . "s ago)<br>\n";
  echo "recalc active = " . $isRecalcActive . "<br>\n";
  echo "recalc progress = $prograt, elapsed $telapsed s, remaining $tremaining s<br>\n";
  
  $status = $isRecalcActive ? "disabled" : "";
?>
</p>

<form method="post">
<p>Actions: 
<input type="submit" name="action" value="Status">
<input type="submit" name="action" value="Recalc" <?= $status ?> >
<input type="submit" name="action" value="Clear" <?= $status ?> >
</p>
</form>


<?php
  // 
  if (!array_key_exists("action", $_POST)) $_POST["action"] = "Status";
  $isRecalc = ($_POST["action"] == 'Recalc');
  $isClear = ($_POST["action"] == 'Clear');
  
  if (($isRecalc || $isClear) && $isRecalcActive) {
    echo "<p style=\"color:red;\">Recalc in progress, no action.</p>\n";
    exit(0);
  }
  
  
  if ($isClear) {
    // remove old cache (in background)
    $cache0 = $cpath . '-' . rand();
    exec("mv $cpath $cache0");
    exec("rm -rf $cache0 >& /dev/null &");
    exit(0);
  }

  //echo "recalc=" . $isRecalc . "<br>\n";
  if (!$isRecalc) exit(0);
  
  //echo "running cache calc...<br>\n";
  //exit(0);
?>

<?php
  set_time_limit(0);
  // open the database
  mkdir("cache/chi2");
  $db = new mysqli("localhost", "mcplots", "", "mcplots");
  if ($db->connect_error) exit;

  //delimiter used in validation pages to divide variables
  $safeDelimiter = "--";

  // list DATA histograms
  $query = $db->query("SELECT *
                        FROM histograms
                        WHERE type='data'
                        ORDER BY beam, process, observable, energy, reference, histid, cuts DESC");
  $mydata = array();
  while ($row = $query->fetch_assoc()) {
    $params  = implode($safeDelimiter, [$row["observable"],$row["energy"],$row["reference"],$row["histid"],$row["cuts"]]);
    $beam = $row["beam"];
    if ($beam == "pp" || $beam == "ppbar") $beam = "ppppbar"; // combine pp/ppbar
    $bp = $beam . "-" . $row["process"];
    $mydata[$bp][$params][] = $row;
  }

  // build list of all g-v-t
  $query = $db->query("SELECT DISTINCT generator, version, tune FROM histograms WHERE type = 'mc' ORDER BY 1,2,3");
  $myrows = array();
  while ($row = $query->fetch_row()) {
    $myrows[] = $row;
  }
  
  $ntot = count($myrows);
  $nidx = 0;
  $t0 = time();
  
  foreach ($myrows as [$generator, $version, $tune]) {

          echo "<b>$generator $version $tune</b><br>\n";

          //get all MCs for each g-v-t
          $query = $db->query("SELECT *
                        FROM histograms
                        WHERE generator = '$generator' AND version = '$version' AND tune = '$tune'
                        ORDER BY beam, process, observable, energy, reference, histid, cuts DESC");

          $mytable = array();
          while ($row = $query->fetch_assoc()) {
            $beam = $row["beam"];
            if ($beam == "pp" || $beam == "ppbar") $beam = "ppppbar"; // combine pp/ppbar
            $bp = $beam . "-" . $row["process"];
            $params  = implode($safeDelimiter, [$row["observable"],$row["energy"],$row["reference"],$row["histid"],$row["cuts"]]);
            $mytable[$bp][$params][] = $row;
          }

          //loop through beam-processes-cuts so through all plots for a given g-v-t
          foreach (array_keys($mytable) as $bp){
              echo "$bp <br>\n";

              $fname = str_replace(" ", "_", "$generator-$version-$tune-$bp");
              $fname = str_replace("/", "_", $fname);
              $fname = str_replace("*", "x", $fname);
              $fname = "cache/chi2/$fname.txt";
              
              // chi2 computed already, skip
              if (file_exists($fname)) continue;
              
              $handler = fopen($fname.".tmp", "w");

              foreach (array_keys($mytable[$bp]) as $params){
                //check if there is a corresponding DATA
                if (!array_key_exists($bp, $mydata)) continue;
                if (!in_array($params,array_keys($mydata[$bp]))) continue;

                $plotname = str_replace(" ", "_", "$generator-$version-$tune-$bp"."$safeDelimiter"."$params");
                $plotname = str_replace("/", "_", $plotname);
                $plotname = str_replace("*", "x", $plotname);
                $plotsteer = "cache/plots/$plotname.script";
                $outfile = "cache/plots/$plotname";

                //create steer file content
                $myrow0 = $mydata[$bp][$params][0];
                $myrow1 = $mytable[$bp][$params][0];
                $dataid=$myrow0["id"];
                $mcid=$myrow1["id"];
                prepare_plotter_steer_for_chi2(array($myrow0, $myrow1), $plotsteer, $outfile);

                //get chi2
                //$chi = `./plotter.exe plot=0 chi2=5 $plotsteer`;
                exec("./plotter.exe plot=0 chi2=5 $plotsteer >> cache/plotter.log 2>&1");
                $line = file_get_contents("$outfile.txt");
                list($chi, ) = explode(";", $line);

                fwrite($handler, "$plotname $chi $mcid $dataid\n");
                unlink("$outfile.txt");
                unlink($plotsteer);
              }
              fclose($handler);
              
              // move the file to the final name as indication of full success
              rename($fname.".tmp", $fname);
          }
        echo "<br>\n";
        
        $nidx += 1;
        $tnow = time();
        file_put_contents("cache/chi2/progress.txt", "$nidx $ntot $t0 $tnow");
    }


    // prepare minimal plotter steering file sufficient for chi2 calculation
    function prepare_plotter_steer_for_chi2(array $rows, $fsteer, $img) {
      $script = "# BEGIN PLOT\n" .
                "outputFileName=$img\n" .
                "# END PLOT\n" .
                "\n";

      foreach ($rows as $row) {
          // set legend
        $legend = $row["experiment"];

        if ($row["type"] == "mc") {
          $legend = $row["generator"] . " " . $row["version"] . " " . $row["tune"];
        }

      // .yoda file contains multiple histograms inside
      // need to specify the particular histogram
      // the full path format:  path/to/file.yoda:/analysis_id/histogram_id
      $hpath = $row["fname"];
      $isYoda = str_ends_with($hpath, ".yoda");
      if ($isYoda) {
        $apath = $row["reference"];
        $hasOptions = ($row["optid"] != "");
        if ($hasOptions)
          $apath .= ":" . $row["optid"];
        
        $hpath .= ":/" . $apath . "/" . $row["histid"];
      }
      
        $script .= "# BEGIN HISTOGRAM\n" .
                  "filename=" . $hpath . "\n" .
                  "legend=$legend\n" .
                  "reference=" . (($row["type"] == "mc") ? "0" : "1") . "\n" .
                  "# END HISTOGRAM\n" .
                  "\n";
      }

      file_put_contents($fsteer, $script);
    }
  ?>
</body>
</html>
