<?php

  list($q_generator, $q_tune, $q_version, $q_ref) = array_pad(explode($safeDelimiter, $q_valid_gtvr), 4, "");

  //if the gen-tune is not specified, choose the first one
  if ($q_tune == "") {
    $query = $db->query("SELECT DISTINCT generator, tune FROM histograms WHERE type = 'mc' ORDER BY 1, 2");
    $row = $query->fetch_row();
    $q_generator  = $row[0];
    $q_tune    = $row[1];
  }

  print_versvalidation_menu();
  $dispgen = $c->name($q_tune, $q_generator);

  echo "<div class=\"rightpage\">\n";
  echo "<h2 class=\"d2\"><span> $dispgen versions validation</span></h2>\n";
  echo "\n";

  // get list of available versions
  $query = $db->query("SELECT DISTINCT version
                        FROM histograms
                        WHERE generator = '$q_generator' AND tune = '$q_tune' AND type = 'mc'
                        ORDER BY version");
  $vers_avail = array();
  while ($row = $query->fetch_assoc()) {
    $vers_avail[] = $row["version"];
  }
  
  // sanitize user input for versions display
  $vers_sel = array_value("vers", $_GET, []);
  $vers_sel = is_array($vers_sel) ? array_intersect($vers_sel, $vers_avail) : array();
  
  // by default display 3 first versions
  if (count($vers_sel) == 0) {
    $vers_sel = array_slice($vers_avail, -3, 3);
  }
  
  // print list of available versions
  echo "  <form method=\"get\">\n";
  echo "  <input type=hidden name=query value=\"" . $_GET["query"] . "\">\n";
  echo "    <table>\n";
  echo "      <tr>\n";
  echo "        <th>Versions:</th>\n";
  echo "        <td>\n";
  
  foreach ($vers_avail as $version) {
    $chk = in_array($version, $vers_sel) ? "checked" : "";
    echo "  <label><input type=checkbox name=vers[] value=\"$version\" $chk><span>$version</span></label>\n";
  }
  
  echo "        </td>\n";
  echo "      </tr>\n";
  echo "      <tr>";
  echo "        <td></td>";
  echo "        <td><input type=\"submit\" value=\"Display\"></td>\n";
  echo "      </tr>\n";
  echo "    </table>\n";
  echo "  </form>\n";
  echo "<br>\n";
  
  // list DATA histograms
  $query = $db->query("SELECT *
                        FROM histograms
                        WHERE type='data'
                        ORDER BY beam, process, observable, energy, cuts, version");
  $mydata = array();
  while ($row = $query->fetch_assoc()) {
    $params  = implode($safeDelimiter, [$row["observable"],$row["energy"],$row["cuts"]]);
    $beam = $row["beam"];
    // TODO: add beam -> beamgroup map function in config.php
    if ($beam == "pp" || $beam == "ppbar") $beam = "ppppbar"; // combine pp/ppbar
    $process = $row["process"];
    $mydata[$beam][$process][$params][] = $row;
  }

  // list MC histograms
  $query = $db->query("SELECT *
                        FROM histograms
                        WHERE generator = '$q_generator' AND tune = '$q_tune' AND version IN ('" . implode("','", $vers_sel) . "')
                        ORDER BY beam, process, observable, energy, cuts, version");
  $mytable = array();
  while ($row = $query->fetch_assoc()) {
    $beam = $row["beam"];
    if ($beam == "pp" || $beam == "ppbar") $beam = "ppppbar"; // combine pp/ppbar
    $process = $row["process"];
    $version = $row["version"];
    $params  = implode($safeDelimiter, [$row["observable"],$row["energy"],$row["cuts"]]);
    $mytable[$beam][$process][$version][$params][] = $row;
  }
  
  $histo_total = 0;
  
  echo "<table class=\"validation\">\n";

  //table Header begin
  $nvers = 0;
  $minrow="";
  $versrow="";
  $maxrow="";
  foreach ($vers_sel as $version) {
    if ($nvers > 0) {
      $maxrow  .="    <th class=\"mcd dup\">worst</th>\n";
      $versrow .="    <th class=\"mcd dmid\">&lt;&Delta;&gt;</th>\n";
      $minrow  .="    <th class=\"mcd ddwn\">best</th>\n";
    }
    
    $maxrow  .="    <th class=\"dup\">max</th>\n";
    $versrow .="    <th class=\"dmid\">$version</th>\n";
    $minrow  .="    <th class=\"ddwn\">min</th>\n";
    $nvers++;
  }
  echo "  <tr>\n";
  echo "    <th class=\"mn\" rowspan=\"3\">&lt;&chi;<sup>2</sup>&gt;<br>\n";
  echo "      <span class=\"smallText\">incl. 5% \"theory uncertainty\" on all points</span></th>\n";
  echo "$maxrow";
  echo "  </tr>\n";
  echo "  <tr>\n$versrow  </tr>\n  <tr>\n$minrow  </tr>\n";
  echo "\n";
  //table header end

  //table body begin >>>>>
  //loop through beam and processes (creating rows)
  foreach (array_keys($mytable) as $beam ){
    foreach (array_keys($mytable[$beam]) as $process){
      //validation row begin >>>>

      //initialization of loop
      $minrow   = "";
      $versrow  = "";
      $maxrow   = "";
      
      $prev = "";
      $lastavg  = "";
      $allchi2old = array();
      
      //loop through versions to create columns of a row begin >>>
      foreach ($vers_sel as $version) {
        // skip versions with no plots for a given process
        //if (!in_array($version, array_keys($mytable[$beam][$process]))) continue;
        
        //comparison of version and DATA
        $curr=$version;
        $ref="data";

        $fname = str_replace(" ", "_", "$q_generator-".$curr."-$q_tune-$beam-$process.txt");
        $fname = str_replace("/", "_", $fname);
        $fname = str_replace("*", "x", $fname);

        $allchi2=array();
        $dtchi2=array();

        if (file_exists("cache/chi2/$fname")){
          $fchi = fopen("cache/chi2/$fname", 'r');
          while(!feof($fchi))
          {
              $line = fgets($fchi);
              $line = trim($line);
              if ($line == "") continue;
              //error_log($fname . " " . json_encode($line));
              list($p,$chi,, ) = explode(" ", $line);
              if (($chi < 0) || (!is_numeric($chi))) {
                continue;
              }
              $arrind = str_ireplace($version, "", $p);
              $allchi2[$arrind] = $chi;
              if ((!is_null($chi)) && array_key_exists($arrind, $allchi2old)) {
                $dtchi2[$arrind]=($chi - $allchi2old[$arrind]);
              }
          }
          fclose($fchi);
        }

        $allchi2old=$allchi2;

        $nchi = count($allchi2);
        $histo_total += $nchi;
        if ($nchi > 0) {
          $maxchi=max($allchi2);
          $minchi=min($allchi2);
          $avgchi=array_sum($allchi2)/$nchi;
        }
        else {
          $maxchi = "";
          $minchi = "";
          $avgchi = "?";
        }
        
        // insert 'delta' column
        if ($prev != "") {
          if (count($dtchi2)>0){
          $dmaxchi=max($dtchi2); //($maxchi-$lastmax)/$lastmax;
          $dminchi=min($dtchi2); //($minchi-$lastmin)/$lastmin;
          }
          else
          {
          $dmaxchi="";
          $dminchi="";
          }
          //$dif = $lastavg - $avgchi;
          //$davgchi=((abs($dif) < 1000) ? ((spec_format($lastavg))-(spec_format($avgchi))) : (spec_format($dif))); //lastavg is avg from previous
          $dif = "";
          $davgchi = "";
          if ((is_numeric($avgchi)) && (is_numeric($lastavg))) {
            $dif = $avgchi - $lastavg;
            $davgchi=((abs($dif) < 1000) ? ((spec_format($avgchi))-(spec_format($lastavg))) : (spec_format($dif)));
          }

          $minrow  .= "    <td class=\"mcd ddwn ".get_deltColor($dminchi,true)." \">"
                           .spec_format($dminchi,true)."</td>\n";
          $param = $q_generator.$safeDelimiter.$q_tune.$safeDelimiter.$prev.$safeDelimiter.$curr;
          $versrow .= "    <td class=\"mcd dmid ".get_deltColor($davgchi,true)." \" >" .
                      sprintf("<a class=\"%s\" href=\"%s\">%s</a>",
                        get_deltColor($davgchi),
                        prepare_link(array("validdetail",$beam,$process,"","","",$param)),
                        spec_format($davgchi,true)) .
                      "</td>\n";
          $maxrow  .= "    <td class=\"mcd dup ".get_deltColor($dmaxchi,true)."\" >"
                           .spec_format($dmaxchi,true)."</td>\n";
        }
        // insert main column
        $minrow  .= "    <td class=\"ddwn ".get_color($minchi)."\" >".spec_format($minchi)."</td>\n";
        $param = $q_generator.$safeDelimiter.$q_tune.$safeDelimiter.$curr.$safeDelimiter.$ref;
        $versrow .= "    <td class=\"dmid ".get_color($avgchi)." \">" .
                    sprintf("<a class=\"clblack\" href=\"%s\">%s</a>",
                      prepare_link(array("validdetail",$beam,$process,"","","",$param)),
                      spec_format($avgchi)) .
                    "</td>\n";
        $maxrow  .= "    <td class=\"dup ".get_color($maxchi)." \">".spec_format($maxchi)."</td>\n";
        
        $prev = $curr;
        $lastavg=$avgchi;

      }
      echo "  <tr>\n";
      echo "    <td class=\"mn right-bord-thick\" rowspan=\"3\" >";
      echo $c->name($beam) . " &rarr; " . $c->name($process);
      echo "</td>\n";
      echo "$maxrow";
      echo "  </tr>\n";
      echo "  <tr>\n$versrow</tr>\n  <tr>\n$minrow</tr>\n";
      echo "\n";
      //columns end <<<
      //physics row end <<<<
    }
  }
  echo "</table>\n";
  
  echo "<p>Legend: ";
  echo "<span class=\"" . get_color(0.5) . "\">[ &chi;<sup>2</sup> &lt; 1 ]</span> / ";
  echo "<span class=\"" . get_color(2) . "\"> [ 1 &le; &chi;<sup>2</sup> &lt; 4 ]</span> / ";
  echo "<span class=\"" . get_color(5) . "\"> [ 4 &le; &chi;<sup>2</sup> ]</span><br>\n";
  
  echo "<p>(click on number in the table cell to see individual observables)</p>\n";
  
  echo "<p>The page data is based on $histo_total histograms.</p>\n";
  
?>
